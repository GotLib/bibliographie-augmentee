import regex as re
import pickle
import json
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst, Join, Compose
from scrapy.exporters import CsvItemExporter, JsonItemExporter, JsonLinesItemExporter, XmlItemExporter
from biblio_items import Biblio

BIBLIO_DIR = "/home/pierre/Documents/ECM/3A_bis/nlp-arxiv-ecm/code/intermediary_results/biblio-test"
META_DIR = BIBLIO_DIR + "/articles-metadata"
PDF_DIR = BIBLIO_DIR + "/articles-pdf"
LATEX_DIR = BIBLIO_DIR + "/articles-latex"


## exportation des données vers le stockage local:
def export_meta(item, savefile_name): # l'utilisation dees FeedExports cause des bugs depuis 
                                      # les 2 dernières mises à jour de scrapy (de mémoire depuis 1.5.2 et 1.6.0)
    with open(META_DIR + '/' + savefile_name + '_meta.json', 'w') as f: 
        to_json_meta = JsonItemExporter(f)
        to_json_meta.export_item(a)

def export_pdf(pdf, savefile_name):
    pass

def export_latex(latex, savefile_name):
    pass

def pickle_meta(item, savefile_name):
    with open(META_DIR + '/' + savefile_name + "_meta_pickled", 'wb') as f:
        p = pickle.Pickler(f)
        p.dump(item)

## construction des urls à regarder dans l'API et dans OAI:
def api_url(title, authors):
    return "https://export.arxiv.org/api/query?search_query=ti:" + title + "+AND+au:" + authors[0] + "&max_results=10"

def oai_url(identifier):
    return "https://export.arxiv.org/oai2?verb=GetRecord&identifier=oai:arXiv.org:" + identifier + "&metadataPrefix=arXiv"

 
class Article(scrapy.Item):
    identifier = scrapy.Field()
    title = scrapy.Field()
    creation_date = scrapy.Field()
    updated = scrapy.Field()
    authors = scrapy.Field()
    categories = scrapy.Field()
    abstract = scrapy.Field()
    references = scrapy.Field()


class ArticleLoader(ItemLoader):
    
    def group_by_individual(self, values):
        l = int(len(values)/2)
        return [{'keyname': values[2*i].upper(), 'forename': values[2*i+1]} for i in range(l)]

    def split_categories_str(self, values):
        for v in values:
            yield v.split(' ')
    
    def reformat_abstract(self, values):
        values = "".join(values).replace("\n", " ")
        return "".join(values)

    def get_id_number(self, values):
        return values[0].split("/")[-1].split('v')[0]

    def parse_date(self, values):
        return values[0].split("T")[0]

    #default_input_processor = TakeFirst()

    identifier_in = get_id_number
    identifier_out = TakeFirst()

    created_in = parse_date

    updated_in = parse_date

    authors_in = Identity()
    authors_out = Identity() #group_by_individual

    categories_in = split_categories_str
    caegories_out = TakeFirst()

    abstract_in = reformat_abstract


title_test = "Character-level Convolutional Networks for Text Classification"
authors_test = ['Xiang ZHANG', 'Junbo ZHAO', 'Yann LECUN']
api_template_url = "https://export.arxiv.org/api/query?search_query=ti:" + "TITLE" + "+AND+au:" + "AUTHOR KEYNAME" + "&max_results=1"
oai_template_url = "https://export.arxiv.org/oai2?verb=GetRecord&identifier=oai:arXiv.org:" + "XXXX.XXXX" + "&metadataPrefix=arXiv"


class ArXivSpider(scrapy.Spider):
    name = "arxiv_spider"
    
    def __init__(self, title, authors, references=[], *args, **kwargs):
        super(ArXivSpider, self).__init__(*args, **kwargs)
        self.title = title
        self.authors = authors
        self.start_urls = [api_url(self.title, self.authors)]
        self.references = references 

    def parse(self, response):
        sel = Selector(response=response)
        sel.remove_namespaces()
        #print(response.body)
        #print("\n")
        #print(sel.xpath('//title/text()').extract())

        a = Article()
        al = ArticleLoader(item=a, selector=sel, response=response)
        al.add_xpath('identifier', "//entry/id/text()")
        al.add_value('title', self.title)       
        al.add_xpath('creation_date', '//published/text()')
        al.add_xpath('updated', '//updated/text()')      
        al.add_value('authors', self.authors)
        al.add_xpath('authors', '//author/name/text()') # il faudra enlever les doublons avec un ItemPipeline      
        al.add_xpath('categories', '//arxiv/@term/text()')       
        al.add_xpath('abstract', '//summary/text()')       
        al.add_value('references', self.references)
        al.load_item()

        #export_meta(a, self.identifier)
        print(a['identifier'])
        pickle_meta(a, a['identifier'])

        print(a)       
        return None



process = CrawlerProcess(settings={
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
})

process.crawl(ArXivSpider, title=title_test, authors=authors_test, references=[])
process.start() 




