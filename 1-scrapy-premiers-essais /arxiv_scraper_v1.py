import regex as re
import pickle
import json
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst, Join, Compose
from scrapy.exporters import CsvItemExporter, JsonItemExporter, JsonLinesItemExporter, XmlItemExporter
from biblio_items import Biblio

BIBLIO_DIR = "/home/pierre/Documents/ECM/3A_bis/nlp-arxiv-ecm/code/intermediary_results/biblio-test"
META_DIR = BIBLIO_DIR + "/articles-metadata"
PDF_DIR = BIBLIO_DIR + "/articles-pdf"
LATEX_DIR = BIBLIO_DIR + "/articles-latex"


## exportation des données vers le stockage local:
def export_meta(item, savefile_name): # l'utilisation dees FeedExports cause des bugs au moins depuis 
                                      # les 2 dernières mises à jour de scrapy (1.5.2 et 1.6.0)
    with open(META_DIR + '/' + savefile_name + '_meta.json', 'w') as f: 
        to_json_meta = JsonItemExporter(f)
        to_json_meta.export_item(a)

def export_pdf(pdf, savefile_name):
    pass

def export_latex(latex, savefile_name):
    pass

def pickle_meta(item, savefile_name):
    with open(META_DIR + '/' + savefile_name + "_meta_pickled", 'wb') as f:
        p = pickle.Pickler(f)
        p.dump(item)
 
class Article(scrapy.Item):
    identifier = scrapy.Field()
    title = scrapy.Field()
    creation_date = scrapy.Field()
    last_arXiv_update = scrapy.Field()
    authors = scrapy.Field()
    categories = scrapy.Field()
    abstract = scrapy.Field()
    references = scrapy.Field()


class ArticleLoader(ItemLoader):
    
    def group_by_individual(self, values):
        l = int(len(values)/2)
        return [{'keyname': values[2*i].upper(), 'forename': values[2*i+1]} for i in range(l)]

    def split_categories_str(self, values):
        for v in values:
            yield v.split(' ')
    
    def reformat_abstract(self, values):
        values = "".join(values).replace("\n", " ")
        return "".join(values)

    #default_input_processor = TakeFirst()

    authors_in = Identity()
    authors_out = group_by_individual

    categories_in = split_categories_str
    caegories_out = TakeFirst()


    abstract_in = reformat_abstract


title = ""
author = ""
api_template_url = "https://export.arxiv.org/api/query?search_query=ti:" + "TITLE" + "+AND+au:" + "AUTHOR KEYNAME" + "&max_results=1"
oai_template_url = "https://export.arxiv.org/oai2?verb=GetRecord&identifier=oai:arXiv.org:" + "XXXX.XXXX" + "&metadataPrefix=arXiv"

start_url = "https://export.arxiv.org/api/query?search_query=title:" + title + "+AND+au:" + author + "&start=0&max_results=1"
class ArXivSpider(scrapy.Spider):

    name = "arxiv_spider"
    
    def __init__(self, identifier='1509.01626', references=[], *args, **kwargs):
        super(ArXivSpider, self).__init__(*args, **kwargs)
        self.start_urls = ["https://export.arxiv.org/oai2?verb=GetRecord&identifier=oai:arXiv.org:" + identifier + "&metadataPrefix=arXiv"]
        self.identifier = identifier
        self.references = references 

    def parse(self, response):
        sel = Selector(response=response)
        sel.remove_namespaces()
        #print(response.body)
        #print("\n")
        #print(sel.xpath('//title/text()').extract())

        a = Article()
        al = ArticleLoader(item=a, selector=sel, response=response)
        al.add_value('identifier', self.identifier)
        al.add_xpath('title', '//title/text()')       
        al.add_xpath('creation_date', '//created/text()')
        al.add_xpath('last_arXiv_update', '//datestamp/text()')       
        al.add_xpath('authors', '//authors/author/*/text()')       
        al.add_xpath('categories', '//categories/text()')       
        al.add_xpath('abstract', '//abstract/text()')       
        al.add_value('references', self.references)
        al.load_item()

        #export_meta(a, self.identifier)
        pickle_meta(a, self.identifier)

        print(a)       
        return None



process = CrawlerProcess(settings={
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
})

process.crawl(ArXivSpider, identifier='1509.01626', references=[])
process.start() 




