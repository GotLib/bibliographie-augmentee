"""Définition des classes Article et Biblio au tout début du projet, avant la décision de créer un 'projet scrapy' à part
   (dossier 2) où ces 2 classes sont redéfinies comme des filles de la classe Item (propre à scrapy). """

import numpy as np
import datetime


class Article:
    
    def __init__(self, identifier, title, date, authors, categories, abstract, local_path, references):
        self._identifier = identifier
        self._title = title
        self._date = date
        self._authors = authors
        self._categories = categories
        self._abstract = abstract
        self._local_path = local_path
        self._references = references
    
    ## Accesseurs :  
    def _get_identifier(self):
        return self._identifier

    def _get_title(self):
        return self._title

    def _get_date(self):
        return self._date

    def _get_authors(self):
        return self._authors

    def _get_categories(self):
        return self._categories

    def _get_abstract(self):
        return self._abstract
    
    def _get_local_path(self):
        return self._local_path
    
    def _get_references(self):
        return self._references
    
    ## Mutateurs :
    def _set_identifier(self, new_identifier):
        self._identifier = new_identifier

    def _set_title(self, new_title):
        self._title = new_title

    def _set_date(self, new_date):
        self._date = new_date

    def _set_authors(self, new_authors):
        self._authors = new_authors

    def _set_categories(self, new_categories):
        self._categories = new_categories

    def _set_abstract(self, new_abstract):
        self._abstract = new_abstract

    def _set_local_path(self, new_local_path):
        self._local_path = new_local_path

    def _set_references(self, new_references):
        self._references = new_references

    ## Propriétés :
    identifier = property(_get_identifier, _set_identifier)
    title = property(_get_title, _set_title)
    date = property(_get_date, _set_date)
    authors = property(_get_authors, _set_authors)
    categories = property(_get_categories, _set_categories)
    abstract = property(_get_abstract, _set_abstract)
    local_path = property(_get_local_path, _set_local_path)
    references = property(_get_references, _set_references)


class Biblio:
    
    def __init__(self, name, adj_matrix, corpus):
        self._name = name
        self._adj_matrix = adj_matrix
        self._corpus = corpus

    ## Accesseurs :
    def _get_name(self):
        return self._name

    def _get_adj_matrix(self):
        return self._adj_matrix

    def _get_corpus(self):
        return self._corpus

    ## Mutateurs :
    def _set_name(self, new_name):
        self._name = new_name

    def _set_adj_matrix(self, new_adj_matrix): 
        self._adj_matrix = new_adj_matrix

    def _set_corpus(self, new_corpus):
        self._corpus = new_corpus

    ## Propriétés :
    name = property(_get_name, _set_name)
    adj_matrix = property(_get_adj_matrix, _set_adj_matrix)
    corpus = property(_get_corpus, _set_corpus)
    
