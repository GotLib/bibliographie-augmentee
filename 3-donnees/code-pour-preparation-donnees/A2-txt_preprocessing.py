# encoding: utf-8

"""Maintenant qu'on a extrait le texte des pdf grâce au script dans le fichier A1, 
   On réalise ici les 3 étapes suivantes :
   - extraction de la section bibliographique de chaque article 
     (plus ou moins fiable et à terminer manuellement) et enregistrement au format csv. 
   - choix de l'alphabet et d'une correspondance entre chaque caractère de l'alphabet 
     et des nombres entiers.
   - traduction des textes en suites d'entiers grâce à cette correspondance 
     et enregistrement au format npz (numpy).
"""

import os
import regex as re
import pandas as pd
from pandas import DataFrame as df
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, LabelBinarizer
import string
from time import time
from pprint import pprint as pp
import numpy as np
import six


TEXTS_DIR = r"../donnees-brutes/texts-from-the-pdf-samples/"
OUTPUT = r"../donnees-preparees/from-txt-(pdf)/"
REFS_FILE = OUTPUT + "refs-dataset/"
text_files = os.listdir(TEXTS_DIR)
textdata_paths = [TEXTS_DIR + name for name in text_files]


ALPHABET = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^',
             '_', '`', '{', '|', '}', '~', ' ', '\t', '\n', '\r', '\x0b', '\x0c', '\n', ' ', '\t', '\r']
""" Ces 2 dictionnaires sont obtenus grâce à ce script (je les ai copiés-collés ici après coup).
VOCAB = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
         'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24,
         'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35,
         'A': 36, 'B': 37, 'C': 38, 'D': 39, 'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49, 'O': 50,
         'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59, 'Y': 60, 'Z': 61,
         '!': 62, '"': 63, '#': 64, '$': 65, '%': 66, '&': 67, "'": 68, '(': 69, ')': 70, '*': 71, '+': 72, ',': 73, '-': 74, '.': 75,
         '/': 76, ':': 77, ';': 78, '<': 79, '=': 80, '>': 81, '?': 82, '@': 83, '[': 84, '\\': 85, ']': 86, '^': 87, '_': 88, '`': 89,
         '{': 90, '|': 91, '}': 92, '~': 93, ' ': 101, '\t': 102, '\n': 100, '\r': 103, '\x0b': 98, '\x0c': 99}

REVERSE_VOCAB = {0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9',
                10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f', 16: 'g', 17: 'h', 18: 'i', 19: 'j', 20: 'k', 21: 'l', 22: 'm', 23: 'n',
                24: 'o', 25: 'p', 26: 'q', 27: 'r', 28: 's', 29: 't', 30: 'u', 31: 'v', 32: 'w', 33: 'x', 34: 'y', 35: 'z',
                36: 'A', 37: 'B', 38: 'C', 39: 'D', 40: 'E', 41: 'F', 42: 'G', 43: 'H', 44: 'I', 45: 'J', 46: 'K', 47: 'L', 48: 'M', 49: 'N',
                50: 'O', 51: 'P', 52: 'Q', 53: 'R', 54: 'S', 55: 'T', 56: 'U', 57: 'V', 58: 'W', 59: 'X', 60: 'Y', 61: 'Z',
                62: '!', 63: '"', 64: '#', 65: '$', 66: '%', 67: '&', 68: "'", 69: '(', 70: ')', 71: '*', 72: '+', 73: ',', 74: '-', 75: '.',
                76: '/', 77: ':', 78: ';', 79: '<', 80: '=', 81: '>', 82: '?', 83: '@', 84: '[', 85: '\\', 86: ']', 87: '^', 88: '_', 89: '`',
                90: '{', 91: '|', 92: '}', 93: '~', 94: ' ', 95: '\t', 96: '\n', 97: '\r', 98: '\x0b', 99: '\x0c', 100: '\n', 101: ' ',
                102: '\t', 103: '\r'}
"""

def get_refs_only(sourcepath=TEXTS_DIR, destpath=REFS_FILE, destname="refs-samples.csv"):
    """Le dossier `sourcepath` ne contient que des articles scientifiques au format texte.
       Cette fonction est censée récupérer, pour chacun, uniquement la partie avec les références,
       et l'enregistrer dans un csv (une section "Références" d'article par ligne).

       *Les champs nb_splits_starcut et nb_split_endcut ne sont pas utiles pour cela : 
       *ils servaient juste à donner des pistes d'amélioration possibles de la fonction, qui est simpliste
       *et n'a pas extrait les bonnes parties dans 15% des cas:
       *La fonction utilise des expressions régulières pour scinder le texte autour de chaque occurence du mot "references", 
       *garde le dernier morceau puis recommence avec le mot "appendix" pour se débarrasser d'éventuels appendices après la partie de biblio.
       *Les 2 champs superflus donnent donc simplement le nombre de segments ainsi découpés, 
       *c'est-à-dire le nombre d'occurences des mots "references" dans tout le texte et "appendix" dans tout ce qui suit la dernière 
       *occurence de "references".
       *Finalement, j'ai peaufiné le résultat à la main en supprimant les champs vides ou en enlevant les parties en trop, car ça suffisait 
       *dans la plupart des cas. (Il reste des erreurs à la fin car je n'avais pas tout corrigé.)
    """
    refs_dataset = []
    regexp_startcut = re.compile(r"(References|REFERENCES)")
    regexp_endcut = re.compile(r"(Appendix|APPENDIX)")
    for name in os.listdir(sourcepath):
        with open(sourcepath + '/' + name, 'r') as f:
            t = f.read()

            refs_and_appendix = re.split(regexp_startcut, t)
            nb_splits_startcut = len(refs_and_appendix) 

            refs_appendix_split = re.split(regexp_endcut, refs_and_appendix[-1])
            nb_splits_endcut = len(refs_appendix_split)
            
            #print(name + " : "+  str(len(refs_appendix_split)) + "\n")

            refs_dataset.append({
                                 "name": name,
                                 "nb_splits_startcut" : nb_splits_startcut,
                                 "nb_splits_endcut" : nb_splits_endcut,
                                 "refs": refs_appendix_split[0]
                                })

    refs_df = df(refs_dataset, columns=["name", "nb_splits_startcut", "nb_splits_endcut", "refs"], index=range(len(refs_dataset)))
    refs_df.to_csv(destpath + '/' + destname)
    pp(refs_df)

def create_vocab_set(): # from https://www.kaggle.com/tomkealy/text-understanding-from-scratch (with more complete alphabet)
    """renvoie un set contenant l'alphabet (check), sa taille, et la correspondance entre chaque caractère et un nombre entier ("indice").
       *Rappel : en Python, un 'set' est une liste qui n'a pas d'ordre, et qui ne peut pas contenir de doublons (comme en maths).
    """
    alphabet = (list(string.printable) + ['\n', ' ', '\t', '\r'])
    vocab_size = len(set(alphabet))
    print('\nalphabet: ')
    print(alphabet)
    check = set(alphabet)

    vocab = {}
    reverse_vocab = {}
    for ix, t in enumerate(alphabet):
        vocab[t] = ix
        reverse_vocab[ix] = t

    return vocab, reverse_vocab, vocab_size, check

def index_encode_singlefile(vocab, filepath=None, text=None):
    """Convertit la chaine `text` (ou celle contenue dans le fichier `filepath`)
       en une liste d'entiers (la suite des indices correpondant à chaque caractère)
       qui va pouvoir être transformée à son tour ('one-hot encoding' ou remplacement par des
       représentations vectorielles de chaque caractère) pour être utilisée comme entrée 
       d'un algo de machine learning. """

    data = []
    if filepath != None:
        with open(filepath, 'r') as f:
            t = f.read()
    else:
        t = text

    for char in t:
        try:
            data.append(vocab[char])
        except:
            data.append(-2) # -2 : unknown character

    if filepath != None:
        #print("length of " + filepath.split('/')[-1].capitalize() + " (in characters): " + str(len(data)))
        pass

    return data

def index_encode_data(vocab, datadir=REFS_FILE, refs_csv=None, savename="indexEncoded-refs-", mode="allconcat"):
    """Appelle la fonction `index_emcode_singlefile` sur tous les fichier d'un répertoire, 
       agrège les listes obtenues et les enregistre au format npz de numpy.
       Options : 
       - suivant la valeur de `datadir`, on encode le texte complet des articles 
         ou bien seulement les références. 
       - suivant la valeur de `mode` ("isolated" ou "allconcat"), on enregistre soit chaque fichier dans
         un dictionnaire, soit toutes les listes d'entiers concaténées en une seule.
       Les options choisies sont rappelées dans le nom du fichier npz en sortie.
    """

    if mode == "allconcat": # tout dans la même liste, séparés par des '-1'

        dataset = []
        if datadir==TEXTS_DIR:
            filepaths = [TEXTS_DIR + f for f in os.listdir(TEXTS_DIR)]
            for fp in filepaths:
                dataset.extend(index_encode_singlefile(vocab=vocab, filepath=fp))
                dataset.append(-1) # -1 stands for <eof> : end of file    
            np.savez(OUTPUT+savename+mode+'.npz', np.array(dataset))

        elif datadir==REFS_FILE:
            df = pd.read_csv(REFS_FILE + '/' + refs_csv, engine='python')
            for text in df.loc[:20]['refs']:
                dataset.extend(index_encode_singlefile(vocab=vocab, text=text))
            np.savez(REFS_FILE+savename+mode+'.npz', np.array(dataset))

    elif mode == "isolated":
        dataset = {}
        if datadir==TEXTS_DIR:
            filepaths = [TEXTS_DIR + f for f in os.listdir(TEXTS_DIR)]
            for fp in filepaths:
                dataset[fp] = np.array(index_encode_singlefile(vocab=vocab, filepath=fp))
            np.savez(OUTPUT+savename+mode+'.npz', np.array(dataset))

        elif datadir==REFS_FILE:
            df = pd.read_csv(REFS_FILE + '/' + refs_csv, engine='python')
            for i, text in enumerate(df.loc[:20]['refs']):
                dataset[i] = np.array(index_encode_singlefile(vocab=vocab, text=text))
            np.savez(REFS_FILE+savename+mode+'.npz', np.array(dataset))




if __name__ == '__main__':

    t1 = time()


    get_refs_only(destname="A1_refs-samples-1ter.csv")

    vocab, reverse_vocab, vocab_size, check = create_vocab_set()
    print('vocab: ')
    pp(vocab)
    
    print('reverse_vocab: ')
    pp(reverse_vocab)

    index_encode_data(vocab=vocab, datadir=REFS_FILE, refs_csv="A1_refs-samples-1ter.csv", mode="isolated")    
    index_encode_data(vocab=vocab, datadir=REFS_FILE, refs_csv="A1_refs-samples-1ter.csv", mode="allconcat")
    index_encode_data(vocab=vocab, datadir=TEXTS_DIR, mode="isolated")    
    index_encode_data(vocab=vocab, datadir=TEXTS_DIR, mode="allconcat")


    t2 = time()
    print("exec time (seconds): " + str(t2-t1)) 







