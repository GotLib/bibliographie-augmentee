# encoding : utf-8

"""Alternative à A1 et au début de A2 :
   extrait les références des articles au format LaTeX, au lieu de convertir les pdf en texte brut.
   C'est en fait plus efficace car on récupère les références individuellement et à tous les coups
   (il suffit de scinder autour des r"\\bibitem"). 
"""


import os
import regex as re
from pickle import Pickler as pck, Unpickler as upck
from pprint import pprint as pp
from time import perf_counter

t1 = perf_counter()

TEX_PATH =  r"../donnees-brutes/tex-files_2003-KDD-cup/2000+2003/"
DICT_SAVEPATH = r"../donnees-preparees/from-txt-(pdf)/refs-dataset/latex-bibitems-pickle" 

samples = ["0303033", "0302223", "0301148", "0002173", "0001001"]
to_check = '0302194' # pour voir la structure de la biblio en latex avec les balises : 14% des pdf de la kdd cup doivent avoir une orga différente car le champ bibitem du dico est une liste vide
name = samples[3]


def build_bibitems_dataset(dict_savepath=DICT_SAVEPATH):
    """Analogue à la fonction get-refs-only de A1:
       utilise des expressions régulières pour isoler les références bibliographiques de chaque article d'un répertoire,
       mais sur des fichiers LaTeX.
       Enregistre le tout dans un dictionnaire au format pickle.
       Je n'ai pas réussi à extraire les titres, c'est pour ça que le champ 'title' est laissé vide pour le moment.
    """
    infos_per_file = {} # 'id' (filename, <str>)  =>   { 'title' => <str> , 'bibitems' => List<str> }
    for name in os.listdir(TEX_PATH):
        with open(TEX_PATH + '/' + name, 'rb') as f:
            doc = str(f.read())

        regexp = re.compile(r"\\bibitem")
        splits = re.split(regexp, doc)

        c=0
        temp = {'title' : "", 'bibitems' : []}
        for s in splits[2:]:
            temp['bibitems'].append(s)
            c += 1

        infos_per_file[name] = temp        

    with open(dict_savepath, 'wb') as sf:
        p = pck(sf)
        p.dump(infos_per_file)

    t2 = perf_counter()
    print("temps d'exécution : " + str(t2 - t1))

def check_dataset_quality(filename, filepath=DICT_SAVEPATH):
    """Permet de jauger la qualité du dataset renvoyé par la fonction build_bibitems_dataset."""
    total_nb_refs = 0
    count_OK = 0
    count_NOK = 0

    with open(filepath + "/" + filename, 'rb') as f:
        u = upck(f)
        infos_per_file = u.load()

    for name in infos_per_file.keys():
        bibitems = infos_per_file[name]['bibitems']
        nb_refs = len(bibitems)
        if nb_refs == 0:
            count_NOK += 1
        else:
            count_OK += 1
            total_nb_refs += nb_refs

    mean_nb_refs = total_nb_refs / float(count_OK)
    Q = float(count_OK) / (count_OK + count_NOK)

    print("nb OK : " + str(count_OK) + "\n nb NOK : " + str(count_NOK) + "\n mean nb refs per article : " + str(mean_nb_refs)  + "total nb refs : " + str(total_nb_refs) + "\n quality : " + str(Q))


if __name__=='__main__':
    build_bibitems_dataset()
    check_dataset_quality(filename="B_latex-bibitems-pickle")
    with open(TEX_PATH + '/' + name, 'rb') as f:
        doc = str(f.read())
        regexp = re.compile(r"title")
        splits = re.split(regexp, doc)
        pp(doc)
    
    
