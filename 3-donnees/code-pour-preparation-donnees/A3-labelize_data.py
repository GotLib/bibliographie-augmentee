"""Après avoir généré un dataset sans labels dans A2 (en représentant les caractères par des entiers),
on peut ajouter des labels à chaque caractère dans l'idée d'entraîner un réseau de neurone à distinguer
dans une références, les parties qui correspondent à des noms d'auteurs, des titres, des dates, un journal ou autre chose,
en initialisant la première couche avec des représentations vectorielles pré-entrainées des caractères (j'ai utilisé les vecteurs d'ELMO).
Mais c'est très long et j'ai à peine labellisé 2500 caractères, ce qui explique peut-être en partie les mauvais résultats obtenus par l'implémentation de CharNER avec keras que j'ai clonée de GitHub (cf dossier 5).
"""

import numpy as np
import pandas as pd

import click
import tqdm


VOCAB = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
         'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24,
         'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35,
         'A': 36, 'B': 37, 'C': 38, 'D': 39, 'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49, 'O': 50,
         'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59, 'Y': 60, 'Z': 61,
         '!': 62, '"': 63, '#': 64, '$': 65, '%': 66, '&': 67, "'": 68, '(': 69, ')': 70, '*': 71, '+': 72, ',': 73, '-': 74, '.': 75,
         '/': 76, ':': 77, ';': 78, '<': 79, '=': 80, '>': 81, '?': 82, '@': 83, '[': 84, '\\': 85, ']': 86, '^': 87, '_': 88, '`': 89,
         '{': 90, '|': 91, '}': 92, '~': 93, ' ': 101, '\t': 102, '\n': 100, '\r': 103, '\x0b': 98, '\x0c': 99}
REVERSE_VOCAB = {0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9',
                10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f', 16: 'g', 17: 'h', 18: 'i', 19: 'j', 20: 'k', 21: 'l', 22: 'm', 23: 'n',
                24: 'o', 25: 'p', 26: 'q', 27: 'r', 28: 's', 29: 't', 30: 'u', 31: 'v', 32: 'w', 33: 'x', 34: 'y', 35: 'z',
                36: 'A', 37: 'B', 38: 'C', 39: 'D', 40: 'E', 41: 'F', 42: 'G', 43: 'H', 44: 'I', 45: 'J', 46: 'K', 47: 'L', 48: 'M', 49: 'N',
                50: 'O', 51: 'P', 52: 'Q', 53: 'R', 54: 'S', 55: 'T', 56: 'U', 57: 'V', 58: 'W', 59: 'X', 60: 'Y', 61: 'Z',
                62: '!', 63: '"', 64: '#', 65: '$', 66: '%', 67: '&', 68: "'", 69: '(', 70: ')', 71: '*', 72: '+', 73: ',', 74: '-', 75: '.',
                76: '/', 77: ':', 78: ';', 79: '<', 80: '=', 81: '>', 82: '?', 83: '@', 84: '[', 85: '\\', 86: ']', 87: '^', 88: '_', 89: '`',
                90: '{', 91: '|', 92: '}', 93: '~', 94: ' ', 95: '\t', 96: '\n', 97: '\r', 98: '\x0b', 99: '\x0c', 100: '\n', 101: ' ',
                102: '\t', 103: '\r'}


LABELS_MAP = {'A' : 0, 'T' : 1, 'J' : 2, 'D' : 3, 'O' : 4}

refs_only = r"../donnees-preparees/from-txt-(pdf)/refs-dataset/A2_indexEncoded-20refs-allconcat.npz"
DATA_LOC = refs_only
DATA = np.load(DATA_LOC)['arr_0']

OUT_CSV = r"./donnees-preparees/from-txt-(pdf)/A3_indexEncoded-20refs-allconcat-WITHLABELS.csv" 
OUT_NPZ = "./donnees-preparees/from-txt-(pdf)/A3b_indexEncoded-20refs-allconcat-WITHLABELS.npz" # (2, trainsize)-2darray
 
EXCLUDED = ['\n', '\t', '\r', '\x0b', '\x0c']


def chunk(start, end, reverse_vocab=REVERSE_VOCAB):
    """Affiche un extrait des références issues de A2 pour pouvoir renseigner son label (fonction label)."""

    strdata = ""
    for i in range(start, end):
        if DATA[i] >= 0:
            if REVERSE_VOCAB[DATA[i]] not in EXCLUDED:
                strdata += reverse_vocab[DATA[i]]
            else:
                strdata += ' '
    print(strdata)
    return strdata

@click.group()
@click.argument('--start', '-s')
@click.argument('--end', '-e')
@click.argument('--label', '-l')
@click.option('--data-loc', '-d', default=refs_only)
@click.option('--out', '-o', default=OUT_CSV)
@click.option('--labelmap', '-m', default=LABELS_MAP)

def label(start, end, label, data_loc, out, labelmap):
    """ Associe le même label à chaque caractère entre start et end, et ajoute ces couples au contenu de OUT.
        'A' : Authors  : 0
        'T' : Title    : 1
        'J' : Journal  : 2
        'D' : Date     : 3
        'O' : Other    : 4
    """

    data = np.load(data_loc)['arr_0']   
    with open(out, 'a') as f: 
        blank = '\n'
        for i in range(start, end):
            if (data[i] >= 0): # not unknown char or file separator (-1/-2)
                if (REVERSE_VOCAB[data[i]] not in EXCLUDED):
                    row = blank + str(data[i]) + ", " + str(labelmap[label.upper()]) + ", " + str(REVERSE_VOCAB[data[i]]) \
                          + ", " + str(label.upper())
                    f.write(row)
                    print(row) # to check everything is ok
                else:
                    row = blank + str(VOCAB[' ']) + ", " + str(labelmap[label.upper()]) + ", " + str(' ') + ", " + str(label.upper())
                    f.write(row)
                    print(row) # to check everything is ok

def data_in_npz():
    pass

if __name__=='__main__':
    
    #strdata = chunk(0, 2420)
    #print(strdata)
    label(-s, -e, -l)
    
