
# convertit tous les fichiers dans PDF_DIR au format texte, avec l'outil en ligne de commande pdftk.

PDF_DIR="../donnees-brutes/pdf-test-samples"

cd $PDF_DIR

for file in ./*
do
    echo "\n\n"
    echo $file
    filename="$(echo $file | cut -d'.' -f2-3)"
    echo "${filename}"
    pdftotext -raw  $file  #  ./../texts-from-the-pdf-samples"${filename}".txt #pas d'option -layout : c'est moins lisible mais 
                                                                         #ce qu'on veut c'est avoir le texte dans l'ordre
    echo "pdf '$file' converted to txt."
done


