\documentclass[a4paper,12pt]{article}

\usepackage{amsmath}
\usepackage{latexsym}

\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{-.5cm}
\setlength{\textwidth}{165.2mm}

\input{commands}

\setcounter{secnumdepth}{2}

\begin{document}
%
\bibliographystyle{phaip}
%
%
%TITLEPAGE
%
\thispagestyle{empty}
\begin{titlepage}

% BUTP Nr.
\vspace*{-1cm}
\hfill \parbox{3.5cm}{BUTP-2000/05} \\
\vspace*{1.0cm}

% Title
\begin{center}
  {\Large {\bf \hspace*{-0.2cm}Dynamical Symmetry Breaking
      \protect\vspace*{0.2cm} \\
      \protect\hspace*{0.2cm} and Static Limits of
      \protect\vspace*{0.2cm} \\      
      \protect\hspace*{0.2cm} Extended Super-Yang-Mills Theories}\large\footnote{Work
      supported in part by the Schweizerischer Nationalfonds.}\Large:
      \protect\vspace*{0.2cm} \\
      \protect\hspace*{0.2cm} \large{\bf A non-Seiberg-Wittian Approach}}
  \vspace*{2.5cm} \\

% Authors
{\bf
    L. Bergamin and P. Minkowski} \\
    Institute for Theoretical Physics \\
    University of Bern \\
    CH - 3012 Bern, Switzerland
   \vspace*{0.8cm} \\  

% Date
March 13, 2000

\vspace*{3.0cm}

% Abstract
\begin{abstract}
\noindent
From a supersymmetry covariant source extension of $N=2$ SYM we study
non-trivial thermodynamical limits thereof. Using an argument by one of us
about the solution of the strong CP problem and the uniqueness of the QCD ground state we find that the dependence of
the effective potential on the defining field operators is severely
restricted. In contrast to the solution by Seiberg and Witten an acceptable
infrared behavior only exists for broken supersymmetry while the gauge
symmetry remains unbroken.
\end{abstract}
\end{center}

\end{titlepage}
%
%TEXT
%
\input{introduction}
\input{minsource}
\input{action}
\input{breaking}
\input{conclusions}
%%
%%APPENDIX
%%
\appendix
\input{model}
\input{strongcp}
\input{cpconcl}
\bibliography{biblio}
\end{document}

