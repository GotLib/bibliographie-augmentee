\documentstyle[12pt]{article}
\setlength{\textwidth}{7.0in}
\setlength{\oddsidemargin}{-0.28in}
\setlength{\evensidemargin}{0.28in}
\setlength{\topmargin}{-0.30in}
\setlength{\textheight}{9.0in}
%\renewcommand{\theequation}{\thesection.\arabic{equation}}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
%\newcommand{\eqnok}{\cite}
%\newcommand{\IR}{\R}
%\renewcommand{\thetheorem}{\arabic{theorem}}
%\renewcommand{\thefigure}{\arabic{figure}}
%\newcommand{\postscript}[2]{\par
%\hbox{\vbox to #1{\vfil \special{ps: plotfile #
%2.ps}}}}
%\setlength{\marginparsep}{0.15in}
%\setlength{\marginparwidth}{0.8in}
%\newcommand{\mycomment}[1]{\marginpar{\footnotesize{#1}}}
\def\btt#1{{\tt$\backslash$#1}}
\def\halft{{\textstyle {{{1}\over{2\pi\alpha'}}} }}
\def\quaft{{\textstyle {{{1}\over{4\pi\alpha'}}} }}
\def\half{{\textstyle{1\over2}}}
\def\quarter{{\textstyle{1\over4}}}
\def\lh{{{il}\over{2}}}
\def\lz{{{il|\sigma^1_-|}\over{2}}}
\def\lbz{ {{\pi l |\sigma^1_-|}\over{2b}} }
\def\ib{{{il}\over{2b}}}
\def\upz{uz/\pi}
\def\ups{u^2}
\def\p1half{{\textstyle{{{p+1}\over{2}}}}}
\def\phalf{{\textstyle{{{p}\over{2}}}}}
\def\23phalf{{\textstyle{{{23-p}\over{2}}}}}
\def\twelfth{{\textstyle{{{1}\over{12}}}}}
\def\quarter{{\textstyle{1\over4}}}
\begin{document}
\thispagestyle{empty}
\begin{titlepage}

\bigskip
\hskip 3.7in{\vbox{\baselineskip12pt
%\hbox{hep-th/0105244}
}}

\bigskip\bigskip\bigskip\bigskip
\centerline{\large\bf Deconfinement and the Hagedorn Transition in
String Theory}

\bigskip\bigskip
\bigskip\bigskip
\centerline{\bf Shyamoli Chaudhuri
\footnote{shyamoli@thphysed.org}
}
\centerline{214 North Allegheny St.}
\centerline{Bellefonte, PA 16823}
\date{\today}

\bigskip\bigskip
\begin{abstract}
Superseded and extended in hep-th/0105110 and hep-th/0208112.
\end{abstract}

\end{titlepage}

\section{Introduction}

\vskip 0.1in This paper has been superseded by hep-th/0105110 and
hep-th/0208112. The results in the published letter are not
incorrect, but the original presentation has been extensively
rewritten to clarify that thermal duality relations arise
naturally as a consequence of modular invariance in closed string
gases. Corresponding results for the type I open and closed string
gas are included in hep-th/0208112 giving a more unified picture
of the statistical mechanics of fermionic string gases, and of the
phase transition to the long string phase. The pedagogical case of
the free closed bosonic string gas appears in hep-th/0105110.
\end{document}

