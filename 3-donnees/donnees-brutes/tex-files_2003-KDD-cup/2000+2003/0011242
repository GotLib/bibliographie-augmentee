\documentclass[12pt]{article}
%\documentclass{JHEP}  
%\usepackage{a4wide}                      % om een brede bladspiegel te krijgen
\usepackage{amssymb,latexsym}            % om mooie symbolen te maken
\usepackage{epsf}                        % om figuren te maken
%\usepackage[spanish,activeacute]{babel} % om makkelijk Spaans te schrijven 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% DEFINITIONS by Bert Janssen %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%envirement definitions
\def\be{\begin{equation}}
\def\ee{\end{equation}}
\def\bea{\begin{eqnarray}}
\def\eea{\end{eqnarray}} 
\def\ba{\begin{array}}
\def\ea{\end{array}}
\def\nn{\nonumber \\}
\def\vsp#1{\vspace{#1}}
\def\hsp#1{\hspace{#1}}

%math symbols
\def\part{\partial}
\def\tfrac#1#2{{\textstyle{#1\over #2}}}
\def\half{\tfrac{1}{2}}
\def\x{\times}
\def\ox{\otimes}
\def\bra{\cal{h}}
\def\ket{\cal{i}}
\def\aprime{{\alpha^\prime}}
\def\Tr{\mbox{Tr}}
\def\Str{\mbox{Str}}

\def\N{\ensuremath{\mathbb{N}}}
\def\Z{\ensuremath{\mathbb{Z}}}
\def\Q{\ensuremath{\mathbb{Q}}}
\def\R{\ensuremath{\mathbb{R}}}
\def\C{\ensuremath{\mathbb{C}}}

%special fonts
\def\tinyE{{\mbox{\tiny E}}}
\def\cKK{{\cal KK}}
\def\Ortin{Ort{\'\i}n }

%diacritic symbols on capitals
\def\cA{\cal A}  \def\hA{\hat A}  \def\tA{\tilde A} \def\dA{\dot A}    
\def\cB{\cal B}  \def\hB{\hat B}  \def\tB{\tilde B} \def\dB{\dot B}
\def\cC{\cal C}  \def\hC{\hat C}  \def\tC{\tilde C}
\def\cD{\cal D}  \def\hD{\hat D}  \def\tD{\tilde D} 
\def\cE{\cal E}  \def\hE{\hat E}  \def\tE{\tilde E}
\def\cF{\cal F}  \def\hF{\hat F}  \def\tF{\tilde F}
\def\cG{\cal G}  \def\hG{\hat G}  \def\tG{\tilde G}
\def\cH{\cal H}  \def\hH{\hat H}  \def\tH{\tilde H} \def\dH{\dot H}
\def\cI{\cal I}  \def\hI{\hat I}  \def\tI{\tilde I}
\def\cJ{\cal J}  \def\hJ{\hat J}  \def\tJ{\tilde J}
\def\cK{\cal K}  \def\hK{\hat K}  \def\tK{\tilde K}
\def\cL{\cal L}  \def\hL{\hat L}  \def\tL{\tilde L}
\def\cM{\cal M}  \def\hM{\hat M}  \def\tM{\tilde M}
\def\cN{\cal N}  \def\hN{\hat N}  \def\tN{\tilde N}
\def\cO{\cal O}  \def\hO{\hat O}  \def\tO{\tilde O}
\def\cP{\cal P}  \def\hP{\hat P}  \def\tP{\tilde P}
\def\cQ{\cal Q}  \def\hQ{\hat Q}  \def\tQ{\tilde Q}
\def\cR{\cal R}  \def\hR{\hat R}  \def\tR{\tilde R} 
\def\cS{\cal S}  \def\hS{\hat S}  \def\tS{\tilde S}
\def\cT{\cal T}  \def\hT{\hat T}  \def\tT{\tilde T} \def\dT{\dot T}
\def\cU{\cal U}  \def\hU{\hat U}  \def\tU{\tilde U}
\def\cV{\cal V}  \def\hV{\hat V}  \def\tV{\tilde V}
\def\cW{\cal W}  \def\hW{\hat W}  \def\tW{\tilde W} \def\dW{\dot W}
\def\cX{\cal X}  \def\hX{\hat X}  \def\tX{\tilde X}
\def\cY{\cal Y}  \def\hY{\hat Y}  \def\tY{\tilde Y}
\def\cZ{\cal Z}  \def\hZ{\hat Z}  \def\tZ{\tilde Z}

\def\ddA{\ddot A}
\def\ddT{\ddot T}

\def\bR{\bar R}

\def\thA{\tilde{\hat A}}
\def\thB{\tilde{\hat B}}
\def\thC{\tilde{\hat C}}
\def\thD{\tilde{\hat D}}
\def\thE{\tilde{\hat E}}
\def\thF{\tilde{\hat F}}
\def\thG{\tilde{\hat G}}
\def\thH{\tilde{\hat H}}
\def\thR{\tilde{\hat R}}
\def\thV{\tilde{\hat V}}

%diacritic symbols on lowe case letters
\def\ha{\hat a}      \def\ta{\tilde a}   \def\da{{\dot a}}
\def\hb{\hat b}      \def\tb{\tilde b}
\def\hc{\hat c}      \def\tc{\tilde c}
\def\hd{\hat d}      \def\td{\tilde d}
\def\he{\hat e}      \def\te{\tilde e}
\def\hf{\hat f}      \def\tf{\tilde f}
\def\hg{\hat g}      \def\tg{\tilde g}   \def\dg{{\dot g}}
\def\bg{{\bar g}}
\def\hh{\hat h}      \def\th{\tilde h}   \def\dh{{\dot h}}
\def\bh{{\bar h}}
\def\hi{\hat \imath} \def\ti{\tilde \imath}
\def\hj{\hat \jmath} \def\tj{\tilde \jmath}
\def\hk{\hat k}      \def\tk{\tilde k}
\def\hl{\hat l}      \def\tl{\tilde l}
\def\hm{\hat m}      \def\tm{\tilde m}
\def\hn{\hat n}      \def\tn{\tilde n}
\def\ho{\hat o}      \def\to{\tilde o}
\def\hp{\hat p}      \def\tp{\tilde p}
\def\hq{\hat q}      \def\tq{\tilde q}
\def\hr{\hat r}      \def\tr{\tilde r}
\def\hs{\hat s}      \def\ts{\tilde s}
\def\hatt{\hat t}    \def\tildet{\tilde t}
\def\hu{\hat u}      \def\tu{\tilde u}
\def\hv{\hat v}      \def\tv{\tilde v}
\def\hw{\hat w}      \def\tw{\tilde w}
\def\hx{\hat x}      \def\tx{\tilde x}   \def\dx{{\dot x}}
\def\hy{\hat y}      \def\ty{\tilde y}   \def\dy{{\dot y}}
\def\hz{\hat z}      \def\tz{\tilde z}   \def\dz{{\dot z}}

\def\thg{{\tilde{\hat g}}}

%diacritic symbols on greek letters
\def\halpha{\hat{\alpha}}            \def\talpha{\tilde{\alpha}}
\def\hbeta{\hat{\beta}}              \def\tbeta{\tilde{\beta}} 
\def\hgamma{\hat{\gamma}}            \def\tgamma{\tilde{\gamma}}
\def\hdelta{\hat{\delta}}            \def\tdelta{\tilde{\delta}} 
\def\hepsilon{\hat{\epsilon}}        \def\tepsilon{\tilde{\epsilon}}
\def\hvarepsilon{\hat{\varepsilon}}
\def\tvarepsilon{\tilde{\varepsilon}}
\def\hzeta{\hat{\zeta}}              \def\tzeta{\tilde{\zeta}}
\def\heta{\hat{\eta}}                \def\teta{\tilde{\eta}}     
\def\htheta{\hat{\theta}}            \def\ttheta{\tilde{\theta}}
\def\hkappa{\hat{\kappa}}            \def\tkappa{\tilde{\kappa}}    
\def\hlambda{\hat{\lambda}}          \def\tlambda{\tilde{\lambda}}    
\def\hmu{\hat{\mu}}                  \def\tmu{\tilde{\mu}}
\def\hnu{\hat{\nu}}                  \def\tnu{\tilde{\nu}}      
\def\hxi{\hat{\xi}}                  \def\txi{\tilde{\xi}}
\def\hpi{\hat{\pi}}                  \def\tpi{\tilde{\pi}}    
\def\hrho{\hat{\rho}}                \def\trho{\tilde{\rho}}
\def\hsigma{\hat{\sigma}}            \def\tsigma{\tilde{\sigma}} 
\def\htau{\hat{\tau}}                \def\ttau{\tilde{\tau}}
\def\hupsilon{\hat{\upsilon}}        \def\tupsilon{\tilde{\upsilon}}   
\def\hphi{\hat{\phi}}                \def\tphi{\tilde{\phi}}       
\def\hvarphi{\hat{\varphi}}          \def\tvarphi{\tilde{\varphi}}
\def\hchi{\hat{\chi}}                \def\tchi{\tilde{\chi}}      
\def\hpsi{\hat{\psi}}                \def\tpsi{\tilde{\psi}}   
\def\homega{\hat{\omega}}            \def\tomega{\tilde{\omega}}

\def\dphi{\dot{\phi}}
\def\ddphi{\ddot \phi}
\def\thphi{{\tilde{\hat\phi}}}

\def\hGamma{\hat{\Gamma}}
\def\tGamma{\tilde{\Gamma}}
\def\bGamma{\bar{\Gamma}}

%short hand indices
\def\mn{{\mu\nu}}
\def\mnr{{\mu\nu\rho}}
\def\hmn{{\hmu\hnu}}
\def\hmnr{{\hmu\hnu\hrho}}

%some tensors
\def\gmn{g_{\mu\nu}}
\def\Bmn{B_{\mu\nu}}
\def\Fmn{F_{\mu\nu}}
\def\Hmnr{H_{\mu\nu\rho}}
\def\Rmn{R_{\mu\nu}}

%diacritic symbols on tensors
\def\hgmn{{\hat g}_{{\hat\mu}{\hat\nu}}}
\def\hBmn{{\hat B}_{{\hat\mu}{\hat\nu}}}
\def\hCmnr{{\hat C}_{{\hat\mu}{\hat\nu}{\hat\rho}}}
\def\hFmn{{\hat F}_{{\hat\mu}{\hat\nu}}}
\def\hHmnr{{\hat H}_{{\hat\mu}{\hat\nu}{\hat\rho}}}
\def\hRmn{{\hat R}_{{\hat\mu}{\hat\nu}}}

\def\thgmn{{\tilde{\hat g}}_{{\hat\mu}{\hat\nu}}}
\def\thBmn{{\tilde{\hat B}}_{{\hat\mu}{\hat\nu}}}
\def\thCmnr{{\tilde{\hat C}}_{{\hat\mu}{\hat\nu}{\hat\rho}}}
\def\thHmnr{{\tilde{\hat H}}_{{\hat\mu}{\hat\nu}{\hat\rho}}}

%sqare root metrics
\def\sqrtg{\sqrt{|g|}}
\def\sqrtge{\sqrt{|g|}\ e^{-2\phi}}
\def\sqrtgE{\sqrt{|g^{\mbox{\tiny E}}|}}
\def\sqrtG{\sqrt{|G|}}

\def\hsqrtg{\sqrt{|{\hat g}|}}
\def\hsqrtge{\sqrt{|{\hat g}|}\ e^{-2{\hat \phi}}}
\def\hsqrtgE{\sqrt{|{\hat g}^{\mbox{\tiny E}}|}}
\def\hsqrtG{\sqrt{|{\hat G}|}}

\def\sqrtgamma{\sqrt{|\gamma|}}
\def\hsqrtgamma{\sqrt{|{\hat \gamma}|}}

%  UNIT MATRIX (AND OTHER STUFF) ACCORDING TO PETER WAGEMANS
%aanroep: \unitmatrixDT
%
\def\makeatletter{\catcode`\@=11}% 11:letter
\makeatletter
\def\mathbox#1{\hbox{$\m@th#1$}}%
% mathaccent  routine
%
% parameters:
%
% 1 lineskiplimit for alignment
% 2 kern added to the left of accent. At the time 
%   of evaluation the accented formula has been 
%   put in \box 0 and the accent in \box 2, so their
%   \ht, \dp and \wd can be used. Parameter 2 is 
%   used as  \kern #2  ; the user
%   is responsible for the correctness of the resulting TeX.
% 3 separation between accent and accented formula. 
%   See parameter 2 for the available dimensions. 
%   Parameter 3 is used as  \dimen@ #3  ; the user is
%   responsible for the correctness of the resulting TeX.
% 4 accent style
% 5 accent formula
% 6 style of accented formula
% 7 accented formula
%
\def\math@ccstyles#1#2#3#4#5#6#7{{\leavevmode
      \setbox0\mathbox{#6#7}%
      \setbox2\mathbox{#4#5}%
      \dimen@ #3%
      \baselineskip\z@\lineskiplimit#1\lineskip\z@
      \vbox{\ialign{##\crcr
             \hfil \kern #2\box2 \hfil\crcr
             \noalign{\kern\dimen@}%
             \hfil\box0\hfil\crcr}}}}
% \maththroughstyles puts formulas in given styles over eachother 
% (overlapping) parameters like \math@ccstyles
%
\def\mathaccstyles{\math@ccstyles\maxdimen}
\def\maththroughstyles{\math@ccstyles{-\maxdimen}}
%
% unit matrix
%
\def\unity%
 {\maththroughstyles{.45\ht0}\z@\displaystyle
 {\mathchar"006C}\displaystyle 1}
%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%\pagestyle{empty}

%titel
\rightline{IFT-UAM/CSIC-00-39}
\rightline{DTP-00-99}
\rightline{SU-GP-00/{\it 11}-{\it 1}}
\rightline{hep-th/0011242}
\rightline{October 2000}
\vspace{1truecm}

%%%%%%%%%%%
\centerline{\Large \bf Dielectric branes and spontaneous symmetry breaking}
\vspace{1truecm}

\centerline{
    {\bf C\'esar G\'omez${}^{a,}$}\footnote{E-mail address: 
                                  {\tt cesar.gomez@uam.es}}
    {\bf Bert Janssen${}^{a,b,}$}\footnote{E-mail address: 
                                  {\tt bert.janssen@durham.ac.uk} }
    {\bf and} 
    {\bf Pedro J. Silva${}^{a,c,}$}\footnote{E-mail address: 
                                  {\tt psilva@suhep.phy.syr.edu}}}

\vspace{.4truecm}
\centerline{{\it ${}^a$ Instituto de F{\'\i}sica Te\'orica, C-XVI}} 
\centerline{{\it Universidad Aut\'onoma de Madrid}}
\centerline{{\it E-28006 Madrid, Spain}}

\vspace{.4cm}
\centerline{{\it  ${}^b$ Department of Mathematical Sciences}}
\centerline{{\it South Road, Durham DH1 3LE }}
\centerline{{\it  United Kingdom}}

\vspace{.4truecm}
\centerline{{\it ${}^c$ Physics Department, Syracuse University}}
\centerline{{\it Syracuse, New York, 13244, United States}}
\vspace{2truecm}

%%%%%%%%%%%%%%%%
\centerline{\bf ABSTRACT}
\vspace{.5truecm}

\noindent
A stable non-commutative solution with symmetry breaking is presented
for a 
system of D$p$-branes in the presence of a RR $(p+5)$-form.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\noindent
In Type IIA string theory, 
a system of $N$ coinciding D0-branes in the presence of an external RR 
three-form expands in the form of a non-commutative two-sphere
\cite{Myers}
(see also \cite{alekseev}) . This is due to the coupling in the 
(non-commutative) Wess-Zumino term of the D0-brane system to RR forms
of 
degree 3. This result can be easily
generalised to systems of coinciding  $p$-branes in the presence of RR 
$(p+3)$-form.\footnote{For instance, D($-1$) instantons in the presence
of 
RR 2-form expand in a $S^{2}$ manifold and the corresponding instanton
effect can be interpreted as a world sheet instanton wrapping the 
non-commutative manifold.}

\noindent
One specially interesting case, connected with the gravitational
description
of $N^{*}=1$ gauge theories, are D3-branes in Type IIB coupled to a 
seven-form field strength \cite{polchinski}. Generalisations to other 
non-commutative manifolds, like 
$S^{2}\x S^{2}$ or $CP^{2}$, have been recently considered in
\cite{indios}.

\noindent
Another interesting case to study is the system of $p$-branes in the
presence
of a RR $(p+5)$-form. As an example we can have in mind D($-1$)
instantons
in the presence of RR four-form, a physical system that could be
relevant
in the study of instanton effects \cite{green} in the $AdS_{5}$
description 
of $N=4$ supersym\-me\-tric Yang-Mills gauge theory \cite{maldacena}.
In this 
short note we consider this special case, namely D-instantons in
presence
of a RR five-form. The natural guess for a solution would be to look
for some
non-commutative generalisation of the four-sphere. However this
solution can 
easily be shown to be unstable \cite{indios}. Next we will present a
very 
special matrix solution that is a local minimum. Geometrically it can
be 
interpreted as a sort of ellipsoid.

\noindent
The action for D($-1$)-branes in presence of five-form field strength 
(four-form gauge field) is given by
\be
V(X)= \tau \lambda^2 \ \Tr  \Bigl \{ 
          \tfrac{1}{4} [ X_\mu, X_\nu] [ X_\nu, X_\mu] 
        - \tfrac{1}{10} \lambda X^{\sigma\lambda\rho\nu\mu} 
                   F_{\mu\nu\rho\lambda\sigma} \Bigr\} ,
\label{potential}
\ee
where $X^\mu$ are $N \x N$ matrices that represent the
(non-commutative) 
coordinates, $\tau$ the tension of the D-instantons and $\lambda$ the 
string coupling constant. The equation of motion of the above action is
given by
\be
[ [X_\mu, X_\nu], X_\nu] \ - \  
     \half \lambda \ X^{\sigma\lambda\rho\nu}
F_{\mu\nu\rho\lambda\sigma} = 0.
\label{eqnmov}
\ee
To solve this equation we use the Ansatz
\be
X_a = \half R \  (\sigma_a\ox \Sigma_3) \ , \hspace{1cm}
X_i = i \alpha R \ (\unity \ox \Sigma_i)
\label{ansatz}
\ee
where $a=1,2,3$ y $i = 4,5$ and $\vec{\Sigma}$ and $\vec{\sigma}$ are 
generators of $SU(2)$. (We suppose that the matrices $X^A$, with
$A=6...10$, 
commute with the above and amongst each other). These $X^\mu$ satisfy
the following
commutation relations:
\be
\begin{array}{l}
[ X_a, X_b]=\half\ i R^2\ \epsilon_{abc}\  (\sigma_c \ox \Sigma_3^2),
\\
\\

[X_a, X_i]= - \alpha R^2  \ \epsilon_{ij}\ (\sigma_a \ox \Sigma_j) , \\
\\

[ X_i, X_j ] = -2\ i \alpha^2 R^2 \ \epsilon_{ij}\ (\unity \ox
\Sigma_3)   \ .
\end{array}
\label{commrel}
\ee 
Note that these $X^\mu$ do not form an algebra since the commutation
relations
(\ref{commrel}) do not close. Therefore we will obtain a particular
solution, 
rather than a family of solutions.

\noindent
Filling in the Ansatz (\ref{ansatz}) in the equations of motion
(\ref{eqnmov}),
we find for the $a$ and $i$-component respectively:
\be
\ba{l}
R^3 \Bigl[1- 4\alpha^2 -3\lambda f \alpha^2 R \Bigr] \
                  (\sigma_a \ox \Sigma_3) = 0 \ , \\ 
\\
i \alpha R^3  \Bigl[ 3- 4\alpha^2 + \tfrac{3}{2}\lambda f  R \Bigr]
                   \ (\unity \ox \Sigma_i) = 0\ .
\ea
\label{eqn}
\ee
Here we had to suppose that $\Sigma_i^2 = \sigma_a^2 = \unity$, which
implies
that 
$\vec{\Sigma}$ and $\vec{\sigma}$ are either two-dimensional
(irreducible) 
representations of $SU(2)$ or $4n$-dimensional reducible
representations,
where $n=n_\sigma n_\Sigma$ and $n_\sigma$ ($n_\Sigma$) is the number
of 
irreps in the reducible representation of $\vec\sigma$
($\vec{\Sigma}$). 
Thus we can only describe systems with a number of D($-1$)-branes which
is a 
multiple of four.

\noindent
Clearly, the equation of motion (\ref{eqn}) are satisfied for a
(trivial) 
commutative solution $\alpha=R=0$, but also a non-trivial
non-commutative one 
given by
\be
R= \frac{1-4\alpha^2}{3 \lambda f \alpha^2}  \hspace{1cm}
\mbox{and}    \hspace{1cm}
R= \frac{8\alpha^2 - 6}{3 \lambda f} \ ,
\ee
which leads to two solutions
\bea
&& \alpha_+^2 = \frac{1}{2}, \hspace{3cm} R_+ = \frac{-2}{3\lambda f} \
; 
\label{solutions1}\\
&& \alpha_-^2 = -\frac{1}{4}, \hspace{2.7cm} R_- = \frac{-8}{3\lambda
f} \ . 
\label{solutions2}
\eea
Notice that due to the non-trivial value of $\alpha$, this solutions
can be 
interpreted as a sort of ellipsoid.

\noindent 
To study the stability of these solutions we use the Ansatz
(\ref{ansatz}) 
in the potential (\ref{potential}), 
\be
V(\alpha, R) = \lambda^2 \tau R^4 n 
  \Bigl[ \tfrac{3}{2} - 12 \alpha^2 + 8 \alpha^4 - 6\lambda f\alpha^2 R
\Bigr],
\label{potential2}
\ee
which evaluated in the solutions (\ref{solutions1})-(\ref{solutions2})
gives 
\be
\ba{l}
V_+ =-\frac{1}{4} \lambda^2 \tau R^4 n \ ,\\ \\
V_- = \lambda^2 \tau R^4 n \ .
\ea
\ee
Thus we see that the solution $(\alpha_+, R_+)$ has lower energy than
the 
commutative solution, while the solution $(\alpha_-, R_-)$ has higher
energy.
Calculating the second variation of the potential (\ref{potential2}),
it is 
easy to see that the solution $(\alpha_+, R_+)$ corresponds to a
minimum,
while $(\alpha_-, R_-)$ is a saddle point. 

\noindent
Therefore we can conclude
that, in 
the presence of a five-form field strength, a set of $4n$ D-instantons
will
decay spontaneously into the stable solution $(\alpha_+,
R_+)$.\footnote{In
\cite{Myers} is was observed that reducible $N\x N$ representations
have 
higher (though still negative) energies than the irreducible $N\x N$ 
representations. However, as we noticed above, our solution for general
$N$, 
only exists  for reducible representations.}

\noindent
Notice that this solution breaks $SO(5)$ invariance of the potential 
(\ref{potential}). However we observe that
the solution is degenerated with respect to 
permutations of the $\Sigma$ matrices: any set of matrices of the form
\be
X_a = \half R \  (\sigma_a\ox \Sigma_{j_0}) \ , \hspace{1cm}
X_i = i \alpha R \ (\unity \ox \Sigma_i)
\label{ansatz2}
\ee
with $i$ different from $j_0$, is also a solution with identical
behaviour as
 (\ref{solutions1})-(\ref{solutions2}). Thus this solution could be
interpreted as 
a sort of spontaneous symmetry breaking for the matrix action. The
extension
to any $(p, p+6)$ system is straight forward.

\noindent
As a spinoff of our analysis we just mention that this solution can be 
naturally used to construct longitudinal five-brane solutions in
M(atrix)
theory (see for instance \cite{rand} and references therein).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{1cm}
\noindent
{\bf Acknowledgements}\\
We wish to thank Patrick Meessen and Enrique \'Alvarez 
for the useful discussions.
The work of C.G. and B.J. has been supported by the TMR program
FMRX-CT96-0012
on {\sl Integrability, non-perturbative effects, and symmetry in
quantum field
theory}. The work of P.J.S. has been supported in part by NSF grant
PHY97-22362 
to Syracuse University and by funds from Syracuse University.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{99}

\bibitem{Myers}  R.~C.~Myers, JHEP 9912 (1999) 022, {\tt
hep-th/9910053}.

\bibitem{alekseev}  A.~Yu.~Alekseev, A.~Recknagel, V.~Schomerus,
                     JHEP 05 (2000) 010, {\tt hep-th/0003187}.

\bibitem{polchinski} J.~Polchinski, M.~J.~Strassler, 
                     {\it The String Dual of a Confining
Four-Dimensional 
                     Gauge Theory}, {\tt hep-th/0003136}. 

\bibitem{indios} S.~P.~Trivedi, S.~Vaidya, JHEP 0009 (2000) 041, 
                  {\tt hep-th/0007011}. 

\bibitem{green} R.~Gopakumar, M.~B.~Green,
                JHEP 9912 (1999) 015, {\tt hep-th/9908020}.  

\bibitem{maldacena} J.~M.~Maldacena, 
                    Adv.~Theor.~Math.~Phys. 2 (1998) 231, {\tt
hep-th/9711200}. 

\bibitem{rand} V.~P.~Nair, S.~Randjbar-Daemi,
               Nucl.Phys. B533 (1998) 333, {\tt hep-th/9802187}.

\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% EINDE VAN DIT DOCUMENT %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

