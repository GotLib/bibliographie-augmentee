\documentclass[11pt]{article}
\usepackage{amsmath,amsthm,amscd,array,amssymb}

\bibliographystyle{unsrt}
\pagestyle{plain}
\pagenumbering{arabic}

\setlength{\parskip}{0cm}
\setlength{\parindent}{0.5cm}
\setlength{\topmargin}{-0.5cm}
\setlength{\headheight}{0cm}
\setlength{\headsep}{0cm}
\setlength{\topskip}{0cm}
\setlength{\headheight}{0cm}
\setlength{\oddsidemargin}{-0.0cm}
\setlength{\textwidth}{16cm}
\setlength{\textheight}{24cm}
\setlength{\footskip}{1cm}

\begin{document}

\begin{center}
{\Large{\bf Euclidean super Yang--Mills theory
\\
\medskip\smallskip
on a quaternionic K\"ahler manifold}}
\\
\bigskip\medskip
{\large{\sc D. M\"ulsch}}$^{a}$ \footnote{Email:
muelsch@informatik.uni-leipzig.de}
and
{\large{\sc B. Geyer}}$^b$
\footnote{Email: geyer@itp.uni-leipzig.de}
\\
\smallskip
{\it$\!\!\!\!\!^a$ Wissenschaftszentrum Leipzig e.V., D--04103
Leipzig, Germany
\\\smallskip
 $^b$ Universit\"at Leipzig,
Naturwissenschaftlich-Theoretisches Zentrum\\ $~$ and Institut
f\"ur Theoretische Physik, D--04109 Leipzig, Germany}
\\
\bigskip
{\small{\bf Abstract}}
\\
\end{center}

\begin{quotation}
\noindent {\small{The construction of a $N_T = 3$ cohomological
gauge theory on an eight--dimensional Riemannian manifold with
$Sp(4) \otimes Sp(2)$ holonomy, whose group theoretical
description was given by Blau and Thompson \cite{1}, is performed
explicitly.}}
\end{quotation}

%\setlength{\baselineskip}{0.7cm}


\bigskip\medskip
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{flushleft}
{\large{\bf 1. Introduction}}
\end{flushleft}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
In recent years the investigation of cohomological gauge theories
on manifolds of special holonomy in diverse dimensions have
attracted a lot of interest \cite{2,3,4,5}. In the context of
string theory and M--theory special holonomy plays a prominent
role primarily because the simplest vacua preserving some part of
supersymmetry are compactifications on manifolds of special
holonomy. So far, Calabi--Yau three--folds with $SU(3)$ holonomy
have received the most intensive study in connection with
heterotic string compactifications and due to the miraculous
property of mirror symmetry of type II strings on such manifolds.
Recently, the exceptional $G_2$-- and $Spin(7)$--holonomy Joyce
seven-- and eight--folds, respectively, have received considerable
attention as well, since they may provide the simplest way to
compactify M--theory to four dimensions and to understand the
dynamics of $N = 1$ supersymmetric field theories.

Moreover, it has been shown that the ideas underlying topological
gauge theories can be extended also to dimensions higher than
four. These cohomological theories, which arise without the need
for a topological twisting, acquire many of the characteristics of
a topological theory. Nevertheless, such theories, which have a
rather intriguing structure, are not topological in the strong
sense, since they are only invariant under certain metric
variations which preserve the {\it reduced} holonomy structure. At
present, the physical status of such theories in $D > 4$ has not
be entirely understood. But, since it is widely believed that the
effective world volume theory of the D--brane is the dimensional
reduction of $N = 1$, $D = 10$ super Yang--Mills theory (SYM)
\cite{6}, such cohomological gauge theories could arise naturally
in the study of wrapped Euclidean D--branes in string theory (see,
e.g., \cite{7}).

Examples of such theories in $D = 8$ are the J--theory of
\cite{3,4} on a Joyce eight--manifold with $Spin(7)$ holonomy ---
a $N_T = 1$ theory describing a higher dimensional analogue of the
Donaldson--Witten theory --- and the H--theory of \cite{3} on a
Calabi--Yau four--fold with $Spin(6) \sim SU(4)$ holonomy --- a
$N_T = 2$ theory describing a holomorphic analogue of the
Donaldson--Witten theory. The only other possible manifold
admitting covariantly constant spinors is the hyper--K\"ahler
eight--fold with $Spin(5) \sim Sp(4)$ holonomy, which could be
interesting as well \cite{8}. In this Letter we consider the
somewhat more involved case of a quarternionic K\"ahler manifold
with $Sp(4) \otimes Sp(2)$ holonomy and construct another example
of a cohomological theory in $D = 8$ which, adopting a similar
shorthand notation as in \cite{3}, will be called Q--theory (it is
a $N_T = 3$ theory which can be obtained also from the H--theory
by reducing the structure group $SU(4)$ to $Sp(4) \otimes Sp(2)$).
Based on a previous work of Ward \cite{9}, a group theoretical
description of the Q--theory was given in \cite{1}. The aim of
this Letter is to give an explicit construction of that theory.

The Letter is organized as follows. In Sect. 2, we first derive,
in accordance with \cite{9}, the $Sp(4) \otimes Sp(2)$ instanton
equations and present the Q--theory in flat Euclidean space, which
arises from the Euclidean $N = 2$, $D = 8$ SYM by breaking down
the Euclidean rotation group $SO(8)$ to $Sp(4) \otimes Sp(2)$. In
Sect. 3, we formulate the Q--theory on the quarternionic K\"ahler
manifold.

\bigskip\medskip
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{flushleft}
{\large{\bf 2. The reduction $SO(8) \rightarrow Sp(4) \otimes Sp(2)$ of
Euclidean super Yang--Mills theory in eight dimensions}}
\end{flushleft}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
If the group $SO(8)$ is reduced to the subgroup $Sp(4) \otimes
Sp(2)$ the Euclidean coordinates $x^m$ ($m = 1, \ldots, 8$) can be
expressed through complex coordinates $z_{A a}$, where $A =
1,2,3,4$ and $a = 1,2$ are $Sp(4)$ and $Sp(2)$ indices,
respectively, thereby preserving the Euclidean metric,
\begin{equation}
\label{2.1}
d s^2 = d x^m \otimes d x_m =
\epsilon^{AB} \epsilon^{ab} d z_{A a} \otimes d z_{B b},
\qquad
x_m = e_m^{\!~~A a} z_{A a}.
\end{equation}
Here, $e_m^{\!~~A a}$ defines a (non--singular) map from the $8$--dimensional
Euclidean space to the $4$--dimensional complex space,
\begin{equation*}
e_m^{\!~~A a} e_{n A a} = \delta_{mn},
\qquad
e_m^{\!~~A a} e^{m B b} = \epsilon^{AB} \epsilon^{ab}.
\end{equation*}
(Later on, in Sect. 3, where the Q--theory is formulated on the
quaternionic K\"ahler manifold, we view $e_m^{\!~~A a}$ locally as
the $8$--bein on that manifold.) The indices $A$ and $a$ are
raised and lowered as follows, $z^{A a} = \epsilon^{AB}
\epsilon^{ab} z_{B b}$ and $z_{B b} = z^{A a} \epsilon_{AB}
\epsilon_{ab}$, with $\epsilon_{AC} \epsilon^{BC} =
\delta_A^{\!~~B}$ and $\epsilon_{ac} \epsilon^{bc} =
\delta_a^{~b}$, where $\epsilon_{AB}$ and $\epsilon_{ab}$ are
invariant symplectic tensors of the group $Sp(4)$ and $Sp(2)$,
respectively, $\epsilon_{12} = - \epsilon_{21} = \epsilon_{34} = -
\epsilon_{43} = 1$. Explicitly, for the components of $z_{A a} =
e^m_{\!~~A a} x_m$ we choose
\begin{alignat*}{4}
&z_{11} = x_1 + i x_2,
&\qquad
&z_{21} = - x_3 + i x_4,
&\qquad
&z_{31} = x_5 + i x_6,
&\qquad
&z_{41} = - x_7 + i x_8,
\\
&z_{12} = x_3 + i x_4,
&\qquad
&z_{22} = x_1 - i x_2,
&\qquad
&z_{32} = x_7 + i x_8,
&\qquad
&z_{42} = x_5 - i x_6,
\end{alignat*}
which will be grouped into the complex conjugated coordinates
$z_A \equiv ( z_{11}, z_{12}, z_{31}, z_{32} )$ and
$\bar{z}_A \equiv ( z_{22}, - z_{21}, z_{42}, - z_{41} )$.

Then, an irreducible, $Sp(4) \otimes Sp(2)$--invariant action of
the gauge field $A_A^{\!~~a}$, being in the adjoint representation
of some compact gauge group $G$, is given by
\begin{equation}
\label{2.2}
S_{\rm YM} = \int d^4z \, d^4\bar{z}\, {\rm tr} \Bigr\{
\hbox{$\frac{1}{4}$} F^{AB}_{\!~~~~ab} F_{AB}^{\!~~~~ab} \Bigr\},
\end{equation}
where $F_{AB}^{\!~~~~ab} = \partial_{[A}^{~~a} A_{B]}^{~~b} + [
A_A^{\!~~a}, A_B^{\!~~b} ]$ is the corresponding field strength.

Besides of (\ref{2.2}), one can construct also a first--stage
reducible, $Sp(4) \otimes Sp(2)$--invariant action, namely
\begin{equation}
\label{2.3}
S_{\rm T} = \int d^4z\, d^4\bar{z}\, {\rm tr} \Bigr\{
\hbox{$\frac{1}{12}$} \Bigr(
\epsilon^{ab} F^{AB}_{\!~~~~ab} \epsilon^{cd} F_{AB cd} -
\epsilon_{AB} F^{AB}_{\!~~~~ab} \epsilon_{CD} F^{CD ab} -
F^{AB}_{\!~~~~ab} F_{AB}^{\!~~~~ba} \Bigr) \Bigr\},
\end{equation}
which can be recast into the form
\begin{equation*}
S_{\rm T} = \int d^4z\, d^4\bar{z}\, {\rm tr} \Bigr\{
\hbox{$\frac{1}{24}$} \epsilon_{ABCD}^{\!~~~~~~~~abcd}
F^{AB}_{\!~~~~ab} F^{CD}_{\!~~~~cd} \Bigr\},
\end{equation*}
where the (fourth rank) tensor $\epsilon_{ABCD}^{\!~~~~~~~~abcd}$
is a totally skew--symmetric in the index pairs $(A a)$, $(B b)$,
$(C c)$ and $(D d)$,
\begin{equation*}
\epsilon_{ABCD}^{\!~~~~~~~~abcd} =
\epsilon_{AB} \epsilon_{CD} ( \epsilon^{ca} \epsilon^{bd} -
\epsilon^{bc} \epsilon^{ad} ) +
\epsilon_{BC} \epsilon_{AD} ( \epsilon^{ab} \epsilon^{cd} -
\epsilon^{ca} \epsilon^{bd} ) +
\epsilon_{CA} \epsilon_{BD} ( \epsilon^{bc} \epsilon^{ad} -
\epsilon^{ab} \epsilon^{cd}).
\end{equation*}
This tensor defines a linear map of the space of field strengths,
$F_{AB}^{\!~~~~ab}$, onto itself, namely $\hbox{$\frac{1}{2}$}
\epsilon_{ABCD}^{\!~~~~~~~~abcd} F^{CD}_{\!~~~~cd} = \lambda
F_{AB}^{\!~~~~ab}$. Therefore, one can look for its eigenvalues
$\lambda$. In accordance with the $Sp(4) \otimes Sp(2)$
decomposition of the adjoint representation of $SO(8)$ \cite{9,1},
\begin{equation*}
\mathbf{28} \rightarrow \mathbf{(4,2)} \otimes \mathbf{(4,2)} =
\mathbf{(1,3)} \oplus \mathbf{(5,3)} \oplus \mathbf{(10,1)},
\end{equation*}
it is straightforward to prove that there exist three irreducible
subspaces which are characterized by the eigenvalues $\lambda =
-5$, $-1$ and $3$ of $F_{AB}^{\!~~~~ab}$. Under the branching
$SO(8) \rightarrow Sp(4) \otimes Sp(2)$ the field strength
decomposes into
\begin{equation}
\label{2.4}
F_{mn} \rightarrow F_{AB}^{\!~~~~ab} = \hbox{$\frac{1}{4}$} \epsilon_{AB}
\epsilon_{CD} F^{CD ab} +
\hbox{$\frac{1}{4}$} \Omega_{ABCD}
\Omega^{abcd} F^{CD}_{\!~~~~cd} +
\hbox{$\frac{1}{2}$} \epsilon^{ab}
\epsilon^{cd} F^{AB}_{\!~~~~cd},
\end{equation}
where, for later use, we still have introduced the following two
projection operators:
\begin{gather}
\label{2.5}
\Omega_{abcd} = \epsilon_{ac} \epsilon_{bd} + \epsilon_{ad} \epsilon_{bc},
\qquad
\hbox{$\frac{1}{2}$} \Omega_{abef} \Omega^{ef}_{~~cd} = \Omega_{abcd},
\nonumber
\\
\Omega_{ABCD} = \epsilon_{AC} \epsilon_{BD} - \epsilon_{AD}
\epsilon_{BC} - \hbox{$\frac{1}{2}$} \epsilon_{AB} \epsilon_{CD},
\qquad \hbox{$\frac{1}{2}$} \Omega_{ABEF} \Omega^{EF}_{\!~~~~CD} =
\Omega_{ABCD}.
\end{gather}
Thereby, $\epsilon_{CD} F^{CD ab}$ and $\epsilon^{cd}
F^{AB}_{\!~~~~cd}$ in the first and third term of (\ref{2.4}), by
virtue to $F_{AB}^{\!~~~~ab} = - F_{BA}^{\!~~~~ba}$, are symmetric
in $(ab)$ and $(AB)$, respectively, and $\Omega_{ABCD}
\Omega^{abcd} F^{CD}_{\!~~~~cd}$ in the second term is symmetric
in $(ab)$ and skew--symmetric and trace free in
$(AB),\,\epsilon^{AB}\Omega_{ABCD}=0$.

On the other hand, the action (\ref{2.2}) can be rewritten as
\begin{equation}
\label{2.6}
S_{\rm YM} = S_{\rm T} + \int d^4z\, d^4\bar{z}\, {\rm tr} \Bigr\{
\hbox{$\frac{1}{6}$} \Omega_{ABCD} F^{AB}_{\!~~~~ab} F^{CD ab} +
\hbox{$\frac{1}{6}$} \epsilon_{AB} F^{AB}_{\!~~~~ab}
\epsilon_{CD} F^{CD ab} \Bigr\}.
\end{equation}
Hence, if we, formally, just as in case of the $Spin(3)$
instantons in $D = 4$, impose $S_{\rm YM} = S_{\rm T}$, by virtue
of (5), it follows that the $Sp(4) \otimes Sp(2)$ instanton
equations are characterized by the eigenvalue $\lambda = 3$,
\begin{equation}
\label{2.7}
\hbox{$\frac{1}{6}$} \epsilon_{ABCD}^{\!~~~~~~~~abcd} F^{CD}_{\!~~~~cd} =
F_{AB}^{\!~~~~ab}
\qquad
\Leftrightarrow
\qquad
\Omega_{ABCD} F^{CD}_{\!~~~~ab} = 0,
\qquad
\epsilon_{AB} F^{AB}_{\!~~~~ab} = 0.
\end{equation}
This result is in accordance with \cite{9}, where Ward discusses
$Sp(4) \otimes Sp(2)$--invariant integrable equations for the
gauge field $A_A^{\!~~a}$, and where he obtains the 18 equations
(\ref{2.7}) as the integrability conditions $\pi_a \pi_b
F_{AB}^{\!~~~~ab} = 0$ for the equations $\pi_a (
\partial_A^{\!~~a} + A_A^{\!~~a}) \psi = 0$, here, $\pi_a$ are
the homogeneous coordinates on the complex projective space,
${\mathbb{CP}}$.
\bigskip

Now, we like to construct the Q--theory, whose action localizes
onto the moduli space of the self--duality equations (\ref{2.7}),
in flat space. Just as in the case of the J-- and H--theory, this
theory can be obtained from the Euclidean $N = 2$, $D = 8$ SYM by
reducing the rotation group $SO(8)$ to $Sp(4) \otimes Sp(2)$.

The gauge multiplet of the Euclidean $N = 2$, $D = 8$ SYM consists
of the vector field $A_m$, a chiral and anti--chiral Weyl spinor,
$\lambda$ and $\bar{\lambda}$, and the scalar fields $\phi$ and
$\bar{\phi}$. The vector, the chiral spinor and the anti--chiral
spinor representation, which are all eight--dimensional, will be
denoted by $\mathbf{8}_{\bf v}$, $\mathbf{8}_{\bf s}$ and
$\mathbf{8}_{\bf c}$, respectively. Under the branching $SO(8)
\rightarrow Sp(4) \otimes Sp(2)$ these representations decompose
as follows \cite{1},
\begin{alignat*}{2}
&\mathbf{8}_{\bf v} \rightarrow \mathbf{(4,2)},
&\qquad
&A_m \rightarrow A_A^{\!~~a},
\\
&\mathbf{8}_{\bf s} \rightarrow \mathbf{(5,1)} \oplus \mathbf{(1,3)},
&\qquad
&\lambda \rightarrow \chi_{AB}, \eta^{ab},
\\
&\mathbf{8}_{\bf c} \rightarrow \mathbf{(4,2)},
&\qquad
&\bar{\lambda} \rightarrow \psi_A^{\!~~a},
\end{alignat*}
i.e., the vector and the anti--chiral representation remains irreducible
under $Sp(4) \otimes Sp(2)$, whereas the reduction of $SO(8)$ to
$Sp(4) \otimes Sp(2)$ induces an invariant decomposition of the spinor
representation. Thereby, $\chi_{AB}$ is skew--symmetric and traceless,
$\epsilon^{AB} \chi_{AB} = 0$, and $\eta^{ab}$ is symmetric.

Omitting any details, after performing the reduction $SO(8) \rightarrow
Sp(4) \otimes Sp(2)$ one ends up with the following $SO(8)$--covariant and
$Sp(4) \otimes Sp(2)$--invariant action of the Q--theory with an extended,
$N_T = 3$, {\it on--shell} equivariantly nilpotent topological supersymmetry,
\begin{align}
\label{2.8}
{S_{\rm SYM}^{(N_T = 3)}}_{\Bigr|Sp(4) \otimes Sp(2) \subset SO(8)} =
\int d^4z\, d^4\bar{z}\, {\rm tr} \Bigr\{&
\hbox{$\frac{1}{4}$} F^{AB}_{\!~~~~ab} F_{AB}^{\!~~~~ab} -
2 D^{A a} \bar{\phi} D_{A a} \phi
\\
& - 2 \chi_{AB} D^{A a} \psi^B_{\!~~a} +
2 \eta^{ab} D^A_{\!~~a} \psi_{A b}  - 2 [ \bar{\phi}, \phi ]^2
\nonumber
\\
& - 2 \bar{\phi} \{ \psi^{A a}, \psi_{A a} \} -
\hbox{$\frac{1}{2}$} \phi \{ \chi^{AB}, \chi_{AB} \} -
\phi \{ \eta^{ab}, \eta_{ab} \} \Bigr\},
\nonumber
\end{align}
where $D_A^{\!~~a} = \partial_A^{\!~~a} + [ A_A^{\!~~a}, ~\cdot~ ]$.
All the fields are in the adjoint representation and take their values
in the Lie algebra $Lie(G)$ of the gauge group $G$.

Furthermore, denoting the 16 (real) supercharges with $Q^{ab}$, $Q_{AB}$
and $\bar{Q}_A^{\!~~a}$, where $Q^{ab}$ is symmetric,
$Q^{ab} = \frac{1}{2} \Omega^{abcd} Q_{cd}$, and $Q_{AB}$ is
skew--symmetric and tracless,
$Q_{AB} = \frac{1}{2} \Omega_{ABCD} Q^{CD}$, for the {\it on--shell} scalar,
tensor and vector supersymmetry transformations one gets
\begin{align}
\label{2.9}
&Q^{ab} A_A^{\!~~c} = \Omega^{abcd} \psi_{A d},
\nonumber
\\
&Q^{ab} \psi_A^{\!~~c} = \Omega^{abcd} D_{A d} \phi,
\nonumber
\\
&Q^{ab} \phi = 0,
\nonumber
\\
\nonumber
&Q^{ab} \bar{\phi} = \eta^{ab},
\nonumber
\\
&Q^{ab} \eta^{cd} = - \hbox{$\frac{1}{4}$} \Omega^{abe}_{\!~~~~g}
\Omega^{cdfg} \epsilon_{AB} F^{AB}_{\!~~~~ef} +
\Omega^{abcd} [ \phi, \bar{\phi} ],
\nonumber
\\
&Q^{ab} \chi_{AB} = \hbox{$\frac{1}{2}$}
\Omega^{abcd} \Omega_{ABCD} F^{CD}_{\!~~~~cd},
\\
\intertext{}
\label{2.10}
&Q_{AB} A_C^{\!~~a} = \Omega_{ABCD} \psi^{D a},
\nonumber
\\
&Q_{AB} \psi_C^{\!~~a} = - \Omega_{ABCD} D^{D a} \phi,
\nonumber
\\
&Q_{AB} \phi = 0,
\nonumber
\\
\nonumber
&Q_{AB} \bar{\phi} = \hbox{$\frac{1}{2}$} \chi_{AB},
\nonumber
\\
&Q_{AB} \eta^{ab} = - \hbox{$\frac{1}{4}$} \Omega_{ABCD}
\Omega^{abcd} F^{CD}_{\!~~~~cd},
\nonumber
\\
&Q_{AB} \chi_{CD} = - \hbox{$\frac{1}{2}$} \Omega_{ABEG}
\Omega_{CDF}^{~~~~~~G} \epsilon^{ab} F^{EF}_{\!~~~~ab} +
\Omega_{ABCD} [ \phi, \bar{\phi} ],
\\
\intertext{and}
\label{2.11}
&\bar{Q}_A^{\!~~a} A_B^{\!~~b} = \epsilon_{AB} \eta^{ab} +
\epsilon^{ab} \chi_{AB},
\nonumber
\\
&\bar{Q}_A^{\!~~a} \psi_B^{\!~~b} = F_{AB}^{\!~~~~ab} -
\hbox{$\frac{1}{2}$} \Omega_{ABCD} \Omega^{abcd} F^{CD}_{\!~~~~cd} +
\epsilon_{AB} \epsilon^{ab} [ \phi, \bar{\phi} ],
\nonumber
\\
&\bar{Q}_A^{\!~~a} \phi = - \psi_A^{\!~~a},
\nonumber
\\
&\bar{Q}_A^{\!~~a} \bar{\phi} = 0,
\nonumber
\\
&\bar{Q}_A^{\!~~a} \eta^{cd} = - \Omega^{abcd} D_{A b} \bar{\phi},
\nonumber
\\
&\bar{Q}_A^{\!~~a} \chi_{CD} = 2 \Omega_{ABCD} D^{B a} \bar{\phi},
\end{align}
respectively, where the projection operators $\Omega^{abcd}$ and
$\Omega_{ABCD}$ have been introduced in (5).

The {\it on--shell} algebraic relations among the supercharges $Q^{ab}$,
$Q_{AB}$ and $\bar{Q}_A^{\!~~a}$ are
\begin{gather*}
\{ Q^{ab}, Q^{cd} \} \doteq - 2 \Omega^{abcd} \delta_G(\phi),
\qquad
\{ Q_{AB}, Q_{CD} \} \doteq \Omega_{ABCD} \delta_G(\phi),
\\
\{ Q^{ab}, Q_{AB} \} \doteq 0,
\\
\{ Q^{ab}, \bar{Q}_A^{\!~~c} \} \doteq - \Omega^{abcd}
( \partial_{A d} + \delta_G(A_{A d}) ),
\qquad
\{ Q_{AB}, \bar{Q}_C^{\!~~a} \} \doteq \Omega_{ABCD}
( \partial^{D a} + \delta_G(A^{D a}) ),
\\
\{ \bar{Q}_A^{\!~~a}, \bar{Q}_B^{\!~~b} \} \doteq
2 \epsilon_{AB} \epsilon^{ab} \delta_G(\bar{\phi}),
\end{gather*}
where $\delta_G(\varphi)$ denotes a gauge transformation with
field--dependent parameters $\varphi = ( A_A^{\!~~a}, \phi, \bar{\phi} )$,
being defined by $\delta_G(\varphi) A_A^{\!~~a} = - D_A^{\!~~a} \varphi$ and
$\delta_G(\varphi) X = [ \varphi, X ]$ for all the other fields.

In order to verify that the transformations (\ref{2.9})--(\ref{2.11})
actually leave the action (\ref{2.8}) invariant one needs the following
basic relations for the projection operators,
\begin{align}
\label{2.13}
\Omega_{abeg} \Omega_{cdf}^{~~~~\!g} +
\Omega_{abfg} \Omega_{cde}^{~~~~\!g} &=
\Omega_{abce} \epsilon_{df} - \Omega_{abde} \epsilon_{cf} +
\Omega_{abcf} \epsilon_{de} - \Omega_{abdf} \epsilon_{ce},
\nonumber
\\
\Omega_{abeg} \Omega_{cdf}^{~~~~\!g} -
\Omega_{abfg} \Omega_{cde}^{~~~~\!g} &= 2 \Omega_{abcd} \epsilon_{ef},
\\
\intertext{and}
\label{2.14}
\Omega_{ABEG} \Omega_{CDF}^{~~~~~~G} +
\Omega_{ABFG} \Omega_{CDE}^{~~~~~~G} &=
\Omega_{ABCE} \epsilon_{DF} - \Omega_{ABDE} \epsilon_{CF} +
\Omega_{ABCF} \epsilon_{DE} - \Omega_{ABDF} \epsilon_{CE},
\nonumber
\\
\Omega_{ABEG} \Omega_{CDF}^{~~~~~~G} -
\Omega_{ABFG} \Omega_{CDE}^{~~~~~~G} &= \Omega_{ABCD} \epsilon_{EF},
\end{align}
where we have made use of the following equalities,
\begin{gather*}
\epsilon_{ac} \epsilon_{bd} + \hbox{cyclic}~(a,b,c) = 0,
\\
\epsilon_{AC} \epsilon_{BD} \epsilon_{EF} -
\epsilon_{AC} ( \epsilon_{BE} \epsilon_{DF} - \epsilon_{BF} \epsilon_{DE} ) -
\epsilon_{BD} ( \epsilon_{AE} \epsilon_{CF} - \epsilon_{AF} \epsilon_{CE} ) +
\hbox{cyclic}~(A,B,C) = 0.
\end{gather*}
Notice, that the different prefactors on the right--hand side of (\ref{2.13})
and (\ref{2.14}) are a consequence of the different normalization
$\epsilon_{ab} \epsilon^{ab} = 2$ and $\epsilon_{AB} \epsilon^{AB} = 4$.

Finally, in order to obtain a cohomological action we still split off from
(\ref{2.8}) the first--stage reducible action (\ref{2.3}),
\begin{equation*}
S^{(N_T = 3)} =
{S_{\rm SYM}^{(N_T = 3)}}_{\Bigr|Sp(4) \otimes Sp(2) \subset SO(8)} -
S_{\rm T},
\end{equation*}
which, by virtue of (\ref{2.6}), yields
\begin{align}
\label{2.15}
S^{(N_T = 3)} = \int d^4z\, d^4\bar{z}\, {\rm tr} \Bigr\{&
\hbox{$\frac{1}{6}$} \Omega_{ABCD} F^{AB}_{\!~~~~ab} F^{CD ab} +
\hbox{$\frac{1}{6}$} \epsilon_{AB} F^{AB}_{\!~~~~ab}
\epsilon_{CD} F^{CD ab}
\nonumber
\\
& - 2 \chi_{AB} D^{A a} \psi^B_{\!~~a} +
2 \eta^{ab} D^A_{\!~~a} \psi_{A b} -
2 \bar{\phi} \{ \psi^{A a}, \psi_{A a} \}
\nonumber
\\
& - \hbox{$\frac{1}{2}$} \phi \{ \chi^{AB}, \chi_{AB} \} -
\phi \{ \eta^{ab}, \eta_{ab} \} - 2 D^{A a} \bar{\phi} D_{A a} \phi -
2 [ \bar{\phi}, \phi ]^2 \Bigr\}.
\end{align}
Then, {\it on--shell}, upon using the equations of motion of $\chi_{AB}$
and $\eta^{ab}$, the action (\ref{2.15}) can be cast into the $Q^{ab}$--exact
form
\begin{equation*}
S^{(N_T = 3)} \doteq Q^{ab} \Psi_{ab},
\end{equation*}
with the gauge fermion
\begin{equation*}
\Psi_{ab} = \Omega_{abcd} \int d^4z\, d^4\bar{z}\, {\rm tr} \Bigr\{
\hbox{$\frac{1}{12}$} \chi_{AB} F^{AB cd} +
\hbox{$\frac{1}{12}$} \eta^c_{\!~~e} \epsilon_{AB} F^{AB de} +
\hbox{$\frac{1}{6}$} \eta^{cd} [ \phi, \bar{\phi} ] -
\hbox{$\frac{1}{3}$} \bar{\phi} D^{A c} \psi_A^{\!~~d} \Bigr\}.
\end{equation*}


\bigskip\medskip

\pagebreak
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{flushleft}
{\large{\bf 3. Euclidean super Yang--Mills theory on the quaternionic
K\"ahler manifold}}
\end{flushleft}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bigskip
We shall now formulate the Q--theory on an eight--dimensional
Riemannian manifold with $Sp(4) \otimes Sp(2)$ holonomy, endowed
with (hermitean) metric $g_{\mu\nu}$ and $Sp(4)$ and $Sp(2)$ spin
connections $\omega_\mu^{\!~~AB}$ and $\omega_\mu^{\!~~ab}$,
respectively. The (real) curved coordinates will be indicated as
$x^\mu$ ($\mu = 1, \ldots, 8$) and the complex coordinates will
again be denoted by $z_{A a}$,
\begin{equation*}
d s^2 = g^{\mu\nu} d x_\mu \otimes d x_\nu,
\qquad
x_\mu = e_\mu^{\!~~A a} z_{A a},
\end{equation*}
where locally $e_\mu^{\!~~A a}$ is the invertible $8$--bein on the
quaternionic K\"ahler manifold,
\begin{equation*}
e_\mu^{\!~~A a} e_{\nu A a} = g_{\mu\nu},
\qquad
e_\mu^{\!~~A a} e^{\mu B b} = \epsilon^{AB} \epsilon^{ab}.
\end{equation*}
In order to break down the structure group $GL(4,C) \otimes
GL(2,C)$ to $Sp(4) \otimes Sp(2)$, we have to require covariant
constancy of the symplectic tensors $\epsilon^{AB}$ and
$\epsilon^{ab}$,
\begin{align*}
&\nabla_\mu \epsilon^{AB} \equiv \partial_\mu \epsilon^{AB} +
\omega_{\mu~~C}^{\!~~A} \epsilon^{CB} +
\omega_{\mu~~C}^{\!~~B} \epsilon^{AC} = 0,
\\
&\nabla_\mu \epsilon^{ab} \equiv \partial_\mu \epsilon^{ab} +
\omega_{\mu\!~~c}^{\!~~a} \epsilon^{cb} +
\omega_{\mu\!~~c}^{\!~~b} \epsilon^{ac} = 0,
\end{align*}
where $\nabla_\mu$ denotes the metric covariant derivative. Then, the
integrability conditions $[ \nabla_\mu, \nabla_\nu ]\break
( \epsilon^{AB}, \epsilon^{ab} ) = 0$ imply that $\epsilon^{AB}$ and
$\epsilon^{ab}$ are constant and that the antisymmetric part of the
spin connections can be chosen to be zero, i.e., $\omega_\mu^{~(AB)}$ and
$\omega_\mu^{~(ab)}$.

Furthermore, in order to ensure covariant constancy of the metric, we impose
\begin{equation}
\label{3.1}
\nabla_\mu e_\nu^{\!~~A a} \equiv \partial_\mu e_\nu^{\!~~A a} -
\Gamma_{\mu\nu}^{~~~\rho} e_\rho^{\!~~A a} +
\omega_{\mu~~B}^{\!~~A} e_\nu^{\!~~B a} +
\omega_{\mu\!~~b}^{\!~~a} e_\nu^{\!~~A b} = 0,
\end{equation}
where $\Gamma_{\mu\nu}^{~~~\rho}$ is the affine connection. Then, from the
corresponding integrability condition for $e_\mu^{\!~~A a}$ it follows that
the affine connection is the (torsionless) Levi--Civita connection.

The crucial ingredient of a quaternionic K\"ahler manifold is a triplet
of complex structures
\begin{equation}
\label{3.2}
(J^\alpha)_\mu^{\!~~\nu} = - i e_\mu^{\!~~A a} (\sigma^\alpha)_a^{\!~~b}
e^\nu_{\!~~A b},
\qquad
\alpha = 1,2,3,
\end{equation}
where $(\sigma^\alpha)_a^{\!~~b}$ are the $Sp(2)$ generators,
$(\sigma^\alpha)_a^{\!~~c} (\sigma^\beta)_c^{\!~~b} =
i \epsilon^{\alpha\beta\gamma} (\sigma_\gamma)_a^{\!~~b} +
\delta^{\alpha\beta} \delta_a^{\!~~b}$, which obey the algebra of the
quaternions,
\begin{equation*}
(J^\alpha)_\mu^{\!~~\rho} (J^\beta)_\rho^{\!~~\nu} =
\epsilon^{\alpha\beta\gamma} (J_\gamma)_\mu^{\!~~\nu} -
\delta_\mu^{\!~~\nu} \delta^{\alpha\beta}.
\end{equation*}
Since the metric $g_{\mu\nu}$ is preserved and hermitean, it holds
\begin{equation*}
(J^\alpha)_\mu^{\!~~\rho} g_{\rho\nu} +
(J^\alpha)_\nu^{\!~~\rho} g_{\rho\mu} = 0.
\end{equation*}
Furthermore, on a quaternionic K\"ahler manifold the complex
structures (\ref{3.2}) are required to be covariantly constant as
well,
\begin{equation}
\label{3.3}
\nabla_\mu (J^\alpha)_\nu^{\!~~\lambda} \equiv
\partial_\mu (J^\alpha)_\nu^{\!~~\lambda} -
\Gamma_{\mu\nu}^{~~~\rho} (J^\alpha)_\rho^{\!~~\lambda} +
\Gamma_{\mu\rho}^{~~~\lambda} (J^\alpha)_\nu^{\!~~\rho} +
2 \epsilon^{\alpha\beta\gamma} \omega_{\mu \beta}
(J_\gamma)_\nu^{\!~~\lambda} = 0,
\end{equation}
where in place of $\omega_\mu^{~ab}$ we have used the triplet representation
$\omega_\mu^{\!~~\alpha} = \frac{1}{2} i (\sigma^\alpha)_{ab}
\omega_\mu^{\!~~ab}$. Since a quaternionic K\"ahler manifold --- in contrast
to a hyper--K\"ahler eight--fold --- does not have a vanishing Nijenhuis
tensor, the Levi--Civita connection and the $Sp(2)$ spin connection can not be
uniquely defined without additional requirements.
\footnote{The covariant constancy condition (\ref{3.3})
is left invariant when one performs simultaneously the replacements
$\Gamma_{\mu\nu}^{~~~\lambda} \rightarrow
\Gamma_{\mu\nu}^{~~~\lambda} + 2 (
\delta_{(\mu}^{~~\rho} \delta_{\nu)}^{~~\lambda} -
(J^\alpha)_{(\mu}^{~~\rho} (J_\alpha)_{\nu)}^{~~\lambda} ) \xi_\rho$ and
$\omega_\mu^{\!~~\alpha} \rightarrow \omega_\mu^{\!~~\alpha} +
(J^\alpha)_\mu^{\!~~\rho} \xi_\rho$, where $\xi_\rho$ is an arbitrary
vector (see, Appendix B of \cite{10}).}

In order to define in (\ref{3.3}) the $Sp(2)$ spin connection
$\omega_\mu^{\!~~\alpha}$ we adopt the two requirements proposed
in \cite{10},
\begin{equation}
\label{3.4}
(J^\alpha)_\mu^{\!~~\nu} \omega_\nu^{\!~~\alpha} = 0,
\qquad
N_{\mu\nu}^{~~~\lambda} = -
(J^\alpha)_{[\mu}^{\!~~~\lambda} \omega_{\nu] \alpha},
\end{equation}
where $N_{\mu\nu}^{~~~\lambda}$ is Nijenhuis tensor (in the normalization
of \cite{10})
\begin{equation*}
N_{\mu\nu}^{~~~\lambda} \equiv
\hbox{$\frac{1}{6}$} (J^\alpha)_\mu^{\!~~\rho}
\partial_{[\rho} (J_\alpha)_{\nu]}^{~~\lambda} -
\hbox{$\frac{1}{6}$} (J^\alpha)_\nu^{\!~~\rho}
\partial_{[\rho} (J_\alpha)_{\mu]}^{~~\lambda} = - N_{\nu\mu}^{~~~\lambda}.
\end{equation*}
The second condition in (\ref{3.4}), which ensures that
$\Gamma_{\mu\nu}^{~~~\lambda}$ is actually torsionless, can be easily solved
for $\omega_\mu^{\!~~\alpha}$. One finds
\begin{equation*}
\omega_\mu^{\!~~\alpha} = - \hbox{$\frac{1}{7}$} N_{\mu\nu}^{~~~\lambda}
(J^\alpha)_\lambda^{\!~~\nu}.
\end{equation*}
Then, one can show that the Levi--Civita connection in (\ref{3.3}) is
equal to the Oproiu connection \cite{11},
\begin{equation}
\label{3.5}
\Gamma_{\mu\nu}^{~~~\lambda} = - \hbox{$\frac{1}{3}$}
\partial_{(\mu} (J^\alpha)_{\nu)}^{~~\rho} (J_\alpha)_\rho^{\!~~\lambda} -
\hbox{$\frac{1}{6}$} \epsilon^{\alpha\beta\gamma} (J_\beta)_{(\mu}^{~~\rho}
\partial_{|\rho|} (J_\gamma)_{\nu)}^{~~\sigma}
(J_\alpha)_\sigma^{\!~~\lambda} -
(J^\alpha)_{(\mu}^{\!~~~\lambda} \omega_{\nu) \alpha};
\end{equation}
it differs from the Obata connection \cite{12} --- the Levi--Civita
connection in the case of a hyper--K\"ahler eight--fold --- by the
$\omega_\mu^{\!~~\alpha}$--dependent term. With that choice for
$\omega_\mu^{\!~~ab} = i (\sigma_\alpha)^{ab} \omega_\mu^{\!~~\alpha}$
and $\Gamma_{\mu\nu}^{~~~\lambda}$ the $Sp(4)$ spin connection
$\omega_\mu^{\!~~AB}$ can be immediately obtained from the condition
(\ref{3.1}).

From the integrability condition $[ \nabla_\mu, \nabla_\nu ]
(J^\alpha)_\rho^{\!~~\sigma} = 0$ for the complex structures it
follows that the Riemannian curvature decomposes into
\begin{equation*}
R_{\mu\nu\rho}^{\!~~~~~\sigma} = - (J_\alpha)_\rho^{\!~~\sigma}
R_{\mu\nu}^{~~~\alpha} - e^\sigma_{\!~~A a} e_{\rho B}^{\!~~~~a}
R_{\mu\nu}^{~~~AB},
\end{equation*}
where $R_{\mu\nu}^{~~~\alpha} = \frac{1}{2} i (\sigma^\alpha)_{ab}
R_{\mu\nu}^{~~~ab}$ and $R_{\mu\nu}^{~~~AB}$ are the corresponding
$Sp(2)$ and $Sp(4)$ curvatures, respectively. Furthermore, it can be shown
that the Ricci tensor $R_{\mu\nu}$ is propotional to the trace of the
$Sp(4)$ curvature, $R_{\mu\nu} = - \epsilon_{AB} R_{\mu\nu}^{~~~AB} =
\hbox{$\frac{1}{16}$} g_{\mu\nu} R \neq 0$, i.e., the quaternionic
K\"ahler manifold is Einstein --- in contrast to the hyper--K\"ahler
eight--fold, which is Ricci--flat ---, and that the $Sp(2)$ curvature
is proportional to the complex structures,
$R_{\mu\nu}^{~~~\alpha} = \hbox{$\frac{1}{192}$} R (J^\alpha)_{\mu\nu}$
\cite{10}.

Since the reduction $SO(8) \rightarrow Sp(4) \otimes Sp(2)$ admits three
parallel spinors $\zeta_\alpha$ the complex structures can be represented
also via
\begin{equation*}
(J^\alpha)_{\mu\nu} = \epsilon^{\alpha\beta\gamma} \zeta^T_\beta
\gamma_{[\mu} \gamma_{\nu]} \zeta_\gamma,
\qquad
\gamma_\mu = e_\mu^{\!~~A a} \gamma_{A a},
\end{equation*}
where $\gamma_{A a}$ are the $Sp(4) \otimes Sp(2)$ gamma matrices.

After having specified the data of a quaternionic K\"ahler manifold let us
now turn to the formulation of the Q--theory on that manifold.
As usual, in order to couple a flat space gauge invariant theory to a
curved background one has to covariantize the action via
$\delta_{mn} \rightarrow g_{\mu\nu}$,
$d^4z\, d^4\bar{z} \rightarrow d^4z\, d^4\bar{z}\, \sqrt{g}$ and
$D_{A a} \rightarrow \hat{D}_{A a} = e^\mu_{~A a} \nabla_\mu +
[ A_{A a}, ~\cdot~ ]$, where $\hat{D}_{A a}$ denotes the gauge and metric
covariant derivative. For (\ref{2.15}) this yields
\begin{align}
\label{3.6}
S^{(N_T = 3)} = \int d^4z\, d^4\bar{z}\, \sqrt{g}\, {\rm tr} \Bigr\{&
\hbox{$\frac{1}{6}$} \Omega_{ABCD} F^{AB}_{\!~~~~ab} F^{CD ab} +
\hbox{$\frac{1}{6}$} \epsilon_{AB} F^{AB}_{\!~~~~ab}
\epsilon_{CD} F^{CD ab}
\nonumber
\\
& - 2 \chi_{AB} \hat{D}^{A a} \psi^B_{\!~~a} +
2 \eta^{ab} \hat{D}^A_{\!~~a} \psi_{A b} -
2 \bar{\phi} \{ \psi^{A a}, \psi_{A a} \}
\nonumber
\\
& - \hbox{$\frac{1}{2}$} \phi \{ \chi^{AB}, \chi_{AB} \} -
\phi \{ \eta^{ab}, \eta_{ab} \} - 2 D^{A a} \bar{\phi} D_{A a} \phi -
2 [ \bar{\phi}, \phi ]^2 \Bigr\}.
\end{align}
Thus, the action (\ref{3.6}) is invariant under the scalar
supersymmetries (\ref{2.9}) if we identify the corresponding
symmetry parameters $\zeta^{ab}$, via $\zeta^{ab} = i
(\sigma_\alpha)^{ab} \zeta^\alpha$, with the parallel spinors
$\zeta^\alpha$. This finishes our construction of the Q--theory.
As pointed out in the introduction such a theory could arise
naturally as wrapped Euclidean Dirichlet $7$--brane on a
quaternionic K\"ahler manifold in string theory.\\
\\

\noindent {\large{\bf Acknowledgement}} We are grateful to M. Blau
for an interesting and useful discussion.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{99}

\bibitem{1} M. Blau and G. Thompson,
            {\it Phys. Lett.} {\bf B 415} (1997) 242
\bibitem{2} C. Vafa and E. Witten,
            {\it Nucl. Phys.} {\bf B 341} (1994) 77
\bibitem{3} I. Baulieu, H. Kanno and I. Singer,
            {\it Commun. Math. Phys.} {\bf 194} (1998);
\bibitem{4} B. S. Acharya, M. O'Loughlin and B. Spence,
            {\it Nucl. Phys.} {\bf B 503} (1997) 657
\bibitem{5} L. Baulieu, A. Losev and N. Nekrasov,
            {\it Nucl. Phys.} {\bf B 522} (1998) 82;
            L. Baulieu and P. West,
            {\it Phys. Lett.} {\bf B 436} (1998) 97;
            B. S. Acharya, J. M. Figueroa-O'Farrill,
            M. O'Loughlin and B. Spence,
            {\it Nucl. Phys.} {\bf B 514} (1998) 583;
            J. M. Figueroa-O'Farrill, A. Imaanpur and J. McCarthy,
            {\it Phys. Lett.} {\bf B 419} (1998) 167
\bibitem{6} E. Witten,
            {\it Nucl. Phys.} {\bf B 460} (1996) 335
\bibitem{7} M. Bershadsky, V. Sadov and C. Vafa,
            {\it Nucl. Phys.} {\bf B 463} (1996) 420
\bibitem{8} L. Baulieu, H. Kanno and I. M. Singer,
            {\it Cohomological Yang-Mills Theory in Eight Dimensions},
            Talk at APCTP Winter School on Dualities in String Theory,
            (Sokcho, Korea), 1997, hep-th/9705127
\bibitem{9} R. S. Ward,
            {\it Nucl. Phys.} {\bf B 236} (1984) 381
\bibitem{10} E. Bergshoeff, S. Cucu, T. de Wit, J. Gheerardyn,
             R. Halbersma, S. Vandoren and A. Van Proeyen,
             {\it JHEP } {\bf 0210} (2002) 045
             %{\it Superconformal $N = 2$, $D = 5$ matter with and without
             %actions}, hep-th/0205230
\bibitem{11} V. Oproiu,
             {\it An. st. Univ. Iasi} {\bf 23} (1977) 287
\bibitem{12} M. Obata,
             {\it Jap. J. Math.} {\bf 26} (1956) 43

\end{thebibliography}






\end{document}


