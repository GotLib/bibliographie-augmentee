# coding: utf-8

from config import Config
from dataset import CharBasedNERDataset
from model import CharacterBasedLSTMModel
from keras.models import model_from_json
from keras.callbacks import EarlyStopping, ModelCheckpoint


if __name__ == '__main__':

    sessions_count = 1

    config = Config()
    dataset = CharBasedNERDataset()
    model = CharacterBasedLSTMModel(config, dataset)
    
    #model.model.load_weights("../results-hydra-OK/weights_" + str(sessions_count-1) + ".hdf5", by_name=False)

    model.fit()
    #model.evaluate()

    model.model.save_weights(filepath="../results/weights_" + str(sessions_count) + ".hdf5")
    json_string = model.model.to_json()
    with open("../results/architecture.json", 'w') as f:
        f.write(json_string)

    print(model.predict_str("""[28] T. Salimans and D.P. Kingma. Weight normalization: A simple reparameterization to accelerate training of
deep neural networks. In NIPS 2016.
[29] A.M. Saxe, P.W. Koh, Z. Chen, M. Bhand, B. Suresh, and A.Y. Ng. On random weights and unsupervised
feature learning. In ICML 2011.
[30] S. Saxena and J. Verbeek. Convolutional neural fabrics. In NIPS 2016.
[31] J. Schmidhuber. Learning to control fast-weight memories: An alternative to dynamic recurrent networks.
In Neural Computation, volume 4, pages 131–139, 1992.
[32] S. Singh, D. Hoiem, and D. Forsyth. Swapout: Learning an ensemble of deep architectures. In NIPS 2016.
[33] J. Snoek, H. Larochelle, and R.P. Adams. Practical bayesian optimization of machine learning algorithms.
In NIPS 2012, .
[34] J. Snoek, O. Rippel, K. Swersky, R. Kiros, N. Satish, N. Sundaram, M.M.A. Patwary, Prabhat, and R. P.
Adams. Practical bayesian optimization of machine learning algorithms. In ICML 2015, .
[35] N. Srivastava, G.E. Hinton, A. Krizhevsky, I. Sutskever, and R. Salakhutdinov. Improving neural networks
by preventing co-adaptation of feature detectors. arXiv Preprint arXiv: 1207.0580, 2012.
[36] R. K. Srivastava, K. Greff, and J. Schmidhuber. Training very deep networks. In NIPS 2015.
[37] K.O. Stanley, D.B. D’Ambrosio, and J Gauci. A hypercube-based encoding for evolving large-scale neural
networks. In Artificial Life, 15(2):185-212, 2009.
[38] M. Suganuma, S. Shirakawa, and T. Nagao. A genetic programming approach to designing convolutional
neural network architectures. In GECCO 2017.
[39] C. Szegedy, S. Ioffe, and V. Vanhoucke. Inception-v4, inception-resnet and the impact of residual
connections on learning. arXiv Preprint arXiv: 1602.07261, 2016.
[40] D. Wierstra, F.J. Gomez, and J. Schmidhuber. Modeling systems with internal state using evolino. In
GECCO 2005.
[41] Z. Wu, S. Song, A. Khosla, F. Yu, L. Zhang, X. Tang, and J. Xiao. 3d shapenets: A deep representation for
volumetric shapes. In CVPR 2015.
[42] J. Yosinski, J. Clune, Y. Bengio, and H. Lipson. How transferable are features in deep neural networks? In
NIPS 2014.
[43] S. Zagoruyko and N. Komodakis. Wide residual networks. arXiv Preprint arXiv: 1605.07146, 2016.
[44] B. Zoph and Q. Le. Neural architecture search with reinforcement learning. In ICLR 2017.
10
"""))
    #print(model.predict_str('La nostalgie n’a rien d’un sentiment esthétique, elle n’est même pas liée non plus au souvenir d’un bonheur, on est nostalgique d’un endroit simplement parce qu’on y a vécu, bien ou mal peu importe, le passé est toujours beau, et le futur aussi d’ailleurs, il n’y a que le présent qui fasse mal, qu’on transporte avec soi comme un abcès de souffrance qui vous accompagne entre deux infinis de bonheur paisible'))
