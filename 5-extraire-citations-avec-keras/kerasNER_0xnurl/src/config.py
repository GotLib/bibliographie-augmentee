# coding: utf-8

class Config:
    sentence_max_length = 2000 #200
    input_dropout = 0.3
    output_dropout = 0.5
    recurrent_stack_depth = 3 #5
    batch_size = 8 #32
    max_epochs = 10
    learning_rate = 0.003 #0.001
    embed_size = 16 #256
    num_lstm_units = 64 #128
    early_stopping = 2





