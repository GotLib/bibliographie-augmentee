# coding: utf-8

import numpy as np
import pandas as pd
from tensorflow import one_hot
from alphabet import CharBasedNERAlphabet

TEXT_PATH = "../data/refs-samples-1ter.csv" # "~/Tensorflow/BA/0xnurl_keras_character_based_ner/data/refs-samples-1ter.csv"
TRAIN_PATH = "../data/refs-with-charlabels.csv" # "~/Tensorflow/BA/0xnurl_keras_character_based_ner/data/refs-with-charlabels.csv"

ELMO_VOCAB = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24, 'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35, 'A': 36, 'B': 37, 'C': 38, 'D': 39, 'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49, 'O': 50, 'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59, 'Y': 60, 'Z': 61, '!': 62, '"': 63, '#': 64, '$': 65, '%': 66, '&': 67, "'": 68, '(': 69, ')': 70, '*': 71, '+': 72, ',': 73, '-': 74, '.': 75, '/': 76, ':': 77, ';': 78, '<': 79, '=': 80, '>': 81, '?': 82, '@': 83, '[': 84, '\\': 85, ']': 86, '^': 87, '_': 88, '`': 89, '{': 90, '|': 91, '}': 92, '~': 93, ' ': 101, '\t': 102, '\n': 100, '\r': 103, '\x0b': 98, '\x0c': 99}
ELMO_REVERSE_VOCAB = {49: '0', 50: '1', 51: '2', 52: '3', 53: '4', 54: '5', 55: '6', 56: '7', 57: '8', 58: '9', 98: 'a', 99: 'b', 100: 'c', 101: 'd', 102: 'e', 103: 'f', 104: 'g', 105: 'h', 106: 'i', 107: 'j', 108: 'k', 109: 'l', 110: 'm', 111: 'n', 112: 'o', 113: 'p', 114: 'q', 115: 'r', 116: 's', 117: 't', 118: 'u', 119: 'v', 120: 'w', 121: 'x', 122: 'y', 123: 'z', 66: 'A', 67: 'B', 68: 'C', 69: 'D', 70: 'E', 71: 'F', 72: 'G', 73: 'H', 74: 'I', 75: 'J', 76: 'K', 77: 'L', 78: 'M', 79: 'N', 80: 'O', 81: 'P', 82: 'Q', 83: 'R', 84: 'S', 85: 'T', 86: 'U', 87: 'V', 88: 'W', 89: 'X', 90: 'Y', 91: 'Z', 39: '&', 127: '~', 35: '"', 93: '\\', 40: "'", 41: '(', 46: '-', 97: '`', 96: '_', 95: '^', 65: '@', 42: ')', 92: '[', 94: ']', 124: '{', 126: '}', 62: '=', 43: '*', 48: '/', 44: '+', 36: '#', 64: '?', 45: ',', 47: '.', 60: ';', 59: ':', 34: '!', 61: '<', 63: '>', 125: '|', 195: '°', 11: '\n', 10: '\t', 33: ' '}

class CharBasedNERDataset:
    NULL_LABEL = '0'
    BASE_LABELS = [] #[NULL_LABEL]

    def __init__(self):
        self.texts = self.get_texts()
        self.alphabet = CharBasedNERAlphabet(self.texts)
        self.labels = self.BASE_LABELS + self.get_labels()
        self.num_labels = len(self.labels)
        self.num_to_label = {0 : 'O', 1 : 'A', 2 : 'T', 3 : 'J', 4 : 'D'}
        self.label_to_num = {'O' : 0, 'A' : 1, 'T' : 2, 'J' : 3, 'D' : 4}
        self.init_mappings()

    def get_texts(self):
        """ Implement with own data source. """
        df = pd.read_csv(TEXT_PATH)
        texts = ""
        for i in range(df.shape[0]):
            texts += df.loc[i]['refs']
        return texts

    def get_x_y(self, sentence_maxlen, dataset_name='all'):
        """ Implement with own data source.

        :param sentence_maxlen: maximum number of characters per sample
        :param dataset_name: 'all', 'train', 'dev' or 'test'
        :return: Tuple (x, y)
                x: Array of shape (batch_size, sentence_maxlen). Entries in dimension 1 are alphabet indices, index 0 is the padding symbol
                y: Array of shape (batch_size, sentence_maxlen). Index 0 is the null label
        """
        df = pd.read_csv(TRAIN_PATH)
        df['charlabel'] = df.iloc[:]['charlabel'].apply(lambda x: self.label_to_num[x])
        df['char'] = df.iloc[:]['char'].apply(lambda x: self.alphabet.get_char_index(x))

        n_batches = df.values.shape[0] // sentence_maxlen
        v = df.values[:n_batches*sentence_maxlen, :] 
        batches = v.reshape((n_batches, sentence_maxlen, 2))
        X = batches[:, :, 0].reshape((n_batches, sentence_maxlen))
        Y = batches[:, :, 1].reshape((n_batches, sentence_maxlen))
        
        # change each label in y to a num-labels-length one-hot vector :
        y = np.zeros((n_batches, sentence_maxlen, len(self.labels)))
        for b in range(y.shape[0]):
            for c in range(y.shape[1]):
                y[b, c, Y[b, c]] = 1

        print("shapes: "+str(X.shape) + ',' + str(y.shape))
        return X, y                   


    def get_x_y_elmo(self, sentence_maxlen, dataset_name='all'):
        df = pd.read_csv(TRAIN_PATH)
        df['charlabel'] = df.iloc[:]['charlabel'].apply(lambda x: self.label_to_num[x])
        try:
            df['char'] = df.iloc[:]['char'].apply(lambda x: self.alphabet.char_to_num[x])
        except:
            print("!!erreur")

        n_batches = df.values.shape[0] // sentence_maxlen
        v = df.values[:n_batches*sentence_maxlen, :] 
        batches = v.reshape((n_batches, sentence_maxlen, 2))
        X = batches[:, :, 0]
        Y = batches[:, :, 1].reshape((n_batches, sentence_maxlen))
        
        # replace each index-of-char in X by its vector representation (here with ELMO pretrained embeddings):
        x = np.zeros((X.shape[0], X.shape[1], self.alphabet.embeddings_size))
        for b in range(X.shape[0]):
            for c in range(X.shape[1]):
                try:
                    #print(ELMO_VOCAB[X[b, c]])
                    char_embed = self.alphabet.elmo_embeddings[ELMO_VOCAB[self.alphabet.num_to_char[X[b, c]]]]
                    for e in range(self.alphabet.embeddings_size):
                        x[b, c, e] = char_embed[e]
                except:
                    if self.alphabet.num_to_char[X[b, c]] == '<PAD>': # (ie X[b, c] == 0)
                        x[b, c] = np.zeros((self.alphabet.embeddings_size))
                    else:
                        x[b, c] = np.random.normal(scale=0.5, size=self.alphabet.embeddings_size)

        # change each label in y to a num-labels-length one-hot vector :
        y = np.zeros((n_batches, sentence_maxlen, len(self.labels)))
        for b in range(y.shape[0]):
            for c in range(y.shape[1]):
                y[b, c, Y[b, c]] = 1

        print("shapes: "+str(X.shape) + ',' + str(y.shape))
        return x, y                     

    def get_x_y_generator(self, sentence_maxlen, dataset_name='all'):
        """ Implement with own data source.

        :return: Generator object that yields tuples (x, y), same as in get_x_y()
        """
        raise NotImplementedError

    def get_labels(self):
        """ Implement with own data source.

        :return: List of labels (classes) to predict, e.g. 'PER', 'LOC', not including the null label '0'.
        """
        df = pd.read_csv(TRAIN_PATH)
        labels = set(self.BASE_LABELS)
        for l in df.loc[:]['charlabel']:
            labels |= set(l)
        return sorted(list(labels))

    def str_to_x(self, s, maxlen):
        x = np.zeros(maxlen)
        for c, char in enumerate(s[:maxlen]):
            x[c] = self.alphabet.get_char_index(char)
        return x.reshape((-1, maxlen))

    def x_to_str(self, x):
        return [[self.alphabet.num_to_char[i] for i in row] for row in x]

    def y_to_labels(self, y):
        Y = []
        for row in y:
            Y.append([self.num_to_label[np.argmax(one_hot_labels)] for one_hot_labels in row])
        return Y

    def init_mappings(self):
        for num, label in enumerate(self.labels):
            #self.num_to_label[num] = label
            #self.label_to_num[label] = num
            pass

if __name__ == '__main__':
    Test = CharBasedNERDataset()
    texts = Test.get_texts()
    #print(texts)
    labels = Test.get_labels()
    print(labels)

    X, y = Test.get_x_y(sentence_maxlen=20)
    print(X)
    print(X.shape)
    print(y)
    print(y.shape)




