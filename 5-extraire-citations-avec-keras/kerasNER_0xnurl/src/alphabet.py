# coding: utf-8

import h5py
import pandas as pd

TEXT_PATH = "../data/refs-samples-1ter.csv" # "~/Tensorflow/BA/0xnurl_keras_character_based_ner/data/refs-samples-1ter.csv"
TRAIN_PATH = "../data/refs-with-charlabels.csv" # "~/Tensorflow/BA/0xnurl_keras_character_based_ner/data/refs-with-charlabels.csv"

ELMO_LOC = "../data/elmo_2x4096_512_2048cnn_2xhighway_weights_PubMed_only.hdf5"
ELMO_VOCAB = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24, 'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35, 'A': 36, 'B': 37, 'C': 38, 'D': 39, 'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49, 'O': 50, 'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59, 'Y': 60, 'Z': 61, '!': 62, '"': 63, '#': 64, '$': 65, '%': 66, '&': 67, "'": 68, '(': 69, ')': 70, '*': 71, '+': 72, ',': 73, '-': 74, '.': 75, '/': 76, ':': 77, ';': 78, '<': 79, '=': 80, '>': 81, '?': 82, '@': 83, '[': 84, '\\': 85, ']': 86, '^': 87, '_': 88, '`': 89, '{': 90, '|': 91, '}': 92, '~': 93, ' ': 101, '\t': 102, '\n': 100, '\r': 103, '\x0b': 98, '\x0c': 99}
ELMO_REVERSE_VOCAB = {49: '0', 50: '1', 51: '2', 52: '3', 53: '4', 54: '5', 55: '6', 56: '7', 57: '8', 58: '9', 98: 'a', 99: 'b', 100: 'c', 101: 'd', 102: 'e', 103: 'f', 104: 'g', 105: 'h', 106: 'i', 107: 'j', 108: 'k', 109: 'l', 110: 'm', 111: 'n', 112: 'o', 113: 'p', 114: 'q', 115: 'r', 116: 's', 117: 't', 118: 'u', 119: 'v', 120: 'w', 121: 'x', 122: 'y', 123: 'z', 66: 'A', 67: 'B', 68: 'C', 69: 'D', 70: 'E', 71: 'F', 72: 'G', 73: 'H', 74: 'I', 75: 'J', 76: 'K', 77: 'L', 78: 'M', 79: 'N', 80: 'O', 81: 'P', 82: 'Q', 83: 'R', 84: 'S', 85: 'T', 86: 'U', 87: 'V', 88: 'W', 89: 'X', 90: 'Y', 91: 'Z', 39: '&', 127: '~', 35: '"', 93: '\\', 40: "'", 41: '(', 46: '-', 97: '`', 96: '_', 95: '^', 65: '@', 42: ')', 92: '[', 94: ']', 124: '{', 126: '}', 62: '=', 43: '*', 48: '/', 44: '+', 36: '#', 64: '?', 45: ',', 47: '.', 60: ';', 59: ':', 34: '!', 61: '<', 63: '>', 125: '|', 195: '°', 11: '\n', 10: '\t', 33: ' '}



class CharBasedNERAlphabet:
    PADDING_SYMBOL = '<PAD>'
    UNKNOWN_CHAR_SYMBOL = '<UNK>'
    BASE_ALPHABET = [PADDING_SYMBOL, UNKNOWN_CHAR_SYMBOL]

    def __init__(self, texts):
        self.characters = self.BASE_ALPHABET + self.get_alphabet_from_texts(texts)
        self.char_to_num = None
        self.num_to_char = None
        self.elmo_embeddings = None
        self.embeddings_size = None
        self.init_mappings()

    def get_alphabet_from_texts(self, texts):
        all_characters = set()

        for t in texts:
            text_characters = set(t)
            all_characters |= text_characters

        alphabet = sorted(list(all_characters))
        return alphabet

    def get_elmo_from_hdf5(self, loc=ELMO_LOC):
        f = h5py.File(loc)
        elmo_embeddings = f['char_embed'][:, :]
        return elmo_embeddings

    def init_mappings(self):
        self.char_to_num = self.get_char_to_num()
        self.num_to_char = self.get_num_to_char()
        self.elmo_embeddings = self.get_elmo_from_hdf5()
        self.embeddings_size = self.elmo_embeddings.shape[-1]

    
    def char_to_vec(self, char):
        elmo_idx = REVERSE_ELMO_VOCAB[ELMO_VOCAB[char]]
        return self.elmo_embedings[elmo_idx]


    def get_char_to_num(self):
        return {char: c for c, char in enumerate(self.characters)}

    def get_num_to_char(self):
        return {c: char for c, char in enumerate(self.characters)}

    def get_char_index(self, char):
        try:
            num = self.char_to_num[char]
        except KeyError:
            num = self.char_to_num[self.UNKNOWN_CHAR_SYMBOL]
        return num

    def __str__(self):
        return str(self.characters)

    def __len__(self):
        return len(self.characters)

    def __iter__(self):
        return self.characters.__iter__()


if __name__=='__main__':

    df = pd.read_csv(TEXT_PATH)
    texts = "" 
    for t in df.iloc[:]['refs']:
        texts += t
    al = CharBasedNERAlphabet(texts=texts)
    print(al.elmo_embeddings.shape)
    print(al.embeddings_size)
    print(al.characters)
    print(len(al.characters))
    print(al.char_to_num)
    print(len(al.char_to_num.keys()))
    print(al.num_to_char)
    print(len(al.num_to_char.keys()))



