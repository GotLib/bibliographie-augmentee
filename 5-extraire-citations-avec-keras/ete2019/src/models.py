# encoding: utf8

from keras.layers import Input, Flatten, Concatenate, Reshape, Permute, Dense, Conv1D, Conv2D, TimeDistributed, MaxPooling1D, GlobalMaxPooling1D, Dropout, SpatialDropout1D
from keras.models import Model, load_model
from keras.optimizers import Nadam
from keras.utils import plot_model

from sklearn.preprocessing._encoders import OneHotEncoder as onehot_encoder

import h5py
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
from time import time
from pprint import pprint


alph = set("abcdefghijklmnopqrstuvwxyz0123456789,;.!?:_@#$%&/*+-=<>()[]{}ABCDEFGHIJKLMNLOPQRSTUVWXYZ ")

#>> General settings :
MODELS = "./../models/"
SAVEPATH = "./../models/saved_models/model2/"
LOGS = "../models/training_logs/trial_2/"

DATAPATH = "../data/"
FORMATTED_DATA = DATAPATH + "onehot_texts/1_separated-samples-labels/"
TRAIN_SAMPLES_PATH = FORMATTED_DATA + "train/samples/"
TRAIN_LABELS_PATH = FORMATTED_DATA + "train/labels/"
TEST_SAMPLES_PATH = FORMATTED_DATA + "test/samples/"
TEST_LABELS_PATH = FORMATTED_DATA + "test/labels/"

NB_CLASSES = 5
#<<


#>> Model0 settings :
NB_EPOCHS = 1
DILATION_RATE = 1
NB_STEPS = 256 # nb of characters in one sample from a text
BATCH_SIZE = 16
ALPH_SIZE = 88 # not including the unknown character, which is embedded as a full-zeros ALPH_SIZE-length vector 
               # (moreover unknown chars are ignored and not included in the dataset)
WINDOW_RADIUS = 10
DROPOUT_RATE = 0.2

INPUT_SHAPE = (BATCH_SIZE, NB_STEPS, ALPH_SIZE)
#<<


def make_model():

    inputs = Input(batch_shape=INPUT_SHAPE) 

    layer1 = Dense(units = 16, activation = 'relu')(inputs)
    print(layer1.__dict__['_keras_shape'])

    drop1 = Dropout(DROPOUT_RATE, seed=10)(layer1)

    layer1bis = Dense(units = 16, activation = 'relu')(drop1)
    print(layer1bis.__dict__['_keras_shape'])
 
    drop1bis = Dropout(DROPOUT_RATE, seed=10)(layer1bis)

    layer2 = Conv1D(filters = 16, kernel_size = 2*WINDOW_RADIUS+1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(drop1bis)
    print(layer2.__dict__['_keras_shape'])

    drop2 = Dropout(DROPOUT_RATE, seed=10)(layer2)

    layer3 = Conv1D(filters = 16, kernel_size = 2*WINDOW_RADIUS+1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(drop2)
    print(layer3.__dict__['_keras_shape'])   

    drop3 = Dropout(DROPOUT_RATE, seed=10)(layer3)

    layer4 = Conv1D(filters = 16, kernel_size = 2*WINDOW_RADIUS+1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(drop3)
    print(layer4.__dict__['_keras_shape'])

    drop4 = Dropout(DROPOUT_RATE, seed=10)(layer4)

    layer5 = Conv1D(filters = ALPH_SIZE, kernel_size = 1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(drop4)
    print(layer5.__dict__['_keras_shape'])

    drop5 = Dropout(DROPOUT_RATE, seed=10)(layer5)

    layer5 = Reshape((NB_STEPS, ALPH_SIZE, 1))(drop5)
    print(layer5.__dict__['_keras_shape'])

    layer_end = Conv2D(filters = 2*WINDOW_RADIUS+1, kernel_size = (2*WINDOW_RADIUS+1, 1), padding='same', activation = 'softmax')(layer5) 
    print(layer_end.__dict__['_keras_shape'])

    layer_end = Permute((1, 3, 2))(layer_end)
    print(layer_end.__dict__['_keras_shape'])

    output = layer_end
    inputs = inputs

    model = Model(inputs=inputs, output=output)
    model.compile(optimizer='nadam',#'rmsprop',
                  loss='mean_squared_error',
                  metrics=['accuracy', 'mae'])

    print(model.summary())

    return model


def gen_test_from_h5_article(sample_article):
    """ Iterates on the lines of onehot_article (2d_array of size nb_chars*89) to get, for each
    char, the lines of its neighbors in a radius of window_radius
    """
    voisinages = []
    for i in range(sample_article.shape[0]):
        if i < WINDOW_RADIUS or sample_article.shape[0] - i <= WINDOW_RADIUS:
            continue
        else:
            voisinages.append(sample_article[i-WINDOW_RADIUS:i+WINDOW_RADIUS+1])
        if i == 10:
            print(voisinages[0].shape)

    labels_article = np.stack(voisinages)

    labels_article = np.vstack((np.zeros((WINDOW_RADIUS, 2*WINDOW_RADIUS+1, ALPH_SIZE)), labels_article, np.zeros((WINDOW_RADIUS, 2*WINDOW_RADIUS+1, ALPH_SIZE))))
            # il faut corriger le décalage intoduit entre train et test 
            # (on a enlevé les chars tout au début et à la fin
            # dans chaque article_test car on n'a pris que les voisinages 
            # complets, et on n'a pas mis de padding)

    print(labels_article.shape)

    return labels_article


def gen_test_set(h5_samples_path=TRAIN_SAMPLES_PATH, h5_labels_path=TRAIN_LABELS_PATH):
    
    h5_samples = h5py.File(h5_samples_path+"onehot_articles.h5", 'r')
    h5_labels = h5py.File(h5_labels_path+"onehot_articles_"+str(WINDOW_RADIUS)+"-radius-window.h5", 'w')

    for article_name in h5_samples.keys():
        sample_article = h5_samples[article_name][:, :]
        h5_labels[article_name] = gen_test_from_h5_article(sample_article)

    h5_samples.close()
    h5_labels.close()


def train_model(model):

    h5_samples = h5py.File(TRAIN_SAMPLES_PATH+"onehot_articles.h5", 'r')
    h5_labels = h5py.File(TRAIN_LABELS_PATH+"onehot_articles_"+str(WINDOW_RADIUS)+"-radius-window.h5", 'r')

    articles_names = [key for key in h5_samples.keys()]
    #print(articles_names)

    errors = 0
    for epoch in range(7, NB_EPOCHS+7):
        print("\n## EPOCH " + str(epoch) + '\n')
        save = 1
        for count, article_name in enumerate(articles_names):
            print("\n# epoch " + str(epoch) +" : article " + str(count+1))
            sample_article = h5_samples[article_name][:, :]
            labels_article = h5_labels[article_name][:, :, :]
                                                                                                 
            assert sample_article.shape[0] == labels_article.shape[0]

            print(sample_article.shape)

            mul = sample_article.shape[0] % (BATCH_SIZE * NB_STEPS) # (batch_size*nb_steps)=8*1024
            sample_article = sample_article[:-mul, :].reshape((-1, NB_STEPS, ALPH_SIZE)) # now sample_article.shape is a multiple of batch_size
            labels_article = labels_article[:-mul, :, :].reshape((-1, NB_STEPS, 2*WINDOW_RADIUS+1, ALPH_SIZE))

            assert sample_article.shape[0] == labels_article.shape[0]
            
            print(mul)
            print(sample_article.shape)

            if True:
                history = model.fit(sample_article, labels_article, epochs=1, batch_size=BATCH_SIZE)
                model.save(SAVEPATH+'model2_epoch_'+str(epoch)+'_save'+str(save)+'.h5')
                save += 1
            else:
                errors += 1

            print('\n')

    model.save(SAVEPATH+'model2_save_last_2'+'.h5')

    h5_samples.close()
    h5_labels.close()

    #print('articles that caused errors: ' + str(errors))


if __name__=='__main__':

    t1 = time()

    alph = "abcdefghijklmnopqrstuvwxyz0123456789\-,;.!?:/\|_@#$%&*+-=<>()[]{}ABCDEFGHIJKLMNLOPQRSTUVWXYZ"
    oh = onehot_encoder(handle_unknown='ignore')
    oh.fit(np.array(list(alph)).reshape(-1, 1))

    ##gen_test_set()

    #model = load_model(SAVEPATH+'model2_save_last_1.h5') 
    #model = make_model()
    ##plot_model(model)

    #train_model(model)

    gen_test_set(h5_samples_path=TEST_SAMPLES_PATH, h5_labels_path=TEST_LABELS_PATH)

    t2 = time()
    print("Durée d'exécution (mins) : " + str((t2-t1)//60))



