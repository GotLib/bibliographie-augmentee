# encoding: utf8

from keras.layers import Input, Flatten, Concatenate, Reshape, Permute, Dense, Conv1D, Conv2D, TimeDistributed, MaxPooling1D, GlobalMaxPooling1D, Dropout, SpatialDropout1D
from keras.models import Model, load_model
from keras.optimizers import Nadam
from keras.utils import plot_model

from sklearn.preprocessing._encoders import OneHotEncoder as onehot_encoder

import h5py
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
from time import time
from pprint import pprint

#>> General settings :
MODELS = "./../models/"
SAVEPATH = "./../models/saved_models/"
LOGS = "../models/training_logs/trial_0/"

#DATAPATH = "./../data/onehot_texts/onehot_articles.h5"
#NEW_H5 =  "./../data/onehot_texts/onehot_articles_with_4-radius_test.h5"
DATAPATH = "./../data/onehot_texts/onehot_articles_with_4-radius_test.h5"
TRAIN = DATAPATH + ""
TEST  = DATAPATH + ""

NB_CLASSES = 5
#<<


def test_model0(path=SAVEPATH+'model0_save_last.h5'):
    pass


if __name__=='__main__':
    
    test_model0()


