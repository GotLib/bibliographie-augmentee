#encoding: utf-8

import pandas as pd
import re
import os
import numpy as np
import h5py
from pprint import pprint
from time import time
from sklearn.preprocessing._encoders import OneHotEncoder as onehot_encoder
import pickle


alph = set("abcdefghijklmnopqrstuvwxyz0123456789,;.!?:_@#$%&/*+-=<>()[]{}ABCDEFGHIJKLMNLOPQRSTUVWXYZ ")


DATAPATH = "../data/"

KDD_TXT = DATAPATH + "plain-text_2003-KDD-cup/"
KDD_TEX = DATAPATH + "tex-files_2003-KDD-cup/"
PDF_TXT = DATAPATH + "texts-from-the-pdf-samples/"
PDF_TXT_BIS = DATAPATH + "test-samples-txt/"

FORMATTED_DATA = DATAPATH + "onehot_texts/1_separated-samples-labels/"
TRAIN_SAMPLES_PATH = FORMATTED_DATA + "train/samples/"
TRAIN_LABELS_PATH = FORMATTED_DATA + "train/labels/"
TEST_SAMPLES_PATH = FORMATTED_DATA + "test/samples/"
TEST_LABELS_PATH = FORMATTED_DATA + "test/labels/"


STATS = {}
TOTAL = 0

LOOKUP = {}
REVESE_LOOKUP = {}



def texts2onehot(h5_path=TRAIN_SAMPLES_PATH, h5_name="onehot_articles.h5", pdf_path=PDF_TXT):
    
    oh = onehot_encoder(handle_unknown='ignore')
    oh.fit(np.array(list(alph)).reshape(-1, 1))

    h5_file = h5py.File(h5_path+h5_name, 'w')

    print('oh.categories_')
    print(oh.categories_)

    for name in os.listdir(pdf_path):
        with open(pdf_path+name, 'r') as f:
            txt = f.read()
        h5_file[name] = oh.transform(np.array(list(txt)).reshape(-1, 1)).toarray()
        print(name)

    h5_file.close()


if __name__=='__main__':

    t1 = time()
    
    print('alph:')
    print(alph)

    print('len(alph):')
    print(len(alph))

    texts2onehot(h5_path=TEST_SAMPLES_PATH, h5_name="onehot_articles.h5", pdf_path=PDF_TXT_BIS)

    t2 = time()
    print('exec time (mins) : ' + str((t2-t1)/60.))


    
