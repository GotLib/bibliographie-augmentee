#encoding: utf-8

import pandas as pd
import re
import os
import numpy as np
import h5py
from pprint import pprint
from time import time
from sklearn.preprocessing._encoders import OneHotEncoder as onehot_encoder
import pickle


DATAPATH = "../data/"
KDD_TXT = DATAPATH + "plain-text_2003-KDD-cup/"
KDD_TEX = DATAPATH + "tex-files_2003-KDD-cup/"
PDF_TXT = DATAPATH + "texts-from-the-pdf-samples/"

FORMATTED_DATA = DATAPATH + "onehot_texts/1_separated-trains-tests/"
TRAIN_PATH = FORMATTED_DATA + "train/"
TEST_PATH = FORMATTED_DATA + "test/"

STATS = {}
TOTAL = 0

LOOKUP = {}
REVESE_LOOKUP = {}

alph = "abcdefghijklmnopqrstuvwxyz0123456789\-,;.!?:/\|_@#$%&*+-=<>()[]{}ABCDEFGHIJKLMNLOPQRSTUVWXYZ"


def get_lookup_and_stats(path=PDF_TXT):
    stats = {}
    for name in os.listdir(path):
        with open(path+name, 'r') as doc:
            txt = doc.read()
        for char in txt:
            if char in stats:
                stats[char] += 1
            else:
                stats[char] = 1

    lookup = dict(zip(stats.keys(), range(len(stats.keys()))))

    for char, occ in stats.items():
        if occ < 97 or occ in [101, 109, 235]:
            del lookup[char]

    reverse_lookup = dict(zip(range(len(lookup.keys())), lookup.keys()))

    total = 0
    for occurences in stats.values():
        total += occurences

    return stats, lookup, reverse_lookup, total

def make_model1():

    inputs = Input(batch_shape=INPUT_SHAPE)

    layer1 = Dense(units = 16, activation = 'relu')(inputs)
    print(layer1.__dict__['_keras_shape'])

    layer1bis = Dense(units = 16, activation = 'relu')(layer1)
    print(layer1bis.__dict__['_keras_shape'])


    layer2 = Conv1D(filters = 16, kernel_size = 2*WINDOW_RADIUS+1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(layer1bis)
    print(layer2.__dict__['_keras_shape'])

    layer3 = Conv1D(filters = 16, kernel_size = 2*WINDOW_RADIUS+1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(layer2)
    print(layer3.__dict__['_keras_shape'])

    layer4 = Conv1D(filters = 16, kernel_size = 2*WINDOW_RADIUS+1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(layer3)
    print(layer4.__dict__['_keras_shape'])

    layer5 = Conv1D(filters = ALPH_SIZE, kernel_size = 1, dilation_rate = DILATION_RATE, padding = 'same', activation = 'relu')(layer4)
    print(layer5.__dict__['_keras_shape'])

    layer5 = Reshape((NB_STEPS, ALPH_SIZE, 1))(layer5)
    print(layer5.__dict__['_keras_shape'])

    layer_end = Conv2D(filters = 2*WINDOW_RADIUS+1, kernel_size = (2*WINDOW_RADIUS+1, 1), padding='same', activation = 'softmax')(layer5)
    print(layer_end.__dict__['_keras_shape'])

    layer_end = Permute((1, 3, 2))(layer_end)
    print(layer_end.__dict__['_keras_shape'])

    output = layer_end
    inputs = inputs

    model = Model(inputs=inputs, output=output)
    model.compile(optimizer='nadam',#'rmsprop',
                  loss='mean_squared_error',
                  metrics=['accuracy', 'mae'])

    print(model.summary())

    return model


if __name__=='__main__':

    stats, lookup, reverse_lookup, total = get_lookup_and_stats()

    for char, occ in sorted(stats.items(), key=lambda item:item[1]):
        print(str(char) + ' : ' + str(occ))

    print(stats)
    print('\n')
    print(total)

    print('\n\n')

    print(lookup)
    print('\n')
    print(reverse_lookup)
    print('\n')
    for char, idx in sorted(lookup.items(), key=lambda item:item[1]):
        print(char + ' : ' + str(idx))

    filtered_stats = {}
    for char, idx in sorted(lookup.items(), key=lambda item:item[1]):
        filtered_stats[char] = stats[char]

    pprint(filtered_stats)




