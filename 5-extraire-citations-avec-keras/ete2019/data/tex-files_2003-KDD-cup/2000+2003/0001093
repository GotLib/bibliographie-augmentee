
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%    INSTITUTE OF PHYSICS PUBLISHING                                   %%%
%%%                                                                      %%%
%%%   `Preparing an article for publication in an Institute of Physics   %%%
%%%    Publishing journal using LaTeX'                                   %%%
%%%                                                                      %%%
%%%    LaTeX source code `ioplau.tex' used to generate `author           %%%
%%%    guidelines', the documentation explaining and demonstrating use   %%%
%%%    of the Institute of Physics Publishing LaTeX preprint macro file  %%%
%%%    `ioplppt.sty'.                                                    %%%
%%%                                                                      %%%
%%%    `ioplau.tex' itself uses LaTeX with `ioplppt.sty'                 %%%
%%%    an optional file iopfts.sty can also be loaded to allow the       %%%
%%%    use of the AMS extension fonts msam and msbm with the IOP         %%%
%%%    preprint style.                                                   %%%
%%%                                                                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%
%%% First we have a character check
%%%
%%% ! exclamation mark    " double quote  
%%% # hash                ` opening quote (grave)
%%% & ampersand           ' closing quote (acute)
%%% $ dollar              % percent       
%%% ( open parenthesis    ) close paren.  
%%% - hyphen              = equals sign
%%% | vertical bar        ~ tilde         
%%% @ at sign             _ underscore
%%% { open curly brace    } close curly   
%%% [ open square         ] close square bracket
%%% + plus sign           ; semi-colon    
%%% * asterisk            : colon
%%% < open angle bracket  > close angle   
%%% , comma               . full stop
%%% ? question mark       / forward slash 
%%% \ backslash           ^ circumflex
%%%
%%% ABCDEFGHIJKLMNOPQRSTUVWXYZ 
%%% abcdefghijklmnopqrstuvwxyz 
%%% 1234567890
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
\documentstyle{ioplppt}                % use this for journal style
%\documentstyle[12pt]{ioplppt}          % use this for preprint style
\def\cqo#1#2{\cos[q\Omega^{\rm #1}_S(b_f,R^s_{#2})]}
%
\begin{document}
\title{ Scales of String Theory \footnote{Talk given at the Strings 99
  Conference, Potsdam, July 1999. To be published in {\em
  Class. Quantum Grav.} {\bf 17} (2000) 1 . }}
\author{C P Bachas}

\address{Laboratoire de Physique Th{\'e}orique de l' Ecole Normale
  Sup{\'e}rieure, \\ 
 24 rue Lhomond, F-75231 Paris Cedex 05, France\\
{\it bachas@physique.ens.fr}}


\begin{abstract}
I review the arguments in favor of/against the traditional hypothesis that
the Planck,  string and  compactification  scales are all within a
couple of orders of magnitude from each other. I explain how  the
extreme  brane-world scenario, with TeV type I scale and
two large (near millimetric) transverse dimensions, creates conditions
analogous  to
those of the energy desert and is thus naturally singled out. I
comment on the puzzle of gauge coupling unification in this context.
\end{abstract}

%
% Uncomment out if preprint format required
%
%\pacs{00.00, 20.00, 42.10}
%\maketitle


\section{Introduction}

    String/M theory \cite{GSW,pro} has a single dimensionful
parameter and a large number of dynamical moduli. The expectation
values of these moduli determine the semiclassical properties of the
vacuum, and in particular the masses of
 its Kaluza-Klein and Regge excitations. 
It is hoped that some still unknown
dynamical mechanism  ultimately selects  the (unique?) vacuum in
which we live,  thereby fixing also the scales of the new
physics. A more down-to-earth approach is to rely on arguments of
theoretical plausibility,  on direct experimental limits 
and on indirect experimental hints. In the wake of the 
`duality revolution' there has been a revival of interest on the
question of scales [3 -- 17]  which I will try to  review in the  present
talk. \footnote{The audio version and transparencies can be found in
 \cite{pro}.}


  The conventional (and conservative) hypothesis  
is that the string, compactification and Planck
scales lie  all to within two or three orders 
of magnitude from each other, and are hence far 
beyond direct experimental reach. The non-gravitational 
physics at lower energies is believed to be
described by a renormalizable supersymmetric
quantum field theory (SQFT), which  must include   the Minimal 
Supersymmetric Standard Model (MSSM). Faith in this hypothesis
has been bolstered by the following well-known 
facts~:  (i) Softly broken SQFTs
 can indeed  be extrapolated consistently to near-Planckian
energies without destabilizing the electroweak  scale~;
(ii) the minimal (or `desert') unification assumption  is in remarkable  
agreement with some of the measured low-energy  parameters of our
world, and (iii) the hypothesis is almost automatic within
the weakly-coupled heterotic string. 


The negative side of the coin is that this story  is at best incomplete~:
coupling the broken SQFT to (super)gravity gives rise to vacuum
instabilities, including an unacceptably-large cosmological term. 
As has been  (re)appreciated  in recent years, our knowledge of
the gravitational interaction is in fact also very limited  in another way: 
Einstein's classical theory has
not been tested  experimentally at distances shorter than
 the macroscopic (millimeter or
$10^{-3}$ eV) regime \cite{gravi}. 
 Since this  is also the observational 
upper bound  on the  cosmological constant \cite{Wein}, it is very tempting to
speculate that a resolution of the associated
 long-standing puzzle will require a drastic modification of gravity at
such  scales. Recent proposals of a higher-dimensional
gravity \cite{ADD,RaS} 
do not  seem to point to a resolution  of
the problem \cite{BMQ},  while  more drastic modifications like  millimeter-sized  
fundamental strings \cite{mms} are  hard to accomodate in a consistent
theoretical framework.  The problem of gravitational (in)stability
is, in any case,  an important motivation for 
pursuing alternatives to the conventional  heterotic compactification scheme. 

Two other  important motivations  are
(i) that  compactification  scenaria which  confine gauge interactions
on a  brane  arise, as we have learned,  very naturally
in  controllable corners
of the  moduli space of M-theory, and  (ii) that they can bring string and
Kaluza-Klein physics closer to experiment. Furthermore, the extreme brane-world
scenario  with TeV type-I string scale \cite{Ly,AADD}
and a (near-millimetric~?)  transverse
space of dimension two, is  singled out as I will explain  in
this talk. The logarithmic sensitivity on the transverse size
\cite{B,AB} sets a stage similar to that of the conventional energy desert~:  it
makes hierarchies of scales very  natural, and 
allows reliable (model-independent)
calculations of the effective parameters in the brane theory.
 The apparent unification
of the measured low-energy gauge couplings, on the other hand,  has not found 
a  convincing explanation outside the traditional energy-desert
scenario  yet. Since this is arguably \cite{Dimo} the only clear quantitative
hint for  physics  beyond the Standard Model, 
I will start and end my 
 discussion from this point.



\section{Hints of Supersymmetric Unification}

  The well-known observation \cite{GQW,SU,LP}
  is that if one extrapolates the three
gauge couplings of $SU(3)\times SU(2)\times U(1)$ using the
$\beta$-funcions of the MSSM they meet at a scale $M_U\simeq  2\times 
10^{16}$ GeV. This is consistent with the one-loop formulae
\begin{equation}
{\alpha_i}^{-1}(\mu ) = {\alpha_U}^{-1} + b_i \; {\rm log} {\mu \over M_U}
+ \Delta_i
\label{eq:unif}
\end{equation}
where $b_i$ are the $\beta$-function coefficients, and 
$\alpha_U$ is the fine structure constant at $M_U$. 
The threshold corrections 
$\Delta_i$ parametrize  our
ignorance of  the details
of the theory at the unification scale,  and of the details of
supersymmetry breaking. 
Assuming that the  $\Delta_i$ are negligible, 
 and treating   $M_U$ and
$\alpha_U$ as input parameters, we have one
prediction which is verified by LEP data  at the level of a few
percent. 

In the minimal 
heterotic unification there is furthermore  one extra relation \cite{G}
between the  input  parameters and the experimental  value of $M_{\rm Planck}$. 
It  implies  $M_U \simeq 5\times 10^{17}$ GeV, which on the (appropriate)
logarithmic scale is a second successful prediction of the theory 
at the level again of a few percent \cite{Ka,Die}. Put
differently: there was  no a priori reason why the extrapolated low-energy
couplings should not have met, if at all, 
 at say $10^{35}$ GeV~! 
It is important here to realize that it is the very existence of the
perturbative  desert which  renders the above
  predictions meaningful and robust. 
Threshold effects make  a few-percent correction to differences 
of couplings only because  the logarithm  in equations (1)
is very large. It would be impossible  to ignore the nitty-gritty
details of the model if the unification  scale were
 say instead at $100$ TeV.


Similar arguments can be given for the mass matrices of quarks and
leptons. Minimal assumptions 
(such as discrete symmetries and Higgs-field  content) 
 determine  boundary conditions for  Yukawa
couplings, which can then be evolved with the equations of
 the renormalization group. 
The  agreement with low-energy data is  suggestive  \cite{LP},  though
less  compelling than for the gauge-coupling constants.  
Finally, I should mention that the existence of the superheavy  scale $M_U$
is indirectly supported by two other independent 
pieces of  data~: the very long lifetime of  the proton \cite{CZ} 
and the extraordinary  smallness of neutrino masses \cite{AF}.  



\section{ Weakly-coupled Heterotic String}

    Let me proceed next to the theoretical arguments 
that make  the SQFT hypothesis quasi-automatic 
within the weakly-coupled heterotic string. 
Both the graviton and the  perturbative gauge  bosons
live in this case in the ten-dimensional bulk, and interact 
through the sphere diagram at tree-level. The four-dimensional  Yang-Mills and
Einstein actions therefore read
\begin{equation}
{\cal L}_{\rm gauge} \sim {(rM_h)^6\over g_h^2}\; {\rm tr} F^2 \ \ 
{\rm and}  \ \ \ 
{\cal L}_{\rm grav} \sim  {r^6 M_h^8\over g_h^2}\; {\cal R}\  ,
\end{equation}
where  $M_h$ and $g_h$ are the heterotic string  scale and string
coupling constant, while $r$ is the typical compactification 
radius. By virtue of  T-duality $r$ can be always taken 
 greater than, or equal to, the string length.
  From the coefficients of these actions  we can read the 
four-dimensional  gauge coupling and Planck mass with the result
\begin{equation}
\alpha_U \sim { g_h^2\over (rM_h)^6}\ , 
\label{eq:gin}
\end{equation}
and 
\begin{equation}
M_h^2 \sim \; \alpha_U M_{\rm Planck}^2 \ .
\label{eq:gins}
\end{equation} 
Factors of $2$'s and $\pi$'s in these relations are irrelevant for our
arguments and have been dropped. I have also dropped the
 level of the corresponding  Kac-Moody algebra which  for all practical purposes is
an integer of order one.

 Assume now  that (i) the bare gauge coupling is of order one, and
(ii)  the heterotic theory stays  weakly coupled ($g_h\leq
1$). Then  the universal  relation (\ref{eq:gins}), which we have
already encountered in the previous section,  implies that the  string
scale is tied automatically  to the Planck mass.  Relation 
(\ref{eq:gin}) on the other hand 
also  implies that $rM_h$ cannot be much larger than one.
Since it cannot be smaller than one by T-duality,  
it is necessarily of order one.
Thus  there is little  leeway for abandoning the conventional  
scenario within  the context of the weakly-coupled 
heterotic string.

 
 We can of course try to relax one
of the above  two assumptions~: either (i) allow $g_h$ to be
hierarchically large and hope that the gauge sector still stays under control
in  some special models \cite{A}, or (ii) 
let $\alpha_U$  be hierarchically
small   and hope that the Standard Model
gauge couplings will be  driven to their
measured values by the large threshold corrections of a higher
dimensional field theory \cite{Ba}.\footnote{Higher-dimensional
 thresholds have been
discussed also in other  contexts, see for example \cite{thr}.} 

 Such  exotic possibilities have been 
motivated in the past  by the search  for classical vacua
with broken low-energy  supersymmetry.  Known
`mechanisms' of continuous  supersymmetry
 breaking  \cite{SS,mag} indeed 
tie the breaking scale to the size of some internal dimensions.  
Classical supersymmetry restoration can   be furthermore  argued to be
singular, in string theory,  on  general grounds \cite{DS}. 
It was thus suggested early on \cite{ABLT} that one
 (or more) radii of  inverse size  at the  TeV 
would be required   if the `observed' supersymmetry breaking 
in nature were  classical.
Tree-level breaking, on the other hand, 
is at best an assumption
 of convenience -- there is no reason in principle 
why the breaking in nature should not
have a non-perturbative origin. 
Furthermore the classical mechanisms  
have  not  so far lead to  new insights on the crucial problems of vacuum 
selection and stability. Thus, there seemed  to be little theoretical
motivation for abandoning the conventional 
compactification scheme,  and
its successful unification predictions,  
in  heterotic string theory. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Brane World and Type-I  Theory}

   One of the important developments
of the `duality revolution' has been the 
realization that various branes -- Dirichlet branes \cite{Po,D}, or their dual
heterotic fivebranes \cite{Wz} and  Horava-Witten walls \cite{HW} --
can trap non-abelian gauge interactions in their
worldvolumes. This has placed on  a firmer basis  an old idea
 \cite{RS} according to which 
 we might be  living  on a  brane 
embedded in a  higher-dimensional world. 
The idea arises  naturally  in
compactifications of type I theory \cite{DLP},  which typically involve
collections of orientifold planes and D-branes. 
I will from now on restrict my discussion to this context, 
because in it the `brane-world' scenario admits a fully
perturbative string description. All   other interesting
possibilities (see for example  \cite{RaS,AP,BO})  
involve some type of non-perturbative dynamics and are a priori harder
to control. 


  In type I string theory  the graviton (a closed-string state) 
lives  in the ten-dimensional bulk, while open-string vector bosons 
are in general  localized on  lower-dimensional D-branes. Furthermore 
while closed strings interact
to leading order via the sphere diagram,  open strings 
 interact via  the disk diagram which is of higher
order in the genus expansion.
The four-dimensional Planck mass and Yang-Mills couplings
therefore take the form
 \begin{equation}
\alpha_U \; \sim \; {g_{\rm I}\over ({\tilde r} M_{\rm I})^{6-n}}  \ \  , \ \ \ 
M_{\rm Planck}^2 \sim { r^{n}{\tilde r}^{\; 6-n} M_{\rm I}^8\over g_{\rm I}^2} ,
\label{eq:typei}
\end{equation}
where $r$ is the typical radius
 of the $n$  compact dimensions transverse to the 
brane,   $\tilde r$ the typical radius
 of the remaining (6-$n$) compact longitudinal
dimensions, $M_{\rm I}$ the type-I string scale and $g_{\rm I}$
the string coupling constant. By appropriate T-dualities we can again
 ensure  that both $r$ and $\tilde r$ are
greater than or equal to the fundamental  string scale. T-dualities
 change $n$ and  may take us either to
Ia  or to Ib theory (also called I or I',  respectively) but  I will
not make a distinction between these two.


It follows from these formulae that
  (i) there is no universal relation between
$M_{\rm Planck}$, $\alpha_U$ and $M_{\rm I}$ anymore, and (ii) tree-level
gauge couplings corresponding to different sets
of D-branes have radius-dependent  ratios and 
need not  unify at all.    
Thus  type-I string theory is
much more flexible (and  less predictive)  than its
 heterotic counterpart.
The fundamental string scale,  $M_{\rm I}$,  in particular  is 
 a free parameter, even if one insists  that
 $\alpha_U$  be  kept fixed and
of order one,  and that the string theory be weakly coupled.
This added flexibility can be used to `remove'  the
order-of magnitude discrepancy
between the apparent unification and string scales of the heterotic theory
\cite{W}, 
to lower $M_{\rm I}$ to an intemediate scale  \cite{Kar,BIQ} or
even all the way down to its 
experimentally-allowed limit of order the TeV  \cite{Ly,AADD}.
Keeping for instance $g_{\rm I}$,
$\alpha_U$ and $({\tilde r} M_{\rm I})$ fixed and of order one, 
leads to the condition
\begin{equation}
r^n \sim {M_{\rm Planck}^2/ M_{\rm I}^{2+n}} .
\label{eq:mm}
\end{equation}
A TeV string scale would then require from  $n=2$  millimetric 
to  $n=6$  fermi-size dimensions transverse to our brane world. The
relative weakness of gravity is  in this picture 
attributed to  the transverse spreading of gravitational flux.


\section{Experimental Bounds}

  What has brought the brane-world idea 
 into focus \cite{ADD} was the realization
that it cannot be a priori ruled out by the existing data,  even in
 the most  extreme case of `TeV-ish'  string scale and 
millimmeter-size  transverse dimensions. 
Gravity is hard to test  at submillimeter  distances because of the
 large  background of residual
 electromagnetic interactions. The ratio for instance of the  Van der
 Waals to Newtonian force 
 between two hydrogen atoms a distance $d$ apart is \cite{ADD}
\begin{equation}
{F_{\rm VdW}\over F_{\rm grav}} \sim 
\left( {1 { mm}\over d} \right)^5 \ .
\end{equation}
At    $d= 10\mu m$ Newton's force is thus ten orders of magnitude
weaker than Van der Waals~!  
As a result the present-day data  \cite{gravi} allows  practically any
modification of Newton's law,  as long as it is  of
comparable strength at and screened beyond the millimeter range. 
This has been 
 appreciated in the past in the context of gravitational
axions \cite{axi}, and similar bounds hold for light  string moduli \cite{mo} 
or  extra
 Kaluza-Klein dimensions \cite{CKM,ADD,KSf}.


       Besides mesoscopic gravity experiments, there are two other 
types of direct experimental limits 
one should worry about~: 
those coming from  precision observables  of the
 Standard Model, and those coming from various  exotic processes. 
Precision  tests of the SM and
compositeness bounds cannot  rule out in a model-independent way any
new physics above the TeV-ish  scale. 
Bounds for instance from LEP data on
four-fermion operators, or bounds on dimension-five operators
contributing to the  $g-2$ 
of the electron/muon are  safe, as long as  the characteristic scale of
the new physics is a few TeV \cite{ADD}. 
Proton decay and other exotic processes could  of course
rule out large classes of low-scale models. There exist however plausible
 suppression mechanisms,  such as  bulk U(1) gauge symmetries  which are 
 spontaneously-broken at some distant brane \cite{ST,proton} and
look like approximate global symmetries  in our brane world. 
One type  of model-independent 
exotic process is  graviton emission  in the bulk, which could be seen
as missing-energy events in  collider experiments 
\cite{missing}. The process  is however
suppressed by the four-dimensional Newton constant  at low
 energies,  and only becomes
 appreciable (as one should expect) 
near string  scale  where quantum gravity effects
 are strong. 

None of these (or other) phenomenological considerations  seems a
 priori fatal to the brane-world scenario, even in its  extreme
 realization. Put together they will, however, probably  make  
 realistic type-I model building  a
 very strenuous  exercise indeed.

\vfil\eject


\section{The Trasverse Desert}

  Although  $M_{\rm I}$ could lie anywhere between the Planck mass and
  the  TeV , lowering it to the latter scale has two advantages~: (i)
  it brings string physics
 within the reach of future acceleretor experiments, and (ii) it is 
a natural starting point for
 discussing the problem  of the gauge hierarchy, which becomes now a
  question in the infrared \cite{ADD,AB}.  In a certain
 sense  this extreme choice is antipodal to the energy-desert scenario:
although the MSSM is a stable  renormalizable field theory, we are shrinking 
its range of validity  to  one order of magnitude at most!
 Nevertheless, as I will now argue, these two scenaria
 share many common features  when the number of large
 dimensions transverse to our brane is exactly  two \cite{B,AB}. 


   The key feature of the SQFT  hypothesis is that low-energy
   parameters receive large logarithmic corrections, which are
  effectively resummed  by the equations of the Renormalization Group. 
  The logarithmic sensitivity of parameters
  also generates naturally  hierarchies of
 scales, and has been  the key ingredient in all
  efforts to understand the origin of 
 the gauge hierarchy in the past \cite{N}. 
Consider now  the brane world scenario. The parameters of the
 effective brane lagrangian are dynamical open- and closed-string
 moduli with constant expectation values along the four non-compact
 space-time dimensions of our world. The closed-string 
moduli, $m_a$,  are bulk fields
 whose expectation values will generically vary as a function of the
 transverse coordinates $\xi$. They include the dilaton,
 twisted-sector massless scalars, the metric of the transverse space etc.
For weak  type-I string coupling these variations can be 
 described by a lagrangian of the form
 \begin{equation}
{\cal L}_{\rm bulk} + {\cal L}_{\rm source}
\sim \int d^n\xi \; \Bigl[ {1\over g_{\rm I}^2}
(\partial_\xi  m_a)^2 +  {1\over g_{\rm I}} \sum_s f_s(m_a) 
\delta(\xi-\xi_s)\Bigr]
.
\label{eq:bb}
\end{equation}
Here ${\cal L}_{\rm bulk}$ is a reduced supergravity Lagrangian 
while the sources are the  D-branes and orientifolds which are  localized
at  positions $\xi_s$ in the transverse space. The couplings
$f_s(m_a)$ may vary  from source to source -- they can for instance
depend on open-string moduli --  and are subject to global
consistency  conditions. What is  important is that they are
 weak  in the type-I limit, leading to weak field variations, 
 \begin{equation}
m_a(\xi) = m_a^0 + g_{\rm I}\;  m_a^1(\xi) + \cdots  , 
\label{eq:bb}
\end{equation} 
with  $m_a^0$  the (constant) 
 average value, $m_a^1(\xi)$  given by a sum of Green's functions, and
so on.  For $n=2$  transverse dimensions the leading variation
 grows  logarithmically with the size,  $r$, 
 of  the transverse space.  Since our
Standard Model parameters will be  a function of the moduli evaluated at
the position of our brane world, they will have  logarithmic
sensitivity on $M_{\rm Planck}$, very much like  the (relevant)
parameters of  a  supersymmetric renormalizable QFT. Similar
sensitivity may  occur even if $n>2$ ,  as long as  some of the `bulk'
moduli propagate  in only two extra large dimensions.
 

The bulk  supergravity Lagrangian  receives 
both stringy and higher-genus corrections, but these involve 
higher derivatives of fields, and should therefore be 
negligible for moduli  varying logarithmically over distances
much larger than the string scale.
 The source functions, $f_s(m_a)$, will also
be  generically  modified by such corrections -- the D-branes have
indeed  string-scale thickness when probed by the supergravity fields
\cite{probe}. Such  source modifications can, however, be absorbed
into boundary conditions for the  supergravity equations,    
at the special marked points $\xi_s$. The situation thus looks (at
 least superficially) analogous to that prevailing under 
 the SQFT hypothesis~: large  corrections to effective low-energy couplings
 can be in both cases  resummed by  differential equations
 subject to appropriate boundary conditions. 
Furthermore `threshold corrections'
parametrizing our ignorance of the detailed physics at distant branes
-- the analog of physics near the unification scale -- 
have a small effect on the relative evolution of parameters,
provided  the transverse two-dimensional space is
sufficiently large. Clearly $n=2$ is
critical: for exactly one  transverse dimension bulk fields 
vary linearly in space and one expects to hit strong-coupling
singularities before $r$ can  grow very large,
while for $n>2$  the 
dynamics on our  brane  completely decouples from the dynamics elsewhere
in the bulk.

  
 
 
\section{The Puzzle of Unification}

 The logarithmic sensitivity  of brane parameters with
$r$ seems a natural setting for   generating  scale hierarchies dynamically,
as in the case of  renormalizable SQFT.  Gauge dynamics   on
a  given brane, for example, could  become strong as  the transverse
space  expands to an exponentially  large size, thereby inducing  gaugino
condensation and  supersymmetry breaking. The problems of vacuum
selection and stability are, to be sure, still with us -- the
situation is, in this respect,  neither better nor worse than 
in the traditional compactification scenario. 

 The apparent unification of the (MS)SM  gauge  couplings, on the
other hand, has not yet found a convincing `explanation' in this (or
any alternative) context. The basic  ingredients are, nevertheless,
present \cite{B} (see also \cite{I,ADM} for related ideas): (i) the
logarithmic variation of bulk fields in the large transverse space can
give equally-robust predictions for brane parameters, as
renormalization group  running
over the energy desert, (ii) some bulk fields (like twisted scalar
moduli) have non-universal couplings to gauge fields that live on the
same set of D-branes \cite{AFIV} -- they 
 can thus split their gauge couplings apart
without separating them in transverse space, and (iii) the boundary
condition imposing  unification  at high energy could be replaced
by a boundary  condition that these non-universally coupled bulk moduli
vanish on some far-distant brane (for instance because of a
non-perturbative superpotential). The main difficulty of  these ideas
is to explain   why 
the coefficients of the logarithmic real-space evolutions  should
have  the same (ratio of?) differences as the MSSM beta
functions. This is clearly required if we are to `understand' the actual
 values of the  SM gauge couplings, as measured at LEP energies. 


\vspace{.75cm}

{\bf Aknowledgements}

 I thank the organizers for the invitation to speak, and for
organizing a marvellous strings 99  conference.
 This work was partially supported
by the TMR contract ERBFMRX-CT96-0090.

\vspace{.75cm}

\vfil\eject

\section*{References}
\begin{thebibliography}{99}


\bibitem{GSW} M.B. Green, J.H. Schwarz and E. Witten, {\em Superstring
Theory} (Cambridge U. Press, 1987);\\ 
 J. Polchinski, {\em String Theory}
(Cambridge U. Press, 1998)~;\\   D. Olive and P. West eds.,
{\em Duality and Supersymmetric Theories} (Cambridge U. Press, 1998);  

\bibitem{pro} http~://www.asi-potsdam.mpg.de/cgi-bin/viewit.cgi .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  low E models


\bibitem{W} E. Witten, {\it Nucl. Phys.} {\bf B471} (1996)
 {135}, hep-th/9602070.

\bibitem{Ly} J.D. Lykken, {\it Phys. Rev.}
 {\bf D54} (1996) {3693}, hep-th/9603133. 

\bibitem{CKM} E. Carceres,  V.S. Kaplunovsky and  I.M. Mandelberg,
 {\it  Nucl.Phys.} {\bf  B493}  (1997) 73,  hep-th/9606036.

\bibitem{ADD} N. Arkani-Hamed, S. Dimopoulos and  G.  Dvali,
     {\it Phys. Lett.} {\bf B429} (1998) {263}, hep-ph/9803315; and
 {\it Phys. Rev.} {\bf D59} (1999){086004}, hep-ph/9807344.

\bibitem{AADD}  I. Antoniadis, N. Arkani-Hamed, S. Dimopoulos and G. Dvali,
 {\it Phys. Lett.} {\bf B436} (1998) {257}, hep-ph/9804398.

\bibitem{ST} G. Shiu and  S.-H. H. Tye,
{\it Phys. Rev.} {\bf D58} (1998) {106007}, hep-th/9805157~.

\bibitem{KT} Z. Kakushadze and S.-H. Henry Tye, 
{\it  Nucl.Phys.} {\bf  B548}  (1999) 180, hep-th/9809147.

\bibitem{B} C. Bachas, {\it JHEP} {\bf  9811} (1998) {023}, hep-ph/9807415~;
 and in {\em Fundamental Interactions~: From Symmetries to Black
 Holes}, J.-M. Fr{\`e}re {\em et al} eds., Brussels 1999, hep-th/9907023.

\bibitem{AB} I. Antoniadis and C. Bachas, {\it Phys. Lett.}
{\bf B450} (1999) {83}, hep-th/9812093. 

\bibitem{Kar}  K. Benakli, {\it Phys.Rev.} {\bf D60}
 (1999) 104002, hep-ph/9809582.

\bibitem{BIQ} C. Burgess, L.E. Ibanez and F. Quevedo, 
{\it Phys.Lett.} {\bf  B447} (1999) 257, hep-ph/9810535.


\bibitem{RaS} L. Randall and R. Sundrum, 
{\it Phys.Rev.Lett.} {\bf  83} (1999) 3370, hep-ph/9905221~; 
 and {\it Phys.Rev.Lett.} {\bf  83} (1999) 4690,  hep-th/9906064.

\bibitem{AP} I. Antoniadis and  B. Pioline,
 {\it Nucl. Phys.} {\bf B550} (1999) 41, hep-th/9902055. 


\bibitem{BO} K. Benakli and Y. Oz, hep-th/9910090. 

\bibitem{revs} See also the talks by L. Ibanez, and by 
 I. Antoniadis and A. Sagnotti, in this volume.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% cosmo constant

\bibitem{gravi} J.C. Long, H.W. Chan and J.C. Price, 
{\it  Nucl.Phys.} {\bf  B539}  (1999) 23, hep-ph/9805217. 

\bibitem{Wein} S. Weinberg, {\it  Rev. Mod. Phys.} {\bf 61} (1989) 1.

\bibitem{BMQ} See however E. Verlinde, in this volume;\\
 C.P. Burgess, R.C. Myers and  F. Quevedo, 
hep-th/9911164.

\bibitem{mms} R. Sundrum, {\it  JHEP} {\bf  9907} (1999) 001,
 hep-ph/9708329;\\  T. Tomaras, private communication.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% GUTS

\bibitem{Dimo} See for instance  S. Dimopoulos, 
in {\em History of Original Ideas and Basic Discoveries in  
                  Particle Physics} ( Erice 1994), hep-ph/9412297.

\bibitem{GQW} H. Georgi, H. Quinn and S. Weinberg, 
{\it Phys. Rev. Lett.} {\bf 33} (1974) {451}.

\bibitem{SU} S. Dimopoulos, S. Raby and F. Wilczek, 
{\it Phys. Rev.} {\bf D24} (1981) 1681;\\
 S. Dimopoulos and H. Georgi,
{\it Nucl. Phys.} {\bf B193} (1981) 150;\\
L. Ibanez and G.G. Ross, {\it Phys. Lett.}
{\bf B106} (1981) 439;\\
 N. Sakai, {\em Z. Phys.} {\bf C11} (1981) 153.

\bibitem{LP} More recent discussions are given in
  P. Langacker and N. Polonsky,
{\it Phys.Rev.} {\bf  D47} (1993) 4028, hep-ph/9210235~; and 
{\it Phys.Rev.} {\bf  D49} (1994) 1454, 
hep-ph/9306205~;\\
 M. Carena, S. Pokorski and C.E.M. Wagner, {\it
  Nucl. Phys.} {\bf B406} (1993) 59, hep-ph/9303202~;\\
 for fermion
  masses see also M. Carena, S. Dimopoulos, S. Raby and  C.E.M. Wagner, {\it
Phys. Rev.} {\bf D52} (1995) 4133, and references therein. 

\bibitem{G} P. Ginsparg, {\it Phys. Lett.}
{\bf B197} (1987){139}.

\bibitem{Ka} V. Kaplunovsky,
{\it Nucl. Phys.} {\bf B307} (1988) {145};\\
 L. Dixon, V. Kaplunovsky and J. Louis,
{\it Nucl. Phys.} {\bf B329} (1990) {27}.

\bibitem{Die}  For a review see 
K. Dienes, {\em Phys. Rep.} {\bf 287} (1997) 447,  and references therein. 

\bibitem{CZ} For a review see G. Costa and  F. Zwirner, {\it
 Riv.Nuovo Cim.} {\bf 9} (1986) 1. 

\bibitem{AF} For a review see G. Altarelli and  F. Feruglio,
hep-ph/9905536, to appear in {\em Physics  Reports}.

%%%%%%%%%%%%%%%%%%%%%

\bibitem{A} I. Antoniadis, {\it Phys. Lett.}
{\bf B246} (1990) {377}.

\bibitem{Ba} C. Bachas, unpublished (1995).

\bibitem{thr} T.R. Taylor and G. Veneziano,
 {\it Phys. Lett.} {\bf B212} (1988) 147~;\\
K. R. Dienes, E. Dudas and  T. Gherghetta, 
{\it Phys. Lett.}
{\bf B436} (1998) {55}, hep-ph/9803466~;\\
Z. Kakushadze and  T.R. Taylor, hep-th/9905137. 


\bibitem{SS} J. Scherk and J.H. Schwarz, {\it Phys. Lett.}
{\bf B82} (1979) {60}; and
{\it Nucl. Phys.} {\bf B153} (1979) {61}~;\\
R. Rohm, {\it Nucl. Phys.} {\bf B237} (1984) {553}~;\\
 S. Ferrara, C. Kounnas and M. Porrati, 
{\it Nucl. Phys.} {\bf B304} (1988) {500}~;\\ 
S. Ferrara, C. Kounnas, M. Porrati and  F.
Zwirner, {\it  Nucl.Phys.} {\bf B318} (1989) 75.  
 

\bibitem{mag} C. Bachas, hep-th/9503030~; and in {\em Topics in QFT},
  D. Tchrakian ed. (World Scientific,  1995), hep-th/9509067~;\\
M. Berkooz, M.R. Douglas and  R.G. Leigh, {\it Nucl.Phys.}
 {\bf  B480} (1996) 265,
hep-th/9606139.


\bibitem{DS} M. Dine and N. Seiberg, {\it Nucl. Phys.} {\bf B301} (1988)
{357};\\
  T. Banks and L.J. Dixon, {\it Nucl. Phys.} {\bf B307} (1988)
{93}.

\bibitem{ABLT} I. Antoniadis, C. Bachas, D. Lewellen
and T. Tomaras, {\it Phys. Lett.}
{\bf B207} (1988){441}.


%%%%%%%%%%%%%%%%%%% branes


\bibitem{Po} J. Polchinski, {\it Phys. Rev. Lett.}
{\bf 75} (1995) {4724}.

\bibitem{D} For reviews see J. Polchinski, TASI96,   hep-th/9611050~;\\
 W. Taylor, in proceedings of the  summer school on 
{\em Particle Physics and
                    Cosmology} (Trieste, 1997), 
hep-th/9801182~;\\
 C. Bachas, in {\em Gauge Theories, Applied Supersymmetry
                  and Quantum Gravity II}, A. Sevrin {\it et al} eds.
                  (Imperial College Press,  1996), 
 hep-th/9701019~; and in last ref. [1], hep-th/9806199. 


\bibitem{Wz} E. Witten, {\it Nucl.Phys.} {\bf  B460} (1996) 541,
hep-th/9511030. 


\bibitem{HW} P. Horava and  E. Witten,  {\it  Nucl.Phys.} {\bf  B460}
  (1996) 506,
hep-th/9510209~; and  {\it Nucl.Phys.} {\bf  B475} (1996) 94,
hep-th/9603142.  

\bibitem{RS}
V. Rubakov and M. Shaposhnikov, {\it Phys. Lett.} {\bf B125} (1983)
136~;\\
 G.W.  Gibbons and D.L. Wiltshire, 
{\it Nucl. Phys.} {\bf B287} (1987) 717. 

\bibitem{DLP}  G. Pradisi and A. Sagnotti, {\it Phys. Lett.}
{\bf B216} (1989) {59};\\
 J. Dai, R.G. Leigh and J. Polchinski,
{\em Mod. Phys. Lett.} {\bf A4}  (1989) 2073;\\
 P. Horava, {\it Phys. Lett.}
{\bf B231} (1989) {251}~;\\
 M. Bianchi and A. Sagnotti, {\it
  Phys.Lett.} {\bf B247} (1990) 517. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\bibitem{axi}
V.A. Kuzmin, I. Tkachev, M. Shaposhnikov, {\it JETP Lett.} {\bf 36}
(1982) 59~;\\
 J.E. Moody and F. Wilczek, {\it Phys. Rev.}
 {\bf D30} (1984) {130}; \\ 
A. De Rujula, {\it Phys. Lett.}
{\bf B180} (1986) {213}.

\bibitem{mo}   T.R. Taylor and G. Veneziano, 
{\it Phys. Lett.} {\bf B213} (1988) {450}~; \\
J. Ellis, N.C. Tsamis and  M. Voloshin,
{\it Phys.Lett.} {\bf B194} (1987) 291~;\\
  S. Dimopoulos and
G. F. Giudice, {\it Phys.Lett.} {\bf B379}
 (1996) 105, hep-ph/9602350 and references therein.  

\bibitem{KSf} A. Kehagias and  K. Sfetsos, 
hep-ph/9905417.

\bibitem{proton} 
N. Arkani-Hamed and  S. Dimopoulos, hep-ph/9811353. 

\bibitem{missing} G.F. Giudice, R. Rattazzi and  J.D. Wells, 
{\it Nucl.Phys.} {\bf  B544} (1999) 3, 
hep-ph/9811291~;\\
 E.A. Mirabelli, M. Perelstein and  M.E. Peskin, 
{\it Phys.Rev.Lett.} {\bf  82} (1999) 2236,
hep-ph/9811337~;\\
 T. Han, J.D. Lykken, R.-J. Zhang, {\it Phys.Rev.}
{\bf  D59}  (1999) 105006,
hep-ph/9811350~;\\
 J.L. Hewett, {\it Phys.Rev.Lett.} {\bf  82}  (1999)
4765, hep-ph/9811356~;\\
 S. Nussinov and  R. Shrock, {\it Phys.Rev.}
{\bf  D59} (1999) 105002, 
hep-ph/9811323. 


%%%%%%%%%%%%%%%%%%%%%%%%

\bibitem{N} See for example
 H.P. Nilles, {\it Phys. Rep.} {\bf 110C} (1984) 1.

\bibitem{probe} A. Hashimoto and I. Klebanov, {\it
Nucl.Phys.Proc.Suppl.}
{\bf B55} (1997) 118, hep-th/9611214.

\bibitem{I} L. E. Ib{\'a}{\~n}ez, hep-ph/9905349. 

\bibitem{ADM} N. Arkani-Hamed, S. Dimopoulos and  J. March-Russell, 
hep-th/9908146. 


%%%%%%%%%%%%%%%%%%%%%

\bibitem{AFIV}
G. Aldazabal, A. Font, L. E. Ibanez and  G. Violero,
{\it Nucl. Phys.} {\bf B536} (1998) {29}~; \\
 L. E. Ibanez, R. Rabadan and  A. M. Uranga,
{\it Nucl. Phys.} {\bf B542} (1999) {112}~;\\ 
 I. Antoniadis, C. Bachas and E. Dudas, {\it Nucl. Phys.}{\bf  B560} (1999) 93,
hep-th/9906039.


%%%%%%%%%%%%%%%%%%%%


\end{thebibliography}
\end{document}








 
  
    


