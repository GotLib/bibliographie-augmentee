\documentstyle[12pt]{article}
\renewcommand{\baselinestretch}{1.1}
\newcommand{\na}{\nabla}  
\newcommand{\al}{\alpha}  
\newcommand{\ga}{\gamma}  
\newcommand{\de}{\delta}  
\def\be{\begin{equation}}
\def\ee{\end{equation}}
\def\bea{\begin{eqnarray}}
\def\eea{\end{eqnarray}}
\def\Ja{J_\mu(\phi)}
\def\Jb{J^\mu(\phi)}
\def\L{l_0}
\def\Na{N_\mu}
\def\Nb{N^\mu}
\def\ma{m_0}
\def\pa{\partial_\mu}
\def\pc{\square}
\def\pb{\partial^\mu}
\def\pd{\Box}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\topmargin -10 mm
\oddsidemargin 0 mm
\evensidemargin 0 mm
%\rightmargin 0 mm
%\Leftmargin 0 mm
\textwidth 160 mm
\textheight 230 mm
%\baselineskip .65 cm
\begin{document}
\begin{flushright}
\end{flushright}
\pagestyle{plain}
\begin{center}
\LARGE{\bf {Geometrization of the Quantum Effects}}\\
\end{center}
\begin{center}
\vspace{1.5cm}
{\Large{\bf H. Motavali$^{1,2}$}}\footnote{e-mail address: motaval@theory.ipm.ac.ir},~~
{\Large{\bf M. Golshani$^{2}$}}\footnote{e-mail address: golshani@ihcs.ac.ir}  \\  \vspace{0.5cm}
\small{$^{1}$ Department of Physics, Amirkabir University of Technology, 15875-4413, Tehran, Iran \\
$^{2}$ Institute for Studies in Theoretical Physics
and Mathematics, 19395-1795, Tehran, Iran}\\ 
\today 
\end{center}
\vspace{.5cm}
\small
\begin{abstract}
A variant of the divergence theory for vacuum-structure developed
previously${}^{1}$  is analyzed from the viewpoint of a conformally invariant gravitational
model. Invariance breaking is introduced by choosing a preferred
configuration 
of dynamical variables. In this way a particle interpretation is presented which leads 
to the geometrization of the quantum aspects of matter.
\end{abstract}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Vacuum-Structure can be sensitive to external and cosmological constraints which 
are applied to the vacuum. The effect of the external constraints on the vacuum
is analyzed by the violation of the Lorentz-invariance. 
The violation of this principle may be considered in curved space-time in
terms 
of two physical
concepts, the universal length $l_0$${}^{2,3}$ and a time-like vector field $N_\mu$.${}^{2,3,4}$ 
Here $l_0$ is used to distinguish between small and large distances, and $N_\mu$ serves as the four-velocity of preferred frame of reference, which  
is singled out by the violation of the Lorentz-invariance. 

In this paper we shall use the divergence theory developed in Ref. 1 to apply 
external constraints on the vacuum through the broken Lorentz invariance.
The effect of Lorentz-noninvariance on the structure of the vacuum may be 
allow a particle interprtation.\\
An undesirable feature of this model is that, it is not conformal
invariant.
To conformally symmetrize this model, we shall use an appropriate
conformal transformation which provides a dynamical interplay between a 
particle and the applied conformal factor. The resulting conformally invariant 
theory may be studied in different conformal frames, depending on the particular choice of the
local standard of length. It is shown that transition from one frame 
to another leads to the geometrazation of the quantum aspects of matter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Divergence theory}
We study the violation of Lorentz-invariance in vacuum by considering 
a dynamical coupling of a real scalar field $\phi$ with the
vector field $N_\mu$ by the introduction of 
the current
\begin{eqnarray}
{\Ja}=-\frac{1}{2}{\phi}\stackrel{\leftrightarrow}{\partial_\mu}\phi^{-1}.
\end{eqnarray}
One can easily check that
\begin{eqnarray}
{\Ja}{\Jb}=\phi^{-2}{\pa {\phi}}{\pb {\phi}}
\end{eqnarray}
and
\begin{eqnarray}
{\pa \Jb}=\phi^{-1}({\Box {\phi}}-\phi^{-1}\pa {\phi}\pb {\phi})
\end{eqnarray}
in which, $\Box=g_{\mu\nu}\partial^\mu\partial^\nu$ denotes the d'Alembertian associated with the Riemannian metric $g_{\mu\nu}$.

From the combination of relations (2) and (3) we obtain the identity
\begin{eqnarray}
{\Box {\phi}}+[-{\Ja}{\Jb}-\pa \Jb ] \phi=0. 
\end{eqnarray}
This equation is a consequence of the
definition (1) and not a dynamical law for $\phi$. However, 
one can construct several dynamical theories 
by specifying the source of the divergence $\pa \Jb$ in (4).

We study a theory in which the source of the divergence is specified as
\begin{eqnarray}
{\pa {\Jb}}=l_0^{-1}{\Na}{\Jb}.
\end{eqnarray}
Substituting this relation in (4) we get
\begin{eqnarray}
\Box \phi-[\Ja \Jb+l_0^{-1} \Na \Jb]\phi=0.
\end{eqnarray}

It is easy to show that under the duality transformation
\begin{eqnarray}
(\phi,\Na,g_{\mu\nu}) \rightarrow  (\phi^{-1},-\Na,{\cal{G}}_{\mu\nu})
\end{eqnarray}
where ${\cal{G}}_{\mu\nu}$ is given by
\begin{eqnarray}
{\cal{G}}_{\mu\nu}=g_{\mu \nu}+2\Na N_\nu
\nonumber
\end{eqnarray}
Eq. (6) is transformed to
\begin{eqnarray}
\Box \phi-[\Ja \Jb+l_0^{-1} \Na \Jb]\phi=-2\phi N_\mu \frac{d}{d \tau} J^\mu (\phi)
\nonumber
\end{eqnarray}
where we have used 
\begin{eqnarray}
N_\mu \partial^\mu=\frac{d}{d \tau} 
\nonumber
\end{eqnarray}
which is the defining relation for what we call the internal time-parameter $\tau$
associated with time-like vector field $N_\mu$. 

The last equation reduces to Eq. (6) in the idealized limit in which any conceivable dependence of 
$J_\mu (\phi)$ on the internal time-parameter $\tau$ is ignored. Using this assumption,
Eq. (6) becomes invariant under the duality transformation (7).
This duality transformation makes possible a transition from the configuration
$(\phi,\Na,g_{\mu\nu})$ on a Lorentzian domain (a domain with the metric $g_{\mu \nu}$ and signature 
$(-+++)$) to the configuration $(\phi^{-1},-\Na,{\cal{G}}_{\mu\nu})$
on an Euclidean domain (with the metric ${\cal{G}}_{\mu\nu}$ and
signature $(++++)$). Therefore the duality transformation connects equivalent
configurations of the vacuum with different signatures. 

To fix the causal structure of the space for our analysis, it is
necessary to combine the divergence law (5) with a duality breaking condition. On a
Lorentzian domain we adopt the time-asymmetric condition${}^5$ 
\begin{eqnarray}
{\pa {\Jb}}=l_0^{-1} {\Na}{\Jb}>0.
\end{eqnarray}

One can easily check that, under the duality transformation (7) the 
source of the divergence in (8) will reverse the sign, so, this condition 
can not be simultaneously satisfied for configurations
related by the duality transformation.
Therefore, the condition (8) implies an assertion about both a preferred arrow
of time which is determined by $N_\mu$, and a preferred
configuration of the scalar field $\phi$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vacuum-Condensation}
To characterize the physical realizability of the scalar field $\phi$, 
we adopt the preferred frames of reference to be 
those in which the vector field $N_\mu$ at each space-time point takes 
the form $(|g_{00}|^{1/2}, 0, 0 ,0)$.  
Then, one may combine the divergence law (5) with (1), getting
\begin{eqnarray}
{\pa ({\Jb}- l_0^{-1} {\Nb}\ln l_0\phi)}=0.
\nonumber            
\end{eqnarray}
A particular solution of this equation is given by
\begin{eqnarray}
\Ja=l_0^{-1} {\Na}{ \ln l_0\phi}+ \sqrt{ \frac{1}{6}R} M_\mu 
\end{eqnarray}
in which $R$ is a constant curvature and $M_\mu$ is a divergence-less  
vector field with $M_\mu M^\mu=1$. Then, using the time-like nature of $N_\mu$, namely $N_\mu N^\mu=-1$,  
one can rewrite the divergence law (5) as
\begin{eqnarray}
{\pa {\Jb}}=- l_0^{-2}{\ln l_0\phi}
\end{eqnarray}
where we have assumed the time-like vector field $N_\mu$ to be orthogonal to the vector field $M_\mu$. 

From a comparison between (8) and (10) one finds the following constraint on
the configuration of the scalar field $\phi$
\begin{eqnarray}
l_0\phi(x)<1.
\end{eqnarray}

Using (9) and (10), the field Eq. (4) takes the form
\begin{eqnarray}
\Box \phi+[l_0^{-2}(({\ln l_0\phi})^2+\ln l_0\phi)-\frac{1}{6}R]\phi=0.
\end{eqnarray}
This equation can be derived from a Lagrangian having the following potential
\begin{eqnarray}
V(\phi)=-\frac{1}{12}R\phi^2+ \frac{1}{2} l_0^{-2} \phi^2 (\ln l_0\phi)^2.
\end{eqnarray}
This potential admits a condensation of the vacuum associated with the minimum of 
$V(\phi)$ which occurs at the constant value $\phi=l_0^{-1}e^\lambda$
with $\lambda= \frac{1}{2} (\sqrt {1+\frac{2}{3}R l_0^2}-1)$.
Contrary to (11), the value of $l_0\phi=e^\lambda$ is larger than one and it violates the universal form of time-asymmetry reflected
in (8), because the right hand side of (8) becomes negative. 

To obtain an approximate form of the potential (13) one can use 
\begin{eqnarray}
\ln l_0\phi \approx (\lambda-1)+e^{-\lambda}l_0\phi
\nonumber
\end{eqnarray}
which corresponds to the two first terms of a Taylor series expansion of $\ln l_0\phi$ 
around the ground state value $l_0\phi=e^\lambda$. Then, the Eq. (12) can be rewritten as 
\begin{eqnarray}
\Box \phi+[e^{-2\lambda}\phi^2+ (2\lambda-1)e^{-\lambda}l_0^{-1}\phi+(\lambda^2-\lambda)l_0^{-2}-R/6]\phi=0.
\nonumber 
\end{eqnarray}

To rewrite this equation in a simple form, the linear term $\phi$ in the brackets 
may be approximated by replacing $\phi$ by its corresponding ground state value 
$\phi=l_0^{-1}e^\lambda$ to obtain
\begin{eqnarray}
\Box \phi+[e^{-2\lambda} \phi^2 -l_0^{-2}]\phi=0.
\nonumber 
\end{eqnarray}

One can easily check that this equation can be derived from a Lagrangian having the 
following potential
\begin{eqnarray}
V(\phi)=-\frac{1}{2}l_0^{-2}\phi^2+\frac{1}{4}e^{-2\lambda}\phi^4 
\nonumber 
\end{eqnarray}
which has the form of a Higgs potential. 
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Particle Concept}
It is possible to show that the Lorentz-noninvariant vacuum contains physical 
characteristics that may be interpreted in terms of an associated ensemble of 
particles. We proceed to derive a dynamical equation characterizing the ensemble 
of particles. Using (9), one finds
\begin{eqnarray}
{\Ja \Jb=- l_0^{-2}({\ln l_0\phi)}^2+\frac{1}{6}R}.
\end{eqnarray}
One can convert this 
equation into a familiar form indicating the existence of a particle with adjustable
mass-scale. For this purpose, first we assume a congruence of time-like 
curves specified by vector field $\nabla_\mu S$. Then, we require the vector field
$I_\mu$ defined by
\begin{eqnarray}
I_\mu=\nabla_\mu S-\Ja
\nonumber 
\end{eqnarray}
have a norm $I_\mu I^\mu =-\Lambda\phi^2-l_0^{-2}(\ln l_0\phi)^2$, in which $\Lambda$
is a dimensionless parameter.
Using this definition, Eq. (14) takes the form 
\begin{eqnarray}
\nabla_\mu S \nabla^\mu S=-M^2 -\frac{1}{6}R
\end{eqnarray}
where we have assumed the scalar field $\phi$ is the constant along the particle  
trajectory, and $M=(\Lambda \phi^2)^{1/2}$. This equation can be interpreted as the Hamilton-Jacobi equation characterizing the ensemble.
In this ensemble a particle has the four-momentum $\nabla_\mu S$, and the varying 
mass $M$ which may be adjusted to take arbitrary values 
by fixing the norm of the vector field $I_\mu$.

Now, to characterize the ensemble in terms of a hydrodynamical equation, it 
not unreasonable to expect that our theory to be essentially 
non-dissipative in the vacuum. We assume $\phi^2$ to be the particle-number density, satisfying the
following equation which expresses the local conservation of particles in the ensemble
\begin{eqnarray}
\nabla^\mu (\phi^2 \nabla_\mu S)=0.
\end{eqnarray}

In order to obtain a generalized form of the Hamilton-Jacobi equation (15), 
one may use conformal transformation 
\begin{eqnarray}
\left\{ \matrix{  
g_{\mu \nu} \longrightarrow \Omega^2(x) g_{\mu \nu}\nonumber \\
\phi \longrightarrow \Omega^{-1}(x) \phi} \right.
\end{eqnarray}
in which the conformal factor $\Omega (x)$ is an arbitrary, positive and smooth function 
of space-time. \\ Under this transformation, Eq. (15) takes the form
\begin{eqnarray}
\nabla_\mu S \nabla^\mu S=-M^2 -\frac{1}{6}R+\frac{\Box \phi}{\phi}
\end{eqnarray}
where we have used the transformation law of the Ricci curvature 
\begin{eqnarray}
R \longrightarrow \Omega^{-2} ( R-6\frac{\Box \Omega}{\Omega}) 
\nonumber
\end{eqnarray}
with $\Omega=\phi_0/\phi$, and $\phi_0$ being a constant. 

Now, it is instructive to derive the Einstein equation
corresponding to Eq. (18). For this purpose, one can 
multiply this equation by $\phi\na_\al \phi$
\begin{eqnarray}
\phi\na_\al \phi \nabla_\mu S \nabla^\mu S+M^2\phi\na_\al \phi +\frac{1}{6}R\phi\na_\al \phi -\na_\al \phi \Box \phi =0.  
\nonumber
\end{eqnarray}
Using Eq. (16) the first term can be written as
\begin{eqnarray}
\phi \na_\al \phi \na_\mu S \na^\mu S  & = & \frac{1}{2}(\na_\al \phi^2) \na_\mu S \na^\mu S \nonumber \\
                                       & = & \frac{1}{2} \na_\nu (\de_\al^{~\nu} \phi^2 \na_\mu S \na^\mu S -2\phi^2\na_\al S \na^\nu S ).
\nonumber 
\end{eqnarray}
The second term may be transformed as
\begin{eqnarray}
M^2 \phi\na_\al \phi=\frac{1}{4}\Lambda \de_\al^{~\nu} \na_\nu \phi^4. 
\nonumber        
\end{eqnarray}  
For the third term, using the Einstein tensor $G_{\mu\nu}=R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R$, we find 
\begin{eqnarray}
\de_\al^{~\nu} R \phi \na_\nu \phi & = &R_\al^{~\nu} \na_\nu \phi^2 - \na_\nu (G_\al^{~\nu}\phi^2)  \nonumber \\
                    & = &\na_\nu(\na_\al \na^\nu -\de_\al^{~\nu} \Box-G_\al^{~\nu})\phi^2 \nonumber 
\end{eqnarray}
where we have used the definition of the curvature tensor
\begin{eqnarray}
[ \na_\al , \na_\beta ] V_\ga=-R^\de_{~\ga\al\beta} V_\de \nonumber
\end{eqnarray}
in which $V_\ga$ is some covariant vector field.

Finally, for the last term we find 
\begin{eqnarray}
\na_\al \phi \Box \phi =\na_\nu (\na_\al \phi \na^\nu \phi 
-\frac{1}{2} \de_\al^{~\nu} \na_\mu \phi \na^\mu \phi).
\nonumber
\end{eqnarray}

Combining all these relations, one gets
\begin{eqnarray}   
\na^\nu {\cal T}_{\mu \nu}=0
\nonumber
\end{eqnarray}  
in which
\begin{eqnarray}
{\cal T}_{\mu \nu}= \phi^2 G_{\mu \nu}+(g_{\mu \nu} \Box - \na_\mu \na_\nu)\phi^2 + 6(\na_\mu \phi \na_\nu \phi+ \phi^2 \na_\mu S \na_\nu S)   \nonumber  \\
-3g_{\mu \nu}(\na_\alpha \phi \na^\alpha \phi +\phi^2 \na_\alpha S \na^\alpha S)- \frac{3}{2}\Lambda g_{\mu \nu} \phi^4 \nonumber
\end{eqnarray}
is conserved. 

One may assum ${\cal T}_{\mu \nu}=0$. Thus, 
one finds
\begin{eqnarray}
\phi^2 G_{\mu \nu}+(g_{\mu \nu} \Box - \na_\mu \na_\nu)\phi^2 + 6(\na_\mu \phi \na_\nu \phi+ \phi^2 \na_\mu S \na_\nu S)  \\ \nonumber
-3g_{\mu \nu}(\na_\alpha \phi \na^\alpha \phi + \phi^2 \na_\alpha S \na^\alpha S)- \frac{3}{2}\Lambda g_{\mu \nu} \phi^4 =0
\end{eqnarray}
which is the Einstein equation.
In the standard way, this equation can be derived from the variation of an action  
leading to the field Eq. (18), with respect to $g_{\mu \nu}$.

One can easily check that, the trace of Eq. (19) leads directly to the Eq. (18). 
This feature is a consequence of the fact that these equations are exactly 
invariant under the conformal transformation (17), which leaves us one degree of freedom
unspecified. This tells us that the theory introduced by these equations
can be studied in various conformal frames. Among various frames which can be 
used with this theory, one may choose a classical frame which is defined by the 
condition $\phi = \phi_0$ = const. In this frame Eqs. (16) and (19) take, 
respectively, the forms
\begin{eqnarray}
\Box S=0
\end{eqnarray}
and
\begin{eqnarray}
G_{\mu \nu}+6\na_\mu S \na_\nu S - 3g_{\mu \nu} \na_\alpha S \na^\alpha S - \frac{3}{2}\Lambda g_{\mu \nu} \phi_0^2 =0
\end{eqnarray}
These equations contain the entire dynamical coupling of the particle's S-function
to gravity in the classical frame. One can show that, the trace of Eq. (21)
leads to the generalized Hamilton-Jacobi equation
\begin{eqnarray}
\na_\mu S \na^\mu S=-{M_0}^2 -\frac{1}{6}R
\end{eqnarray}
where $M_0=(\Lambda \phi_0^2)^{1/2}$ being a constant mass-parameter.

A new frame, which we call quantum frame, can be obtained by the application of the conformal transformation 
\begin{eqnarray}
\left\{ \matrix{  
g_{\mu \nu} \longrightarrow \Omega^2(x) g_{\mu \nu}\nonumber \\
\phi_0 \longrightarrow \Omega^{-1}(x) \phi_0} \right.
\end{eqnarray}
to the classical frame, with $\Omega =\frac{\phi_0}{\phi}$.
Using the transformation law of the Ricci tensor $R_{\mu\nu}$ under (23), we then find ${}^6$
\begin{eqnarray}
R_{\mu\nu} \longrightarrow R_{\mu\nu}+ \Omega^{-2} \{4 {D_\mu \Omega D_\nu \Omega}-2\Omega{D_\mu D_\nu \Omega}
-g_{\mu\nu}g^{\alpha \beta}(\Omega{D_\alpha D_\beta \Omega}+{D_\alpha \Omega D_\beta \Omega}) \}
\nonumber
\end{eqnarray}
\begin{eqnarray}
G_{\mu\nu} \longrightarrow G_{\mu\nu}+\Omega^{-2} \{ (g_{\mu\nu} \Box^g
 - D_\mu D_\nu)\Omega^2 + 6D_\mu \Omega D_\nu \Omega-3g_{\mu \nu} D_\alpha \Omega D^\alpha \Omega \}.
\nonumber
\end{eqnarray}
Consequently the Eq. (21) transforms as
\begin{eqnarray}
\Omega^2 G_{\mu \nu}+ (g_{\mu \nu} \Box^g - D_\mu D_\nu)\Omega^2 + 6(D_\mu \Omega D_\nu \Omega+ \Omega^2 D_\mu S D_\nu S)  \\ \nonumber
-3g_{\mu \nu}(D_\alpha \Omega D^\alpha \Omega + \Omega^2 D_\alpha S D^\alpha S)- \frac{3}{2} M_0^2 \Omega^2  g_{\mu \nu} =0.
\nonumber
\end{eqnarray}
Taking the trace of Eq. (24), we find
\begin{eqnarray}
g_{\mu \nu} \pa S \partial^\nu S=-{M_0}^2 + \frac{ \Box^g \Omega}{\Omega}-\frac{1}{6}R.
\end{eqnarray}
Comparison of Eqs. (21) and (22) indicates that, the new frame is equivalent
to the classical frame, except for the particle mass which is modified by 
an extra term $\frac{ \Box \phi}{\phi}$. The appearance of this term is a consequence of the 
conformal invariance breakdown by the introduction of a physical frame. 

One must note that Eq. (20) under (23) is left unchanged. However, it is convenient 
to rewrite this equation in the following form
\begin{eqnarray}
\partial_\mu (\sqrt {-g} \Omega^2 g^{ \mu \nu} \partial_\nu S)=2\sqrt{-g}{\bar{M}} \Omega^2 \frac{d}{d \bar{\tau}} \ln \Omega
\end{eqnarray}
where we have used 
$\partial_\mu S={\bar{M}} U_\mu $ and $ U_\mu \partial^\mu =\frac{d}{d \bar{\tau}} $
in which $\bar{\tau}$ is a parameter along the particle trajectory and the value ${\bar{M}}$ is dependent
on the frame one is using.
The right hand side of this equation, which in principle may be
different from zero, indicates
that, if we relate $\Omega^2$ to the particle-number density, the
conservation of particles in the ensemble may be violated.

Comparison of Eqs. (21) and (22) indicates that, the new frame is equivalent
to the classical frame, except for the particle mass which is modified by 
an extra term $\frac{ \Box \Omega}{\Omega}$. The appearance of this term is a consequence of the 
conformal invariance breakdown by the introduction of a physical frame. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{ Relativistic Motion of the Particle }

In the quantum frame, the equation of motion a particle in the ensemble may be  
described in terms of a pilot wave
\begin{eqnarray}
\psi= \Omega e^{iS}.
\nonumber
\end{eqnarray}
This wave satisfies the equation 
\begin{eqnarray}
\Box \psi- {M_0}^2 \psi=(\frac{1}{6}R+2i\bar{M} \frac{d}{d \bar{\tau}} \ln |\psi|)\psi
\end{eqnarray}
which is obtained from combination of Eqs. (22) and (23).

In the background approximation $g_{\mu\nu} \approx \eta_{\mu \nu}$,
Eq. (24) reduces to the following form 
\begin{eqnarray}
\Box^\eta \psi- {M_0}^2 \psi=(2i\bar{M} \frac{d}{d \tau} \ln |\psi|)\psi
\nonumber
\end{eqnarray}
in which, $\Box^\eta=\eta_{\mu\nu}\partial^\mu\partial^\nu$ denotes the d'Alembertian 
associated with the Minkowski metric $\eta_{\mu\nu}$. 
The right hand side of this equation is a consequence of violation of the 
conservation of particles in the ensemble, Eq. (23). In fact, the 
appearance of this term, which is non-linear in terms of $\psi$, is a 
consequence of geometrization of the quantum aspects of matter. 
Further attempts seems to be necessary to understand relationship of non-linear
quantum mechanics and geometrization of the quantum effects.

Under the assumption that the amplitude $\phi$ is independent of the parameter
$\bar{\tau}$ along the particle trajectory, this equation reduces to the massive 
Klein-Gordon equation
\begin{eqnarray}
\Box^\eta \psi-M_0^2 \psi=0.
\nonumber
\end{eqnarray}
The merit of introducing the wave $\psi$ is that it acts
as a sort of a pilot wave in the sense of causal interpretation of
quantum mechanics,${}^{7,8}$ the term $\frac{\Box \Omega}{\Omega} $ on the right
hand side of Eq. (22) being the associated quantum potential.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Geodesic Equation}
One can show that Eq. (21) is equivalent with geodesic equation for a free particle.
For this purpose, by taking divergence of Eq. (21), one find
\begin{eqnarray}
U^\nu \nabla_\nu U^\mu + U^\mu \nabla_\nu U^\nu=0
\end{eqnarray}
where we have used the guidance formula 
$P^\mu ={\cal M}_0 U^\mu=\nabla^\mu S$ in which ${\cal M}_0=(M_0^2+\frac{1}{6}R)^{1/2}$.
By multiplying this equation by $U_\mu$, and then using the time-like nature of
four-velocity $U_\mu$, namely  
\begin{eqnarray}
U_\mu U^\mu =-1,
\end{eqnarray}
we get
\begin{eqnarray}
U_\mu U^\nu \nabla_\nu U^\mu - \nabla_\nu U^\nu=0.
\end{eqnarray}
Using (26) and Eq. (27), one can check that the second term is zero. So, Eq. (28)
reduce to 
\begin{eqnarray}
U^\nu \nabla_\nu U^\mu =0
\end{eqnarray}
or equivalently
\begin{eqnarray}
\frac{d U_\mu}{ds}+\Gamma^\mu_{\nu \lambda} U^\nu U^\lambda=0
\end{eqnarray}    
in which $U^\mu \partial_\mu =\frac{d}{ds}$.
The last equation is the geodesic equation for a free particle.

Similarly, using $P^\mu ={\cal M} U^\mu=\nabla^\mu S$ with  
${\cal M}=(M_0^2-\frac{\Box \Omega}{\Omega}+\frac{1}{6}R)^{1/2}$,
Eq. (22) transforms as 
\begin{eqnarray}
\frac{d U_\mu}{ds}+\Gamma^\mu_{\nu \lambda} U^\nu U^\lambda=-{\cal M}^{-1}(g_{\mu\nu}+U_\mu U_\nu) \nabla^\nu {\cal M}
\nonumber
\end{eqnarray}    
where the added term on the right-hand side is the quantum force.${}^6$ 
\footnote{ To avoid a 
tachyonic mass, the parameter $\Lambda$ in $M_0=(\Lambda \phi_0^2)^{1/2}$  may be adjusted such that ${\cal M}$ 
and ${\cal M}_0$ to be positive-definite.}
Clearly, because of quantum force the particle does not move on a geodesic of the geometry. 
However, 
in the new units of length and time appropriate for the new geometry 
$\bar{g}_{\mu\nu}=\Omega^2 g_{\mu\nu}$ with $\Omega=\frac{\phi_0}{\phi}$,  
this equation takes the form 
\begin{eqnarray}
\frac{d \bar{U}_\mu}{d \bar{s}}+{\bar{\Gamma}}^\mu_{\nu \lambda} \bar{U}^\nu \bar{U}^\lambda=0
\nonumber
\end{eqnarray}    
where we have used $\bar{U}_\mu=\Omega U_\mu$  
and $d \bar{s}^2=\Omega^2 d s^2$.  
Thus the particle with variable mass ${\cal M}$, behaves as a free particle 
with the constant mass in the new geometry.                                                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Concluding Remarks}
We have studied the consequence of the violation of Lorentz-invariance in vacuum.
In this approach, signature change duality transformation has been considered 
as a natural symmetry transformation which provides transition between two dual 
configurations. The duality-braking condition (8) which can not simultaneously 
be satisfied for these configurations implies an asymmetry in the direction of the 
vector field $N_\mu$ in vacuum. This indicates an assertion about the 
emergence of a preferred arrow of time.${}^2$ The symmetry-braking law (8) 
implies that the term $[-{\Ja}{\Jb}-\pa \Jb ]$ which plays role of a dynamical mass  
in Eq. (4) may be tachyonic.
This can produce a corresponding condensation of vacuum.

As a final remark, It is shown that the Lorentz-noninvariant vacuum involves 
physical characteristics that 
can be interpreted in terms of an associated ensemble of particles.  
In this approach we have considered a conformally invariant coupling of a 
particle to gravity.
The conformal invariance implies that particle properties are dependent on the 
frame one is using. Among various conformal frames, we chose two different frames, 
namely the classical frame and the quantum one.
The transition from the classical frame to the quantum one 
was led to the appearance of the quantum potential. This potential is a  
consequence of the applied conformal transformation.
Therefore quantum effects may be contained in the conformal degree of freedom of the space-time
metric. This is what we call geometrization of the quantum effects.${}^{9,10,11}$


\begin{thebibliography}{99}
1. H. Salehi, H. Motavali and M. Golshani, submitted to Int. J. Theor. Phys.\\
2. D.I. Blokhintsev, Phys. Lett. {\bf 12}, 272 (1964).           \\
3. H.B. Nielsen and I. Picek, Nucl. Phys. B {\bf 211}, 269 (1983). \\
4. P.R. Phillips, Phys. Rev. {\bf 139}, 491 (1965).                  \\
5. H. Salehi and H. R. Spanji, Phys. Lett. A {\bf 251}, 95 (1999).     \\
6. R.M Wald, General Relativity (Chicago Univ. Press, Chicago, 1984).
7. P.R. Holland, The quantum theory of motion (Cambridge Univ. Press, Cambridge, 1993). \\
8. D. Bohm, Phys. Rev. {\bf 85}, 166, 180 (1952); D. Bohm and B. J. Hiley, The Undivided Universe 
$~~~~{}$(Routledge, 1993).   \\
9. H. Motavali, H. Salehi and M. Golshani, Mod. Phys. Lett. {\bf A14}, 2481 (1999). \\
10. H. Motavali, H. Salehi and M. Golshani, Int. J. Mod. Phys. Lett. {\bf A15}, 983 (2000). \\
11. F. Shojai and M. Golshani, Int. J. Mod. Phys. {\bf A13}, 677 (1998).\\ 
\end{thebibliography}

\end{document} 

