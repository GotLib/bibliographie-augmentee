\documentstyle[12pt]{article}

\textheight=24truecm

\textwidth=16truecm

\hoffset=-1.3truecm

\voffset=-2truecm


\newcommand{\beq}{\begin{equation}}

\newcommand{\eeq}{\end{equation}}

\newcommand{\bra}{\begin{array}}

\newcommand{\era}{\end{array}}

\newcommand{\be}{\beta}

\newcommand{\al}{\alpha}

\newcommand{\ga}{\gamma}

\newcommand{\de}{\delta}

\newcommand{\da}{\dagger}

\newcommand{\Te}{\Theta}

\newcommand{\Om}{\Omega}

\newcommand{\ot}{\otimes}

\newcommand{\ep}{\epsilon}

\begin{document}
\begin{center}

{\bf\huge The FSUSY Hamiltonian in the connexion with the Chern-Simons
gauge theory.}\\


\vskip1.2truecm
{\bf J. Douari}
\footnote[1]
{Junior Associate of the Abdus Salam ICTP.\\
\null\hspace{0.5cm} E-mail: douarji@ictp.trieste.it}\\
\small{The Abdus Salam ICTP - strada costiera 11, 34100 Trieste, Italy.}\\
\small{and}\\
\small{Facult\'e des Sciences, D\'epartement de Physique, LPT-ICAC}\\
\small{Av. Ibn Battouta, B.P.1014, Agdal, Rabat, MOROCCO}\\
\vspace*{0.5cm}
{\bf Y. Hassouni}
\footnote[2] 
{Regular Associate of the Abdus Salam ICTP.\\
\null\hspace{0.5cm} E-mail: Y-hassou@fsr.ac.ma}\\
\small{Facult\'e des Sciences, D\'epartement de Physique, LPT-ICAC}\\
\small{Av. Ibn Battouta, B.P.1014, Agdal, Rabat, MOROCCO}


\end{center}
\hoffset=-1cm

\hoffset=-1cm\textwidth=20cm\pagestyle{empty}
\vspace*{0.5cm}
\section*{Abstract}
\hspace{.3in}On the two-dimensional lattice, the construction of anyonic
operators and its algebras is discussed. Thus, the fractional
supersymetry (FSUSY) and the associated FSUSY Hamiltonian basing on the
quonic anyons are recalled. Its particular case for bosonic anyon and
fermionic anyon is given. The FSUSY Hamiltonian in the connexion with
the Chern-Simons gauge theory is constructed.
\newpage
\section{introduction}
\hspace{.3in}There are at least twenty years that the supersymmetry (SUSY) has been a popular area of research for physicists and mathematicians. Originally this SUSY was introduced in relativistic quantum field theories as a generalization of Poincar\'e symmetry$^{\cite{1,2}}$. Which can be parametrized by the ordinary space-time coordinates supplemented by Grassmann variables transforming as a spinor. In $1976$ Nicolai suggested an analoguous generalization for non-relativistic quantum mechanics$^{\cite{3}}$. Where the model consists of one-dimensional lattice with $N$ sites. Each site may be occupied by bosonic and fermionic degrees of fredom. With the bosonic degrees of fredom are characterized by bosonic creation and annihilation operators obeying the usual commutation relations. Also the fermionic degrees of fredom are characterized by fermionic creation and annihilation operators which obey the usual anti-commutation relations. Furthermore with the one-dimensional model introduced by Witten$^{\cite{4}}$ in $1981$, the SUSY became a major tool in quantum mechanics and mathematical, statistical and condensed matter physics.\\
\hspace*{.3in}As a generalization of this ordinary SUSY, there are a large studies$^{\cite{5,6,7,8}}$ of fractional supersymmetry (FSUSY). Some of these deal with a complex Grassmann variable. Other$^{\cite{9}}$ with quonic operators which are used to construct the fractional supercharges and the FSUSY Hamiltonian of the quantum system consisted of two quons with different deformation parameters. To generalize this last new version of FSUSY we gave in the paper \cite{10} a new construction of the FSUSY from one system of two quonic anyons of different kinds. Where the quonic anyons$^{\cite{10}}$ are particles interpolating between bosonic and ferminic anyons$^{\cite{18,19}}$. They are constructed by means of the generalized Jordan-Wigner transformation$^{\cite{11}}$, which transmutes in this case bosons and fermions into anyons respectively. Yet so, it transmutes quons into a quonic anyons.\\
\hspace*{.3in}Thus, let us recalling that these three kinds of anyons are nothing but the three aspects those the anyons$^{\cite{12,13,14,15,16,17}}$ can take on the two-dimensional space. Such as, in mathematical point of view, the creation and annihilation anyonic operators for fermionic, bosonic and quonic anyons were constructed respectively in the references \cite{10,18,19,24}.\\
\hspace*{.3in}In the present work, we give the $N=2$ FSUSY Hamiltonian by basing on the definition of anyons indicated by Wilczek$^{\cite{13}}$, as a particular case of this realization. Where an anyon is a charge-flux composition in considering a non-relativistic particle of mass $m$ and electric charge $q$ that moves in the magnetic field $B$ created by an infinitely long and thin solenoid passing through the origin and directed along the $z$-axis, such as the particles moves on the two-dimensional space $(X,Y)$. Otherwise said, we are going to construct the FSUSY Hamiltonian in the connexion with the Chern-Simons gauge theory. This result is seen as a particular case of the work \cite{10}.\\
\hspace*{.3in}This paper can be thus organized as follows; in the second section we briefly give the definition of the anyonic operators for its three aspects and its associated algebras. In the third section, we discuss the notion of FSUSY for quonic anyons defined on the two-dimensional lattice. Finally in the fourth section, we construct the FSUSY Hamiltonian of the system consisting of one bosonic anyon and one fermionic anyon. Where we consider the definition of anyons owing to Wilczek, such as the anyon is the elementary particle associated to Chern-Simons gauge field moving on the two-dimensional space.
\section{Anyonic Operators and its Algebras}
\hspace{.3in}By means of the generalized Jordan-Wigner transformation$^{\cite{11}}$ the anyonic oscillators can be constructed from the three kinds of particles; bosons, fermions and quons (the q-oscillators which interpolate between bosons and fermions in any dimension). Such as this anyonic oscillators are two-dimensional non-local operators$^{\cite{20,21,22}}$. Also they interpolate between bosons and fermions in two space dimension strenghtening that the anyonic oscillators don't have to be confused with the quons.\\
\hspace*{.3in}for the corresponding transformation there are an essentiel ingredient. It is the function angle$^{\cite{21,23}}$ $\Te(x,y)$ defined on the two-dimensional lattice. Which characterizes the kind of each anyon on the lattice $\Om$. Let us noting here that, in the continuous plane the function angle is a rather familiar angle. Then owing to the references \cite{18,24,25} let us give briefly a description of $\Te(x,y)$.\\
\hspace*{.3in}On the two-dimensional lattice $\Om$, we choose one cut associated to each site $x$ denoted $\ga_{x}$ from munis infinity of $x$-axis and we fix a base point $B$ at the positive infinity of $x$-axis. The function angle is defined as an angle under which the oriented path $Bx$ is seen from a point $y^*$. With $y^*$ the dual point of $y$ in the dual lattice $\Om^*$, such as $y^{*}=y+0^*$ and $0^*=(\frac{1}{2}\ep,\frac{1}{2}\ep)$ is its origin ($\ep\ll 1$ the lattice spacing). With this type of cuts we fix the path orientation $Bx$ on $\Om$ and then the direction of the rotation on $\Om$ in considering the particles defined on each site of the lattice. In this case the function angle is denoted by $\Te_{\ga_{x}}(x,y)$. Also we can choose another kind of it denoted $\Te_{\de_{x}}(x,y)$. Now, the cuts $\de_{x}$ is defined from the positive infinity $x$-axis to the dual point $^{*}x\in\Om^{*}$ ($^{*}x=x-0^*$), and the base point will be chosen in the munis infinity of the $x$-axis . These two types of the function angle satisfy the following relations
\beq
\bra{rcl}
\Te_{\ga_{x}}(x,y)-\Te_{\ga_{y}}(y,x)&=&
\left\{\bra{cc}
\pi,&  x>y \\
-\pi,& x<y
\era 
\right.
\\
\Te_{\de_{x}}(x,y)-\Te_{\de_{y}}(y,x)&=&
\left\{\bra{cc}
-\pi,&  x>y \\
\pi,& x<y
\era
\right.
 \\
\Te_{\de_{x}}(x,y)-\Te_{\ga_{x}}(x,y)&=&
\left\{\bra{cc}
-\pi,& x>y \\
\pi,& x<y
\era
\right.
 \\
\Te_{\de_{x}}(x,y)-\Te_{\ga_{y}}(y,x)&=&0, \forall x,y\in\Omega.
\era
\eeq
That is because of the ordering and opposite ordering conditions involved by the two kinds of cuts $\ga_{x}$ and $\de_{x}$. These are described by
\beq
x_{\de}<y_{\de} \Leftrightarrow  x_{\ga}>y_{\ga} \Leftrightarrow x>y \Leftrightarrow \left\{
\bra{cc}
x_{2} > y_{2},& \\
x_{1}>y_{1}, x_{2}=y_{2}.
\era
\right.
\eeq
\hspace*{.3in}By indicating two kinds of the function angle, it exist then two kinds of anyons defined as
\beq
\bra{rcl}
a_{i}(x_{\al})=D_{i}(x_{\al})c_{i}(x),\\
a^{\da}_{i}(x_{\al})=c^{\da}_{i}(x)D^{\da}_{i}(x_{\al})
\era
\eeq
with $\al=\ga, \de$ and $i\in \bf{N}\rm^*$. In this expression $a_{i}(x_{\al})$ and $a^{\da}_{i}(x_{\al})$ are the annihilation and the creation anyonic operators respecively. Also $c_{i}(x)$ and $c^{\da}_{i}(x)$ are the annihilation and the creation operators respecively which can be either bosonic or fermionic or quonic operators denoted respecively by the sets $\{b_{i}(x), b^{\da}_{i}(x)\}$, $\{f_{i}(x), f^{\da}_{i}(x)\}$ and $\{d_{i}(x), d^{\da}_{i}(x)\}$. Thus in the equation $(3)$ we introduced the disorder operators $D_{i}(x_{\al})$ which can be expressed by
\beq
D_{i}(x_{\al})=e^{i\nu\sum\limits_{y\neq x}\Te_{\al_{x}}(x,y)[N_{i}(y)-\frac{1}{2}]}.
\eeq
Which satisfy the following equations
\beq
\bra{rcl}
D^{\dag}_{i}(x_{\al})c_{i}(y)&=&e^{i\nu \Te_{\al_{x}}(x,y)}c_{i}(y)D^{\dag}_{i}(x_{\al}) \\
D^{\dag}_{i}(x_{\al})c_{i}^{\dag}(y)&=&e^{-i\nu \Te_{\al_{x}}(x,y)}c_{i}^{\dag}(y)D^{\dag}_{i}(x_{\al}) \\
D_{i}(x_{\al})c_{i}(y)&=&e^{-i\nu \Te_{\al_{x}}(x,y)}c_{i}(y)D_{i}(x_{\al}) \\
D_{i}(x_{\al})c_{i}^{\dag}(y)&=&e^{i\nu \Te_{\al_{x}}(x,y)}c_{i}^{\dag}(y)D_{i}(x_{\al}) \\
D^{\dag}_{i}(x_{\al})D_{i}(y_{\al})&=&D_{i}(y_{\al})D^{\dag}_{i}(x_{\al})
\era
\eeq
Where $\nu$ is seen as the statistical parameter. $N_{i}(x)$, $x\in\Om$, is the number operator on the two-dimensional lattice associated to each kind of particles involved in the construction of anyons. We can define in the general case $N_{i}(x)$ by
\beq
\bra{rcl}
d_{i}^{\dag}(x)d_{i}(x)&=&[N_{i}(x)]_{q_{i}}\\
d_{i}(x)d_{i}^{\dagger}(x)&=&[N_{i}(x)+\bf{1}]_{\rm{q_{i}}}
\era
\eeq
i.e. for quons characterized by the deformation parameter $q_{i}$ which take $\pm 1$ for bosons and fermions respectively. In the equation $(6)$ we have the notation $[x]_{q}=\frac{q^{x}-1}{q-1}$. Thus the quonic operators satisfy the following oscillator algebra
\beq
\bra{rcl}
\lbrack d_{i}(x),d_{j}^{\dag}(y)\rbrack_{q_{i}^{\de_{ij}}}&=&\de_{ij}\de(x,y)\\
\lbrack d_{i}(x),d_{j}(y)\rbrack_{q_{i}^{\de_{ij}}} &=&\bra{cc}
0& \mbox{$\forall x,y$, $\forall i,j$}
\era\\
\lbrack d_{i}^{\dag}(x),d_{j}^{\dag}(y)\rbrack_{q_{i}^{\de_{ij}}} &=&\bra{cc}
0& \mbox{$\forall x,y$, $\forall i,j$}
\era\\
\lbrack N_{i}(x),d_{j}(y)\rbrack &=&-\de_{ij}\de(x,y)d_{i}(x)\\
\lbrack N_{i}(x),d_{j}^{\dag}(y)\rbrack &=&\de_{ij}\de(x,y)d_{i}^{\dag}(x)\\
\era
\eeq
with 
\beq
\bra{rcl}
(d_{i}(x))^{\ell_{i}}=(d_{i}^{\dag}(x))^{\ell_{i}}=0,& \ell_{i}\geq 2.
\era
\eeq
Where the deformation parameter is supposed to be a root of unity; i.e. $q_{i}^{\ell_{i}}=1$ so $q_{i}=e^{i\frac{2\pi}{\ell_{i}}}$. In the particular case of $q_{i}=+1$ we will have the bosonic algebra as follows
\beq
\bra{rcl}
\lbrack b_{i}(x),b_{j}^{\dag}(y)\rbrack &=&\de_{ij}\de(x,y)\\
\lbrack b_{i}(x),b_{j}(y)\rbrack &=&\bra{cc}
0& \mbox{$\forall x,y$, $\forall i,j$}
\era\\
\lbrack b_{i}^{\dag}(x),b_{j}^{\dag}(y)\rbrack &=&\bra{cc}
0& \mbox{$\forall x,y$, $\forall i,j$}
\era\\
\lbrack N_{i}(x),b_{j}(y)\rbrack &=&-\de_{ij}\de(x,y)b_{i}(x)\\
\lbrack N_{i}(x),b_{j}^{\dag}(y)\rbrack &=&\de_{ij}\de(x,y)b_{i}^{\dag}(x)\\
\era
\eeq
with $N_{i}(x)=b_{j}^{\dag}(x)b_{j}(x)$ is the number operator for the bosons. Also for $q_{i}=-1$ the fermionic algebra will be
\beq
\bra{rcl}
\lbrace f_{i}(x),f_{j}^{\dag}(y)\rbrace &=&\de_{ij}\de(x,y)\\
\lbrace f_{i}(x),f_{j}(y)\rbrace &=&\bra{cc}
0& \mbox{$\forall x,y$, $\forall i,j$}
\era\\
\lbrace f_{i}^{\dag}(x),f_{j}^{\dag}(y)\rbrace &=&\bra{cc}
0& \mbox{$\forall x,y$, $\forall i,j$}
\era\\
\lbrack N_{i}(x),f_{j}(y)\rbrack &=&-\de_{ij}\de(x,y)f_{i}(x)\\
\lbrack N_{i}(x),f_{j}^{\dag}(y)\rbrack &=&\de_{ij}\de(x,y)f_{i}^{\dag}(x)\\
\era
\eeq
In the relations $(9)$ and $(10)$ we define the Dirac function as
\beq
\de(x,y)=\left\{\bra{cc}
1& \mbox{if $x=y$} \\
0&\mbox{ if $x\neq 0$}.
\era
\right.
\eeq
\hspace*{.3in}Owing to the relations $(7)$ as a general case of bosonic and fermionic operators, it is easy to prove that the operators of $(3)$ obey the following anyonic algebra
\beq
\bra{rcl}
\lbrack a_{i}(x_{\ga}),a_{i}(y_{\ga})\rbrack_{q_{i}p^{-1}}&=&\mbox{$0$, $x>y$}\\
\lbrack a^{\dag}_{i}(x_{\ga}),a^{\dag}_{i}(y_{\ga})\rbrack_{q_{i}p^{-1}}&=&\mbox{$0$, $x>y$}\\
\lbrack a_{i}(x_{\ga}),a^{\dag}_{i}(y_{\ga})\rbrack_{q_{i}p}&=&\mbox{$0$, $x>y$}\\
\lbrack a^{\dag}_{i}(x_{\ga}),a_{i}(y_{\ga})\rbrack_{q_{i}p}&=&\mbox{$0$, $x>y$}\\
\lbrack a_{i}(x_{\ga}),a^{\dag}_{i}(x_{\ga})\rbrack_{q_{i}}&=&1\\
\lbrack a_{i}(x_{\ga}),a_{j}(y_{\ga})\rbrack &=&\mbox{$0$, $\forall i\neq j$, $\forall x,y\in \Omega$}\\
\lbrack a^{\dag}_{i}(x_{\ga}),a^{\dag}_{j}(y_{\ga})\rbrack &=&\mbox{$0$, $\forall i\neq j$, $\forall x,y\in \Omega$}\\
\lbrack a^{\dag}_{i}(x_{\ga}),a_{j}(y_{\ga})\rbrack &=&\mbox{$0$, $\forall i\neq j$, $\forall x,y\in \Omega$}\\
\lbrack a_{i}(x_{\ga}),a^{\dag}_{j}(y_{\ga})\rbrack &=&\mbox{$0$, $\forall i\neq j$, $\forall x,y\in \Omega$}.
\era
\eeq
The same results can be obtained for the kind $\de$ in exchanging $p$ by $p^{-1}$. For the different kinds the commutation relations can be
\beq
\bra{rcl}
\lbrack a_{i}(x_{\de}),a_{j}(y_{\ga})\rbrack_{q_{i}^{\de_{ij}}}&=&0, \mbox{$\forall$ $x,y\in \Omega$}\\
\lbrack a_{i}(x_{\de}),a^{\dag}_{j}(y_{\ga})\rbrack_{q_{i}^{\de_{ij}}}&=&
\de_{ij}\de(x,y)p^{-[\sum\limits_{z<x}-\sum\limits_{z>x}][N_{i}(z)-\frac{1}{2}]}
\era
\eeq
with $p=e^{i\nu\pi}$. Thus, according to $(8)$ it is easy to see that
\beq
(a_{i}(x_{\al}))^{\ell_{i}}=(a^{\dag}_{i}(x_{\al}))^{\ell_{i}}=0
\eeq
which generalizes the hard-core condition in the work \cite{18} for $\ell_{i}=2$; i.e. for the fermionic anyons.\\
\hspace*{.3in}Then with this construction of anyonic algebra in general case basing on the quonic algebra we pointed out to define in the work \cite{10} one version of FSUSY through the anyons constructed from the quons.
\section{N=2 FSUSY Through Quonic Anyons}
\hspace{.3in}In this section we will give a reminder about the construction of the FSUSY from one system consists of two quonic anyons ($q_{i}$-anyons, $i=1,2$). The two anyons were constructed from quons of different deformation parameters. Also we considered two different kinds of anyons to construct the supercharges of our N=2 FSUSY.\\
\hspace*{.3in}On the two-dimensional lattice $\Om$, we introduced in the paper \cite{10} the supercharges as follows
\beq
\bra{rcl}
Q_{+}(x)&=&a^{\dag}_{1}(x_{\ga})a_{2}(x_{\de})\\
Q_{-}(x)&=&a_{1}(x_{\de})a^{\dag}_{2}(x_{\ga}).
\era
\eeq
With the nilpotency condition, according to the equality $(14)$,
\beq
(Q_{\pm}(x))^{\ell_{2}}=0,
\eeq
in supposing $\ell_{2}<\ell_{1}$.\\
\hspace*{.3in}In introducing the hermitian conjugate of the generators $Q_{\pm}(x)$ as
\beq
\bra{rcl}
Q^{\dag}_{+}(x)&=&a^{\dag}_{2}(x_{\de})a_{1}(x_{\ga})\\
Q^{\dag}_{-}(x)&=&a_{2}(x_{\ga})a^{\dag}_{1}(x_{\de}).
\era
\eeq
Then the FSUSY Hamiltonian operator on each site $x$ in $\Om$ corresponding to the discussed system can be given by the following operation
\beq
q_{1}Q_{+}(x)Q_{-}(y)+q_{1}^{-1}Q_{-}^{\dag}(y)Q_{+}^{\dag}(x)-q_{2}Q_{-}(y)Q_{+}(x)-q_{2}^{-1}Q_{+}^{\dag}(x)Q_{-}^{\dag}(y)=\de(x,y)H(x).
\eeq
In a straightforward calculation we can obtain 
\beq
[N_{i}(x)]_{q_{i}^{-1}}=q_{i}^{1-N_{i}(x)}[N_{i}(x)]_{q_{i}}.
\eeq
Consequently, owing to the equations $(17)$, $(18)$ and $(19)$ the FSUSY Hamiltonian operator on $x\in\Om$ can be written as
\beq
H(x)=[P^{-1}q_{1}^{-N_{1}(x)}+Pq_{1}][N_{1}(x)]_{q_{1}}-
     [P^{-1}q_{2}^{-N_{2}(x)}+Pq_{2}][N_{2}(x)]_{q_{2}},
\eeq 
where the operator $P$ is defined as
\beq
P=p^{[\sum\limits_{z<x}-\sum\limits_{z>x}][N_{1}(z)+N_{2}(z)-1]}.
\eeq
\hspace*{.3in}In order to strenghten this result we assume in this work one study of particular case of its extremities. That is, we will discuss the $N=2$ FSUSY for one system consists of one bosonic anyon and one fermionic anyon. From which we are passing to write the corresponding Hamiltonian operator. Otherwise, in the following section, we are seeing to obtain the FSUSY Hamiltonian in the connexion with the Chern-Simons gauge theory, in order to make appear the properties of the fractional statistics of this system and its FSUSY.\\
\hspace*{.3in}Let us assume the following limits
\beq
\bra{rl}
q_{1}=1,& q_{2}=-1
\era
\eeq
and the nilpotency condition becomes
\beq
(Q_{\pm}(x))^{2}=0.
\eeq
Then the FSUSY Hamiltonian operator will be
\beq
H(x)=[P^{-1}+P][N_{b}(x)]-[P^{-1}(-1)^{-N_{f}(x)}-P][N_{f}(x)],
\eeq 
with 
\beq
\bra{rcl}
N_{b}(x)=N_{1}(x)=a^{\dag}_{1}(x_{\al})a_{1}(x_{\al})=B^{\dag}(x)B(x)\\
N_{f}(x)=N_{2}(x)=a^{\dag}_{2}(x_{\al})a_{2}(x_{\al})=F^{\dag}(x)F(x)
\era
\eeq
$N_{b}(x)$ and $N_{f}(x)$ are respectively the bosonic and the fermionic number operators defined onthe site $x\in\Om$. Thus $B^{\dag}(x)$ $(F^{\dag}(x))$ and $B(x)$ $(F(x))$ are the creation and annihilation bosonic (fermionic) operators respectively.\\ \hspace*{.3in}Thus, namely
\beq
\vert n_{x_{b}}, n_{x_{f}}>=\vert n_{x_{b}}>\ot\vert n_{x_{f}}>
\eeq
an element of Fock space associated to our two particles system \{bosonic anyon, fermionic anyon\}. We will have
\beq
\bra{rcl}
N_{b}(x)\vert n_{x_{b}}, n_{x_{f}}>&=&n_{x_{b}}\vert n_{x_{b}}, n_{x_{f}}>\\
N_{f}(x)\vert n_{x_{b}}, n_{x_{f}}>&=&n_{x_{f}}\vert n_{x_{b}}, n_{x_{f}}>,
\era
\eeq
$n_{x_{b}}$ and $n_{x_{f}}$ are the numbers of bosonic anyons and fermionic anyons respectively on $x$, with
\beq
\bra{rl}
n_{x_{b}}\in\bf{N}\rm,& n_{x_{f}}=0,1
\era
\eeq
in our SUSY we assumed that we have only one bosonic and one fermionic anyon on the site $x$. Then $n_{x_{f}}=1$. In this case we remark that
\beq
(-1)^{-N_{f}(x)}\Longrightarrow -1
\eeq 
and the corresponding Hamiltonian will be 
\beq
H(x)=[P^{-1}+P][N_{b}(x)+[N_{f}(x)].
\eeq 
\hspace*{.3in}Yet, let us pass to examine the operator $P$ (see Eq.$(21)$). Always, in the same context, where we have only one bosonic anyon and one fermionc anyon on the site $x$. For the other sites there are no particles. Then the eigenvalues of the number operators $N_{b}(z)$ and $N_{f}(z)$ are zero ($z\neq x$)
\beq
n_{z_{b}}=0=n_{z_{f}}
\eeq 
for this case
\beq
P\Longrightarrow \bf{1}\rm
\eeq 
and the equation $(30)$ becomes
\beq
H(x)=2[B^{\dag}(x)B(x)+F^{\dag}(x)F(x)].
\eeq 
Such as 
\beq
\bra{rl}
\lbrack B(x),B^{\dag}(x)\rbrack = 1,& \lbrace F(x),F^{\dag}(x)\rbrace = 1.
\era
\eeq
This FSUSY Hamiltonian operator can be written as the sum 
\beq
H(x)=H_{b}(x)+H_{f}(x),
\eeq 
such as $H_{b}(x)$ is the Hamiltonian of the bosonic anyon and $H_{f}(x)$ is the Hamiltonian of the fermionic anyon , with
\beq
\bra{rcl}
H_{b}(x)&=&2[B^{\dag}(x)B(x)+\frac{1}{2}]\\
\\
H_{f}(x)&=&2[F^{\dag}(x)F(x)-\frac{1}{2}].
\era
\eeq
\hspace*{.3in}By considering the local operators with a repeated use of comultiplication. We define the global operators as follows
\beq
\bra{rcl}
H&\equiv& \sum\limits_{x\in \Om}\bf{1}\rm\ot...\ot\bf{1}\rm\ot H(x)\ot\bf{1}\rm\ot...\ot\bf{1}\rm\\
\\
B&\equiv& \sum\limits_{x\in\Om}\bf{1}\rm\ot...\ot\bf{1}\rm\ot B(x)\ot\bf{1}\rm\ot...\ot\bf{1}\rm\\
\\
B^{\dag}&\equiv& \sum\limits_{x\in\Om}\bf{1}\rm\ot...\ot\bf{1}\rm\ot B^{\dag}(x)\ot\bf{1}\rm\ot...\ot\bf{1}\rm\\
\\
F&\equiv& \sum\limits_{x\in\Om}\bf{1}\rm\ot...\ot\bf{1}\rm\ot F(x)\ot\bf{1}\rm\ot...\ot\bf{1}\rm\\
\\
F^{\dag}&\equiv& \sum\limits_{x\in\Om}\bf{1}\rm\ot...\ot\bf{1}\rm\ot F^{\dag}(x)\ot\bf{1}\rm\ot...\ot\bf{1}\rm.
\era
\eeq
Then 
\beq
H=2[B^{\dag}B+F^{\dag}F].
\eeq 
\hspace*{.3in}Always the two particles are defined on the two-dimensional lattice. Consequently, the notion of the fractional statistics will be hidden in the definition of the operators $B$, $B^{\dag}$, $F$ and $F^{\dag}$. It will be described by the introduction of the Chern-Simons field.
\section{The FSUSY Hamiltonian}
\hspace{.3in}Following Wilczek$^{\cite{13}}$ an anyon is a composite of (fictitious) electric charge $q$ and magnetic flux $\Phi$. Let us note here that this fictitious charge and flux have nothing to do with the ordinary electro-magnetism. But rather a new gauge field is introduced purely as a mathematical device, called a Chern-Simons gauge field. This kind of field given by the (fictitious) vector potential
\beq
\bra{rl}
A^{i}(x)=\frac{\Phi}{2\pi}\ep_{ij}\frac{x^{j}}{|x|^{2}},& i=1,2.
\era
\eeq 
can realizes a flux tube attached on a point particle. With $x=(x_{1},x_{2})$ on the plane (if we tend the lattice spacing to zero), and the corresponding flux $\Phi$ is given by
\beq
\oint A_{\ell}dx^{\ell}=\Phi
\eeq 
for any path winding around the associated particle in anti-clockwise direction. We add that there is no classical force since the magnetic field vanishes everywhere away from the origin where the point particle is localized.\\
\hspace*{.3in}Let us noting here that we can continue our investigation on the plane instead of the two-dimensional lattice without any ambiguities.\\
\hspace*{.3in}With the intention of making appear the fractional property of the statistics and the SUSY of our two different particles system. We are going to use the complex notation to simplify the calculation in investigating the Hamiltonian of one bosonic anyon. We can write
\beq
\bra{rcl}
z= x^{1}+ix^{2},& \partial=\frac{1}{2}(\partial_{1}-i\partial_{2})\\
\\
\bar{z} =x^{1}-ix^{2},& \bar{\partial}=\frac{1}{2}(\partial_{1}+i\partial_{2})
\era
\eeq
with
\beq
\bra{rcl}
\partial_{1}\equiv\frac{\partial}{\partial x^{1}},& \partial\equiv\frac{\partial}{\partial z}\\
\\
\partial_{2}\equiv\frac{\partial}{\partial x^{2}},& \bar{\partial}\equiv\frac{\partial}{\partial \bar{z}}.
\era
\eeq
For the Chern-Simons field we put
\beq
\bra{rcl}
A=-\frac{1}{2}(A^{1}-iA^{2})=-i\frac{\Phi}{4\pi}\frac{1}{z}\\
\\
\bar{A} =-\frac{1}{2}(A^{1}+iA^{2})=i\frac{\Phi}{4\pi}\frac{1}{\bar{z}}.
\era
\eeq
Yet, in a straightfoward way, we obtain the following commutation relations
\beq
\bra{rcl}
\lbrack z, \partial\rbrack =1,& \lbrack \bar{z}, \bar{\partial}\rbrack =1,& \lbrack \bar{z}, \partial\rbrack =0=\lbrack z, \bar{\partial}\rbrack \\
\\
\lbrack \frac{1}{z}, \partial\rbrack =-\frac{1}{z^2},& \lbrack \frac{1}{\bar{z}}, \bar{\partial}\rbrack =-\frac{1}{\bar{z}^2},& \lbrack \frac{1}{z}, \bar{\partial}\rbrack =\lbrack \frac{1}{\bar{z}}, \partial\rbrack =\pi\de^{(2)}(z).
\era
\eeq
\hspace*{.3in}In the complex notation again, let us start by the definition of some creation and annihilation operators denoted $b^{\da}$ and $b$ respectively. These new operators can be written as follows
\beq
\bra{rcl}
b^{\da}&=&\frac{\Phi}{\sqrt{8}\pi}q_{b}\frac{1}{z}-\sqrt{2}\partial+\frac{1}{\sqrt{2}}\bar{z} \\
\\
b&=&\frac{\Phi}{\sqrt{8}\pi}q_{b}\frac{1}{\bar{z}}-\sqrt{2}\bar{\partial},
\era
\eeq
which satisfy the usual commutation relation 
\beq
\lbrack b, b^{\da}\rbrack =1.
\eeq
Such as, owing to the equation $(44)$
\beq
\bra{rcl}
b^{\da}b&=&\frac{\Phi^2}{8\pi^2}q_{b}^2\frac{1}{z\bar{z}}-2\partial\bar{\partial}+\bar{z}=\bar{z}\partial+\frac{\Phi}{4\pi}\\
\\
bb^{\da}&=&\frac{\Phi^2}{8\pi^2}q_{b}^2\frac{1}{\bar{z} z}-2\bar{\partial} \partial+=\partial\bar{z}+\frac{\Phi}{4\pi}.
\era
\eeq
Thus, $q_{b}$ is the charge of bosonic and $\Phi$ is its associated flux.\\
\hspace*{.3in}From these operators, we can define our operators $B^{\da}$ and $B$ which appear in the Hamitonian $H_{b}$ as creation and annihilation bosonic anyon operators. Then we assume the following definition
\beq
\bra{rcl}
B^{\da}B+\frac{1}{2}&=&\frac{1}{2}[b^{\da}b-\bar{z}\bar{\partial}-\frac{\Phi}{4\pi}]=\frac{1}{2}[\frac{\Phi^2}{8\pi^2}q_{b}^2\frac{1}{z\bar{z}}-2\partial\bar{\partial}]\\
\\
BB^{\da}+\frac{1}{2}&=&\frac{1}{2}[bb^{\da}-\bar{\partial}\bar{z}-\frac{\Phi}{4\pi}]=\frac{1}{2}[\frac{\Phi^2}{8\pi^2}q_{b}^2\frac{1}{\bar{z} z}-2\bar{\partial}\partial],
\era
\eeq
which lead to the usual commutation reltion
\beq
\lbrack B, B^{\da}\rbrack =1.
\eeq
\hspace*{.3in}Consequently, the Hamiltonian $H_{b}$ can be written as
\beq
\bra{rcl}
H_{b}&=&2[B^{\da}B+\frac{1}{2}]\\
\\
&=&q_{b}^2\frac{\Phi^2}{8\pi^2}\frac{1}{z\bar{z}}-2\partial\bar{\partial}.
\era
\eeq
Also it is very easy to verify that, in cartesian coordinates, the Hamiltonian $H_{b}$ take the following expression
\beq
H_{b}=-\frac{1}{2}(\bf{\nabla}\rm-iq\bf{A}\rm)^{2},
\eeq 
in taking account of the equations $(40)$, $(41)$, $(42)$, $(43)$, $(46)$, $(47)$ and $(48)$.\\
\hspace*{.3in}Otherwise, for the fermionic anyon system the same proposition and the definition given by Wilczek can be associated to it. Again once the fermionic anyon is a composite of the charge $q_{f}$ and the flux $\Phi$.\\
\hspace*{.3in}According to the relations $(36)$ and $(37)$, the associated Hamiltonian $H_{f}$ reads
\beq
H_{f}=2[F^{\da}F-\frac{1}{2}].
\eeq
In terms of the Pauli matrices
\beq
\bra{ccc}
\sigma_{1}=\pmatrix{0&1\cr 1&0\cr},& \sigma_{2}=\pmatrix{0&-i\cr i&0\cr},& \sigma_{3}=\pmatrix{1&0\cr 0&-1\cr}  
\era
\eeq
we define the fermionic anyon operators as follows
\beq
\bra{rcl}
F^{\da}&=&\frac{1}{\sqrt{2}}(\sigma_{1}+i\frac{q_{f}\Phi}{2}\de^{(2)}(z)\sigma_{2})\\
\\
F&=&\frac{1}{\sqrt{2}}(\sigma_{1}-i\frac{q_{f}\Phi}{2}\de^{(2)}(z)\sigma_{2})
\era
\eeq
which satisfy
\beq
\lbrace F, F^{\da}\rbrace =1+\frac{q_{f}^{2}\Phi^{2}}{4}\de^{(2)}(z).
\eeq
Then the relation $(52)$ becomes
\beq
H_{f}=q_{f}\Phi\de^{(2)}(z)\sigma_{3}+\frac{q_{f}^{2}\Phi^{2}}{4}\de^{(2)}(z).
\eeq
Let us assume, in this context, that $\Phi$ is a weak flux; i.e. $\Phi\ll 1$. Then we can stop at the first order of $\Phi$ in our results as
\beq
\bra{rcl}
\lbrace F, F^{\da}\rbrace =1,\\
\\
H_{f}=q_{f}\Phi\de^{(2)}(z)\sigma_{3}.
\era
\eeq
\hspace*{.3in}Then the FSUSY Hamiltonian, will be explicitely read
\beq
H=\frac{1}{2}(\bf{\nabla}\rm-iq\bf{A}\rm)^{2}+q_{f}\Phi\de^{(2)}(z)\sigma_{3}.
\eeq
From this equation, it is very clear that the FSUSY Hamiltonian $H$ describes a system consisting of one bosonic anyon and one fermionic anyon. Such as the two kinds of particles are associated to a Chern-Simons gauge field $\bf{A}\rm$. Then, in this SUSY there are non-trivial property for the corresponding statistics. This system is characterized by two statistical parameters which can be defined for the bosonic anyonic system and the fermionic ones respectively as
\beq
\bra{rl}
\nu_{b}=\frac{q_{b}\Phi}{2},& \nu_{f}=\frac{q_{f}\Phi}{2}.
\era
\eeq
Consequently, the FSUSY Hamiltonian, in this context, is well defined
for one system characterized by fractional statistics.
\section{Conclusion}
\hspace{.3in}To summarize we can say that, in this paper, we have
discussed the construction of anyonic operators and its algebras, on the
two-dimensional lattice, from bosonic, fermionic operators and quonic
ones as a general case. We have used for this subject the Jordan-Wigner
transformation$^{\cite{11}}$ basing on the references
\cite{10,18,19,24,25}.\\
\hspace*{.3in}By using the quonic anyons we have thus recalled the FSUSY
construced in the work \cite{10} on the two-dimensional lattice. Where
the supercharges have defined by associating two different kinds of
quonic anyons with different deformation parameters. As a particular
case, we have tended the deformation parameters to $\pm 1$. Then we have
obtained one FSUSY Hamiltonian for a system consists of one bosonic
anyon and one fermionic anyon.\\  
\hspace*{.3in}Finally, we have given this FSUSY Hamiltonian in the
connexion with the Chern-Simons gauge theory. Such as the fractional
property of the statistics of the corresponding system was clear.
Furthermore, let us note that the SUSY of this example can be enlarged
to different quonic anyons with non-trivial interaction which will be
mentioned in the connexion with the Chern-simons gauge theory. That is
our subject to discuss in our future paper \cite{26}.

\section*{Acknowledgments} 
\hspace*{.3in}The author J. Douari would like to thank the Abdus Salam
International Centre for Theoretical Physics, Trieste, for link
hospitality during the stage in which this work was achieved within the
framework of the Associateship Scheme. She would like also to thank
Prof. G. Thompson for hepful discussions and comments.


\begin{thebibliography}{21}
\bibitem{1}A. Salam and J. Strathdee, Nucl. Phys. B\bf{76}, \rm{477 (1974)}; Fortshr. der Physik \bf{26}, \rm{57 (1978)}.
\bibitem{2}S. Ferrara, J. Wess and B. Zumino, Phys.Lett. B\bf{21}, \rm{239} (1974).
\bibitem{3}H. Nicolai, J. Phys. A\bf{9}, \rm{1497} (1976).
\bibitem{4}E. Witten, Nucl. Phys. B\bf{188}, \rm{513 (1981)}.
\bibitem{5}L. Baulieu and E. G. Floratos, Phys.Lett. B\bf{258}, \rm{171} (1991).
\bibitem{6}C. Ahn, D. Bernard and A. Leclair, Nucl. Phys. B\bf{346}, \rm{409 (1990)}.
\bibitem{7}R. Kerner, J. Phys. A\bf{33}, \rm{403} (1992).
\bibitem{8}A. T. Filippov, A. P. Isaev and R. D. Kurdikov, Mod. Phys. Lett, A\bf{7}, \rm{2129 (1992)}.
\bibitem{9}M. Daoud and Y. Hassouni, Prog. Theor. Phys., \bf{96}, \rm{1033 (1997)}.
\bibitem{10}M. Daoud, J. Douari and Y. Hassouni, preprint LPT-ICAC, submitted in J. Phys. A
\bibitem{11}P. Jordan and E. P. Wigner, Z. Phys. \bf{47}, \rm{631} (1928).
\bibitem{12}J. M. Leinaas and J. Myrheim, Nuovo Cimento B\bf{37}, \rm{1} (1977).
\bibitem{13}F. Wilczek. Phys. Rev. Lett. \bf{48}, \rm{1144} (1982).
\bibitem{14}R. Jackiw, Phys. Rev. D\bf{42}, \rm{3500} (1990).
\bibitem{15}A. Lerda, Anyons: Quantum Mechanics of Particles with Fractional Statistics (Springer-Verlag, Berlin, Germany, 1992).
\bibitem{16}J. M. Leinaas, "Symmetry and Structural Proporties of Condensed Matter", Poznan1992 (World Sci., Singapore, 1993).
\bibitem{17}A. Khare, "Fractional statistics and quantum theory", (World Sientific, Singapore (1997)).
\bibitem{18}A. Lerda, S. Sciuto, Nucl. Phys. B\bf{401}, \rm{613 (1993)}.
\bibitem{19}M. Frau, M. A R-Monteiro and S. Sciuto, J. Phys. A:Math. Gen. \bf{27}, \rm{801} (1994).
\bibitem{20}E. Fradkin, Phys. Rev. Lett. \bf{63}, \rm{322} (1989).
\bibitem{21}M. L\"uscher, Nucl. Phys. B\bf{326}, \rm{557 (1989)}.
\bibitem{22}D. Eliezer and G. W. Semenoff, Phys. Lett. B\bf{266}, \rm{375} (1991).
\bibitem{23}V. F. M\"uller, Z. Phys. C\bf{47}, \rm{301 (1990)}.
\bibitem{24}L. Frappat, A. Sciarrino, S. Sciuto and P. Sorba, Phys. Lett. B\bf{369}, \rm{313} (1996).
\bibitem{25}M. Daoud, J. Douari and Y. Hassouni, preprint IC/98/132
\bibitem{26}J. Douari and Y. Hassouni, in preparation.
\end{thebibliography}
\end{document}

