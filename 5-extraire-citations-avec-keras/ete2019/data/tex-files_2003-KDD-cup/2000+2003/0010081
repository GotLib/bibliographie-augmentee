
\date{December 15, 2000}


%\documentclass[12pt]{article}
%\setlength{\textwidth}{16cm}
%\setlength{\textheight}{22cm}
%\hoffset=-1.30cm			%-1.50cm
%\voffset=-1.50cm			%-0.00cm

\documentstyle[aps,twocolumn]{revtex}
%\documentstyle[aps,preprint]{revtex}

\begin{document}

%\baselineskip=20pt

\def\no{\noindent}
\def\be{\begin{eqnarray}}
\def\ee{\end{eqnarray}}
\def\non{\nonumber}


\title{BFFT formalism applied to the minimal chiral Schwinger model}

\author{C. P. Natividade,$^{1,2}$\thanks{e-mail: cesar@if.uff.br} 
H. Boschi-Filho$^3$\thanks{e-mail: boschi@if.ufrj.br} and 
L. V. Belvedere$^{1}$\thanks{e-mail: lvb@if.uff.br}}

\address{
$^1$Instituto de F\'\i sica, Universidade Federal Fluminense\\
Avenida Litor\^anea s/n, Boa Viagem, Niter\'oi, 24210-340 Rio de Janeiro, 
Brazil}

\address{
$^2$Departamento de F\'\i sica e Qu\'\i mica, 
Universidade Estadual Paulista\\Avenida Ariberto Pereira da Cunha 333, 
Guaratinguet\'a, 12500-000 S\~ao Paulo, Brazil}

\address{
$^3$Instituto de F\'\i sica, 
Universidade Federal do Rio de Janeiro\\Caixa Postal 68528, 
Rio de Janeiro, 21945-970 Rio de Janeiro, Brazil}


\maketitle
\begin{abstract}
The minimal chiral Schwinger model is discussed from the 
Batalin-Fradkin-Fradkina-Tyutin point of view. 
The conversion of second-class constraints to first-class ones 
results in an extended gauge-invariant theory which is equivalent 
for $a=2$ to the vector Schwinger model at the Lagrangian level.
Here, we present arguments which show that such equivalence does
no exist at the operatorial level.
\end{abstract}


%\vfill


%\noindent PACS: \par
%\noindent Keywords: 



%\pagebreak

\section{Introduction}
\renewcommand{\theequation}{1.\arabic{equation}}
\setcounter{equation}{0}

Following Dirac's conjecture a critical point in the study of 
a gauge model is the presence of first-class constraints \cite{Dirac}. 
First-class constraints are related to symmetries while the 
second-class ones may imply some ambiguities when treated 
as quantum operators. The physical status of a theory is chosen 
by imposing complementary conditions
which are given by the first-class constraints.
The presence of second-class constraints is in general avoided.
There are many procedures which allow the exclusion of these 
constraints from the effective action \cite{Henneaux}.
One of them is the Batalin-Fradkin-Fradkina-Tyutin (BFFT) method 
\cite{Batalin}, which converts second-class constraints into 
first-class ones by introducing auxiliary fields. 
The BFFT formalism has been employed in different physical systems, 
as for example, the chiral boson model \cite{Amorim}, 
the massive Maxwell and Yang-Mills theories \cite{Fujikawa,Banerjee97}, 
the CP$^{N-1}$ model \cite{Banerjee94}, 
the chiral Schwinger model \cite{Kim97,Park98}
and more recently a fluid field theory \cite{NB00}.
As expected, the implementation of the BFFT method through the 
introduction of new fields gives rise to a kind of a Wess-Zumino term
which turns the resulting extented theory gauge invariant.
In particular, an elegant way of obtaining the Wess-Zumino term and 
the effective action is the BRST-BFV procedure \cite{Henneaux85}. 


On the other side, 
two dimensional models have played an important role in theoretical
physics as a laboratory where many interesting phenomena can be studied
in a fashion which is usually easier to handle than more realistic
four dimenional theories \cite{AAR}. One well known  model is the 
two dimensional quantum electrodynamics which was introduced long ago 
by Schwinger \cite{Schwinger} to discuss dynamical mass generation 
for gauge fields without breaking the gauge symmetry \cite{Swieca}.
More recently, Jackiw and Rajaraman proposed a model \cite{Rajaraman} 
with a chiral coupling between the two dimensional gauge and fermion 
fields mimicing the weak interactions of the standard model. 
This two dimensional model, known as the chiral Schwinger model, 
happens to be gauge anomalous although unitary and carries 
an arbitrary regularization parameter $a$ in all of its physical 
quantities (mass, propagator, etc) \cite{Carvalhaes}. 
Another interesting two dimensional chiral model is the one that 
describes right or left movers, {\sl i. e.}, chiral bosons 
which were introduced by Siegel \cite{Siegel} inspired on the 
heterotic string and later reobtained by Floreanini and Jackiw 
\cite{Floreanini}.
In particular, Harada \cite{Harada} considered a version 
of the chiral Schwinger model but without the right-handed fermions.
He showed that this model known as the minimal chiral Schwinger model
corresponds to a gauged version of the Floreanini-Jackiw chiral boson. 
Naturaly, this model share some properties with the 
complete chiral model as the gauge anomaly and the depedence
on an arbritrary parameter (which is usually called $a$, as in the original
Jackiw-Rajaraman model) and the novelty here is the description of chiral bosons
which in some sense represents the motion of superstrings.
Furthermore, a left-handed Wess-Zumino action has been built for this model 
by considering an antichiral constraint \cite{Kye,Dutra}.
 

In this paper we discuss the Hamiltonian formalism for the minimal 
chiral Schwinger model using the BFFT method. 
The application of this method introduces in a very natural way 
the chiral constraints in the model. Otherwise, one would be forced
to put these constraints by hand.
Then, using the BRST-BFV procedure we obtain the Wess-Zumino term 
with a set of the first-class constraints and total effective action. 
As a last step, we are going to discuss the gauge invariance of the 
model in the limit of $a=2$. As we shall see, this limit in the 
present model does not physically represent the (vector) Schwinger 
model in respect to $\theta$-vacua states, which is a consequence 
of the gauge invariance at the operator level.
This result is in contrast to the equivalence 
advocated in the literature \cite{Kye,Carena}. 
 

This paper is organized as follows: In section II we discuss the conversion
of the second-class to first-class constraints by using the BFFT method for
the miminal chiral Schwinger model (MCSM). Then, in section III we construct
the BRST Hamiltonian and the gauge invariant effective action which brings 
in a Wess-Zumino term. Finally, in section IV we present our conclusions
and give arguments contrary to the equivalence at the operatorial level 
of the miminal model with the Wess-Zumino term to the vector Schwinger 
model. An appendix is also included where we present some details on the 
calculation of the extended canonical Hamiltonian. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Quantization}
\renewcommand{\theequation}{2.\arabic{equation}}
\setcounter{equation}{0}

In order to implement the canonical BFFT scheme, it is necessary
to specify the Hamiltonian together with the set of the constraints
of the model \cite{Batalin}. 
Here we are going to apply the general BFFT method following the lines
of work in Refs. \cite{Fujikawa,Banerjee97}. 

Let us start by considering the bosonized version of the minimal
chiral Schwinger model \cite{Harada}. Such Lagrangian density is
given by\footnote{This Lagrangian coincides with the expression (1) 
from Kye {\sl et al} \cite{Kye} without the Wess-Zumino term, but 
only coincides with expression (1) from Dutra \cite{Dutra}, 
if we pick up the metric $g^{\mu\nu}=g_{\mu\nu}={\rm diag}(-,+)$, 
which is the opposite of the metric of Bjorken and Drell 
\cite{BjorkenDrell} or Jackson \cite{Jackson}.}

\be
{\cal L} &=& \dot\phi\phi^\prime - (\phi^\prime)^2 
		+ 2 e \phi^\prime (A_0-A_1) - \frac 12 e^2 (A_0-A_1)^2
\non\\		
	& &	+ \frac 12 a e^2 (A_0)^2 - \frac 12 a e^2 (A_1)^2
		- \frac 14 F_{\mu\nu}F^{\mu\nu},
\ee

\no where $a\not=1$, overdot means partial time derivative 
($\dot\phi=\partial_0\phi=\partial\phi/\partial t=\partial^0\phi$) 
and prime denotes partial space derivative 
($\phi^\prime=\partial_1\phi=\partial\phi/\partial x^1
=-\partial^1\phi$). 
The canonical momenta are given by
\be
&&\Pi^\mu = \frac{\partial{\cal L}}{\partial \dot{A}_\mu}
= F^{\mu0};
\\
&&\Pi_\phi = \frac{\partial{\cal L}}{\partial \dot{\phi}}
=\phi^\prime ,
\ee

\no which imply the primary constraints
\be
&&\Omega_1 = \Pi^0 \approx 0;
\label{Omega1}
\\
&&\Omega_2 = \Pi_\phi -\phi^\prime \approx 0.
\label{Omega2}
\ee

\no Then, the corresponding canonical Hamiltonian is 
\be
H_c &=& \int {\rm d}x^1 \Big\{ \frac 12 (\Pi^1)^2 + (\phi^\prime)^2 
	- 2 e \phi^\prime (A_0-A_1) 
\non\\
&&	\qquad
	+ \frac 12 e^2 (A_0-A_1)^2
	- \frac 12 a e^2 (A_0)^2
\non\\
&&	\qquad 
	+ \frac 12 a e^2 (A_1)^2 
	- \Pi^1 \partial_1 A_0 \Big\},
\label{Hc}
\ee

\no where we are using the conventions\footnote{The electric 
field is given by $\vec E=-\nabla A^0-\partial\vec A/\partial t$, 
with the speed of light $c=1$.
In four dimensional Minkowski space-time one puts $\mu=0,1,2,3$. 
Other conventions are:
$\partial^\mu=\frac{\partial}{\partial x_\mu}=
(\frac{\partial}{\partial x^0},-\nabla)$,
$\partial_\mu=\frac{\partial}{\partial x^\mu}=
(\frac{\partial}{\partial x^0},\nabla)$,
$A^\mu=(A^0,\vec A)$, $A_\mu=(A^0,-\vec A)$, 
$\partial^\mu A_\mu=\partial_\mu A^\mu =
\frac{\partial A^0}{\partial x^0}+\nabla\cdot\vec A$.
Here, in two dimensions, we use the same definitions but $\mu=0,1$.
We take, as in Bjorken and Drell,
$F^{\mu\nu}=\partial^\nu A^\mu - \partial^\mu A^\nu$, 
which is the opposite convention
of Jackson. Note also that $F^{0\mu}=-F_{0\mu}$.}
$F^{10}=\Pi^1= - E^1=\partial^0 A^1 -\partial^1 A^0  
=\partial_1 A_0 - \dot{A_1}$. 
Time conserving of the primary constraints lead to the Gauss Law
\be
\Omega_3 	&=& \dot{\Omega}_1=\left\{\Pi^0,H_c\right\}
\non\\
		&=& \partial_1 \Pi^1 - 2 e \phi^\prime - (a-1) e^2 A_0 - e^2 A_1
		\approx 0.
\label{Omega3}
\ee

\no 
Now, we note that the system given by the Poisson brackets 
$\{\Omega_1,\Omega_3\}$ constraints is second-class. 
The time evolution of $\Omega_3$ does not lead to any new 
constraints but determines the Lagrange multiplier of the 
$\Omega_1$ constraint. So, the algebra of the constraints is given 
by a set $\{\Omega_j\}$ which can be determined using the BFFT scheme. 
In order to simplify this procedure we shall implement the constraints 
$\Omega_j=0$ strongly by introducing Dirac brackets \cite{Banerjee97}. 
Through the Dirac's procedure we have that
\be
\left\{\Omega_i,\Omega_j\right\}^D=0,
\ee

\no and the remaining
\be
\left\{\chi_i(x),\chi_j(y)\right\}^D=\Delta_{ij}(x,y),
\ee

\no where we defined $\chi_1=\Omega_1$ and $\chi_2=\Omega_3$,
from now on $x\equiv x^1$, $y\equiv y^1$ and 
\be
\label{Delta}
\Delta_{ij}(x,y)
=
\left(
\begin{array}{cc}
0 		& e^2(a-1)\\
-e^2(a-1) 	& 2e^2\partial_{x}
\end{array}
\right)
\delta(x-y).
\ee

In order to reduce the second-class system to a first-class
one, we begin by extending the phase space including the new fields
$\theta_i(x)$ which satisfy the algebra:
\be
\left\{\theta_i(x),\theta_j(y)\right\}^D=-\epsilon_{ij}\delta(x-y),
\ee

\no where $\epsilon^{12}=-\epsilon_{12}=+1$.

The first-class $\tilde\chi_i$ are now constructed as power series
\cite{Fujikawa,Banerjee97}
\be
\label{tildechi}
\tilde\chi_i = \chi_i + \sum_{n=1}^\infty \chi_i^{(n)},
\ee

\no where $\chi_i^{(n)}$ are homogeneous polynomials of order $n$ 
in the auxiliary fields $\theta_i(x)$, 
to be determined by the requirement that the 
constraints $\tilde\chi_i$ be strongly involutive
\be
\left\{\tilde\chi_i(x),\tilde\chi_j(y)\right\}^D=0.
\ee

The first-order correction for the expression (\ref{tildechi}) can be
written as
\be
\label{tildechi2}
\tilde\chi_i = \chi_i + \int {\rm d}y\ \sigma_{ij}(x,y)\ \theta_j(y),
\ee

\no where the quantities $\sigma_{ij}(x,y)$ are implicitly defined by 
\be
\Delta_{ij}(x,y)=\int {\rm d}z\ {\rm d}z^\prime \sigma_{ik}(x,z) 
\epsilon_{kl}\sigma_{jl}(z^\prime,y),
\ee

\no with $\Delta_{ij}(x,y)$ given by eq. (\ref{Delta}).
By performing the calculations and choosing $\sigma_{ij}(x,y)$ such
that $\tilde\chi_i$ are linear in the fields $\theta_i(x)$, 
we obtain
\be
\label{sigma}
\sigma_{ij}(x,y)
=
\left(
\begin{array}{cc}
1		&  0        \\
\frac1{a-1}\partial_{x}  & -e^2(a-1)
\end{array}
\right)
\delta(x-y).
\ee

\no Consequently, we get
\be
&&\tilde\chi_1=\chi_1+\theta_1(x)
\non\\
&&\tilde\chi_2=\chi_2+\frac 1{a-1}\partial_{1}\theta_1 
- e^2(a-1)\theta_2,
\ee

\no which are first-class. 
The above set permit us to compute the extended
first-class Hamiltonian:
\be
\tilde H = \sum_{n=0}^\infty H^{(n)},
\ee

\no where $H^{(n)}\sim\theta_n$, with the subsidiary condition 
$H^{(0)}=H_c$. 
The general expression for the iterated Hamiltonian $H^{(n+1)}$ 
is given as a recurrence relation as in Refs. 
\cite{Fujikawa,Banerjee97}:
\be
H^{(n+1)}&=& - \frac {1}{n+1} \int {\rm d}x\ {\rm d}y\ {\rm d}z\ 
\theta_\alpha(x) 
\non\\
&&\qquad\qquad\times
\left(\omega_{\alpha\beta}\right)^{-1}
\left(\sigma_{\beta\gamma}\right)^{-1}
G_\gamma^{(n)},
\ee

\no where
\be\label{Gn}
G_\gamma^{(n)}=\left\{\chi_\gamma,H^{(n)}\right\}.
\ee

\no Here, we mention that $\theta_n=0$, for $n\geq 2$. 
Since $(\omega_{\alpha\beta})^{-1}$ and 
$(\sigma_{\beta\gamma})^{-1}$
are proportional to Dirac delta functions and considering the 
Eq. (\ref{Hc}), we get
\be
G_1^{(0)}&=&\left\{\chi_1,H_c^{(0)}\right\}
\non\\
&=& \chi_2
\non\\
G_2^{(0)}&=&\left\{\chi_2,H_c^{(0)}\right\}
\non\\
&=& e^2\left[(a-1)\partial_{1}A_1 - \Pi^1\right].
\ee

\no Now, by using the shift in the fields 
\be
\label{shift1}
\theta_1(x) &\longrightarrow & e(a-1)\theta
\\
\label{shift2}
\theta_2(x) &\longrightarrow & \frac 1{e(a-1)}\Pi_\theta,
\ee

\no we obtain the first-order correction for the canonical Hamiltonian
\be
H_c^{(1)} &=& - \int {\rm d}x \Big\{
\frac 1{e(a-1)}\left(\theta^\prime + \Pi_\theta\right)\chi_2
\non\\
&&\qquad\quad
+\left[(a-1)\partial_1 A_1 - \Pi^1\right]\theta \Big\},
\label{Hc1}
\ee

\no where $\theta^\prime\equiv\partial_1\theta$.

Following the same steps leading to the first-order corrections
we obtain the second-order Hamiltonian (see the Appendix A)
\be
H_c^{(2)} &&= - \frac 12 \int {\rm d}x \left[ \frac 1{a-1}(\Pi_\theta)^2
		- \beta (\theta^\prime)^2 + e^2 \theta^2 \right],
\non\\&&
\label{Hc2}
\ee

\no with $\beta=(a-1)+(a-1)^{-1}$. 

Putting together the results from Eqs. (\ref{Hc}), (\ref{Hc1})
and (\ref{Hc2}) we find the extended Hamiltonian
\be
\tilde H_c = H_c^{(0)} + H_c^{(1)} + H_c^{(2)},
\ee

\no which is strongly involutive with respect to the constraints
$\tilde\chi_i(x)$. On the other hand, an inspection of the complete
set of constraints reveals that
\be
\left\{\tilde\chi_i,\tilde\chi_j\right\}=0,
\ee

\no with $i,j=1,2,3$. These results clearly iluminate the 
first-class nature of the system. One can then calculate
the effective action which should be gauge invariant, as we are 
going to derive it in the following section.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{BRST Hamiltonian and Effective Action}
\renewcommand{\theequation}{3.\arabic{equation}}
\setcounter{equation}{0}

We now pass to construct the effective action through the BRST-BFV 
formalism \cite{Henneaux85}. This method permit us to obtain
the effective action in a direct way by including Lagrange multipliers
and ghost fields with the corresponding canonical momenta and a gauge
fixation function which together with the BRST charge operator generate the
terms that lead to the expected gauge invariant action.

Therefore, following the usual BFV prescription and 
considering the Eqs. (\ref{Hc1}), (\ref{Hc2}) the effective
action can be written as
\be
S_{eff}&=&\int {\rm d}x \Big\{\Pi^0\dot A_0 -\Pi^1\dot A_1 
	+ \Pi_\phi\dot\phi
	+ \Pi_\theta\dot\theta - {\cal H}_c^{(0)}
\non\\	
&&\qquad\qquad
	+\frac 1{e(a-1)}(\Pi_\theta+\theta^\prime)\chi_2
\non\\	
&&\qquad\qquad
	 -e\left[(a-1)\partial_1A_1+\Pi^1\right]\theta
\non\\	
&&\qquad\qquad
	-\frac 1{2(a-1)}(\Pi_\theta)^2
\non\\	
&&\qquad\qquad
	+\frac 12\beta(\theta^\prime)^2
	-\frac 12e^2\theta^2\Big\}
\non\\	
&&+\int {\rm d}x \left[\dot\lambda_a p_a + \bar{\cal P}_a \dot c_a
	+{\dot{\bar c}}_a {\cal P}_a +\left\{\Psi,Q\right\}\right],
\label{Seff}
\ee

\noindent where $(c_a,\bar{{\cal P}_a})$ and 
$({\cal P}_a,\bar{c_a})$ form a pair of canonical ghost-antighost 
with opposite Grassmanian parity
\be
\{c_a(x),\bar{{\cal P}_b}(y)\}=\{{\cal P}_a(x),\bar{c_b}(y)\}
=\delta_{ab}\delta(x-y),
\ee

\no while $(\lambda_a,p_a)$ is a canonical Lagrange multiplier set 
\be
\{\lambda_a(x),p_b(y)\}=\delta_{ab}\delta(x-y).
\ee

\no The charge operator $Q$ is defined as
\be
Q=c_a\tilde\chi_a +p_a{\cal P}_a,
\ee

\no with $\tilde\chi_a$ being the first-class constraints, as
discussed in the previous section.
Finally, the fermion operator $\Psi$ is
\be
\Psi={\bar c}_a\alpha_a + {\bar{\cal P}}_a\lambda_a,
\ee

\no where $\alpha_a$ are the Hermitian gauge-fixing functions.
Different choices of the gauge functions $\alpha_a$ can be done
in order to obtain the effective action. 
The partition function is then given by
\be
{\cal Z} = \int [{\rm d}\Sigma] e^{iS},\label{Z}
\ee

\no where [d$\Sigma]$ includes all the fields appearing in the action 
(\ref{Seff}).

Before going on, we can make the changes $\alpha_a\to\alpha_a/M$,
$p_a\to Mp_a$ and $\Sigma_a\to M\Sigma_a$, in such a way that
the Jacobian of this transformation is trivially equal to the unity. 
One can then verifies that in the limit 
$M\to 0$, the action is independent of ghost and antighost fields.

Now, we can perform the choice of the gauge function and do 
some of the integrations implied in [d$\Sigma$].
So, we choose
\be
&&\alpha_1(x)=\Pi_\phi-\phi^\prime + \dot{\lambda}_1
\\
&&\alpha_2(x)=\Pi_\theta+\theta^\prime + e(a-1)A_0+eA_1
+\dot{\lambda}_2.
\ee

Since the first-class constraints are
\be
\tilde{\chi_1}&=&\chi_1+e(a-1)\theta
\\
\tilde{\chi_2}&=&\chi_2+e(\theta^\prime-\Pi_\theta),
\ee

\no where $\chi_1\equiv\Omega_1$ and $\chi_2\equiv\Omega_3$ 
given by Eqs. (\ref{Omega1}), (\ref{Omega2}) and (\ref{Omega3}), 
we can compute the Poisson bracket
\be
\{\Psi,Q\} &=& -[\Pi_\phi-\phi^\prime + \dot{\lambda}_1]p_1
\non\\
&&		-[\Pi_\theta+\theta^\prime + e(a-1)A_0+eA_1 
		+\dot{\lambda}_2]p_2
\non\\
&&		-[\Pi_0+e(a-1)\theta]\lambda_1
\non\\
&&		-[\chi_2+e(\theta^\prime-\Pi_\theta)]\lambda_2.
\label{PsiQ}
\ee

The dynamical terms $\dot{\lambda}_ap_a$ which appear in Eq. (\ref{PsiQ}) 
are cancelled by the similar ones in the original action. After integrations
over $(p_1,p_2)$ and $(\lambda_1,\lambda_2)$ fields we arrive at the delta 
functionals
$\delta(\Pi_\theta+\theta^\prime+e(a-1)A_0+eA_1)$,
$\delta(\Pi_\phi-\phi^\prime)$,
$\delta(\Pi_0+e(a-1)\theta)$ and
$\delta(\chi_2+e\theta^\prime-e\Pi_\theta)$ 
in the partition function (\ref{Z}).

By performing the integrations over $(\Pi_\phi,\Pi_\theta)$ and $(\Pi^0,\Pi^1)$
and noting that the Hamiltonian $H_c^{(0)}$ is quadratic in the $\Pi^1$-field, 
we get
\be
S_{eff}^{INV}=S_\phi+S_\theta,
\ee

\no where
\be
\label{Sphi}
S_\phi&=&	\int {\rm d}x \Big\{ \dot\phi\phi^\prime - (\phi^\prime)^2
		+ 2e\phi^\prime(A_0-A_1)
\non\\
&&\qquad	-\frac 12 e^2(A_0-A_1)^2
		+ \frac 12 e^2a[(A_0)^2-(A_1)^2]\Big\}
\non\\
&&
\\
\label{Stheta}
S_\theta&=&\int {\rm d}x \Big\{ -\dot\theta\theta^\prime 
		-\frac 12 \beta(\theta^\prime)^2
		+e\beta A_1\theta^\prime+2eA_0\theta^\prime
\non\\
&&\qquad	-\frac 12 \frac {e^2}{a-1}[(a-1)A_0+A_1]\Big\}.
\ee

\no As  can be seen, the sum of the above actions gives rise to an extended gauge 
invariant action. This could be achieved with the use of the first-class
constraints $\tilde\chi_1$, $\tilde\chi_2$, representing the gauge symmetry of the
model, introduced by the use of the BFFT method as discussed in the previous section. 
The generators of the symmetry transformations can be written as
\be
G=\int {\rm d}x \left( a_1\tilde\chi_1 + a_2\tilde\chi_2 \right),
\ee

\no where the coeffients $a_j$ are determined through the relations
$\{\chi_1,H_c\}=a_1\chi_1$ and $\{\chi_2,H_c\}=a_2\chi_2$ \cite{Galvao}. 
Now, from Eq. (\ref{Omega3}), we obtain
\be
\delta A_1 &=& \{A_1,G\}\epsilon 
\non\\
&=& - \frac 1e \partial_1\epsilon
\ee

\no and similarly $\delta\phi=\epsilon=-\delta\theta$.
Here, $\epsilon=\epsilon(x)$ is a gauge parameter.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Concluding Remarks}
\renewcommand{\theequation}{4.\arabic{equation}}
\setcounter{equation}{0}

We have obtained a gauge invariant version of the minimal chiral
Schwinger model (for $a\not=1$) by using the BFFT method.
Consequently, this gauge invariance might suggest that this version
of the model (possibly with a convenient choice of the Jackiw-Rajaraman 
regularization parameter $a$) has some correspondence with the 
vector Schwinger model (two dimensional massless QED), 
which is naturally gauge invariant, or in other words, 
is not gauge anomalous since its anomaly resides in the axial current.

Indeed, it has been argued by Kye {\sl et al.} \cite{Kye} that 
in the case $a=2$ there is a correspondence between the
model described by Eqs. (\ref{Sphi}), (\ref{Stheta}) and the vector
Schwinger model both at the Lagrangian and operator levels 
(see also \cite{Carena}).

However, we mention here that this equivalence holds {\sl only} 
at the Lagrangian level. In fact, it has been shown recently
by the present authors and a collaborator that such equivalence 
at the operatorial level does not exist \cite{Carvalhaes}.

This can be seen by using the machinery of the structural aspects 
of the algebra of the corresponding Hilbert space. 
The operatorial solution of the chiral Schiwnger model (CSM) 
shows that the ``cluster decomposition'' 
properties are {\sl not} violated by the Wightman functions that are 
representations of the intrinsic field algebra which defines the model, 
as one can learn in Ref. \cite{Swieca}. 
So, the $\theta$-vacuum parametrization suggested in Ref. 
\cite{Carena} becomes completly unnecessary. Of course, the suggested
equivalence of the CSM for $a=2$ and the vector Schwinger model can
not be stablished if we consider the Hilbert space in which the 
intrinsic field algebra of the model is represented. The equivalence
proposed in Refs. \cite{Kye,Carena} is  a consequence of an incorrect
decomposition of the closure of the Hilbert space that implied a 
choice of a field operator that {\sl does not belong} to the algebra which
represents the fermionic content of the model.

Finally, let us comment on the operatorial solution of the gauge non-invariant 
(GNI) and gauge-invariant (GI) formulations of the CSM \cite{Carvalhaes} and its 
relavance to the above discussion. 
There, we observe an isomorphism between the Wightman 
functions that are representations of the local field algebra generated
from the intrinsic field operators defining the GNI
formulation and the corresponding Wightman functions of the 
gauge transformed field operators defining the GI formulation.
This means that the Wess-Zumino (WZ) field enlarges the theory 
and replicates it, changing neither its algebraic structure nor its
physical content. Consequently, the conclusions about the ``cluster
decomposition'' and $\theta$-vacuum parametrization and the flaw 
equivalence between the CSM with $a=2$ and the VSM which were drawn for the 
GNI formulation (adding then the WZ terms) are the same for the GI formulation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\acknowledgements
The authors were partially supported  by CNPq 
-- Brazilian research agency. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Appendix A}
\renewcommand{\theequation}{A.\arabic{equation}}
\setcounter{equation}{0}

Here, we are going to calculate $H_c^{(2)}$, Eq. (\ref{Hc2}).
First of all, we give some details of the calculation of $G_1^{(1)}$ and 
$G_2^{(1)}$, which are necessary for determining $H_c^{(2)}$.  
From the definition of $G_\gamma^{(n)}$, 
Eq. (\ref{Gn}), we have
\be
G_1^{(1)}&=&\left\{\chi_1,H_c^{(1)}\right\}
\non\\
&=& \theta_2(x)\left\{\chi_1,-\chi_2\right\}
 \non\\
&& + \left(\partial_1\theta_1(x)\right)\ \frac 1{e^2(a-1)^2}
    \left\{\chi_1, e^2(a-1) A_0\right\}
\non\\
&=& - \frac 1{a-1}\left(\partial_1\theta_1(x)\right)
    - e^2(a-1)\theta_2(x).
\ee

\no Also,
\be
G_2^{(1)}&=&\left\{\chi_2,H_c^{(1)}\right\}
\non\\
&=& - \left(\partial_1\theta_1(x)\right)
		\frac 1{e^2(a-1)}
		\left\{\chi_2,\chi_2\right\}
\non\\
&&  - \theta_1(x)\frac 1{e^2(a-1)}
   \left\{\chi_2,e^2\left[(a-1)\partial_1A_1+\Pi^1\right]\right\}
\non\\
&&  - \theta_2(x)\left\{\chi_2,\chi_2\right\}
\non\\
&=& - \frac 2{(a-1)^2}\left(\partial_1\theta_1(x)\right)\partial_{1}
    - \theta_1(x)(\partial_{1})^2 
\non\\
&&  - \frac {e^2}{a-1}\theta_1(x)
    - 2e^2\theta_2(x)\partial_{1}.
\ee

\no Now, let us calculate $H_c^{(2)}$
\be
H_c^{(2)}
&=& - \frac 12 \int {\rm d}x\ \theta_1(x) \epsilon_{12} \sigma^{21} G_1^{(1)}
\non\\
&&  - \frac 12 \int {\rm d}x\ \theta_1(x)\epsilon_{12} \sigma^{22} G_2^{(1)}
\non\\
&&  - \frac 12 \int {\rm d}x\ \theta_2(x) \epsilon_{21} \sigma^{11} G_1^{(1)}
\ee

\no where $\sigma^{ij}=(\sigma_{ij})^{-1}$, with $\sigma_{ij}$ given by
Eq. (\ref{sigma}), so that
\be
(\sigma_{ij})^{-1}
&&=\left(
\begin{array}{cc}
1		&  0        \\
\frac 1{e^2(a-1)^2}\partial_{x}  & -\frac 1{e^2(a-1)}
\end{array}
\right)
\delta(x-y).
\non\\
&&\label{sigma-1}
\ee

\no Then, we have
\be
H_c^{(2)}
&=&-\frac 12\int{\rm d}x\ \theta_1(x) \frac 1{e^2(a-1)^2}\partial_x
		\Big[\frac 1{a-1}(\partial_1\theta_1(x))
\non\\
&&\qquad\qquad\qquad
		+(a-1)e^2\theta_2(x)\Big]
\non\\
&+&\int{\rm d}x\ \theta_1(x) \frac 1{e^2(a-1)}
		\Big[\frac 1{(a-1)^{2}}(\partial_1\theta_1(x))\partial_x
\non\\
&&\qquad\qquad
		+\frac 12\theta_1(x)(\partial_x)^2
		+\frac 12\frac 1{(a-1)e^2}\theta_1(x)
\non\\
&&\qquad\qquad
		+e^2\theta_2(x)\partial_x\Big]
\non\\
&+&\frac 12\int{\rm d}x\ \theta_2(x) 
	\Big[\frac 1{a-1}(\partial_1\theta_1(x))
\non\\
&&\qquad\qquad\qquad
	+(a-1)e^2\theta_2(x)\Big]
\ee

\no Finally, after some algebra and making the shift, 
Eqs. (\ref{shift1}), (\ref{shift2}) one arrives at 
$H_c^{(2)}$, Eq. (\ref{Hc2}).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{thebibliography}{}
%\begin{references}


\bibitem{Dirac} P. A. M. Dirac, Can. J. Phys. 2 (1950) 129;
``Lectures on Quantum Mechanics'', Yeshiva University, New York, 1964.

\bibitem{Henneaux} M. Henneaux and C. Teitelboim, 
``Quantization of Gauge Systems'', Princeton University Press,
Princeton, 1992.


\bibitem{Batalin} I. A. Batalin and E. S. Fradkin, 
Phys. Lett. {\bf B 180}, 157 (1986);
Nucl. Phys. {\bf B279}, 514 (1987);
I. A. Batalin, E. S. Fradkin and T. E. Fradkina, 
{\sl ibid.} {\bf B314}, 158 (1989); {\bf B323}, 734 (1989);
I. A. Batalin and I. V. Tyutin, 
Int. J. Mod. Phys. A {\bf 6}, 3255 (1991).

\bibitem{Amorim} R. Amorim and J. Barcelos-Neto, 
Phys. Lett. {\bf B 333}, 413 (1994); 
Phys. Rev. {\bf D 53}, 7129 (1996).

\bibitem{Fujikawa} T. Fujikawa, Y. Igarashi and J. Kubo,
Nucl. Phys. {\bf B341}, 695 (1990).

\bibitem{Banerjee97} R. Banerjee and J. Barcelos-Neto,
Nucl. Phys. {\bf B499} (1997) 453; Y. W. Kim and K. D. Rothe,
Nucl. Phys. {\bf B 511}, 510 (1998).

\bibitem{Banerjee94} N. Banerjee, R. Banerjee and S. Gosh,
Phys. Rev. {\bf D 49}, 1996 (1994).

\bibitem{Kim97} W. T. Kim et al, J. Phys. G {\bf 23}, 325 (1997).

\bibitem{Park98} M. I. Park, Y. J. Park and S. Yoon, 
J. Phys. G {\bf 24}, 2179 (1998).

\bibitem{NB00} C. P. Natividade and H. Boschi-Filho,
Phys. Rev. {\bf D 62}, 025016-1 (2000).

\bibitem{Henneaux85} M. Henneaux, Phys. Rep. {\bf 126}, 1 (1985).

\bibitem{AAR} E. Abdalla, M. C. B. Abdalla and K. D. Rothe,
``Non-perturbative methods in 2 dimensional quantum field theory'',
World Scientific, Singapore, 1991.

\bibitem{Schwinger} J. Schwinger, Phys. Rev. {\bf 128}, 2425 (1962).

\bibitem{Swieca} J. H. Lowestein and J. A. Swieca,
Ann. Phys. (N.Y.) {\bf 65}, 172 (1971).

\bibitem{Rajaraman} R. Jackiw and R. Rajaraman,
Phys. Rev. Lett. {\bf 54}, 1219 (1985).

\bibitem{Carvalhaes} C. G. Carvalh\~aes, L. V. Belvedere,
C. P. Natividade and H. Boschi-Filho,
Ann. Phys. (N.Y.) {\bf 258}, 210 (1997).

\bibitem{Siegel} W. Siegel, Nucl. Phys. {\bf B238}, 307 (1984).

\bibitem{Floreanini} R. Floreanini and R. Jackiw, 
Phys. Rev. Lett. {\bf 59}, 1872 (1987).

\bibitem{Harada} K. Harada, Phys. Rev. Lett. {\bf 64}, 139 (1990).

\bibitem{Kye} W.-H. Kye, W.-T. Kim and J.-K. Kim, 
Phys.  Lett. {\bf B 268}, 59 (1991).

\bibitem{Dutra} A. de Souza Dutra, 
Phys.  Lett. {\bf B 286}, 285 (1992).

\bibitem{Carena} C. E. M. Wagner and M. Carena,
Int. J. Mod. Phys. {\bf A 6}, 243 (1991).

\bibitem{BjorkenDrell} J. D. Bjorken and S. D. Drell, 
``Relativistic Quantum Fields'', McGraw-Hill, New York, 1965.

\bibitem{Jackson} J. D. Jackson, 
``Classical Electrodynamics'', 2nd ed., John Wiley, New York, 1975.

\bibitem{Galvao} Carlos A. P. Galv\~ao and J. B. Boechat, 
J. Math. Phys. {\bf 31}, 448 (1990).




\end{thebibliography}
%\end{references}


\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
