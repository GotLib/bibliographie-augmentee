%%
%%  Revtex file, to be processed twice
%%
\documentstyle[twocolumn,twoside,prl,floats,aps,epsfig]{revtex}
\def\thefootnote{\fnsymbol{footnote}}
\def\de{\partial}
\def\oh{\frac{1}{2}}
\def\Li{{\rm Li}}
\newcommand{\eq}{\begin{equation}}
\newcommand{\en}{\end{equation}}
\newcommand{\eqa}{\begin{eqnarray}}
\newcommand{\ena}{\end{eqnarray}}
\newcommand{\spz}{\hspace{0.7cm}}
\newcommand{\lbl}{\label}
\newcommand{\lhi}{\hat\lambda_{i}}
\newcommand{\br}{\langle}
\newcommand{\kt}{\rangle}
\newcommand{\um}{\frac12}
\newcommand{\th}[1]{\vartheta_{#1}(0,\tau)}
\newcommand{\ZZ}{\hbox{{\rm Z{\hbox to 3pt{\hss\rm Z}}}}}
\newcommand{\Zt}{\ZZ_2}

\def\bea{\begin{eqnarray}}
\def\eea{\end{eqnarray}}
\def\be{\begin{equation}}
\def\ee{\end{equation}}
\newcommand{\tr}[1]{\:{\rm tr}\,#1}
\newcommand{\Tr}[1]{\:{\rm Tr}\,#1}
\def\const{{\rm const}}
\def\e{{\,\rm e}\,}
\def\d{\partial}
\def\D{\delta}
\def\dd{^{\dagger}}
%\newcommand{\br}[1]{\left( #1 \right)}
\newcommand{\rf}[1]{(\ref{#1})}
\newcommand{\non}{\nonumber \\*}
\hyphenation{di-men-sion-al}
\hyphenation{di-men-sion-al-ly}
\def\A{{\cal A}}
\def\s{\sigma}
\def\l{\lambda}
\def\p{\varphi}
\def\bs{Bethe-Salpeter }
\def\t{\Delta}
\def\L{\Lambda}
\newcommand{\k}{\delta K}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A macro for short figure captions
\def\mycaptions#1{%
\refstepcounter{figure}
\begin{center}
\hskip 1pt\vskip -0.6cm
\small {\bf Fig.\hskip -3pt \arabic{figure}}: {\sl #1}
\null\hskip 1pt\vskip -0.2cm
\end{center}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A macro for long figure captions
\def\mycaptionl#1{%
\refstepcounter{figure}
\begin{center}
\hskip 1pt\vskip -0.6cm
\begin{minipage}{12cm}
\small {\bf Fig. \hskip -3pt\arabic{figure}}: {\sl #1}
\end{minipage}
\null\hskip 1pt\vskip -0.2cm
\end{center}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nicknames for reviews in bibliography
\newcommand{\JP}[1]{J.\ Phys.\ {\bf #1}}
\newcommand{\NP}[1]{Nucl.\ Phys.\ {\bf #1}}
\newcommand{\PL}[1]{Phys.\ Lett.\ {\bf #1}}
\newcommand{\NC}[1]{Nuovo Cim.\ {\bf #1}}
\newcommand{\CMP}[1]{Comm.\ Math.\ Phys.\ {\bf #1}}
\newcommand{\PR}[1]{Phys.\ Rev.\ {\bf #1}}
\newcommand{\PRL}[1]{Phys.\ Rev.\ Lett.\ {\bf #1}}
\newcommand{\MPL}[1]{Mod.\ Phys.\ Lett.\ {\bf #1}}
\newcommand{\IJMP}[1]{Int.\ J.\ Mod.\ Phys.\ {\bf #1}}
\newcommand{\JETP}[1]{Sov.\ Phys.\ JETP {\bf #1}}
\newcommand{\TMP}[1]{Teor.\ Mat.\ Fiz.\ {\bf #1}}

\addtolength{\topmargin}{1cm}
\tighten
\begin{document}
\draft
\vskip 0.4cm

\title{  
 Bound states in the three-dimensional $\phi^4$  model.}

\author{
M. Caselle$^a$
M. Hasenbusch$^b$ P. Provero$^{c,a}$ and K. Zarembo$^{d,e}$}

\address{$^a$  Dip. di Fisica 
Teorica dell'Universit\`a di Torino and I.N.F.N.
 via P.Giuria 1, I-10125 Turin,Italy\\
$^b$ Humboldt Universit\"at zu Berlin, Institut f\"ur Physik
 Invalidenstr. 110, D-10099 Berlin, Germany\\
 $^c$ Dip. di Scienze e Tecnologie Avanzate,
Universit\`a del Piemonte Orientale, I-15100 Alessandria, Italy\\
 $^d$ Dep. of Physics and Astronomy, University of British
Columbia, Vancouver, BC V6T 1Z1, Canada\\
$^e$ Institute of Theoretical and Experimental Physics, 
B. Cheremushkinskaya 25, 117259 Moscow, Russia\\
\parbox{14 cm}{\medskip\rm\indent
We discuss the spectrum of the three dimensional 
$\phi^4$ theory in the broken symmetry phase. In this phase the effective
potential between  the elementary quanta of the model is attractive and 
bound states of two or more of them may exist. We give theoretical and
numerical evidence for the existence of these bound states. Looking in 
particular at the Ising model realization of the $\phi^4$ theory we show,
by using duality, that
these bound states are in one-to-one correspondence with the glueball states of
the gauge Ising model. We discuss some interesting consequences of this
identification.
\\
PACS numbers: 
05.50.+q; 75.10.Hk; 11.10.Kk 
\\
Preprint DFTT 4/2000, HU-EP-00/09, ITEP-TH-84/99.
}}
\maketitle
\narrowtext
Three dimensional
statistical systems with global $Z_2$ symmetry, the Ising model
being the classic example, lie in the 
universality class of the $\phi^4$ field theory. Critical phenomena
in such systems are known to be accurately described by simple
perturbative methods  \cite{zin96}.
Given the success of perturbative methods, the appearance of excited states
in the broken symmetry phase of the critical Ising model and in the
3D $\phi^4$ theory, which were
found in \cite{chp99,chp99b},
comes as a surprise, since 
scalar field theory apparently describes only one particle
as long as  interactions can be treated perturbatively. We shall argue that 
this is not the case and there is  room for a rich spectrum 
of excitations in the broken symmetry phase of the $\phi^4$
theory even if the interaction is weak.
\par
The excited states show up as poles of the 
correlation functions in the complex momentum plane and give visible 
contribution to certain universal quantities. 
The first excited state lies just below the two-particle threshold: its 
mass is $M=1.83(3)m$ \cite{chp99},
where $m$ is the mass gap. 
The closeness of $M$ to the threshold suggests the interpretation of 
this excitation as a weakly coupled bound state of two elementary excitations. 
\par
% CP
Indeed, 
the two-particle forces are attractive in 
the broken-symmetry phase of the $\phi^4$ 
theory, and  
%The 3d $\phi^4$ theory in the symmetric phase describes 
%a single particle with local four-body interactions. 
%It is well known that this interaction gives rise to a repulsive
%force between a pair of these particles. It is perhaps less known that
%in the broken-symmetry phase, the presence of the additional 
%three-body interaction makes the force between two
%particles become attractive. 
%As a consequence, 
bound states of two or more
elementary quanta  may in principle be formed. 
%A similar situation also occurs in four dimensions and was 
%studied in detail in~\cite{lw88,gl98}. In these papers
%it was shown that, while 
In four dimensions,
these states indeed exist in the low-temperature
regime, but disappear as the continuum limit is 
approached~\cite{lw88,gl98},
in agreement with triviality.
\par
In this letter we address the three-dimensional case.
Numerical simulations show
 that non-perturbative states survive the continuum limit
in 3d \cite{chp99,chp99b}.
% CP
%In this letter 
We shall argue that these states can be identified
with the multiparticle bound states. By considering the Ising 
realization of the $\phi^4$ model and using duality
we shall also show that there is an exact one-to-one mapping between the
 bound states of the Ising model (and hence, thanks to universality,
also of the $\phi^4$ theory) and the glueball states of the gauge 
Ising model. 

%This letter is based on the results recently reported
% in~\cite{chp99,chp99b} where it was shown by using numerical simulations
% that non-perturbative states indeed exist in the
% spectrum of the 3d $\phi^4$ model in the broken symmetry phase
%and survive in the continuum limit.
%In this letter we shall argue that these states can be identified
%with the bound states discussed above.
%Non-perturbative techniques based on the Bethe-Salpeter equation 
%are needed to explore their properties. By considering the Ising 
%realization of the $\phi^4$ model and using duality
%we shall show that there is an exact one-to-one mapping between the
% bound states of the Ising model (and hence, thanks to universality,
%also of the $\phi^4$ theory) and the glueball states of the gauge 
%Ising model. 
\vskip0.5cm
{\bf Bound states in the $\phi^4$ theory.}
%When considering $\phi^{4}$ theory in the broken-symmetry phase it is 
%natural to work in terms of the field $\s$ which 
%represents the displacement of the field $\phi$ with respect to one of the 
%two stable classical solutions. 
We consider the $\phi^4$ theory: 
$$S=\int d^3x\,\left[\frac12\,(\d\phi)^2+\lambda(\phi^2-v^2)^2\right].$$
The field $\s=\phi-v$ acquires 
the mass $m^2=8\lambda v^2$ at the tree level and
is reasonably weakly coupled in the critical regime \cite{zin96}, since
the critical value of the dimensionless interaction constant, 
$\lambda/m$, is not too big. The forces between
elementary quanta of
the field $\s$ are attractive: This can be shown 
by inspecting the scattering of two 
non-relativistic particles. 
There are three leading-order diagrams contributing to 
this process, shown in Fig.~\ref{kernel}. 
\begin{figure}[h]
\hspace*{1.5cm}
\epsfig{file=kernel.eps,height=6cm,angle=-90}
\caption[x]{\small Feynman diagrams contributing to the $2\rightarrow 2$
amplitude at the leading order in $\l$.}
\label{kernel}
\end{figure}
The contact interaction, diagram $(a)$, 
contributes $-12\l$ to the scattering amplitude. 
As a first approximation, we can neglect 
altogether the momentum flow in diagrams $(b)$ and $(c)$.
% it can be shown 
%that we can obtain nevertheless the correct dependence of the binding 
%energy from the coupling $\l$.
In this way, diagrams $(b)$ and $(c)$ contribute $-12\l$ and $72\l$, 
respectively.
Collecting the three terms together, we get for the amplitude
$\A=48\l$.
The positive sign of the amplitude means that the particles attract 
each other.
\par
% CP
In this limit, the non-relativistic Hamiltonian describing the interaction of
two particles is
%In the non-relativistic limit, the interaction of two particles
%can be described by the Hamiltonian
\be
H=\frac{{\bf p}_1^2}{2m}+\frac{{\bf p}_2^2}{2m}-\frac{12\l}{m^2}\,
\D({\bf x}_1-{\bf x}_2),
\ee
which
reproduces the field-theory 
scattering amplitude in the Born approximation \cite{albeverio}. 
Note the factor of $1/(2m)^2$,
which accounts for the relativistic normalization of the wave
functions in field theory.
%is in this approximation
The quantum-mechanical system of two
dimensional particles interacting via a $\delta$-type potential
 develops short-distance divergences and requires
a regularization \cite{t79,h92}. In our case, the cutoff is
proportional to $m$, because at momenta of order $m$ 
the non-relativistic approximation becomes inadequate.
%the momentum dependence
%of the scattering amplitude, which was dropped, becomes important.
\par
The binding energy $\t m$, $\t\ll 1$, is determined by the Schr\"odinger
equation  for the relative motion wave function:
\be
\left(-\frac{1}{m}\,\d^2+\t m\right)\psi({\bf r})=\frac{12\l}{m^2}\,
\psi(0)\D({\bf r}),
\ee
which, after the
Fourier transform, gives the consistency condition:
\be
1=\frac{12\l}{m^2}\int\frac{d^2p}{(2\pi)^2}\,\frac{1}{p^2/m+\t m}
=\frac{3\l}{\pi m}\ln\frac{\Lambda^2}{\t m^2}.
\ee
Taking $\Lambda^2=\kappa m^2$, we get for the binding energy:
\be
\t=\kappa\exp\left(-\frac{\pi m}{3\l}\right).
\ee
The constant $\kappa$ cannot be determined in the approximation used 
above and requires the inclusion of loop corrections and of the 
momentum dependence of diagrams $b$ and $c$. 
%This analysis will be 
These corrections can be systematically taken into account
in the approach based on the Bethe-Salpeter equation, which will be
reported elsewhere \cite{chpz00};
here we only quote the result: 
$\kappa=4/\sqrt{3}$.
%$\kappa=\frac{4}{\sqrt{3}}$.
%\par
%The exponential dependence of the binding energy on
%the coupling follows simply from dimensional analysis.
%The system of particles with $\delta$-type interactions is classically 
%scale-invariant.
%The regularization breaks this invariance and develops
%a dynamical scale $\epsilon=\Lambda\exp(-\pi m/3\l)\propto
%m\exp(-\pi m/3\l)$. Any quantity with the dimensions of a mass, in particular
%the binding energy, is proportional to $\epsilon$.
\par
The above discussion tells us that only one bound state of two elementary
quanta may exist in the broken phase of the $\phi^4$ model in three dimensions.
%In principle one could have other bound states of two particles, 
%with a non trivial
%angular momentum, or with higher values of the radial quantum number.
%However since the potential has a very short range it is rather unlikely that
%these excited states can form, and in fact 
This is in accord with numerical simulations 
\cite{chp99,chp99b}.
%suggest that only one bound state of two particles exists.
%\par
However, a rich spectrum of bound states, with different values of the angular
momentum, can be found if we look at
the bound states of three or more elementary quanta.
%The fact that we are dealing with a short-range potential strongly limits the 
%number of possible bound states if we fix
%the number of the elementary particles involved, but as the number of 
%elementary quanta increases we may obtain stable bound states with
%all possible values of the angular momentum.
%\par
Bound states of $n\ge 2$ particles could be studied in principle 
within the Bethe-Salpeter approach, 
% described above, 
but even the non-relativistic approximation described above
becomes too complicated
as the number of particles involved in the bound state increases. 
%it 
%becomes rather difficult to obtain quantitative results even in the 
%non-relativistic approximation. 
Up to our knowledge, the only existing result 
in the literature is a discussion of the $0^+$ bound state of three particles 
which can be found in~\cite{bt79}. The counterpart of this state is
also seen in the numerical simulations \cite{chp99b}. 
An easier way to understand the qualitative features of the bound states
is to study the Ising model in the low-temperature phase far below
criticality.
%the study of the 
%low-temperature phase of the Ising model provides an easier way to 
%understand the qualitative features of the bound states.
\vskip0.5cm 
{\bf The Ising model at low temperature.}
To proceed in understanding the structure of the bound state spectrum in the
$\phi^4$ theory, let us address the same problem in the case of the 
low temperature phase of the 3d Ising model. 
 The Ising model and the $\phi^4$ theory belong to the same
universality class. 
%%CC I think that universality of the spectrum is part of the standard 
%%CC expectation. Else Lattice QCD-spectroscopy would make little sense.
%%This means first of all that in the neighborhood of the
%%critical point  they share the same critical behavior.
Therefore they should have the same spectrum in the critical limit.
Indeed,  
in~\cite{chp99,chp99b} it was shown in a Monte Carlo study
that 
the two models share the same spectrum of non-perturbative
states.

%%CC
The main advantage of working with the Ising model
is that the spectrum can be analyzed in a low temperature expansion of the 
transfer matrix (see~\cite{FC71,C73}). The starting point of this expansion
is to ignore the 
% CP
%inter-time-slice interaction. 
interactions between time-slices.
In this approximation, the 
vectors that correspond to a single configuration on a time-slice 
become eigenvectors of the transfer matrix. The eigenvalues of the transfer 
matrix
are directly 
given by the number of frustrated bonds.
%we have an intuitive way to understand the
%appearance of bound states (see~\cite{lw88}). 
%Deep in the low-temperature phase we may
%identify the elementary particle excitation 
%of the model with a single spin flipped with respect to the ground state
%magnetization. The number of broken bonds, {\em i.e.} the number of frustrated 
%nearest neighbor couplings due to the spin flipping 
%gives an idea of the energy which is needed to
%excite this state from the vacuum.
\par 
In this framework, the bound state of
two particles is obtained by flipping two nearby spins.
%: in fact,
If we flip two spins
which are separated by a distance of more than one lattice spacing,
%they behave, as a first approximation, 
%as two separate particles, 
%since 
the total number of frustrated bonds is exactly twice that of a single 
particle.
On the contrary, if we flip two nearby spins,  
the number of frustrated bonds is
reduced by two.
This difference is the binding energy of
the bound state. The fact that we are constrained to choose the two spins in
nearest neighbor sites is another way to state that the attractive force
between the two particles has a very short range. 
This procedure can be iterated, and one can construct clusters of $k$ nearby
flipped spins which have a non-zero binding energy and are related to  bound 
states of higher mass.
\par
It is also possible to select bound states of  non-zero
angular momentum. These combinations can
be constructed by using standard group-theoretical techniques.  They are
discussed in~\cite{acch96}. Let us only recall here two results which are of
interest for the present analysis.
\par
On a (2+1) dimensional lattice the group of rotations and parity reflections 
is reduced to the dihedral group $D^4$ which has four one-dimensional
and one two-dimensional irreducible representations. 
The $0^+$ state is associated to the trivial one-dimensional 
irreducible representation.
The $2^+$ and $2^-$ states are degenerate and correspond to two other 
one-dimensional representations. The simplest possible realization of
the $2^-$ state is represented in Fig. 2. At least three flipped spins are 
needed
to create such a state (the $2^+$ state could be also realized in a simpler 
way, but a general theorem forces its mass to be the same of the $2^-$ one in  
the continuum limit). Thus we expect that this state should appear as a bound
state of at least three elementary quanta.
The last one-dimensional irreducible representation corresponds to the 
$0^-$ state.
The simplest possible representation of this
state is reported in Fig. 3 and requires at least four flipped spins.
Finally, all the states with odd angular momentum 
are collected in the two dimensional representation. As in
the $2^+$,$2^-$ case they are all degenerate in parity. 
\par
Let us summarize the pattern of bound states as it emerges from these  
considerations. With two elementary quanta we may only
create a bound state with quantum numbers $0^+$. We shall denote it with 
$0^{+,*}$ to distinguish it from the single particle excitation which has the 
same quantum numbers. With three quanta we may create a pair of bound states 
$2^{+}$ and $2^{-}$ and a new $0^+$ excitation that we shall call $0^{+,**}$. 
With four particles we shall have a $2^{\pm,*}$ pair, a $0^{+,***}$ state and 
a new state with quantum numbers $0^-$. With five particles a new pair of 
states of the type $1^{\pm}$ appears, and so on.
\par
Let us now make the crucial assumption that the binding energy is always
much smaller that the mass of the constituent particles: then the mass 
of each bound state will be essentially given by the number of particles
needed for its formation, minus a small correction given by the binding
energy. In this way one obtains
a detailed prediction of the qualitative features of the spectrum, based 
only on the interpretation of the states as bound states and the 
group-theoretical facts described above. 
\par
Numerical simulations \cite{chp99,chp99b,chpz00,acch96} 
show that not only these predictions
are fulfilled, but that the same qualitative features of the spectrum 
survive well beyond the low-temperature regime and into the scaling region. 
Connected correlators of several composite operators
are computed in Monte Carlo simulations and used to extract the spectrum
in the various angular momentum channels. 
The measured masses
exactly follow the pattern suggested above.
This is a strong indication that
the spectrum is indeed made of bound states of the elementary quanta, and 
that these bound states survive in the continuum limit.
\par
The fact that states with angular momentum $2$ are lighter than that with
angular momentum $0^-$ is rather unexpected in standard quantum field theory.
However it is a well established feature of the
glueball spectrum in (lattice) gauge theories. This is the first 
hint that the spectrum of bound states of the 3d Ising model has 
something to do with the glueballs of gauge theory. The reason for 
this is obviously the duality between spin model and gauge model, 
that we will now discuss.
\vskip0.5cm
\begin{figure}
\begin{center}
\null\hskip 1pt
\epsfxsize 4cm
\epsffile{fig2.eps}
\vskip 0.15cm\noindent
\end{center}
\mycaptionl{Operators for $2^-$.}
\end{figure}

\begin{figure}
\begin{center}
\null\hskip 1pt
\epsfxsize 8cm
\epsffile{fig3.eps}
\vskip 0.15cm\noindent
\end{center}
\mycaptionl{Operators for $0^-$.}
\end{figure}
{\bf Duality.}
% CP
Duality is usually expressed as an exact equality between partition functions 
in infinite volume, 
% Duality is an exact equality between partiton functions,
hence in
principle it does not automatically implies that the two theories must have the
same spectrum. However it can be shown that duality holds not only in the
thermodynamic limit {\sl but also for finite lattices}. This 
correspondence is
not trivial and requires a careful analysis of the boundary
conditions of the two models~\cite{chpz00}. 
Since the approach to the thermodynamic limit of the finite
volume partition function is driven by the full spectrum of excited states of
the theory, the finite volume duality implies that the spectra of the two 
models must coincide. In particular the bound state of quantum numbers $J^P$ of
the Ising spin model coincides (hence has exactly the same mass) with the
$J^P$ glueball of the gauge Ising model. This identification has two
interesting consequences. 
% CP
The first one is that the Bethe-Salpeter approach to the calculation of
bound states in $\phi^4$ theory, described above, becomes an analytical
%The first one is that it gives (by using the 
%techniques described in sect.2 to evaluate the mass of the corresponding bound
%states) an analytic 
tool to evaluate the masses of 
the first states of the glueball spectrum 
of the gauge Ising model. In principle
(apart from technical difficulties) this could be extended to the whole
glueball spectrum, and represents a powerful alternative to the Isgur-Paton
model, which in the case of the Ising gauge model gives rather poor 
result~\cite{acch96}.
A second, more important consequence of this identification is that it gives a
possible explanation for a peculiar degeneracy observed in the Monte Carlo
estimates of the glueball masses in the 3d gauge Ising model~\cite{acch96} for
which no alternative explanation exists.
This intriguing feature of the spectrum 
can be immediately appreciated by
looking at Tab.~1, (data taken from~\cite{acch96,t98}). 
In Tab.~1 the asterisks denote
the radial excitations, thus $0^+$ is the lowest state in the family with
quantum numbers $0^+$, $0^{+,*}$ the next one and  $0^{+,**}$ the third one. 
$0^+$ is related by duality to the single-particle state of the 3d Ising model,
$0^{+,*}$ to the first bound state and so on. 
The degeneracy involves the
pairs $(0^{+,**},2^{\pm})$, $(0^{-},2^{\pm,*})$, $(0^{-,*},1^{\pm})$ (the
last one is only roughly established, it holds within the errors).
Let us stress that
this degeneracy has no obvious physical reason. The only one which we would
expect on physical grounds is the one
 between $J^+$ and $J^{-}$ states (for $J\neq
0$) (see~\cite{acch96} for a discussion of this point)
 which is indeed present and has been already taken into account in Tab.~1.
Moreover it {\em is not} explained by the Isgur-Paton model (last column
of Tab. 1). This degeneracy seems to be  a rather
 deep phenomenon since it is also present in the glueball spectrum of the SU(2)
model in (2+1) dimensions.
In the third column of Tab. 1 we report the data on SU(2)
obtained by Teper~\cite{t98} (the underlined values are our extrapolations of
the finite-$\beta$ values reported in~\cite{t98}). One can easily see that
the same pattern of
degeneracy is present both in the SU(2) and in the Ising gauge spectra.
On the contrary, all these degeneracies seem to be
lost in  the $SU(N)$, $(N>2)$ case (see the data reported in~\cite{t98}).
\par
This degeneracy is well explained by the interpretation of the glueballs
as bound states of the dual spin model: the degenerate glueball states 
are simply bound states of the {\em same number} $n_c$ of constituent 
particles, namely $n_c=3,4,5$ respectively for the 
$(0^{+,**},2^{\pm})$, $(0^{-},2^{\pm,*})$ and  $(0^{-,*},1^{\pm})$ 
degeneracies.
In fact, according to the assumption stated above,
 the major contribution to the mass of the bound state is given by the
number of elementary quanta involved. The dependence on the various quantum
numbers is encoded in 
the binding energies $\Delta$ which however give only a small correction to 
the mass. This results in the approximate degeneracies observed in the
simulations. Notice that we do not expect to have {\sl exact} degeneracies,
since there is no reason to expect 
% CP
% that 
the binding energy 
%are 
to be
exactly the same for
different bound states.
%
\begin{table}[ht]
\label{ip5f}
\caption{\sl Comparison between the  Ising, SU(2) and Isgur-Paton spectra. The
masses are measured in units of the string tension.}
  \begin{center}
\begin{tabular}{|l|c|c|c|}
\hline
$J^P$ &  Ising & SU(2) & IP    \\
\hline
$0^+$  &  3.08(3)& 4.718(43) & 2.00  \\
$0^{+,*}$ & 5.80(4)& 6.83(10)   & 5.94  \\
$0^{+,**}$  &  7.97(11)& 8.15(15) & 8.35   \\
$2^{\pm}$  &  7.98(8) & 7.84(14)& 6.36  \\
$2^{\pm,*}$  &  9.95(20)&\underline{9.30(50)}  & 8.76  \\
$0^-$   &   10.0(5) & 9.95(32) & 13.82 \\
$0^{-,*}$  & 13.8(6) & \underline{11.30(80)}  & 15.05  \\
$(1)^\pm$   & 12.7(5)& 10.75(50) & 8.04  \\
\hline
\end{tabular}
  \end{center}
\end{table}
\vskip0.5cm
{\bf Conclusions.}
Our analysis shows that bound states are very likely to exist in the 
broken-symmetry phase of 3d $\phi^{4}$ and Ising models. Their 
existence can be inferred both from the Bethe-Salpeter equation of the 
field theory and the strong-coupling analysis of the spin model, and 
is strongly confirmed by numerical simulations. 
\par
Duality allows one to apply the same analysis to the glueball spectrum 
of the 3d Ising gauge model, which exactly coincides with the one of
the spin model. The interpretation of the latter as a spectrum of 
bound states provides a natural explanation for several  
features of the glueball spectrum, such as its peculiar dependence on 
the angular momentum and its characteristic degeneracies.
\vskip 1cm
{\bf  Acknowledgements.}
We would like to thank M. Campostrini, A. Pelissetto, P. Rossi and E. Vicari 
for fruitful discussions, and the organizers of the INTAS meeting 1999 
for providing the stimulating environment in which this work was started.
This work was partially supported by the 
European Commission TMR programme ERBFMRX-CT96-0045.
The work of K.Z. was supported by the PIMS Postdoctoral Fellowship and
by NSERC of Canada.
\begin{references}
\bibitem{zin96} J. Zinn-Justin, {\it Quantum Field Theory and Critical
Phenomena}, (Oxford University Press, 1996).
\bibitem{chp99} M. Caselle, M. Hasenbusch and P. Provero, 
Nucl. Phys. {\bf B556} (1999) 575 (hep-lat/9903011).
\bibitem{chp99b} M. Caselle, M. Hasenbusch and P. Provero, hep-lat/9907018.
\bibitem{lw88} M. L\"uscher and P. Weisz, Nucl. Phys. {\bf B295} (1988) 65.
\bibitem{gl98} F. Gliozzi and M. Leone, Nucl. Phys. Proc. Suppl. {\bf 73}
(1999) 766
(hep-lat/9808044).
\bibitem{albeverio}
Note that the $\delta$-function potential in non-relativistic quantum 
mechanics in two 
spatial dimensions is not well defined mathematically, see {\em e.g.} 
S. Albeverio, F. Gesztesy, R. Hoegh-Krohn and H. Holden,
{\it ``Solvable Models in Quantum Mechanics''} (Springer-Verlag, New York, 
1988).In our case these difficulties are an artifact of the non-relativistic
approximation, and do not appear in the relativistic field-theoretical 
treatment. We thank R. Soldati for drawing our attention to this point.
\bibitem{t79} C. Thorn, Phys. Rev. {\bf D19}, 639 (1979).
\bibitem{h92} K. Huang, {\it Quarks, Leptons and Gauge Fields},
(World Scientific, 1992).
\bibitem{chpz00} M. Caselle, M. Hasenbusch, P. Provero and K.Zarembo, 
in preparation.
\bibitem{bt79} L. W. Bruch and J. A. Tjon, Phys. Rev. {\bf A19} (1979) 425.
\bibitem{FC71} M. E. Fisher and W. J. Camp,  Phys. Rev. Lett. 
{\bf 26} (1971) 565.
\bibitem{C73}  W. J. Camp,  Phys. Rev. {\bf B7} (1973) 3187.
\bibitem{acch96} V. Agostini, G. Carlino, M. Caselle and M. Hasenbusch,
Nucl. Phys. {\bf B484} (1997) 331 (hep-lat/9607029).
\bibitem{t98} M. Teper, Phys. Rev. {\bf D59} (1999) 014512, (hep-lat/9804008).
\end{references} 
\end{document}


