Do We Need Neural Models to Explain Human Judgments of
Acceptability?
Wang Jing
Beijing International Studies University
Beijing, China
wangjing1204@foxmail.com
M. A. Kelly
Penn State
University Park, PA, USA
mak582@psu.edu
David Reitter
Google Research
New York, NY, USA
reitter@google.com
Abstract
Native speakers can judge whether a sentence
is an acceptable instance of their language.
Acceptability provides a means of evaluating
whether computational language models are
processing language in a human-like manner.
We test the ability of computational language
models, simple language features, and word
embeddings to predict native English speak-
ers’ judgments of acceptability on English-
language essays written by non-native speak-
ers. We find that much of the sentence ac-
ceptability variance can be captured by a com-
bination of features including misspellings,
word order, and word similarity (Pearson’s
r = 0.494). While predictive neural models
fit acceptability judgments well (r = 0.527),
we find that a 4-gram model with statistical
smoothing is just as good (r = 0.528). Thanks
to incorporating a count of misspellings, our
4-gram model surpasses both the previous un-
supervised state-of-the art (Lau et al, 2015,
r = 0.472), and the average non-expert native
speaker (r = 0.46). Our results demonstrate
that acceptability is well captured by n-gram
statistics and simple language features.
1 Introduction
Proficient language users, when given a sentence
in their language, are able to judge the acceptabil-
ity of the sentence when asked whether the sen-
tence is natural, well-formed, or grammatical. Ac-
ceptability is of interest to cognitive scientists be-
cause it provides a means for evaluating whether
computational models of language are processing
language in a manner similar to humans.
At the same time, models of acceptability have
applications in natural language processing. For
example, they can be used to evaluate the fluency
of machine translation outputs, of answers pro-
duced by question answering systems, and auto-
matically generated language snippets quite gener-
ally (Lau et al., 2015). In computer-assisted learn-
ing, acceptability models can help grade essays
and provide feedback to native speakers as well
as language learners. Acceptability may also be
a more precise training signal for general-purpose
language models (albeit a costly-to-obtain one).
Recently, great progress has been made in lan-
guage modeling. But do the models process lan-
guage in a manner similar to humans? And are
the models good at discriminating between accept-
able and unacceptable language, or are the models
merely sensitive to the distinction between proba-
ble and improbable language? Is acceptability sta-
tistical in nature, or best understood through the
rules of a formal grammar?
In this paper, we investigate what models and
language features can capture the acceptability
judgements of native English speakers on sen-
tences of English. We train different word embed-
dings, including Word2Vec (skip-gram and contin-
uous bag of words; Mikolov et al., 2013a,b), the
GloVe model (Pennington et al., 2014), and the Hi-
erarchical Holographic Model (HHM; Kelly et al.,
2017), to investigate the role of semantic fea-
tures in acceptability. We train n-gram models,
simple recurrent neural network language mod-
els (RNN), and long-short term memory language
models (LSTM), to obtain sentence-level prob-
abilities. We explore how individual word fre-
quency, n-gram frequency, spelling errors, the or-
der of words in sentences, and the semantic coher-
ence of a sentence, contribute to human judgments
of sentence acceptability.
Acceptability exhibits gradience (Keller, 2000;
Lau et al., 2014). Accordingly, we treat accept-
ability as a continuous variable. We evaluate
model performance by measuring the correlation
between each model’s acceptability prediction and
a gold standard score based on human judgments.
We are concerned with models that learn in
arXiv:1909.08663v1
[cs.CL]
18
Sep
2019
an unsupervised fashion. Supervised training re-
quires copious labelled data and explicit examples
of both acceptable and unacceptable sentences.
Conversely, we use only the type of data that is
most available to a human learner. Humans learn
mainly through exposure to a language. While ex-
plicit training on what is and is not acceptable of-
ten occurs in school, this training is unnecessary
for native language competence.
In what follows, we discuss related work (§2),
provide an overview of our methodology (§3), in-
cluding models (§3.1 & §3.2), how we calculate
an acceptability estimate from the models (§3.3),
and data set (§3.4), before presenting the results of
our experiments with word embeddings (§4.1) and
language models (§4.2).
2 Related Work
Prior work has focused on predicting sentence ac-
ceptability using syntactic parsing (Blache et al.,
2006; Wong and Dras, 2010; Ferraro et al., 2012;
Clark et al., 2013a), statistical language models
(Clark et al., 2013a; Lau et al., 2015), and lin-
guistic features (Heilman et al., 2014; Clark et al.,
2013b). Neural language models have shown ev-
idence of acquiring deeper grammatical compe-
tence beyond mere n-gram statistics (Gulordava
et al., 2018), suggesting that the models are a good
basis for modelling human acceptability judge-
ments (Lau et al., 2015).
However, little work has been done on using
distributional semantic models to predict sentence
acceptability. Vecchi et al. (2017) uses word em-
beddings to predict human judgements of seman-
tic acceptability on adjective - noun pairs (e.g.,
remarkable onion vs. legislative onion). Sim-
ilarly, Marelli and Baroni (2015) compose mor-
pheme embeddings to model the acceptability of
novel word forms (e.g., undiligent vs. unthird).
3 Methodology
There are many ways to determine to what extent a
sentence is acceptable. Are the words spelled cor-
rectly? Are the words arranged in a correct order?
Do the words create a coherent meaning?
3.1 Semantic Coherence
We hypothesize that if a sentence is acceptable to
a native speaker, the words in the sentence will be
compatible with each other with respect to mean-
ing and to topic. Conversely, we hypothesize that
if a word does not belong to a sentence, it is likely
to be dissimilar in meaning to the other words. We
call this measure semantic coherence. Semantic
coherence can be measured by computing the co-
sine similarity between the word and its context.
The context can be the words near to the target
word or all other words in the sentence.
3.2 Language Models
Language models predict probability distributions
over sequences of words. Theoretically speaking,
a language model can capture semantic coherence.
Lau et al. (2015) demonstrate that a good language
model can predict acceptability well. We improve
upon Lau et al.’s method for predicting acceptabil-
ity by also taking misspellings into account.
3.3 Acceptability Measures
To estimate acceptability using language models
or word embeddings, we take the log probability
or cosine similarity produced by each and normal-
ize it to the range [0,1] to get the model’s Score.
We then convert the Score into an acceptability
measure using one of the methods in Table 1.
We introduce one more variable: misspellings
(Mis), which reflects the exponential effect of the
count of misspellings.
Because Lau et al. (2015) found that sentence
acceptability is invariant with respect to sentence
length or word frequency, we also normalize by
sentence length and word frequency to produce a
set of final scores based on the measures proposed
by Lau et al.. We normalize by word frequency
in the NormMul (normalized multiply) and Norm-
Sub (normalized subtract) measures. The Syntac-
tic Log Odds Ratio (SLOR) measure, proposed by
Pauls and Klein (2012), accounts for both word
frequency and sentence length. Lau et al. found
that SLOR was the best predictor of acceptabil-
ity. But, as we find that SLOR does not perform
well using our models, we include NormMul, and
NormSub, which are variants of the measures pro-
posed by Lau et al.. For each measure we com-
pute its Pearson correlation coefficient with the
sentence’s gold standard to evaluate its effective-
ness in predicting acceptability.
3.4 The GUG Dataset
To test the ability of the models to predict ac-
ceptability, we need a collection of sentences that
exhibit varying degrees of acceptability with rat-
ings from native speakers. We use the Grammat-
Measures Equation
Mis Score(ξ)(m+α)
NormMul Mis × Pu(ξ)
NormSub Mis − Pu(ξ)
SLOR NormSub
|ξ|
Table 1: Measures for predicting the acceptability of a
sentence. Notation: Score is an estimate produced by
a model (i.e., semantic coherence or sentence log prob-
ability), and normalized to [0,1]; ξ is the sentence; |ξ|
is the sentence length; m is the number of misspelled
words in a sentence; α is a fitting parameter; Pu(ξ) is
the unigram probability of the sentence. We train the
unigram model on English Wikipedia.
ical versus Ungrammatical (GUG) data set built
by Heilman et al. (2014). The GUG data set con-
tains 3129 sentences randomly selected from es-
says written by non-native speakers of English as
part of a test of English language proficiency (see
Table 2). Heilman et al. (2014) crowd-sourced ac-
ceptability ratings for the sentences on a 1 (incom-
prehensible) to 4 (perfect) scale, obtaining five rat-
ings for each sentence. Each sentence also has
an “expert” rating from a linguist. Heilman et al.
(2014) randomly split the data into training (50%),
development (25%), and test (25%) sets.
In order to compare to the previous results in
Lau et al. (2015), we only use the GUG test set.
We remove 23 sentences from GUG that have less
than 5 words, lower-case all words, and extract
744 sentences for our test set. We take the aver-
age of the crowd-sourced ratings (across 5 work-
ers) as the gold standard. To evaluate the mod-
els, we compute the correlation of the predicted
ratings and the gold standard ratings. We cor-
rect misspelled words using the PyEnchant1 spell-
checker. We use PyEnchant’s first suggestion as
the corrected spelling. We count the number of
misspelled words in every sentence, which serves
as a feature for the Mis measure (see Table 1).
To illustrate the difficulty of predicting accept-
ability, we compute the Pearson’s correlation co-
efficient between the ratings of each human rater
and the mean acceptability rating. We find that the
correlations for crowd workers range from 0.440
1
http://pythonhosted.org/pyenchant/
to 0.485. However, the correlation between the
expert and the average of all non-expert ratings
is high, r = 0.753. The high correlation could
be an artifact of crowd worker selection. Crowd
workers were screened using a qualifying test that
assessed the agreement between their ratings and
the expert ratings on a set of trial sentences, which
could force a correlation. Conversely, the higher
correlation for the expert and the average of the
workers could reflect the expert’s language exper-
tise and the wisdom of the crowd.
In sum, to achieve non-expert performance
at predicting acceptability, computational models
need a correlation to the mean acceptability rating
of at least r = 0.440, but to achieve expert perfor-
mance may require a much higher correlation.
4 Experiments
4.1 Acceptability as Semantic Coherence
We hypothesize that more acceptable sentences
have higher semantic coherence. We quantify se-
mantic coherence between a word and its context
as the cosine similarity between the vector repre-
senting the word and the vector representing the
sentence without the word. The semantic coher-
ence of the sentence as a whole is then computed
as either the minimum or average of each word’s
similarity to the rest of the sentence.
To compute semantic coherence, we train word
embeddings on two English language corpora:
the British National Corpus (BNC; 100 mil-
lion words; British National Corpus Consortium,
2007) and the Novels Corpus (NC; 145 million
words; Johns et al., 2016). The word embeddings
we train are Word2Vec (skip-gram, or SK, and
continuous bag of words, or CBOW), GloVe, and
the Hierarchical Holographic Model (HHM).
4.1.1 Word2Vec
Using CBOW, Word2Vec predicts the current
word from a window of surrounding context
words, while in SK, Word2Vec uses the current
word to predict the surrounding window of con-
text words (Mikolov et al., 2013a,b). We use Gen-
sim Word2vec to train CBOW and SK models with
300 dimensions and a window size of 5.2
4.1.2 GloVe
GloVe takes aggregate word-word co-occurrence
statistics from a corpus (Pennington et al., 2014)
2
Gensim Word2vec from: https://
radimrehurek.com/gensim/
Table 2: Example sentences from Heilman et al. (2014)’s GUG data set with acceptability ratings.
sentence expert workers mean
For not use car. 1 [3, 4, 3, 4, 3] 3.0
I would like to initiate, myself , whatever I do on my trip to get
much out of my trip.
2 [4, 3, 2, 3, 3] 2.8
These kind of fish can’t live so long in water that contain salt. 3 [3, 3, 4, 3, 4] 3.3
So if you want me to choose right now, I will choose ordinary milk
instead of that special kind.
4 [3, 4, 3, 4, 4] 3.7
and performs a dimensional reduction on the co-
occurrence matrix to produce a set of word em-
beddings. We build 300 dimensional GloVe em-
beddings on the BNC and NC corpora3.
4.1.3 Hierarchical Holographic Model
GloVe and Word2vec treat sentences as unordered
sets of words. In English, word order conveys
much of the meaning of the sentence, and is criti-
cal in constructing a grammatical sentence. To ac-
count for this, we include in our analysis the Hier-
archichal Holographic Model (HHM; Kelly et al.,
2017), a model sensitive to the order of words in
a sentence. HHM generates multiple levels of rep-
resentations, such that higher levels are sensitive
to more abstract relationships between words, im-
proving the ability of the model to capture part-of-
speech relationships (Kelly et al., 2017).
We trained three levels of representations with
1024 dimensions and a context window of 5 words
to the left and right of each target word. Number
of dimensions, level, and window size are HHM’s
only parameters, and as such, HHM is comparable
to the other word embeddings.
4.1.4 Semantic Coherence
The semantic coherence of a sentence is either:
Score = min(cosine(wordi, contexti)) (1)
Score = avg(cosine(wordi, contexti)) (2)
where min is the minimum, avg is the average,
wi is the i-th word’s representation in a sentence
and contexti is the context representation of the
sentence without wi. If a word in the GUG test set
is not in the corpus (169 test set words not in NC,
0 words not in BNC), we use a random embedding
instead. We have two methods for computing the
3
Our GloVe implementation comes from https://
github.com/stanfordnlp/GloVe. Other parameter
values of the GloVe vectors: xmax = 100; window_size =
10; iter = 100
context representation contexti of word wi. One
method is to sum:
contexti =
P
(w1, ..., wi−1, wi+1, ...wn)
n
(3)
We can also get the context by building a holo-
graphic representation using HHM’s environment
vectors via a method described in Jones and Me-
whort (2007) and Kelly et al. (2017). By us-
ing aperiodic convolution to combine environment
vectors into bigrams, trigrams, tetragrams, etc., up
to n-grams of sentence length, we can construct a
representation of the sentence that accounts for the
ordering of the words within it.
4.1.5 Results
Tables 3 and 4 show that semantic coherence, by
itself, correlates weakly with acceptability when
the sentence context is computed as a sum (as in
Eqn. 3). Correlations for the Score range from
r = −0.181 (CBOW on BNC) to r = 0.185 (SK
on NC) with an average correlation across word
embeddings and corpora of only r = 0.058.
We can improve the correlation by incorporat-
ing misspellings and unigram probability. GloVe
is the best performing model when using the com-
bined measures. We experimented with different
values of the fitting parameter α for the Mis mea-
sure and found the highest correlation for α = 0
and using the minimum (rather than average) se-
mantic coherence of the sentence (GloVe, r =
0.449 on NC and r = 0.446 on BNC), which can
be further improved using NormMul by multiply-
ing by the unigram probability (GloVe, r = 0.464
on NC and r = 460 on BNC). However, an α of
zero indicates that the representations rely heavily
on the misspellings to predict acceptability.
Conversely, when the context vector is com-
puted holographically, such that the ordering of
the words in the sentence is preserved, we find
that the semantic coherence score is a stronger pre-
dictor of acceptability then when using a sum to
construct the context (see Table 5). Score ranges
from r = 0.140 to r = 0.314 and an average
of r = 0.201. By incorporating misspellings
(α = 0.3), the correlation for average coherence
increases to r = 0.471 (NC) and 0.457 (BNC)
for Level 1 of HHM and to 0.467 (NC) and 0.494
(BNC) for Level 2 of HHM.
In sum, while semantic coherence is not a strong
predictor of acceptability, when combined with
sensitivity to the order of the words in the sentence
and the number of misspellings, it can act as an ef-
fective predictor of acceptability (r = 0.494).
4.2 Acceptability using Language Models
Language models can predict the probability of
a sequence of words. Lau et al. (2015) com-
pared acceptability judgments against predictions
by several language models including n-gram
models, variations of Hidden Markov Models,
latent Dirichlet allocation, a Bayesian chunker,
and a recurrent neural network language model
(RNNLM). Lau et al. (2015) obtained acceptabil-
ity scores using crowd-sourcing and the corpus
of sentences was obtained using round-robin ma-
chine translation from English to a second lan-
guage, and back. On this (unpublished) data, the
neural language model (RNNLM) performed best.
However, a 4-gram model trained on the British
National Corpus beat the RNNLM when tested on
the GUG test data (r = 0.472; Lau et al., 2015).
We take the 4-gram model’s performance (r =
0.472) as the baseline in the following experi-
ments. The primary difference between the trans-
lation dataset and the GUG dataset is that sen-
tences in the translation dataset are produced by
Google Translate automatically, while sentences
in the GUG dataset are produced by a non-native
speaker. We suspect that a language model may
perform better on a dataset produced by machine
translation than on a naturalistic dataset comprised
of essays from L2 speakers, which may contain
greater linguistic variability.
4.2.1 Training
We produce lexical 4- and 5-gram language mod-
els (Stolcke, 2002), and a basic RNNLM 4 (El-
4
We use the Mikolov et al. (2010) implementa-
tion for the RNNLM: http://www.fit.vutbr.cz/
~imikolov/rnnlm/. Meta-parameter values of the
RNNLM included the following: number of classes = 550;
man, 1998; Mikolov, 2012), on two large cor-
pora of “acceptable” English: the British National
Corpus (BNC: 130K unique words, 100M tokens;
British National Corpus Consortium, 2007) and
on a corpus of novels (NC: 39K unique words,
145M tokens; Johns et al., 2016). We also train
the RNNLM and an LSTMLM on NC using Ten-
sorflow. The embedding layer of the models is
initialized either to a Gaussian distribution of val-
ues with 1024 dimensions or a pre-trained embed-
ding with 3072 dimensions (we use a concatena-
tion of HHM1, HHM2 and HHM3 here, details
about HHM are in section 4.1). We set the LSTM’s
hidden layer size to 1024, the projection layer size
to 128, and the maximum sequence length to 25.
The corpora are tokenized using NLTK5 and all
words are converted to lower case. In the GUG
test set, we replace out-of-vocabulary words (i.e.,
words not in the corpora) and low-frequency (< 5)
words with the <unk> signature. Sentences with
less than 8 words are also removed.
4.2.2 Results
Score_C in Table 6 equals the log probability
of sentences in the spell-corrected test data and
Score_O is the log probability of sentences in the
original test data.
Setting the Mis score’s fitting parameter α =
1.3 (see Table 1) yielded best results for the lex-
ical 4-gram model on BNC. The 4-gram model’s
correlation of r = 0.528 improves notably upon
Lau et al.’s best correlation of 0.472. The RNNLM
trained on NC performs similarly, at r = 0.527.
Table 7 shows the performance of the LSTM
and RNN trained on NC with an embedding layer.
We find that the best performing model is the
LSTM with a pre-trained embedding layer that is
not fixed and can be trained further.
Mis works well for capturing misspelled words.
Table 6 shows that the correlation of Mis exceeds
the original log probability no matter whether the
log probability is computed on corrected test data
or the original test data. The Mis score’s high
correlation demonstrates that accounting for mis-
spellings allows for a more accurate translation of
log probability to acceptability.
Unlike Lau et al. (2015), who found SLOR to
be the best measure, we find that NormSub per-
forms better than other measures across all mod-
els (Table 6). NormSub removes the influence of
bptt = 4; bptt-block = 100, hidden = 600.
5
https://www.nltk.org
measure HHM1 HHM2 HHM3 CBOW SK GloVe
min avg min avg min avg min avg min avg min avg
Score 0.08 0.073 0.15 0.114 0.157 0.115 0.029 -0.176 0.185 -0.028 0.089 -0.071
Mis 0.425 0.405 0.378 0.367 0.35 0.338 0.444 0.41 0.427 0.397 0.449 0.426
NormMul 0.444 0.431 0.405 0.403 0.383 0.381 0.458 0.44 0.445 0.436 0.464 0.445
NormSub 0.339 0.306 0.284 0.249 0.252 0.21 0.346 0.273 0.332 0.238 0.356 0.339
SLOR 0.283 0.263 0.268 0.25 0.247 0.225 0.288 0.239 0.298 0.242 0.323 0.276
Table 3: Pearson’s r between semantic coherence and acceptability with α = 0 for Mis. Semantic coherence com-
puted using minimum and average similarity between word and context representations. Context representation
obtained via Equation 3. Word embeddings were trained on NC. Boldface indicates the best performing measure.
measure HHM1 HHM2 HHM3 CBOW SK GLOVE
min avg min avg min avg min avg min avg min avg
Score 0.076 0.071 0.155 0.111 0.155 0.113 0.035 -0.181 0.173 -0.029 0.064 -0.068
Mis 0.422 0.404 0.380 0.367 0.351 0.337 0.445 0.41 0.425 0.399 0.446 0.426
NormMul 0.442 0.430 0.407 0.402 0.385 0.381 0.459 0.440 0.444 0.437 0.460 0.445
NormSub 0.334 0.306 0.285 0.249 0.252 0.21 0.348 0.275 0.327 0.243 0.351 0.339
SLOR 0.282 0.262 0.269 0.249 0.249 0.225 0.29 0.24 0.301 0.248 0.318 0.276
Table 4: As in Table 3, but with word embeddings trained on BNC.
measure HHM1_NC HHM2_NC HHM3_NC
min avg min avg min avg
Score 0.153 0.198 0.19 0.27 0.191 0.232
Mis 0.454 0.471 0.459 0.467 0.447 0.462
NormMul 0.457 0.476 0.453 0.462 0.436 0.455
NormSub 0.346 0.36 0.365 0.38 0.363 0.378
SLOR 0.239 0.176 0.243 0.204 0.293 0.219
HHM1_BNC HHM2_BNC HHM3_BNC
min avg min avg min avg
0.097 0.129 0.217 0.314 0.14 0.278
0.435 0.457 0.475 0.494 0.425 0.482
0.438 0.459 0.463 0.479 0.432 0.464
0.308 0.318 0.381 0.404 0.274 0.409
0.131 0.096 0.183 0.169 0.155 0.24
Table 5: Context representations obtained via the holographic method for HHMs. Measures computed with α =
0.3. Boldface indicates the best-performing measure.
unigram probability from the acceptability score.
Word frequency has little influence on acceptabil-
ity (r = 0.2 between acceptability and unigram
probability), but greatly affects the log probability
computed by the language models.
To test the robustness of our methodology on
the test data, we also test the models on the GUG’s
development set (747 sentences after preprocess-
ing) and find that a 5-gram model trained on the
BNC gets the best correlation at r = 0.467.
4.2.3 Perplexity versus Acceptability
The perplexity of a language model on test data
is commonly used to evaluate model performance.
In this section we explore the relationship between
perplexity and acceptability.
We trained several neural network language
models using Tensorflow including RNNLM,
GRULM and LSTMLM on the Penn Tree Bank
corpus (PTBC)6. PTBC is smaller than BNC and
NC with a 10k vocabulary size, so it is faster
to train on. We set the word embedding size to
300, initializing to random values sampled from a
6
https://catalog.ldc.upenn.edu/
LDC99T42
Gaussian Distribution, and set the embeddings to
be to trainable in the model. We set the maximum
sequence length to 25 and hidden, recurrent layer
size to 1024. Model training halts if validation loss
does not decrease for three consecutive epochs and
the perplexity (ppl) on the PTBC test data is used
to evaluate models in the last epoch.
Figure 1: Perplexity versus correlation of NormSub to
acceptability across different models.
Over the course of training, as the perplexity on
the validation data decreases, we find that the cor-
relation to acceptability tends to increase. How-
ever, when comparing perplexities across differ-
ent types of models, the perplexity is not strongly
predictive of which type of model makes more
human-like acceptability predictions, as shown in
measure
4-gram 5-gram RNN
BNC NC BNC NC BNC NC
Score_O 0.284 0.250 0.252 0.311 0.337 0.284
Score_C 0.315 0.302 0.315 0.302 0.295 0.314
Mis 0.465 0.460 0.464 0.459 0.448 0.461
NormMul 0.424 0.418 0.423 0.417 0.410 0.421
NormSub 0.528 0.518 0.527 0.518 0.510 0.527
SLOR 0.469 0.457 0.474 0.460 0.472 0.485
Table 6: Pearson’s r between model predictions and mean acceptabiliy for language models trained on the BNC
and NC corpora with α = 1.3. Boldface indicates the best performing models and measures.
Measures LSTM LSTM RNN LSTM
HHM (fixed) HHM (trainable) Gaussian Gaussian
Score 0.42 0.408 0.307 0.344
Mis 0.493 0.49 0.418 0.445
NormMul 0.453 0.447 0.385 0.406
NormSub 0.509 0.522 0.453 0.489
SLOR 0.38 0.4 0.339 0.374
Table 7: Pearson’s r between model predictions and mean acceptability for language models trained on the NC
corpus with α = 2.1. The models are an LSTM with fixed embeddings, an LSTM with trainable embeddings, an
RNN with randomly initialized trainable embeddings, and an LSTM with randomly initialized trainable embed-
dings. Boldface indicates the best performing models and measures.
Figure 1. Even though the GRULM has the low-
est perplexity of the three models (92.773), the
LSTMLM has the highest correlation to human ac-
ceptability (r = 0.513). Thus, a different model
with a different architecture and a lower perplex-
ity may not be better able to predict acceptability.
5 Discussion
Modeling acceptability provides a window into the
human brain’s language engine. We explore what
aspects of language are important in order to ac-
count for what people consider acceptable, well-
formed sentences. In particular, we examine the
importance of misspellings, semantic coherence,
and n-gram probability. To examine each, we use
simple features, word embeddings, n-gram mod-
els, and predictive neural language models.
We find that accounting for misspellings consid-
erably improves the ability of unsupervised mod-
els to capture acceptability judgments. Prior work
by Heilman et al. (2014) with supervised models
found that the number of misspelled words was an
important feature for their models. We develop a
technique for incorporating misspellings into un-
supervised models by correcting all spelling errors
so that the model works with clean data, and then
raising the model’s acceptability estimate to the
power of the number of misspelled words.
Using word embeddings to compute the seman-
tic coherence of a sentence, we examine the con-
tribution of semantic coherence with respect to
acceptability. By itself, semantic coherence cor-
relates poorly with acceptability, providing evi-
dence that meaning constitutes only a small part
of what makes a sentence acceptable or unaccept-
able. The finding that semantics plays only a small
role in acceptability is in keeping with Chomsky
(1956)’s arguments for a distinction between syn-
tactically well-formed sentences and semantically
well-formed sentences.
However, when semantic coherence is com-
bined with misspellings and unigram probabilities
(i.e., word frequencies), we can account for much
of the variability in acceptability (r = 0.46). We
can further improve the correlation by incorporat-
ing the order of the words in the sentence into our
measure of semantic coherence (r = 0.49). These
results suggest that acceptability is not wholly in-
dependent of semantics.
Language models provide a means of estimat-
ing the probability of a sentence, which in turn can
be used to predict acceptability. We replicate prior
work (Lau et al., 2015) in finding that the RNNLM
is a good language model for accounting for ac-
ceptability (r = 0.53). Yet, a simple 4- or 5-gram
model with statistical smoothing is just as good as
an RNNLM. So, 4- or 5-gram frequencies may be
what the neural network model primarily learns to
rely upon to model human language with respect
to acceptability judgments.
The quality of a language model is typically
evaluated using perplexity, a measure of the uncer-
tainty of the model’s predictions. We find that, un-
surprisingly, over the course of training, as the lan-
guage model’s perplexity decreases, the model’s
ability to predict acceptability increases. However,
the decreases in perplexity across language model
architectures do not directly map onto increases in
correlation to human acceptability judgements.
We replicate Lau et al. (2015)’s finding that
the log probabilities produced by language mod-
els need to be normalized by subtracting the un-
igram probability. This normalization, Norm-
Sub, prevents the models from underestimating
the acceptability of sentences with low frequency
words. Lau et al. (2015) found that the Syntactic
Log Odds Ratio (SLOR; Pauls and Klein, 2012),
which further normalizes by dividing by sentence
length, proved the best method for converting the
log probabilities of language models into an ac-
ceptability prediction. However, we consistently
found that SLOR performed less well at predicting
acceptability than NormSub (see Tables 6 and 7).
The difference between our findings and Lau
et al.’s may be attributable to the very different
datasets we use: Lau et al. primarily report re-
sults for a machine translation dataset, whereas we
evaluate results on a dataset of sentences produced
by second language speakers. The appropriateness
of normalizing by sentence length thus may be de-
pendent on the characteristics of the language be-
ing evaluated for acceptability.
In summary, the best and simplest model of ac-
ceptability judgment needs only three things: 4-
or 5-gram frequency (with statistical smoothing),
misspellings, and word (or unigram) frequency.
Accounting for human judgments of acceptabil-
ity does not require a complex model of syntax
or even a sophisticated predictive neural model.
Rather, acceptability appears to be a function of
correct spelling, the statistical relationships be-
tween words, word meanings, and word order.
Strictly ‘grammatical’ language is not necessarily
idiomatic, and NLP methods may want to strive
for idiomatic and acceptable rather than merely
grammatical language.
The correlation of non-expert of human raters
to mean acceptability ranges from 0.440 to 0.485.
Our models’ correlation with the mean acceptabil-
ity exceeds the rater reliability.
Yet even our best model is not as good as the
expert rater, which has a correlation of 0.75 with
the mean rating. This is an obvious challenge for
future work (cf., D ˛
abrowska, 2010). Do experts
have more experience and thus, different assump-
tions of lexical or syntactic distributions, or do
they just interpret the task differently, perhaps iso-
lating grammaticality from the less well-defined
acceptability? Do their judgments represent less
dialectal variety, or are they correlated with ease-
of-processing? Future models will, hopefully, be
able to capture this expert-level performance.
We have considered only unsupervised models
of acceptability on the assumption that unsuper-
vised data best reflects the learning environment of
the average native speaker of the language. How-
ever, supervised approaches may be appropriate
for modelling expert performance, as language ex-
perts have likely had the benefit of explicit train-
ing on what is and is not acceptable and ‘proper’
language. Though it remains an open question
whether capturing such expertise is even desirable
given that ‘proper’ language is typically associated
with a specific region or class, rather than the pop-
ulation as a whole (McArthur, 1992, pp. 984-985).
6 Conclusion
While more sophisticated neural language models
may have lower perplexity than simpler language
models, we find that simple n-gram models per-
form just as well at predicting acceptability. By
incorporating a count of misspellings, our 4-gram
model surpasses the previous unsupervised state-
of-the art (Lau et al, 2015, r = 0.472), reduc-
ing the gap to expert performance (r = 0.75) and
surpassing the average non-expert native speaker
(r = 0.46). It is also possible to achieve a high
correlation to acceptability without using a pre-
dictive language model at all, by combining num-
ber of misspellings, semantic similarity between
words, and word order information (r = 0.494).
Our results suggest that the human ability to
judge whether or not a given sentence is accept-
able may be derived from sensitivity to simple, sta-
tistical language features, without necessarily re-
quiring syntactic rules or even a particularly com-
plex machine learning model.
Acknowledgments
This work was supported by a Post-Doctoral Fel-
lowship from the Natural Sciences and Engineer-
ing Research Council of Canada (NSERC) to M.
A. Kelly and a National Science Foundation grant
(BCS-1734304) to David Reitter and M. A. Kelly.
References
Philippe Blache, Barbara Hemforth, and Stéphane
Rauzy. 2006. Acceptability prediction by means
of grammaticality quantification. In Proceedings of
the 21st International Conference on Computational
Linguistics and the 44th Annual Meeting of the Asso-
ciation for Computational Linguistics, pages 57–64,
Sydney, Australia.
British National Corpus Consortium. 2007. British na-
tional corpus version 3 (BNC XML edition). Dis-
tributed by Bodleian Libraries, University of Oxford,
on behalf of the BNC Consortium.
N. Chomsky. 1956. Three models for the description of
language. IRE Transactions on Information Theory,
2:113–124.
Alexander Clark, Gianluca Giorgolo, and Shalom Lap-
pin. 2013a. Statistical representation of grammat-
icality judgements: the limits of n-gram models.
In Proceedings of the Fourth Annual Workshop on
Cognitive Modeling and Computational Linguistics
(CMCL), pages 28–36.
Alexander Clark, Gianluca Giorgolo, and Shalom Lap-
pin. 2013b. Towards a statistical model of grammat-
icality. In Proceedings of the 35th Annual Meeting
of the Cognitive Science Society, pages 2064–2069,
Austin, TX. Cognitive Science Society.
Ewa D ˛
abrowska. 2010. Naive v. expert intuitions: An
empirical study of acceptability judgments. The Lin-
guistic Review, 27(1):1–23.
Jeffrey Elman. 1998. Generalization, simple recur-
rent networks, and the emergence of structure. In
Proceedings of the Twentieth Annual Conference of
the Cognitive Science Society, page 6. Mahwah, NJ:
Lawrence Erlbaum Associates.
Francis Ferraro, Matt Post, and Benjamin Van Durme.
2012. Judging grammaticality with count-induced
tree substitution grammars. In Proceedings of the
Seventh Workshop on Building Educational Appli-
cations Using NLP, pages 116–121. Association for
Computational Linguistics.
Kristina Gulordava, Piotr Bojanowski, Edouard Grave,
Tal Linzen, and Marco Baroni. 2018. Colorless
green recurrent networks dream hierarchically. In
Proceedings of the 2018 Conference of the North
American Chapter of the Association for Computa-
tional Linguistics: Human Language Technologies,
Volume 1 (Long Papers), pages 1195–1205. Associ-
ation for Computational Linguistics.
Michael Heilman, Aoife Cahill, Nitin Madnani,
Melissa Lopez, Matthew Mulholland, and Joel
Tetreault. 2014. Predicting grammaticality on an
ordinal scale. In Proceedings of the 52nd Annual
Meeting of the Association for Computational Lin-
guistics, volume 2, pages 174–180.
Brendan T. Johns, Michael N. Jones, and D. J. K. Me-
whort. 2016. Experience as a free parameter in the
cognitive modeling of language. In Proceedings of
the 38th Annual Meeting of the Cognitive Science
Society, pages 1325–1330, Austin, TX. Cognitive
Science Society.
M. N. Jones and D. J. K. Mewhort. 2007. Representing
word meaning and order information in a composite
holographic lexicon. Psychological Review, 114:1–
37.
Frank Keller. 2000. Gradience in grammar: Exper-
imental and computational aspects of degrees of
grammaticality. Ph.D. thesis, University of Edin-
burgh.
Matthew A. Kelly, David Reitter, and Robert L. West.
2017. Degrees of separation in semantic and syntac-
tic relationships. In Proceedings of the 15th Inter-
national Conference on Cognitive Modeling, pages
199–204. University of Warwick, Warwick, U.K.
Jey Han Lau, Alexander Clark, and Shalom Lappin.
2014. Measuring gradience in speakers’ grammat-
icality judgements. In Proceedings of the 36th An-
nual Meeting of the Cognitive Science Society, pages
821–826, Austin, TX. Cognitive Science Society.
Jey Han Lau, Alexander Clark, and Shalom Lap-
pin. 2015. Unsupervised prediction of acceptabil-
ity judgements. In Proceedings of the 53rd Annual
Meeting of the Association for Computational Lin-
guistics and the 7th International Joint Conference
on Natural Language Processing, volume 1, pages
1618–1628.
Marco Marelli and Marco Baroni. 2015. Affixation
in semantic space: Modeling morpheme meanings
with compositional distributional semantics. Psy-
chological Review, 122(3):485–515.
Tom McArthur, editor. 1992. The Oxford Companion
to the English Language. Oxford University Press.
Tomáš Mikolov. 2012. Statistical language models
based on neural networks. Presentation at Google,
Mountain View, 2nd April.
Tomáš Mikolov, Kai Chen, Greg Corrado, and Jef-
frey Dean. 2013a. Efficient estimation of word
representations in vector space. arXiv preprint
arXiv:1301.3781.
Tomáš Mikolov, Martin Karafiát, Lukáš Burget, Jan
Černockỳ, and Sanjeev Khudanpur. 2010. Recur-
rent neural network based language model. In
Eleventh Annual Conference of the International
Speech Communication Association, pages 1045–
1048, Makuhari, Chiba, Japan.
Tomáš Mikolov, Ilya Sutskever, Kai Chen, Greg S Cor-
rado, and Jeff Dean. 2013b. Distributed representa-
tions of words and phrases and their compositional-
ity. In Advances in Neural Information Processing
systems, pages 3111–3119.
Adam Pauls and Dan Klein. 2012. Large-scale syntac-
tic language modeling with treelets. In Proceedings
of the 50th Annual Meeting of the Association for
Computational Linguistics, volume 1, pages 959–
968. Association for Computational Linguistics.
Jeffrey Pennington, Richard Socher, and Christo-
pher D. Manning. 2014. GloVe: Global vectors for
word representation. In Proceedings of the 2014
Conference on Empirical Methods in Natural Lan-
guage Processing, pages 1532–1543.
Andreas Stolcke. 2002. SRILM - an extensible lan-
guage modeling toolkit. In Seventh International
Conference on Spoken Language Processing, pages
257–286.
Eva M. Vecchi, Marco Marelli, Roberto Zamparelli,
and Marco Baroni. 2017. Spicy adjectives and nom-
inal donkeys: Capturing semantic deviance using
compositionality in distributional spaces. Cognitive
Science, 41(1):102–136.
Sze-Meng Jojo Wong and Mark Dras. 2010. Parser fea-
tures for sentence grammaticality classification. In
Proceedings of the Australasian Language Technol-
ogy Association Workshop 2010, pages 67–75.
