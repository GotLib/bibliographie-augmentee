PCA-Initialized Deep Neural Networks Applied To
Document Image Analysis
Mathias Seuret∗, Michele Alberti∗, Rolf Ingold∗, and Marcus Liwicki∗‡,
∗University of Fribourg, Department of Informatics
Bd. de Pérolles 90, 1700 Fribourg, Switzerland
Email: firstname.lastname@unifr.ch
‡ University of Kaiserslautern, Germany
Abstract—In this paper, we present a novel approach for
initializing deep neural networks, i.e., by turning Principal
Component Analysis (PCA) into neural layers. Usually, the ini-
tialization of the weights of a deep neural network is done in one
of the three following ways: 1) with random values, 2) layer-wise,
usually as Deep Belief Network or as auto-encoder, and 3) re-use
of layers from another network (transfer learning). Therefore,
typically, many training epochs are needed before meaningful
weights are learned, or a rather similar dataset is required for
seeding a fine-tuning of transfer learning. In this paper, we
describe how to turn a PCA into an auto-encoder, by generating
an encoder layer of the PCA parameters and furthermore adding
a decoding layer. We analyze the initialization technique on real
documents. First, we show that a PCA-based initialization is quick
and leads to a very stable initialization. Furthermore, for the
task of layout analysis we investigate the effectiveness of PCA-
based initialization and show that it outperforms state-of-the-art
random weight initialization methods.
I. INTRODUCTION
A recent trend in machine learning is training very deep
neural networks. Although artificial neurons have been around
for a long time, the depth of commonly used artificial neural
networks has started to increase significantly only for roughly
15 years [9]1
. This is due to both a combination of higher
computational power available to researchers, and to a coming
back of layer-wise training methods [2] (referred to as Deep
Belief Networks [10], and often composed of Restricted Boltz-
mann Machines [18]).
However, the training of deep neural networks still suffers
from two major drawbacks. First, training a Deep Neural Net-
work (DNN) still takes some time, and second, initializing the
weights of DNN with random values adds some randomness
to the obtained results.
Especially for large input images or patches with di-
verse content, the training time becomes a crucial issue, as
many weights have to be trained and a training epoch takes
much time. Historical Document Image Analysis (DIA) is
en example for such tasks as input documents could be of
several megabytes and the historical documents can be quite
diverse. The diversity has several origins: degradations present
in the documents, complexity and variability of the layouts,
overlapping of contents, writing styles, bleed-through, etc.
In this paper, we present a novel initialization method based
on the PCA which allows to quickly initialize the weights of a
1Note that deep neural architectures where proposed already much earlier,
but they have not been heavily used in practice [16]
neural network with non-random values, and additionally leads
to a high similarity in the weights of two networks initialized
on different random samples from a same dataset.
II. STATE OF THE ART
There are currently three main trends for neural network
initialization: 1) random initial initial weights [8], layer-wise
unsupervised pre-training [2], [10], or transfer learning [3].
Random initialization is fast and simple to implement. The
most used one, presented by Glorot et al. recommend [8] to
initialize weights of a neuron in
h
−
√
N,
√
N
i
, where N is
the number of inputs of the neuron. This initialization is often
called “Xavier” initialization, motivated by the first author’s
name in [8].
The link between PCA and neural networks is not novel. In
1982, Oja introduced a linear neuron model learning to gen-
erate the principal component of its input data [13]. Oja also
later investigated the possibility of having linear neural layers
learn principal and minor components of a PCA [14]. Since
then, other publication present methods teaching networks to
compute PCA. PCA is also used for dimensionality reduction
as a pre-processing steps [12], [19]. Chan et al. showed that
PCA filters can be convolved and stacked in order to create
efficient feature extractors [4].
Note that while we were conducting our experiments
(published in a Master thesis [1]) a similar, but independent,
research has been published at ICLR 2016 [11]. While the
general initial idea of using PCA for initialization is similar, it
is applied to Convolutional Neural Network (CNN) in [11].
In this work, we introduce a mathematical framework for
generating Convolutional Auto-Encoder (CAE) out of the PCA,
taking into account the bias of neural layers, and provide a
deeper analysis of the behavior of PCA-initialized networks –
both for CAE and CNN – with a focus on historical document
images.
III. PCA AS AUTO-ENCODER
An Auto-Encoder (AE) is a machine learning method
which is trained to encode its input as a (usually) compressed
representation, and decode this representation to recover the
input with an accuracy as good as possible. This compressed
representation, also called feature vector, contains a represen-
tation of the input of the AE and can be used for classification
purpose. A DNN can be trained by stacking AEs, as the layers
arXiv:1702.00177v1
[cs.LG]
1
Feb
2017
can be trained one after another, without time-consuming back-
propagation through all layers.
A. Encoder Computation
A standard artificial neural layer takes as input a vector
~
x, multiplies it by a weight matrix W, adds a bias vector ~
b,
and applies a non-linear activation function f to the result to
obtain the output ~
y:
~
ynn = f

~
b + W · ~
x

(1)
An AE is usually composed of two neural layers, one which
is trained to encode, and the second to decode. The weights
of these layers can have a relationship (e.g., in Restricted
Boltzmann Machines (RBMs), the matrix of the decoder is
the transpose of the matrix of the encoder), but this is not
mandatory.
A PCA is computed by subtracting the mean vector from
the data ~
m to an input vector ~
x, and multiplying the result by
a matrix R to obtain the result y:
~
ypca = R · (~
x − ~
m) = R · ~
x − R · ~
m (2)
In order to have a neural layer having a similar behavior
as the PCA, we have to also apply the activation function f to
the PCA, and thus obtain what we call activated PCA:
~
yapca = f (R · ~
x − R · ~
m) (3)
We want that ~
yapca = ~
ynn. Let ~
x0 = ~
0. Then, we have
f

R ·~
0 − R · ~
m

= f

~
b + W ·~
0

(4)
Assuming that f is strictly monotonous, we get that ~
b =
−R · ~
m. We can inject this result in Equation 3 to get
f

R · ~
x +~
b

= f

~
b + W · ~
x

(5)
Thus, we get that W = R. This allows us to set the
weights and bias of a neural layer such that it behaves exactly
as an activated PCA. The additional computational cost for
computing ~
b is negligible, thus the initialization cost of the
network depends only on the number of samples used for
computing the PCA.2
In order to decrease the dimensionality of the encoded
values, only some of the principal components can be kept. For
reasons of notation simplicity, we will however ignore this in
the formulas. We will also show only the 9 first components in
the figures, as having more would make them tiny and harder
to analyze.
If the activation function is not strictly monotonous, e.g.,
as the rectified linear unit, then for each sample the activation
2note that the samples used for PCA can be less than the full training set.
We investigate this aspect in the experiments
probability of each feature is 50%. If N features are used, then
1
2N of the samples will not activate any feature. If this is an
issue, this can be tackled by duplicating one of the component
and multiplying its weights by -1.
B. Decoder Approximation
In the previous step, we proved that a PCA can be used to
initialize the weights of a single neural layer. However, a layer
in an AE is composed of two neural layers: the encoder and
the decoder. While PCA-initialized neural layers can be used
for generating the encoder, initializing the decoder requires
another approach if we want to have two neural layers having
the same implementation. Encoding and decoding an input ~
x
with two layers, having respectively the weights W1 and W2,
and the bias ~
b1 and ~
b2 is done as the following:
~
x ≈ f

~
b2 + W2 · f

~
b1 + W1 · ~
x

(6)
However, the reconstruction of ~
x cannot be perfect for most
activation functions f. Thus, we propose to compute ~
b2 and
W2 by minimizing the squared error of the reconstruction of
at least as many samples as there are dimensions in the PCA.
Let us assume that X is a matrix which columns are
composed of at least as many input vectors as components
from the PCA have been kept. We can create the matrices
X+
by appending a row composed of 1’s to X, and W+
1 by
appending a column composed of the vector ~
b1 to W1.
Then, we can compute the matrix Y storing the encoded
samples in its columns as
Y = f W+
1 · X+

(7)
Then, to compute ~
b2 and W2, we have to find the matrix
W+
2 approximating the following with the smallest square
error:
X ≈ f W+
2 · Y

(8)
where X is an input vector filled either with real samples,
or random values. In other words, we have to find the least-
squares solution to the following equation system: W+
2 · Y =
f−1
(X). The vector ~
b2 is then the last column of W+
2 , and
W2 is the other columns of W+
2 .
C. Differences Between PCA and AEs
The matrix R of a PCA can be inverted, thus we have
~
x = R−1
· R · (~
x − ~
m) + ~
m (9)
However, in the case of an AE, the non-linearity prevents
from reconstructing perfectly the input. In practice, this leads
to having a lower contrast when decoding, as the encoded val-
ues, which are unbounded when applying a PCA are restricted
to the interval ]−1, 1[ when applying a PCA-initialized neural
layer. This effect is illustrated in Figure 1.
Note that AEs are used for initializing the weights of neural
networks, so the decoding layer is not used for classification
tasks. Thus, this is not an issue for classification if we get low-
contrast reconstructions. However, in order to allow a better
visual inspection of the features, we will enhance their contrast
in this paper.
(a) PCA features (b) AE features
Fig. 1: Features computed by a PCA, and their version obtained
when turning the PCA into an AE.
IV. IMPACT EVALUATION
In this section, we evaluate two aspect of a PCA-based ini-
tialization: first, how stable the features are, and second, what
the impact of a PCA-based initialization is for classification
tasks. All time measurements have been done on a computer
with an i5-4570 processor; note that some of the tasks were
done too fast for it to increase its frequency.
We used six manuscripts coming from two datasets. The
first dataset is composed of pages from the manuscripts
SG8573
, SG5624
, and GW105
. The layout has been annotated
with hand-drawn polygons using DIVADIA [6]. The different
classes are out-of-page, background, text, comments, and an-
notations. The second dataset is called DIVA-HisDB [17]. It
is composed of pages from the manuscripts CB556
, CS187
,
and CS8638
. The layout has been annotated using Graph-
Manuscribble [7] using a mix of pen-based interactions and
automatic polygon generation.
A. Initialization Stability
As we keep only some of the principal components of the
data samples used for training, we can expect the features
to be stable because they will represent main trends of the
data and thus be rather stable with regard to the number of
samples used for computing the PCA, or the pages used as
sample sources. Features obtained through PCA-initialization
of three-levels AEs are shown in Fig. 2. Note that the six
AEs used for generating the features of Fig 2 were trained
completely independently. Differences between initializations
with 500 and 50’000 samples lead to similar features, however
3St. Gallen, Stiftsbibliothek, Cod. Sang. 857 (13th century)
4St. Gallen, Stiftsbibliothek, Cod. Sang. 562 (9th century)
5G. W. Papers, Library of Congress, (18th century)
6Köln, Fondation Bodmer, Cod. Bodm. 55 (14th century)
7St. Gallen, Stiftsbibliothek, Cod. Sang. 18 (11th century)
8St. Gallen, Stiftsbibliothek, Cod. Sang. 863
the initialization times were very different, with less than 1
second for 500 samples, and ≈ 2100 seconds for 50’000
samples. Note that due to the differences in training and tests,
the results we obtained cannot be compared to our previous
works.
Another kind of stability can be observed. If we initialize
an AE on two different pages of a same manuscript, we can
expect the features to be similar. While it is not the case when
training a network as a standard AE, we can see in Fig. 2e
and 2f that when using a PCA to initialize the AE the features
look very similar. They are not identical, but some patterns are
shared. Note that these features are in the third layer; features
from the first and second layers, which are not shown for space
reason, are almost indistinguishable.
B. Feature Activation Map
Visualizing features as shown in Fig. 2 is not necessarily
helpful to know where they are active or not. For this reason,
we generate heatmaps showing where some features are the
most active. We center the CAE on each pixel of the input
image, compute its output, and store the activation value of the
three first values as the red, green and blue components of the
corresponding pixel of the output image. Note that there is no
labelled training involved in this task – the AE are completely
unsupervised.
Very good features should have high levels of correlation
with the classes to discriminate. We can see in Fig. 3b that
the PCA-initialized AE leads to a smooth representation of
its input, with clear distinctions between the main part of the
decoration (green), the thin red lines below the decoration
(purple), and also hints the text line positions (yellow) and
interlines (gray). However, while the Xavier-initialized AE
seems to capture some areas of the main part of the decoration,
the feature activation map is more noisy, and the colors seem
less correlated to the kind of content, as there is mostly
only red, green and blue. The background is however better
represented by the Xavier-initialized AE.
C. Classification Task
We evaluate our method for a document image layout
analysis task at pixel-level. The goal is to label the pixels
of a document image as corresponding to different kinds
of content, e.g., background or interlinear glosses. This is a
continuation of our previous work [5], with the main difference
that instead of extracting features from the central position
of each layer and feed them to an Support Vector Machine
(SVM), we add a classification layer on top of the AE and
do the backpropagation through the whole network. This is an
approach which we have already successfully applied on the
manuscripts of the DIVA-HisDB [17].
The datasets which we use are split into three parts:
training, test and validation. We used document images from
both training and test subsets. However, as one of our goal is
to study the accuracy on the test data during the training, a
large quantity of tests have had to be done. For this reason,
we decided to use the following restrictions: we used only two
pages from the training set, and test networks on the labelling
of 10’000 pixels from the test set. Also in order to run a fair
comparison between Xavier and PCA initializations, we use
(a) CB55, 500 samples (b) CB55, 5’000 samples
(c) CB55, 10’000 samples (d) CB55, 50’000 samples
(e) CS18, page 105 (f) CS18, page 118
Fig. 2: Illustration of the stability of features of the third layer
of an AE. The features a, b, c, and d were obtained from
different number of samples randomly selected from CB55.
The features shown in (e) and (f) were obtained using two
different pages from a same manuscript. Note that the features
have been enhanced for better visual inspections (See end of
Section III-C).
the same training and test samples for all networks trained on
a same manuscript. Thus, differences of accuracy at any point
in the training are due only to the initial weights of a network.
The results obtained during the training are shown in Fig. 4.
We can see that PCA initialization always outperforms Xavier
initialization, although after a long training they converge
toward similar results. We can however see that although
the initial features obtained by PCA are visually stable, the
variance of the accuracy for the two approaches is roughly the
same at any point of the training, excepted for the networks
trained on CS18 (Fig. 4b) because the PCA-initialized networks
finished converging very quickly. This is explained by the fact
(a) CS857 sample (b) PCA activation
(c) Xavier activation (d) Xavier enhanced
Fig. 3: Activation of the three first neurons of CAE on a test
image sample. The activation values are represented by the
intensity of the red, green and blue component of the resulting
images. As with Xavier initialization this leads to a grayish
result, we also show a second version with color enhancement.
that small differences in initial conditions can lead to very
different results [15], thus although rather similar, the PCA
initialization leads to an error entropy similar to a random,
i.e., Xavier, initialization.
D. Error Backpropagation
We can see in Fig. 4 that PCA-initialized networks converge
very quickly. To investigate this, we define and use the Relative
Backpropagated Error (RBE):
rbe (n) =
P
i |en,in,i|
P
i |en,out,i|
(10)
where n is a network, en,in,i is the error of backpropagated
to the i-th neuron of the first neural layer of n, and en,out,i is
the error of the output layer of n with regard to the expected
classification result.
The RBE is directly linked to how much the weights of
a network are modified during a gradient descent. Thus, it
can give hints about how fast a network is learning, and can
be used to compare two converging networks, larger RBE
indicating larger learning rate per sample. However, the RBE
value alone cannot predict whether a network converges or not
– a diverging network could even lead to apparently favorable
RBE. In this paper, all networks are converging correctly,
therefore the RBE is a reliable measurement.
(a) CS18, short (b) CS18, long
(c) CB55, long (d) GW10, short (e) GW10, long
(f) SG562, long (g) CS863, long (h) SG857, long
Fig. 4: Test accuracy of networks during training, for different
manuscripts. Each image contains data from 16 networks: 8 us-
ing Xavier initialization (green), and 8 using PCA initialization.
Short training uses 50’000 training samples (1 epoch), and tests
were run every 100 samples. Long training uses 2’000’000
training samples (1 epoch), and tests were run every 10’000
samples.
We can see in Fig. 5 that PCA-initialized networks (in
purple) always lead to higher RBE than Xavier-initialized
networks. This explains why our approach converges much
faster. At the beginning of the training, the RBE can be up
to 25 times larger when using a PCA initialization. After a
long training, the RBE for both approaches seem to converge
toward similar values, in a similar fashion as the accuracy
does in Fig. 4, but even after 2 million weights adjustments,
the PCA initialization still leads to more favorable RBE. This
explains why the PCA-initialized networks always reach a high
accuracy earlier than xavier-initialized networks.
V. CONCLUSION AND FUTURE WORK
In this paper, we have investigated a new approach for
initializing deep auto-encoders using PCA. This leads to sig-
nificantly better performances for layout analysis tasks for an
initialization time that can be considered as negligible. We
also investigated the stability of the features learned by AE
initialized with PCA, and show that very few samples are
required for computing the PCA.
(a) CS18, short (b) CS18, long
(c) CB55, long (d) GW10, short (e) GW10, long
(f) SG562, long (g) CS863, long (h) SG857, long
Fig. 5: Relative Backpropagated Error (RBE) of networks dur-
ing training, for different manuscripts. Each graphic contains
data from 16 networks, where horizontal corresponds to the
number of training samples used, and vertical to the logarithm
of the sum of RBE measured on 100 samples for short training
and 10’000 samples for long training. Green points correspond
to Xavier initialization, and purple points to PCA initialization.
Note that the plots match the ones from Fig. 4.
As future work, we intend to study the use of Linear
Discriminant Analysis (LDA) to initialize deep auto-encoders
for various types of classification tasks. We assert that this
would not only lead to meaningful initializations, as it is the
case with PCA, but also generate weights more useful for
classification tasks.
We also hypothesize that a PCA-initialized network would
outperform a Xavier-initialized network in case of transfer
learning, as its weights would be tuned faster to the new task.
This, however, has to be validated on varios scenarios.
ACKNOWLEDGMENT
We would like to thank Kai Chen for feedback he provided
during the investigations. The work presented in this paper has
been partially supported by the HisDoc III project funded by
the Swiss National Science Foundation with the grant number
205120 169618.
REFERENCES
[1] Michele Alberti. Using linear discriminant analysis for deep neural
network initialization. 2016. Master thesis.
[2] Dana H Ballard. Modular learning in neural networks. In AAAI, pages
279–284, 1987.
[3] Rich Caruana. Multitask learning. In Learning to learn, pages 95–133.
Springer, 1998.
[4] Tsung-Han Chan, Kui Jia, Shenghua Gao, Jiwen Lu, Zinan Zeng, and
Yi Ma. Pcanet: A simple deep learning baseline for image classification?
IEEE Transactions on Image Processing, 24(12):5017–5032, 2015.
[5] Kai Chen, Mathias Seuret, Marcus Liwicki, Jean Hennebert, and Rolf
Ingold. Page segmentation of historical document images with convolu-
tional autoencoders. In Document Analysis and Recognition (ICDAR),
2015 13th International Conference on, pages 1011–1015. IEEE, 2015.
[6] Kai Chen, Mathias Seuret, Marcus Liwicki, Jean Hennebert, and Rolf
Ingold. Ground truth model, tool, and dataset for layout analysis of
historical documents. Document Recognition and Retrieval XXII, 2015,
To appear (Oral).
[7] A. Garz, M. Seuret, F. Simistira, A. Fischer, and R Ingold. Creating
ground truth for historical manuscripts with document graphs and
scribbling interaction. International Association of Pattern Recognition
(IAPR), pages 126–131, 2016.
[8] Xavier Glorot and Yoshua Bengio. Understanding the difficulty of
training deep feedforward neural networks. In Aistats, volume 9, pages
249–256, 2010.
[9] Ian Goodfellow, Yoshua Bengio, and Aaron Courville. Deep Learning.
MIT Press, 2016. http://www.deeplearningbook.org.
[10] Geoffrey E Hinton, Simon Osindero, and Yee-Whye Teh. A fast learning
algorithm for deep belief nets. Neural computation, 18(7):1527–1554,
2006.
[11] Krähenbühl, Philipp and Doersch, Carl and Donahue, Jeff and Darrell,
Trevor. Data-dependent initializations of convolutional neural networks.
arXiv preprint arXiv:1511.06856, 2015.
[12] Jiquan Ngiam, Aditya Khosla, Mingyu Kim, Juhan Nam, Honglak Lee,
and Andrew Y Ng. Multimodal deep learning. In Proceedings of the
28th international conference on machine learning (ICML-11), pages
689–696, 2011.
[13] Erkki Oja. Simplified neuron model as a principal component analyzer.
Journal of mathematical biology, 15(3):267–273, 1982.
[14] Erkki Oja. Principal components, minor components, and linear neural
networks. Neural networks, 5(6):927–935, 1992.
[15] Henri Poincaré. Les Méthodes Nouvelles de la Mécanique Céleste, vol-
ume 3. Gauthier-Villars, Bureau des Longitudes, école polytechnique,
1899.
[16] Jürgen Schmidhuber. Deep learning in neural networks: An overview.
Neural Networks, 61:85–117, 2015. Published online 2014; based on
TR arXiv:1404.7828 [cs.NE].
[17] Fotini Simistira, Mathias Seuret, Nicole Eichenberger, Angelika Garz,
Rolf Ingold, and Marcus Liwicki. Diva-hisdb: A precisely annotated
large dataset of challenging medieval manuscripts. In International
Conference on Frontiers in Handwriting Recognition, China, 10 2016.
[18] Paul Smolensky. Parallel Distributed Processing: Explorations in the
Microstructure of Cognition, volume 1. MIT Press, Cambridge, MA,
USA, 1986.
[19] Yi Sun, Xiaogang Wang, and Xiaoou Tang. Deep learning face
representation from predicting 10,000 classes. In Proceedings of the
IEEE Conference on Computer Vision and Pattern Recognition, pages
1891–1898, 2014.
