Multiscale Residual Mixture of PCA:
Dynamic Dictionaries for Optimal Basis Learning
Randall BALESTRIERO
ECE Department
Rice University
HOUSTON, TX
randallbalestriero@gmail.com
Abstract
In this paper we are interested in the problem of learning an over-complete basis
and a methodology such that the reconstruction or inverse problem does not need
optimization. We analyze the optimality of the presented approaches, their link
to popular already known techniques s.a. Artificial Neural Networks,k-means or
Oja’s learning rule. Finally, we will see that one approach to reach the optimal
dictionary is a factorial and hierarchical approach. The derived approach lead to
a formulation of a Deep Oja Network. We present results on different tasks and
present the resulting very efficient learning algorithm which brings a new vision on
the training of deep nets. Finally, the theoretical work shows that deep frameworks
are one way to efficiently have over-complete (combinatorially large) dictionary
yet allowing easy reconstruction. We thus present the Deep Residual Oja Network
(DRON). We demonstrate that a recursive deep approach working on the residuals
allow exponential decrease of the error w.r.t. the depth.
1 Introduction
We first discuss briefly the problem of dictionary learning and the different choices one has to make
to perform this task depending on the problem at hand. We also introduce notations and standard
operators.
1.1 Why a Dictionary ?
In order to analyze a given finite dataset X := {xn ∈ RD
, n = 1, .., N} different approaches are
possible. One of them lies on the assumption that those observations actually come from some latent
representation that are mixed together, different mixing leading to different observations but with
fixed dictionary ΦK := {φk ∈ RD
, k = 1, ..., K} with usually K  N. One can thus rewrite
xn = f(Φ, xn). (1)
In practice, a linear assumption is used for f and we write the prediction as
x̂n = ˆ
f(Φ̂K, xn), (2)
where one estimate the functional and the filter-bank through a reconstruction loss defined as
En = ||xn − x̂n||2
, (3)
where we assume here a squared error but any loss function can be used in general. The linear assump-
tion imposed on f leads to the estimation weighting coefficients we denote by αn,k weighting for
observation n the atom k, and those attributes are the new features used to represent the observations.
arXiv:1707.05840v1
[stat.ML]
18
Jul
2017
This new representation of xn via its corresponding feature vector αn can be used for many tasks s.a.
clustering, denoising, data generation, anomaly detection, compression and much more. Depending
on the constraints one imposes on the feature vectors αn and the dictionary ΦK, one can come
back to standard known frameworks s.a. Principal Component Analysis (PCA)[17], Independent
Component Analysis (ICA)[13], Sparse Coding (SC)[20], (Semi) Nonegative Matrix Factorization
(sNMF)[10, 18], Gaussian Mixture Model (GMM)[4], and many more, but all those approaches
can be categorized into two main categories: Complete Dictionary Learning and Over-Complete
Dictionary Learning.
The use of a dictionary also extends to standard template matching, the mot popular technique and
optimal in the GLRT sense, where new examples are to be mapped to one of the template to be
clustered or else.
1.2 (Over)complete Dictionary Learning
As we saw, the dictionary learning problem finds many formulations but the main difference resides
in the properties of the learned dictionary, namely if it is complete or over-complete. The general
case of complete or orthogonal basis imposes the following constraint on the filter-bank:
< φj, φk >= 0, ∀j 6= k, K = D, (4)
and with sometimes the complementary constraints that ||φk|| = 1, ∀k leads to an orthonormal basis.
The orthogonality of the atoms allow to have exact reconstruction leading to En = 0, ∀n as by
definition one has
x̂n =
X
i
< xn, φ̂k >
||φ̂k||2
φ̂k, ∀n. (5)
Given a dictionary Φ this decomposition is unique and thus we have (Φ̂K, xn) ⇒ α̂n, ∀n. However,
while through the Gram-Schmidt process existence of such a basis is guaranteed, it is not unique.
On the other hand, ΦK can be an over-complete basis with the main difference coming that now
K > D and with the extreme case of K = N. Containing more atoms than the dimension of
the space leads to interesting properties in practice such as sparse and independent features (ICA),
clustering properties (K-means, NMF), biological understanding as is has been empirically shown
that visual and auditory cortex of many mammals contains over-complete dictionaries. Yet, all those
benefits coming from the redundancy of the atoms also lead to the non-unique features αn even when
the dictionary is kept fixed, thus we have that (Φ̂K, xn) 6⇒ α̂n. As a result, one has to solve the
additional optimization problem of
α̂n = arg min
α∈Ω⊂RK
||xn −
X
k
αkφ̂k||. (6)
As a result, whether one is in a complete or over-complete dictionary setting, the optimization
problems are always ill-posed by the non-uniqueness of the solutions forcing to impose additional
structures or constraints in order to reach well posed problems. For the complete case, the main
approach consist in imposing that as few atoms as possible are used leading to PCA, a very powerful
approach for dimensionality reduction and compression. For over-complete cases, different sparsity
criteria are imposed on the features αn such as norm-(0,1,2). For a norm-0 we are in the Matching
pursuit case, norm1 is sparse coding and norm2 is ridge regression. For each of those cases many
very efficient exact or iterative optimization algorithms have been developed to estimate Φ̂k and α̂n
yet there still exist a conceptual gap between the two concepts and the two approaches are often seen
as orthogonal.
As we have seen, both settings lead to different beneficial aspects, compression, easy of projection
and reconstruction or sparsity/clustering but more complex optimization problems, but, at a higher
level, the signal processing community has always put a gap between those frameworks. As well put
by Meyer in [CITE] one has to choose between encoding and representation.
We thus propose in this paper a novel approach allowing to have an over-complete dictionary yet
given an input, a dynamic basis selection reduces it to an optimal complete dictionary without need
for optimization in order to reconstruct. The selection is done without optimization in a forward
manner leading to an efficient algorithm. This thus allows to inherit from all the properties induced by
2
orthonormal basis while allowing adaptivity for better allowing to learn an over-complete basis with
a nonlinear basis selection leading to an orthonormal basis when conditioned on the input. We also
provide results from a low-dimensional manifold perspective and show that our approach perform
nonparametric orbit estimation. We validate on compression, dictionary learning tasks and clustering.
2 Deep Residual Oja Network
2.1 Shallow Model
Is it possible to learn a dictionary inheriting the benefits or complete and over-complete dictionaries ?
We present one solution here. We first motivate the need for such a framework as well as present the
general approach and notations. Throughout the next sections, the choice of the Oja name for the
algorithm will become blatant for the reader.
Keeping the previously defined notation, we aim at learning an over-complete basis with the number
of atoms defined by FK with F > 1 called the increase factor, note that F = 1 lead to a complete
basis, K = D unless otherwise defined. By definition, the following projection-reconstruction
scheme
x̂n =
X
k
< x,φ̂k >
||φ̂k||2
φ̂k, (7)
can not reach an error En <  and in fact En increases with F. One way to resolve this issue comes
from an optimal basis selection point of view leading to
En := ||xn −
X
k
δn,k
< x,φ̂k >
||φ̂k||2
φ̂k|| < , (8)
with δn, k ∈ {0, 1}, ∀n, k representing a mask allowing to use a subset of the dictionary Φ̂F K we
denote by ρδn,.
[Φ̂F K].
2.1.1 Error Bounds, Learning and Link with Oja Rule
We first provide an error upper-bound for the proposed scheme (S1)κ=1. To simplify notations be
also define by φκ(xn) the atom defined by
φκ(xn) = φk0 , k0
= arg max
k
| < xn, φk > |2
||φk||2
. (9)
Theorem 1. The error induced by (S1)κ=1 is ||xn||2
− |<x,φκ(xn)>|2
||φκ(xn)||2 which is simply the recon-
struction error from the best atom as since only one filter is used, nothing else is present.
Proof. By the incomplete basis theorem, there exists a basis s.t. it contains the atom φκ, we denote
by φk such a basis, with k = 1, ..., D, we thus have
En =||xn −
< x, φκ(xn) >
||φκ(xn)||2
φκ(xn)||2
=||
X
k
< xn, φk >
||φk||2
φk −
< x, φκ(xn) >
||φκ(xn)||2
φκ(xn)||2
incomplete basis theorem
=||
X
k6=κ
< xn, φk >
||φk||2
φk||2
=
X
k6=κ
| < xn, φk > |2
||φk||2
=||x||2
−
| < xn, φκ(xn) > |2
||φκ(xn)||2
Parseval’s Theorem
=||x||2

1 − cos(θ(xn, φκ(xn))2

(10)
And we have by definition En ≥ 0, En = 0 ⇐⇒ xn = φκ(xn)
3
There is first a few comments on the loss and updates. First of all, the loss is closely related to a
k-mean objective with cosine similarity and specifically spherical k-means which is the case where
the centers and the data points are re-normalized to unit norm and has the objective to minimize
X
n
(1 − cos(xn, pc(n))). (11)
2.1.2 Learning and Oja Rule
In order to learn the filter-bank ΦK, a common approach is to use an alternating scheme between
finding the cluster belongings and optimizing the atoms w.r.t. this estimate. We first derive a gradient
descent scheme to update the atoms and study some of its characteristics.
If we now denote by n(k) := {n : n = 1, ..., N|κ(xn) = k} be the collection of the sample index in
cluster k, he resulting loss En(k) :=
P
n∈n(k) En
Card(n(k)) we can now derive a gradient descent step as
φ
(t+1)
k (λ) = φ
(t)
k − λ
dEn(k)
dφk
, (12)
with
dEn(k)
dφk
=
1
Card(n(k))
X
n∈n(k)
2| < xn, φk > |
||φk||2
| < xn, φk > |φk
||φk||2
− (−1)1<xn,φk><0
xn

,
=
1
Card(n(k))
X
n∈n(k)
2 < xn, φk >
||φk||2
< xn, φk > φk
||φk||2
− xn

. (13)
On the other hand, if one adopts an adaptive gradient step λ per atom and point with one of the two
strategies λ1, λ2 defined as
λ1 =
< xn, φk >
2||xn||2
(14)
λ2 =
||φk||4
2 < xn, φk >2
(15)
then we have the
φ
(t+1)
k (λ1) = φ
(t)
k −
1
P
n cos(θ(xn, φk))2
X
n∈n(k)
cos(θ(xn, φk))2
< xn, φk > φk
||φk||2
− xn

, (16)
φ
(t+1)
k (λ2) =
1
Card(n(k))
X
n∈n(k)
||φk||2
< xn, φk >
xn (17)
we thus end up with in the λ1 case to a simple update rule depending on a weighted average of the
points in the cluster based on their cosine similarity squared whereas for λ2 we obtain a rule a la
convex NMF with is a plain without update combination of the points available.
On the other hand, it is clear that minimizing En from Eq. 10 is equivalent to maximizing E+
n =
<xn,φκ(xn)>
||φκ(xn)||2 . As a result, one can seize in Eq. 13 the Oja rule as we can rewrite a GD update of φk
as
φ
(t+1)
k = φ
(t)
k + γ
dE+
n
dφk
(φ
(t)
k ) (18)
φ
(t+1)
k = φ
(t)
k + γ

xn
< xn, φk >
||φk||2
− (
< xn, φk >
||φk||2
)2
φk

(19)
known as the Oja rule. SPEAK ABOUT OJA RULE. And in fact, the convergence of Oja rule toward
the first eigenvector-eigenvalue is not surprising as E+
n(k) leads explicitly to
φk = arg max
φ
1
Card(n(k))
φT
X(k)T
X(k)φ
φT φ
, (20)
4
which is known as the Rayleigh quotient and is a formulation of PCA leading to a one step global
optimum being the greatest eigenvector-eigenvalue.
Algorithm 2.1: FILTER-BANK LEARNING STRATEGY(X, K)
Initialize ΦK
while not converged
do



for k ← 1 to K
do

Compute n(k) with current ΦK
Update φkwith n(k) and X(k) according to Eq. 20
return (Φk)
Algorithm 2.2: ONLINE FILTER-BANK LEARNING STRATEGY(X, K)
Initialize ΦK
while not converged
do





for n ← 1 to N
do
(
κ = arg maxk
|<xn,φk>|2
||φk||2||xn||2
Update φκ according to Eq. 16 or Eq.17 or Eq.19
return (Φk)
Theorem 2. If the distribution of the xn in the space is uniformly distributed, the optimal over-
complete basis for (S1)kappa=1 is thus the one corresponding of a quantization of the sphere, it is
unique up to a change of sign and global rotations (same applied to each atom). For the 2-dimensional
case it is easy to see that the maximum error for any given point xn is exactly upper-bounded by
||x||2

1 − cos( π
2F K )2

if FK growths exponentially .(?? CHECK POWER OF HIGH DIMENION
COSINE decompose as union of 2D spaces)
Proof. For 2D we now the upper bound is ||x||2

1 − cos( π
2F K )2

with FK atoms, we thus rewrite
||x − x̂||2
=
D/2
X
d=1
||xd − x̂d||2
5
and see that in order to have the upper bound for each subspace we need the cartesian product of all
the subspace basis ΦF K leading to FK atoms. Thus one need to grow exponentially the number of
atom w.r.t the dimension to have a loss increasing linearly.
However this pessimistic upper-bound assumes the worst possible scenario: uniform distribution of
the data point in the space RD
which in general is false. In fact, many dataset have inherent structures
and at least lye only in a small subset sometime regular of RD
. In general, data might be living in
unions of subsets and thus providing a general strategy or optimal basis is a priori more complex thus
pushing the need to learn the atoms as it is done in general for k-mean applications.
Theorem 3. A sufficient condition for (S1)kappa=1 to be optimal is that the data are already clustered
along FD lines, or orbits ??? :)
We now present one way to tackle the curse of dimensionality in the next section
2.2 Multiple Atoms
En = ||xn −
K
X
k=1
< x, φk
κ(xn) >
||φk
κ(xn)||2
φk
κ(xn)||2
(21)
For learning atom after atom a la coordinate ascent we have that
φ̂k0
j = arg min
φk0
j
X
n
En
= arg min
φk0
j
X
n∈n(k,j)
||

xn −
K
X
k=1,6=k0
< x, φk
κ(xn) >
||φk
κ(xn)||2

−
< x, φk0
j >
||φk0
j ||2
φk0
κ (xn)||2
as we showed in the last section we end up with the same update rule but with the input being
substracted by the other used atoms. Thus we still perform PCA but on the input minus the other
atoms. Note that this ensures orthogonality between the atoms.
2.3 From Shallow to Deep Residual for better Generalization Error Bounds and
Combinatorially Large Dictionaries
We now consider the analysis of the generalization performance on out of bag observations as well as
the problem of having really large dataset.
Theorem 4. If we suppose an finite training set and an Oja Network with sufficient filters in order to
reach a training error of 0 then we know that the generalization error is directly lower-bounded by
how close the testing and training examples are. In fact
Enew ∝ cos(θ(xκ, xnew)), κ = arg min
n
cos(θ(xn, xnew)). (22)
6
The proof is straightforward as we know the network is able to perfectly reconstruct the training set
and only the training set. As a result, if the training set is well and uniformly sampled among the
space of possible observations, a shallow Oja Network can be considered as optimal also for the
testing set. However, and especially for computer vision task, it is well known that every observation
is very far form each other in term of distance when dealt with in the pixel domain, also, requiring
a proper sampling of the space of images is clearly outrageous. We thus now present the result
motivating deep architectures in general including the Oja Network.
R(l)
n =R(l−1)
n −
< R
(l−1)
n , φ
(l)
κ >
||φ
(l)
κ ||2
φ(l)
κ , κ = arg max
k
| < R
(l−1)
n , φ
(l)
k > |
||R
(l−1)
n ||2||φ
(l)
k ||2
R(0)
n =xn (23)
as a result as soon as the input and the template are not orthogonal there is convergence.
Theorem 5. Since we have by definition that the selected atom is the one with smaller angle, if it is 0
it means that the input R(l−1)
is orthogonal to all the learned dictionary Φ(l)
cos

θ(R(l−1)
n , φ(l)
κ )
2
= 0 ⇐⇒ R(l−1)
indepφ
(l)
k ∀k, (24)
and thus they live in two orthogonal spaces. TO PROVE : ALL THE NEXT ONES ARE ALSO 0
Theorem 6. The residual decreases exponentially w.r.t. the depth of the model.
Proof.
||R(l)
n ||2
=||R(l−1)
n −
< R
(l−1)
n , φ
(l)
κ >
||φ
(l)
κ ||2
φ(l)
κ ||2
=||R(l−1)
n ||2
−
< R
(l−1)
n , φ
(l)
κ >2
||φ
(l)
κ ||2
=||R(l−1)
n ||2

1 − cos

θ(R(l−1)
n , φ(l)
κ )
2
(25)
=||xn||2
l
Y
l=1

1 − cos

θ(R(l−1)
n , φ(l)
κ )
2
(26)
The final template can be flattened via
Tn =
X
l
< R
(l−1)
n , φ
(l)
κ >
||φ
(l)
κ ||2
φ(l)
κ (27)
=
X
l
P(l)
n (28)
7
2.3.1 Learning
Computing the gradient finds a great recursion formula we define as follows:
Ai,j =







0 ⇐⇒ j < i
<R(i−1)
,φ(i)
κ >Id+R(i−1)
φ(i)
κ
||φ
(i)
κ ||2
+
2<R(i−1)
,φ(i)
κ >φ(i)
κ φ(i)T
κ
||φ
(i)
κ ||4
⇐⇒ i = j
Ai,j−1 −
φ(j)
κ φ(j)T
κ Ai,j−1
||φ
(j)
κ ||2
⇐⇒ j > i
(29)
thus Ai,j ∈ RD×D
then we have
Ln =||R(L)
n ||, (30)
dL2
dφ
(l)
κ
=2R(L)
n
dR
(L)
n
dφ
(l)
κ
(31)
(32)
However as we will see below we have a nice recursive definition to compute all those derivatives, in
fact
Init.



dP (l)
n
dφ
(l)
κ
=
<R(l−1)
n ,φ(l)
κ >Id+R(l−1)n φ(l)
κ
||φ
(l)
κ ||2
+
2<R(i−1)
n ,φ(l)
κ >φ(l)
κ φ(l)T
κ
||φ
(l)
κ ||4
,
dR(l)
n
dφ
(l)
κ
= −
dP (l)
n
dφ
(l)
κ
(33)
Recursion



dP (l+1)
n
dφ
(l)
κ
=
φ(l+1)
κ φ(l+1)T
κ
||φ
(l+1)
κ ||2
dR(l)
n
dφ
(l)
κ
,
dR(l+1)
n
dφ
(l)
κ
=
dR(l)
n
dφ
(l)
κ
−
dP (l+1)
n
dφ
(l)
κ
(34)
Algorithm 2.3: RESIDUAL OJA NETWORK(X, K)
Rn ← Xn, ∀n
for l ← 1 to L
do



















Initialize Φ
(l)
K from R
while not converged
do





for k ← 1 to K
do
(
Compute n(k) with current Φ
(l)
K
Update φ
(l)
k with n(k) and R(k) according to Eq. 20
Rn = (Rn −
<Rn,φ(l)
κ >
||φ
(l)
κ ||2
φ
(l)
κ )
return (Φ
(l)
k , ∀l)
Algorithm 2.4: ONLINE RESIDUAL OJA NETWORK(X, K)
Rn ← Xn, ∀n
for l ← 1 to L
do





















Initialize Φ
(l)
K from R
while not converged
do







for n ← 1 to N
do



κ = arg maxk
|<Rn,φ
(l)
k >|2
||φ
(l)
k ||2||Rn||2
Update φ
(l)
κ according to Eq. 16 or Eq.17 or Eq.19
Rn = (Rn −
<Rn,φ(l)
κ >
||φ
(l)
κ ||2
φ
(l)
κ )
return (Φ
(l)
k , ∀l)
8
Theorem 7. With a Deep (Oja) Network, the previously presented lower-bound of the generalization
error becomes an upper-bound.
In addition of guaranteeing better generalization errors through depth, we also benefit from another
gain. The depth as we will see allows for an exponential amount of possible templates to be
constructed perfectly with only a linear increase in the number of learned parameters.
Code 1: Input to Mask
####################
# INPUT: X(N,channels,Ix,Jx),w(n_filters,channels,Iw,Jw)
####################
k = T.nnet.conv.conv2d(x,w,stride=stride,
border_mode=’valid’,flip_filters=False,input_shape=(N,channels,Ix,Jx),
filters_shape=(n_filters,channels,Iw,Jw))#(N,n_filters,(Ix-Iw)/stride+1,(Jx-Jw)/stride+1)
output = ((k>0)*2-1)*T.signal.pool.max_pool_2d_same_size(
theano.tensor.abs_(k).dimshuffle([0,2,3,1]),
(1,n_filters)).dimshuffle([0,3,1,2])#(N,n_filters,(Ix-Iw)/stride+1,(Jx-Jw)/stride+1)
mask = T.switch(T.eq(output,0),0,1)#(N,n_filters,(Ix-Iw)/stride+1,(Jx-Jw)/stride+1)
Code 2: Reconstruction
####################
# INPUTS: Z(N,n_filters,Iz,Jz),w(n_filters,channels,Iw,Jw),stride
####################
dilated_output = T.set_subtensor(T.zeros((N,n_filters,(Iz-1)*stride),(Iz-1)*stride),
dtype=’float32’)[:,:,::stride,::stride],Z)#(N,n_filters,Ix-Iw+1,Jx-Jw+1)
rec = T.nnet.conv.conv2d(dilated_Z,w.dimshuffle([1,0,2,3]),stride=1,
border_mode=’full’,flip_filters=False)#(N,channels,Ix,Jx)
Code 3: Mask to Grad
###################
# INPUT : rec(N,C,Ix,Jx),mask(N,n_filters,Iz,Jz),Iw,Jw
###################
d_W,outputs=theano.scan(fn=lambda acc,i,X,mask:
acc+conv2d(rec[i].dimshuffle([0,’x’,1,2]),mask[i].dimshuffle([0,’x’,1,2]),
input_shape=(C,1,Ix,Jx),
filter_shape=(n_filters,1,Iz,Jz)).dimshuffle([1,0,2,3]),
sequences=[theano.tensor.arange(N,dtype=’int32’)],
non_sequences=[rec,mask],outputs_info =
T.zeros((n_filters,C,Iw,Jw),dtype=’float32’))
d_W = d_W[-1]
\caption{algo}
2.4 Previous Work
The proposedm ethod can be seen as bilinear sparse coding with one-hot latent vector y [12] for the
case of only one filter used. There is also a direct link with the probabilistic version of this work,
namely mixture of PPCA [23, 24], as here we are in a ”hard clustering” case similarly to k-means
versus GMM. By the selection of the best matching atom, we find some links with matching pursuit
[21, 26] and also locality sensitive hashing [15, 16] especially in the cosine similarity distance.
This problem can also be seen from a best basis selection point of view coupled with dictionary learn-
ing. Popular examples with varying degrees of computational overhead include convex relaxations
such as L1-norm minimization [3, 7, 22], greedy approaches like orthogonal matching pursuit (OMP)
[21, 25], and many flavors of iterative hard-thresholding (IHT) [5, 6] Variants of these algorithms
find practical relevance in numerous disparate domains, including feature selection [9, 11], outlier
removal [8, 14], compressive sensing [2], and source localization [1, 19]
9
Figure 1: Top : evolution of the reconstruciton error w.r.t. epochs. Bottom: evolution of the energy
captured per level during training. At first stage the last levels capture everything since random
initialization makes the global filters almost orthogonal to images, during training global filters learn
to capture the low frequencies. Since it is known that natural images have a 1/f decay of energy over
frequencies f we can see that the final energy repartition is indeed bigger for low-frequency/global
filters and go down for smaller filters.
Figure 2: Example of decomposition and reconstruction of some CIFAR10 images. From right to left
is the final residual (reconstruction minus original), the original image, the reconstructed images and
then all the decompositions, on the left is the global/large one. Summing elementwise columns 1 to 8
leads to column 9 the reconstrued input.
3 Conclusion
We presented a hierarchical version of the deterministic mixture of PCA and presented results on
CIFAR10 images. We also provided algorithms allowing GPU computation for large scale dataset
and speed. The main novelty comes from the deterministic formulate of the probabilistic mixture
of PCA which allows easier use as it is known in general that MPPCA is unstable for large scale
problems. From this we derived its hierarchical residual version which inherits many benefits and
allow for exponentially good reconstruction w.r.t. the depth. We also believe that this residual
approach allowing to learn orthogonal spaces will lead to interesting dictionary learning mixing for
example residual networks with this approach.
10
References
[1] Sylvain Baillet, John C Mosher, and Richard M Leahy. Electromagnetic brain mapping. IEEE
Signal processing magazine, 18(6):14–30, 2001.
[2] Richard G Baraniuk. Compressive sensing [lecture notes]. IEEE signal processing magazine,
24(4):118–121, 2007.
[3] Amir Beck and Marc Teboulle. A fast iterative shrinkage-thresholding algorithm for linear
inverse problems. SIAM journal on imaging sciences, 2(1):183–202, 2009.
[4] Jeff A Bilmes et al. A gentle tutorial of the em algorithm and its application to parameter
estimation for gaussian mixture and hidden markov models. International Computer Science
Institute, 4(510):126, 1998.
[5] Thomas Blumensath and Mike E Davies. Iterative hard thresholding for compressed sensing.
Applied and computational harmonic analysis, 27(3):265–274, 2009.
[6] Thomas Blumensath and Mike E Davies. Normalized iterative hard thresholding: Guaranteed
stability and performance. IEEE Journal of selected topics in signal processing, 4(2):298–309,
2010.
[7] Emmanuel J Candès, Justin Romberg, and Terence Tao. Robust uncertainty principles: Exact
signal reconstruction from highly incomplete frequency information. IEEE Transactions on
information theory, 52(2):489–509, 2006.
[8] Emmanuel J Candes and Terence Tao. Decoding by linear programming. IEEE transactions on
information theory, 51(12):4203–4215, 2005.
[9] Shane F Cotter and Bhaskar D Rao. Sparse channel estimation via matching pursuit with
application to equalization. IEEE Transactions on Communications, 50(3):374–377, 2002.
[10] Chris HQ Ding, Tao Li, and Michael I Jordan. Convex and semi-nonnegative matrix fac-
torizations. IEEE transactions on pattern analysis and machine intelligence, 32(1):45–55,
2010.
[11] Mario AT Figueiredo et al. Adaptive sparseness using jeffreys’ prior. Advances in neural
information processing systems, 1:697–704, 2002.
[12] David B Grimes and Rajesh PN Rao. Bilinear sparse coding for invariant vision. Neural
computation, 17(1):47–73, 2005.
[13] Aapo Hyvärinen, Juha Karhunen, and Erkki Oja. Independent component analysis, volume 46.
John Wiley & Sons, 2004.
[14] Satoshi Ikehata, David Wipf, Yasuyuki Matsushita, and Kiyoharu Aizawa. Robust photometric
stereo using sparse regression. In Computer Vision and Pattern Recognition (CVPR), 2012
IEEE Conference on, pages 318–325. IEEE, 2012.
[15] Piotr Indyk and Rajeev Motwani. Approximate nearest neighbors: towards removing the
curse of dimensionality. In Proceedings of the thirtieth annual ACM symposium on Theory of
computing, pages 604–613. ACM, 1998.
[16] William B Johnson and Joram Lindenstrauss. Extensions of lipschitz mappings into a hilbert
space. Contemporary mathematics, 26(189-206):1, 1984.
[17] Ian Jolliffe. Principal component analysis. Wiley Online Library, 2002.
[18] Daniel D Lee and H Sebastian Seung. Learning the parts of objects by non-negative matrix
factorization. Nature, 401(6755):788–791, 1999.
[19] Dmitri Model and Michael Zibulevsky. Signal reconstruction in sensor arrays using sparse
representations. Signal Processing, 86(3):624–638, 2006.
[20] Bruno A Olshausen and David J Field. Sparse coding with an overcomplete basis set: A strategy
employed by v1? Vision research, 37(23):3311–3325, 1997.
11
[21] Yagyensh Chandra Pati, Ramin Rezaiifar, and PS Krishnaprasad. Orthogonal matching pursuit:
Recursive function approximation with applications to wavelet decomposition. In Signals,
Systems and Computers, 1993. 1993 Conference Record of The Twenty-Seventh Asilomar
Conference on, pages 40–44. IEEE, 1993.
[22] Robert Tibshirani. Regression shrinkage and selection via the lasso. Journal of the Royal
Statistical Society. Series B (Methodological), pages 267–288, 1996.
[23] Michael E Tipping and Christopher M Bishop. Mixtures of probabilistic principal component
analyzers. Neural computation, 11(2):443–482, 1999.
[24] Michael E Tipping and Christopher M Bishop. Probabilistic principal component analysis.
Journal of the Royal Statistical Society: Series B (Statistical Methodology), 61(3):611–622,
1999.
[25] Joel A Tropp. Greed is good: Algorithmic results for sparse approximation. IEEE Transactions
on Information theory, 50(10):2231–2242, 2004.
[26] Joel A Tropp and Anna C Gilbert. Signal recovery from random measurements via orthogonal
matching pursuit. IEEE Transactions on information theory, 53(12):4655–4666, 2007.
12
