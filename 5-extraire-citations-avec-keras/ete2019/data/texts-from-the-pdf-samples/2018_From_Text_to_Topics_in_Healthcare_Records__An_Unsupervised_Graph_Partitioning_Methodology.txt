From Text to Topics in Healthcare Records: An Unsupervised
Graph Partitioning Methodology
M. Tarik Altuncu
Dept. of Mathematics, Imperial College London
Erik Mayer
Dept. of Medicine, Imperial College London
Sophia N. Yaliraki
Dept. of Chemistry, Imperial College London
Mauricio Barahona
Dept. of Mathematics, Imperial College London
ABSTRACT
Electronic Healthcare Records contain large volumes of unstruc-
tured data, including extensive free text. Yet this source of detailed
information often remains under-used because of a lack of method-
ologies to extract interpretable content in a timely manner. Here
we apply network-theoretical tools to analyse free text in Hospi-
tal Patient Incident reports from the National Health Service, to
find clusters of documents with similar content in an unsupervised
manner at different levels of resolution. We combine deep neural
network paragraph vector text-embedding with multiscale Markov
Stability community detection applied to a sparsified similarity
graph of document vectors, and showcase the approach on incident
reports from Imperial College Healthcare NHS Trust, London. The
multiscale community structure reveals different levels of meaning
in the topics of the dataset, as shown by descriptive terms extracted
from the clusters of records. We also compare a posteriori against
hand-coded categories assigned by healthcare personnel, and show
that our approach outperforms LDA-based models. Our content
clusters exhibit good correspondence with two levels of hand-coded
categories, yet they also provide further medical detail in certain
areas and reveal complementary descriptors of incidents beyond
the external classification taxonomy.
CCS CONCEPTS
• Theory of computation → Unsupervised learning and clus-
tering; Dynamic graph algorithms; • Computing methodologies
→ Topic modeling; • Information systems → Document topic
models;
KEYWORDS
Text Embedding, Topic Clustering, Graph Theory, Unsupervised
Multi-Resolution Clustering, Markov Stability Graph Partitioning
ACM Reference Format:
M. Tarik Altuncu, Erik Mayer, Sophia N. Yaliraki, and Mauricio Barahona.
2018. From Text to Topics in Healthcare Records: An Unsupervised Graph
Partitioning Methodology. In Proceedings of 2018 KDD Workshop on Machine
Learning for Medicine and Healthcare (MLMH’18). ACM, New York, NY, USA,
Article 4, 4 pages.
Permission to make digital or hard copies of part or all of this work for personal or
classroom use is granted without fee provided that copies are not made or distributed
for profit or commercial advantage and that copies bear this notice and the full citation
on the first page. Copyrights for third-party components of this work must be honored.
For all other uses, contact the owner/author(s).
MLMH’18, August 2018, London, UK
© 2018 Copyright held by the owner/author(s).
ACM ISBN 123-4567-24-567/08/06.
1 INTRODUCTION
The vast amounts of data collected by healthcare providers in con-
junction with modern data analytics techniques present a unique
opportunity to improve the quality and safety of medical care, for
patient benefit. In the United Kingdom, the National Health Service
(NHS) has a long history of documenting extensively the differ-
ent aspects of healthcare provision. The NHS is currently in the
process of increasing the availability of several databases, properly
anonymised, with the aim to leverage advanced analytics to iden-
tify areas of improvement in its services. One such resource is the
National Reporting and Learning System (NRLS), a central repos-
itory of patient safety incident reports from England and Wales
collected since 2004, which now contains more than 13 million de-
tailed records. The incidents are reported using a set of standardised
categories and contain a wealth of organisational and spatiotempo-
ral information (structured data) as well as, crucially, a substantial
component of free text (unstructured data). The incidents are wide
ranging: from patient accidents to lost forms or referrals; from de-
lays in admission and discharge to serious untoward incidents, such
as retained foreign objects after operations. The review of such data
provides critical insight into complex procedures in healthcare with
an aim towards service improvement.
Although statistical analyses are routinely performed on the
structured components of the data, the free text component remains
largely unused. Free text can be read manually but this task is time
consuming, hence often ignored in practice. Methods that provide
automatic, content-based categorisation of incidents from the free
text could help sidestep difficulties in assigning incident categories
from a priori pre-defined lists, reducing human error and burden,
as well as offering a unique insight into the root cause analysis of
incidents that could improve safety, quality of care and efficiency.
Here, we showcase an algorithmic methodology that detects
content-based groups of records in an unsupervised manner, based
only on the free, unstructured textual description of the incidents.
To do so, we combine deep neural-network high-dimensional text-
embedding algorithms (Doc2vec) with network-theoretical meth-
ods (multiscale Markov Stability (MS) community detection) applied
to a sparsified geometric similarity graph of documents derived
from text vector similarities.
Traditional natural language processing tools have generally
used bag-of-words representations of documents followed by statis-
tical methods based on Latent Dirichlet Allocation (LDA) to cluster
documents. More recent approaches have used deep neural network
based language models, without a full multiscale graph analysis [4],
while previous applications of network theory to text analysis [6]
were carried out at a single scale and used bag-of-word arrays
arXiv:1807.02599v1
[cs.CL]
7
Jul
2018
MLMH’18, August 2018, London, UK M.T. Altuncu et al
lacking the power of neural network text embeddings. In contrast,
multiscale community detection allows us to find groups of records
with consistent content at different levels of resolution; hence the
content categories emerge from the textual data, rather than fitting
to pre-designed classifications.
Our analysis starts by training a Doc2vec vector text embedding
using the 13 million NRLS records. (We have also trained models
to 1 and 2 million records and the results are similar.) Once the text
embedding is obtained, we use it to produce document vectors for
a subset of 3229 incident reports from St Mary’s Hospital, London
(Imperial College Healthcare NHS Trust) over three months in 2014.
Our graph clustering method is then applied to cluster these records
across different levels of resolution, revealing multiple levels of
intrinsic structure in the topics of the dataset, as shown by the
extraction of relevant word descriptors from the groups of records.
Upon reporting by the operator, the records had been indepen-
dently hand-coded with up to 170 features per record, including
a two-level manual classification of the incidents: 15 categories at
Level 1; 95 sub-categories at Level 2. We carried out an a posteri-
ori comparison against the hand-coded categories assigned by the
reporter. Several of our content-based clusters exhibit good corre-
spondence with well-defined hand-coded categories at both levels;
yet our results also provide further resolution in certain areas and
a complementary characterisation of the incidents, not defined in
the a priori external classification. We find that our methodology
provides improved performance over LDA models, as quantified by
the Uncertainty Coefficient against the hand-coded categories.
2 METHODOLOGY
Text Pre-processing and Doc2Vec Model Training. The pre-
processing of the raw text consisted of: lowering capital letters;
tokenising sentences into words; stemming; and removing punctu-
ation, stop-words and all numeric tokens. We then trained Doc2Vec
models [7] using the PV-DBOW method from the Gensim [10] li-
brary. To ascertain the effect of the training of the Doc2Vec model
on performance, we repeated the training with a broad range of pa-
rameters sets (window size, minimum count, subsampling). We car-
ried Doc2Vec training both on a standard (generic, non-specialised)
Wikipedia English corpus and on the full NRLS dataset (13+ million
records with specialised language). Table 1 shows that, while the
Wikipedia corpus is useful to train models when the parameters
sets are weak, the NRLS dataset performs better for optimised pa-
rameter sets. Once optimised, the Doc2Vec model trained on the
NRLS corpus is used to infer vectors for each of the N = 3229
records in our analysis dataset.
Graph Construction. We constructed a normalised similarity
matrix ˆ
S based on the cosine similarity by: computing the matrix of
cosine similarities between all pairs of records, Scos; transforming
it into a distance matrix Dcos = 1 − Scos ; applying element-wise
max norm to obtain D̂ = ∥Dcos ∥max ; and obtaining the normal-
ized similarity matrix ˆ
S = 1 − D̂ which has values in [0, 1]. This
(full) similarity matrix can be viewed as a completely connected,
weighted graph. However, such a graph contains many edges with
small weights (i.e., weak similarities) since, in high dimensional
noisy datasets, even the least similar nodes present a non negligible
Model Parameters Training Corpus
Window
Size
Minimum
Count
Subsampling Wikipedia NRLS
5 20 0.00001 465 379
15 20 0.00001 424 387
5 5 0.001 580 798
5 20 0.001 587 809
15 20 0.001 532 832
15 5 0.001 531 836
Table 1: The last two columns show the scores of Doc2Vec
paragraph vector models trained using different hyper-
parameter sets on different corpora. The scores are obtained
by: (i) calculating centroids for the 15 hand-coded categories;
(ii) selecting the 100 nearest reports for each centroid; (iii)
counting the number of incidents (out of 1500) correctly as-
signed to their centroid.
degree of similarity. We thus apply a simple geometric sparsifi-
cation to the normalized distance matrix D̂ using the MST-kNN
method[12], a geometric heuristic that preserves the global con-
nectivity of the graph while retaining the local geometry of the
dataset. The MST-kNN graph is a weighted graph obtained by the
union of the minimum spanning tree (MST) of D̂, and adding edges
connecting each node to its k nearest nodes (kNN). We scanned
k = 17, 13, 5, 1 for the graph construction and found that the MST-
kNN graph with k = 13 presents a reasonable balance between
local and global structure in the dataset, and thus analyse it with
the multi-scale graph partitioning framework. However, the results
are robust to the choice of k within a set of values. Note that the
MST-kNN method avoids global similarity thresholding.
Markov Stability Multiscale Graph Partitioning. We apply
Markov Stability (MS), a multiscale community detection method,
to the MST-kNN graph in order to detect clusters of documents
with similar content at different levels of resolution. MS is an unsu-
pervised method that scans across all scales to detect robust and
stable graph partitions using a continuous time diffusion process on
the graph. The method does not need to choose a priori the number
or type of relevant subgraphs, other than it retains the diffusive
flow over the Markov time t. Hence t acts as a resolution parameter
revealing relevant partitions that persist over particular time scales
in an unsupervised manner. For more details see [1, 3, 5, 11].
Briefly, the method optimises the MS function r(t,H) over the
space of graph partitions H at each time t. MS is defined as the trace
of the clustered autocovariance matrix of the diffusion process (1):
r(t,H) = trace [R(t,H)] = trace
h
HT
[Πe−t L
− ππT
]H
i
, (1)
where H is the membership matrix of the partition, L is the random
walk Laplacian of the graph, pi is the steady-state distribution of
the process and Π = diag(π). Our method searches for the partition
H∗(t) that maximises r(t,H) at each Markov time. The partition
H∗(t) is formed by communities (subgraphs) that tend to preserve
the flow within themselves over time t, since in that case the diago-
nal (off-diagonal) elements of R(t,H) will be large (small). Although
the maximization of (1) is NP-Hard, there are optimisation methods
From Text to Topics in Healthcare Records: An Unsupervised Graph Partitioning Methodology MLMH’18, August 2018, London, UK
that work well in practice. Here we use the Louvain Algorithm [2]
which is efficient and known to give good results for benchmarks.
We look to obtain robust partitions, i.e., partitions that are relevant
across scales (i.e., consistently found over extended Markov time)
and highly reproducible (i.e., consistently found by the Louvain op-
timisation). This is achieved by running the Louvain algorithm 500
times with different initialisations at each Markov time, picking the
50 with the highest MS value, and computing the variation of infor-
mation VI(t) [9] of this ensemble. In addition, we also compute the
variation of information between the optimised partitions found
across time, VI(t,t′). Robust partitions are indicated by dips of
VI(t) and extended plateaux of VI(t,t′), indicating a partition that
is robust to the optimisation and valid over extended scales [1, 5].
Visualization and interpretation of results.
Tracking membership through Sankey diagrams. Sankey diagrams
visualise the flow of node memberships across different partitions
and categories. We use Sankey diagrams with two different objec-
tives: (i) a multilayer Sankey diagram to represent the results of the
multi resolution MS community detection across different scales
(Fig. 1); (ii) two-layer Sankey diagrams to indicate the correspon-
dence between MS clusters and the hand-coded external categories
at a given level of resolution (Fig. 2).
Normalized contingency tables. Normalised contingency tables
allow us to compare the relative membership of content clusters
in terms of the external categories. We plot contingency tables as
heatmaps of a z-score (Fig. 2). We score the quality of this cor-
respondence using the uncertainty coefficient, an informational
theoretical measure of similarity between groupings.
Word clouds of increased intelligibility through lemmatisation. To
visualise the content of the document clusters, which can be under-
stood as a type of topic detection, we used Word Clouds weighted
through lemmatisation as an intuitive way to summarise content
and compare a posteriori with hand-coded categories. Word clouds
can also provide an aid for monitoring when used by practitioners.
3 RESULTS
We applied full MS across an extended, finely sampled span of
Markov times (0.01–100 in steps of 0.01) to the similarity MST-kNN
graph of 3229 NRLS incident records. Figure 1 presents a summary
of this analysis including the number of clusters and the two metrics
of variation of information across all Markov times. The existence
of several long plateaux in VI(t,t′) coupled to the existence of dips
in the VI(t) imply the presence of robust partitions at different
levels of resolution. We choose several robust partitions, from finer
to coarser, and examine their structure and content. Their relative
node membership is shown with a multi-level Sankey diagram.
Quasi-hierarchical structure: themes at different resolu-
tion levels. The MS analysis in Figure 1 reveals a rich multi-level
structure of partitions, with a strong quasi-hierarchical organiza-
tion. It is important to remark that, although the Markov time acts
as a natural resolution parameter from finer to coarser partitions,
our process of optimization does not impose such a hierarchical
structure. Hence this organisation is intrinsic to the data and implies
the existence of content communities which naturally integrate
with each other as sub-themes of larger thematic categories. The
7
17 3
12
44
Number of
content clusters:
Figure 1: (Top) Markov Stability (MS) analysis across Markov
time t: number of clusters of the optimised partitions (red
line), VI(t) for the ensemble of Louvain optimised solutions
at each t (blue line); VI(t,t′) between optimised partitions
across Markov times (background colourmap). Relevant par-
titions (indicated by numbers and blue vertical lines) corre-
spond to dips ofVI(t) and extended plateaux ofVI(t,t′). (Bot-
tom) The Sankey diagram illustrates the quasi-hierarchical
relationship of the communities of documents (indicated by
numbers and colours) across levels of resolution.
detection of intrinsic scales within the graph at which robust parti-
tions exist, thus allows us to obtain thematically-based clusters of
records at different levels of resolution.
Interpretation of the MS communities: Word clouds and a
posteriori comparison against hand-coded categories. To as-
certain the relevance and relationship between the layers of MS
clusters, we examined in detail the five levels of resolution in Fig-
ure 1. For each level, we prepared word clouds (lemmatised for
increased intelligibility) and a Sankey diagram and contingency
table linking content clusters with the hand-coded categories as-
signed by the operator. Note that this comparison was only done a
posteriori, i.e., the external categories were not used in our analysis,
hence our approach is truly unsupervised. As an example, Figure 2
MLMH’18, August 2018, London, UK M.T. Altuncu et al
shows the 17-community partition with word clouds for all clusters
and the comparison with the 15 hand-coded categories in Level 1.
15 16
17
10 11
2
Accidents with sharps
Pressure ulcer
Samples, lab tests Medication
IV, cannula
Radiotherapy
7
Falling and Slipping
4
Staffing/Nursing
14 12
8 9 3
1
Patient transfer Admittance, beds
Blood transfusions Labour ward Healthcare at home
Self Harm
13
Medical records, letters
5
Referrals, appointments
6
Forms, ID stickers
1
10
2
3
4
5
6
7
8
9
11
12
13
14
15
16
17
1
10
2 3 4
5
6
7 8 9
11 12
13 14
15
16 17
Pa#ent'accident
Medica#on
Aggressive'behaviour
Pa#ent'abuse
Self8harm
Clinical'assessment
Documenta#on
Infec#on'control
Treatment'/'
Procedure
Other
Devices/Equipment
Consent
Admissions/
Transfer
Monitoring
Infrastructure
Figure 2: Summary of the 17-community MS partition. The
content of each cluster is summarised with a word cloud
(name tags given by us based only on the word cloud). The
a posteriori comparison to the 15 hand-coded categories (in-
dicated by names and colours) is presented in two equiva-
lent ways: a Sankey diagram showing the correspondence
between categories and communities and the heatmap of a
z-score contingency table.
Comparison of the MS content clusters against other NLP
methods. We compared the MS document partitioning results
against LDA models with a range of similar number of topics. LDA
models are trained and inferred using the Gensim module on the
same set of documents. We then quantify the match of the MS
clusters with the hand-coded categories using the Uncertainty Co-
efficient [13]
U (T |C) =
H(T) − H(T |C)
H(T)
=
I(T;C)
H(T)
where H(T) is the entropy of the hand-coded categories and H(C)
is the entropy of the clustering. The MS clusters show improved
correspondence with both Level 1 and 2 categories as compared to
LDA and spectral clustering methods (Fig. 3).
4 DISCUSSION
This work has applied a multiscale graph partitioning algorithm
(MS) to determine topic clusters for a textual dataset of healthcare
safety incident reports in an unsupervised manner at different levels
of resolution. The method uses paragraph vectors to represent the
records and obtains an ensuing similarity graph of documents con-
structed from their content. This method brings the advantage of
multi-resolution algorithms capable of capturing clusters without
imposing a priori the number or structure of the clusters. Further-
more, it selects different levels of resolution of the clustering to
suit the requirements of each task depending on the level of detail.
The a posteriori analysis against hand-categories showed that the
method recovers meaningful categories and outperformed LDA at
both categorisation levels. Furthermore, some of the MS content
Figure 3: The Uncertainty Coefficient measures the match-
ing of LDA and MS clusterings over different scales against
the hand-coded Level 1 and Level 2 categories. MS commu-
nities are consistently more coherent with the hand-coded
categories than LDA and spectral clustering. The vertical
dashed lines indicate the number of hand-coded categories
for Level 1 and 2 (i.e., 15 and 95, respectively)
clusters capture topics of medical relevance, which provide comple-
mentary information to the external classifications. The nuanced
information and classifications extracted from free text analysis
suggest a complementary axis to existing approaches to charac-
terise patient safety incident reports, as the method allows for the
discovery of emerging topics or classes of incidents directly from
the data when such events do not fit the pre-assigned categories.
REFERENCES
[1] K A Bacik, M T Schaub, M Beguerisse-Díaz, Y N Billeh, and M Barahona. 2016.
Flow-Based Network Analysis of the Caenorhabditis elegans Connectome. PLOS
Computational Biology 12, 8 (2016), 1–27.
[2] V D Blondel, J-L Guillaume, R Lambiotte, and E Lefebvre. 2008. Fast unfolding
of communities in large networks. Journal of Statistical Mechanics: Theory and
Experiment 2008, 10 (2008), P10008.
[3] J-C Delvenne, S N Yaliraki, and M Barahona. 2010. Stability of graph communities
across time scales. Proceedings of the National Academy of Sciences of the United
States of America 107, 29 (7 2010), 12755–60.
[4] K Hashimoto, G Kontonatsios, M Miwa, and S Ananiadou. 2016. Topic detection
using paragraph vectors to support active learning in systematic reviews. Journal
of Biomedical Informatics 62 (8 2016), 59–65.
[5] R Lambiotte, J C Delvenne, and M Barahona. 2014. Random Walks, Markov
Processes and the Multiscale Modular Organization of Complex Networks. IEEE
Transactions on Network Science and Engineering 1, 2 (7 2014), 76–90. https:
//doi.org/10.1109/TNSE.2015.2391998
[6] A Lancichinetti, M I Sirer, J X Wang, D Acuna, K Körding, and L N Amaral. [n. d.].
High-Reproducibility and High-Accuracy Method for Automated Topic Classifica-
tion. Phys. Rev. X 1 (jan [n. d.]), 11007. https://doi.org/10.1103/PhysRevX.5.011007
[7] Q Qv Le and T Mikolov. 2014. Distributed Representations of Sentences and
Documents. International Conference on Machine Learning - ICML 2014 32 (2014),
1188âĂŞ1196. https://doi.org/10.1145/2740908.2742760
[8] R Lupton and Leonardo. 2017. ricklupton/sankeyview: v1.1.7. https://doi.org/10.
5281/zenodo.1098904
[9] Marina Meilă. 2007. Comparing clusteringsâĂŤan information based distance.
Journal of Multivariate Analysis 98, 5 (5 2007), 873–895.
[10] R Rehurek and P Sojka. 2010. Software Framework for Topic Modelling with
Large Corpora. In Proceedings of the LREC 2010 Workshop on New Challenges for
NLP Frameworks. ELRA, Valletta, Malta, 45–50.
[11] M T Schaub, J C Delvenne, S N. Yaliraki, and M Barahona. 2012. Markov dy-
namics as a zooming lens for multiscale community detection: Non clique-like
communities and the field-of-view limit. PLoS ONE (2012). https://doi.org/10.
1371/journal.pone.0032210
[12] P Veenstra, C Cooper, and S Phelps. 2017. Spectral clustering using the kNN-MST
similarity graph. In 2016 8th Computer Science and Electronic Engineering Confer-
ence, CEEC 2016 - Conference Proceedings. Institute of Electrical and Electronics
Engineers Inc., 222–227. https://doi.org/10.1109/CEEC.2016.7835917
[13] JV White, Sam Steingold, and CG Fournelle. 2004. Performance metrics for
group-detection algorithms. Proceedings of Interface 2004 (2004).
