ResFeats: Residual Network Based Features for Image Classification
A. Mahmood?
, M. Bennamoun, S. An
The University of Western Australia
?
ammar.mahmood@research.uwa.edu.au
F. Sohel
Murdoch University, Australia
Abstract
Deep residual networks have recently emerged as the
state-of-the-art architecture in image segmentation and ob-
ject detection. In this paper, we propose new image fea-
tures (called ResFeats) extracted from the last convolutional
layer of deep residual networks pre-trained on ImageNet.
We propose to use ResFeats for diverse image classifica-
tion tasks namely, object classification, scene classification
and coral classification and show that ResFeats consistently
perform better than their CNN counterparts on these clas-
sification tasks. Since the ResFeats are large feature vec-
tors, we propose to use PCA for dimensionality reduction.
Experimental results are provided to show the effectiveness
of ResFeats with state-of-the-art classification accuracies
on Caltech-101, Caltech-256 and MLC datasets and a sig-
nificant performance improvement on MIT-67 dataset com-
pared to the widely used CNN features.
1. Introduction
Deep convolutional neural networks (CNNs) have shown
outstanding results on challenging image classification and
detection datasets since the seminal work of [18]. Off-
the-shelf image representations learned by these deep net-
works are powerful and generic. These generic features
have been used to solve numerous visual recognition prob-
lems [23, 7]. Given the promising performance of these off-
the-shelf CNN features, they have become the first choice
for solving most computer vision problems [1].
Training a deep network from scratch is not a feasi-
ble option when solving a classification problem with a
small number of labelled training examples. Recent ev-
idence [30, 13, 7] suggests that off-the-shelf CNN fea-
tures have outperformed previous handcrafted features for
datasets with a limited amount of training data. These fea-
tures are domain independent and can be transferred to any
specific target task without compromising on performance
[1]. Network width, depth and optimization parameters
along with the network layer from which these features are
extracted play a key role in the effectiveness of transfer
Image
Part
Annotations
Strong
DPM
Learn
Normalized
Pose
Extract Features
RGB, gradient,
LBP
SVM
CNN
Representations
ResFeats
Representations
Classifier
Conventional Features
(SIFT, HOG, gradient,
LBP)
ResFeats
Representations
CNN
Representations
Output
Class
Input Image
Classifier
Output
Class
Classifier
Output
Class
Figure 1: Evolution of classification pipelines (the most re-
cent an the bottom). Off-the-shelf ResFeats have the po-
tential to replace the previous classification pipelines and
improve performance for image classification tasks.
learning. This paper attempts to provide an answer to the
following question: What are the criteria to select an initial
deep network (pre-trained on ImageNet) to extract generic
features in order to maximize performance and transferabil-
ity across domains? To answer this question, we hypoth-
esise that a better optimized and a high performing deep
network on ImageNet should result in more powerful and
generic image representations. One such network is the
deep residual network (ResNet) presented in [14].
ResNets are easier to train as opposed to other CNN ar-
chitectures e.g. VGGnet [25]. For example, a 152-layer
ResNet which is 8 times deeper than VGGnet, is still less
complex and trains faster. Moreover, a 34-layer ResNet
contains 3.6 billion multiply-add operations whereas a 19-
layer VGGnet has 19.6 billion multiply-add operations (less
than 20%) [14]. Very deep networks are known to cause
overfitting and saturation in accuracy. However, resid-
ual learning and the identity mappings (shortcut connec-
tions) [15] in ResNets have been shown to overcome these
problems. This enables ResNets to achieve outstanding
results in image detection, localization and segmentation
tasks [14]. In this paper, we explore the discrimination
power of the image representations extracted from pre-
trained ResNets. We name these off-the-shelf ResNet fea-
tures as ResFeats. Fig. 1 depicts the evolution of traditional
1
arXiv:1611.06656v1
[cs.CV]
21
Nov
2016
Pre-trained
Resnet
F Classifier
ResFeats
ResFeats
ResFeats
ResFeats
Input
Dimension
Reduction
Output Label
Feature
Extraction
n-channels
Figure 2: Block diagram of the proposed method. F is the final feature vector obtained after dimension reduction.
classification pipelines.
The main contributions of this paper are listed below:
• We introduce ResFeats, which are image features ex-
tracted from pre-trained ResNets and test them on
diverse image classification tasks including objects,
scenes and corals.
• We analyse the performance of ResFeats extracted
from the outputs of different convolutional layers of
ResNet-50 [14] for image classification. We also
compare the performance of ResFeats extracted from
ResNet-50 with those extracted from a deeper 152-
layer ResNet.
• We propose a compact 2048-dimensional generic fea-
ture vector obtained after dimensionality reduction
which is half of the size of the traditional CNN based
feature vector (4096 dimensions).
• We show that ResFeats achieve a superior classifica-
tion accuracy compared to off-the-shelf CNN features.
We also provide experimental evidence that our pro-
posed method achieves state-of-the-art performance on
three out of the four popular and challenging image
classification datasets.
The rest of the paper is organized as follows: We briefly
discuss the related work in the next section. In Sec. 3.1,
we introduce our proposed approach and explain the feature
extraction from ResNets. In Sec. 3.2, we describe the di-
mensionality reduction and classification approaches. Sec.
4 reports the experimental results and Sec. 5 concludes the
paper.
2. Related Work
Recent success stories [18, 25, 7, 9] have established
deep CNNs as the first choice to solve challenging computer
vision tasks. However, training a network from scratch
requires a large amount of training data, time and GPUs.
Donahue et al. [7] and Zeiler and Fergus [30] provided ev-
idence that the generic image representations learned from
pre-trained CNNs outperform previous state-of-the-art hand
crafted features. However, they did not experiment on a
large number of computer vision datasets. Razavian et
al. [23] built on the concept of generic CNN features and
proved that off-the-shelf CNN features outperform existing
methods. They experimented with more than 10 datasets
for tasks such as image classification, object detection, fine
grained recognition, attribute detection and visual instance
retrieval. OverFeat [24] was used as the source CNN in the
work of [23].
Chatfield et al. [5] evaluated the performance of CNN
based methods for image classification and compared their
methods with previous feature encoding methods. Their
findings established that deeper CNN performed better than
the shallower models of the same network trained on aug-
mented data. VGGnet [25] was used as the source CNN
in their work. They improved the classification accuracies
of popular datasets such as VOC, Caltech-101 and Caltech-
256. He et al. [13] used spatial pyramid pooling of CNN
features to further improve the classification accuracy on the
Caltech datasets and reported state-of-the-art object classi-
fication results.
Scene classification is quite different from object classi-
fication due to the presence of multiple objects in a single
scene. These object instances can be of varying size and
pose, and can be located at different locations in a number of
possible layouts in the test image. Consequently, the state-
of-the-art performance on scene datasets such as MIT-67
(81% in [6]) is comparatively lower than the performance
on object classification datasets (93.4% for Caltech-101 in
[13]). Towards indoor scene classification, a bag of features
approach was proposed to perform VLAD pooling [16] of
CNN features in [10]. Another example is “spatial layout
and scale invariant convolutional activations (S2
ICA)” in-
troduced in [12] to increase the robustness of CNN features.
Cimpoi et al. [6] proposed Fisher Vector (FV) pooling of a
deep CNN filter bank (FV-CNN) for texture and material
classification. They achieved an accuracy of 81% on MIT-
67 dataset (an improvement of 10% over previous state-of-
the-art).
Conv1
(112x112)
Conv2
(56x56)
Conv3
(28x28)
Conv4
(14x14)
Conv5
(7x7)
x 3 x 4 x 6 x 3
Layer Name
(Output Size)
Input
224x224x3
1x1,64
3x3,64
1x1,256
7x7, 64
1x1,128
3x3,128
1x1,512
1x1,256
3x3,256
1x1,1024
1x1,512
3x3,512
1x1,2048
FC 1000
Output
(1 x nClasses)
ResFeat
s
ResFeat
s
ResFeat
s
Res3d
28x28,
512 channels
ResFeat
s
ResFeat
s
ResFeat
s
Res4f
ResFeat
s
ResFeat
s
ResFeat
s
Res5c
14x14,
1024 channels
7x7,
2048 channels
Figure 3: ResNet-50 architecture [14] shown with the residual units, the size of the filters and the outputs of each convolu-
tional layer. ResFeats extracted from the different layers of this network are also shown.
Coral classification is a target task which is very differ-
ent from the source dataset on which deep networks are
pre-trained (ImageNet in this case). Despite this dissim-
ilarity, off-the-shelf CNN features have improved the re-
sults of existing methods of coral classification [20, 17, 21],
thereby demonstrating their strength for transfer learning.
The baseline performance on MLC dataset was first re-
ported in [2]. In [20], a hybrid (hand-crafted + CNN) fea-
ture vector was proposed to improve the classification accu-
racy on this dataset. Khan et al. [17] used feature vectors
extracted from VGGnet alongside cost-sensitive learning to
address the class imbalance problem of MLC dataset.
3. Proposed Method
In the following subsections, we describe various steps
that are involved in our proposed method with a block dia-
gram in Fig. 2.
3.1. Deep Residual Networks
Deep residual networks are made up of residual units.
Each residual unit can be expressed as:
yi = h(xi) + F(xi, wi) (1)
xi+1 = f(yi) (2)
where F is a residual function, f is a ReLU function, wi is
the weight matrix, and xi and yi are the inputs and outputs
of the i-th layer. The function h is an identity mapping [14]
given by:
h(xi) = xi (3)
The residual function F is defined in [15] as:
F(xi, wi) = wi · σ(B(w0
i) · σ(B(xi))) (4)
where B(xi) is the batch normalization,“·” denotes convo-
lution and σ(x) = max(x, 0). The essential idea behind
residual learning is the branching of the paths for gradient
propagation. For CNNs, this idea was first introduced in
the form of parallel paths in the inception models of [28].
Residual networks share a few similarities with the high-
way networks [27] such as residual blocks and shortcut con-
nections. However, the output of each path in the highway
network is controlled by a gating function which is learned
during the training phase.
The residual units in ResNets are not stacked together
as is the case with convolutional layers in a conventional
CNN. Instead, shortcut connections are introduced from the
input of each convolutional layer to its output. Using iden-
tity mappings as shortcut connections decreases the com-
plexity of the residual networks resulting in deep networks
that are faster to train. ResNets can be seen as an ensemble
of many paths, instead of viewing it as a very deep archi-
tecture. However, all of these network paths in the ResNets
are not of the same length. Only one path goes through all
of the residual units. Moreover, all of these signal paths
do not propagate the gradient which accounts for the faster
optimization and training of ResNets. ResNets as deep as
1001-layers have been proposed to achieve superior perfor-
mances on CIFAR datasets [15]. However, in this paper we
have only used ResNet-50 and ResNet-152 whose architec-
tures are described in detail in [14].
3.2. ResFeats
This section introduces ResFeats and elaborates on the
process to extract those features from deep residual net-
works. Generally, the image representations extracted from
the deeper layers of a CNN capture higher level features
and increase the classification performance[30]. A typical
residual unit in a ResNet consists of a block of three con-
volutional layers [14]. ResFeats are the outputs of residual
units unlike the conventional CNN features which usually
are the activations of the fully connected layers [23]. The
activations of the fully connected layers capture the over-
all shape of the object contained in the region of interest.
The local spatial information is lost when the outputs of the
convolutional layer are max-pooled to obtain a 4096 dimen-
sional vector for the activation of FC layer [19]. However,
the output vector of a convoltuional layer is rich in spatial
information.
ResFeats can be viewed as the output of a deep filter
bank. This output is a vector of the form w × h × d where
w and h is the width and height of the resulting feature vec-
tor and d being the number of channels in the convolutional
layer. Thus ResFeats can be considered as 2-D arrays of
local features with d dimensions. The local spatial informa-
tion of this feature vector will be lost when it is propagated
to the fully connected layer. Therefore, we do not use the
activations of the FC layer of ResNet as a feature vector.
Fig. 3 shows the architecture of the ResNet-50 deep net-
work which we have used for feature extraction. We initial-
ize the network with the weights pre-trained on ImageNet.
The learned weights of the deeper layers are usually more
class specific e.g. the fully connected layer of ResNet-50
(since there is only one FC layer). We were interested in
the classification performance of the output vectors of the
preceding convolutional layers. If used appropriately, the
convolutional layers of a deep network form very power-
ful features. Therefore, we extracted the outputs of the last
residual unit of the convolutional layers 3, 4 and 5 and used
them as feature vectors. These feature vectors were denoted
by Res3d, Res4f and Res5c respectively (the letters d, f and
c correspond to 4 ,6 and 3 which is the number of the last
residual blocks of each layer). Features extracted from the
3rd layer have a lower dimension than the features extracted
from the 5th layer. We expected an increase in the perfor-
mance of ResFeats as we used deeper features. We also ex-
tracted these intermediate features from a deeper version of
ResNet: ResNet-152 [14]. ResNet-152 have shown a lower
error on the ImageNet classification challenge than ResNet-
50. Res5c features extracted from the 152-layer ResNet
tend to perform better than their ResNet-50 counterparts.
The classification results of these features are reported in
Sec. 4.
3.3. Dimensionality Reduction and Classification
The outputs of the convolutional layers are much larger
in size than the traditional 4096-dimensional CNN based
features, for example, the Res5c feature vector is 7 × 7 ×
2048 in dimension (more than 100k elements). In order to
reduce the computational costs associated with the manipu-
lation of large feature vectors, we propose two methods for
dimension reduction. The first method involves implement-
ing a shallow CNN network with one convolutional layer,
one max-pooling layer and two fully-connected (FC) lay-
ers. We will refer to this network as sCNN in the rest of
the paper. The first convolutional layer consists of small fil-
ters (i.e. 1 × 1) along 512 channels. This layer reduces the
dimension of Res5c to 7 × 7 × 512 which is of the same
size as the output of the last convolutional layer of VGGnet
[25]. The stride is set to 1 and the padding is set to zero
for the convolutional layer. This layer is then followed by a
max-pooling layer, two FC layers and a soft-max layer for
classification. The resulting shallow CNN is very similar to
the FC portion of the VGGnet (configuration D [25]). The
resulting sCNN is initialized with random weights and is
then trained for each dataset specifically. Fig. 4 (a) shows
the architecture of sCNN along with the dimensions of the
layers used for Res5c.
In the second proposed method for dimension reduc-
tion, we use Principal Component Analysis (PCA) to re-
duce the Res5c feature vector to an n-dimensional vector.
Here n is the number of channels in the convolutional layer
from which ResFeats are extracted. A validation set from
each dataset is used to calculate the optimal n. The max-
imum validation accuracy is achieved when n is set equal
to the number of channels in the corresponding ResFeat.
For example, Res5c (7 × 7 × 2048) is reduced to a 2048-
dimensional vector by PCA. The resulting feature vectors
are then classified using a linear support vector machine
(SVM) classifier. We were motivated to use PCA-SVM
classification pipeline due to its popularity to classify off-
the-shelf CNN features [1, 6, 23]. Fig. 4 (b) shows the
pipeline for PCA-SVM module for Res5c. A comparison of
the performance of these two methods is also given in Sec.
4. Our results show that the dimensionality of the ResFeats
can be reduced significantly without having a considerable
performance drop.
4. Experiments and Results
4.1. Datasets
Object Classification: Caltech-101 [8] contains 9,144
images, divided into 102 categories. The number of images
for each category varies between 31 and 800 images. In our
experiments, we used 30 images from each class for training
Output
(1 x nClasses)
Conv1
1x1, 512
Res5c
7 x 7 x 2048
FC1
4096
Conv2
7x7, 512
FC2
4096
Output
(1 x nClasses)
PCA
Res5c
7 x 7 x 2048
Linear
SVM
2048d
Feature
Vector
(a) (b)
Res5c
7 x 7, 2048
Conv1
1x1, 512
(a)
FC2
4096
Output
Class
Res5c
7 x 7 x 2048
PCA
2048d
Feature
Vector
Linear SVM
(b)
Output
Class
maxpool Soft-
max
loss
FC1
4096
Figure 4: Dimension reduction and classification pipelines:
(a) sCNN with two convolutional layers and two fully con-
nected layers. (b) PCA-SVM.
and the remaining images were used for testing. Caltech-
101 is a very popular dataset for object classification.
Object Classification: Caltech-256 [11] contains
30,607 images, divided into 257 classes (256 objects +1
background). Each category has at least 80 images. This
dataset is less popular but more challenging compared to
Caltech-101. In our experiments, following [30], we used
30 and 60 images from each class for training and the rest
of the images were used for testing.
Scene Classification: MIT-67 [22] is a very challenging
and popular dataset for indoor scene classification. It con-
sists of 15,620 images belonging to 67 classes. The num-
ber of images varies between 101 and 738 per class. We
followed the standard protocol [22] which uses a subset of
6700 images (100 per class) for training and testing. There
are 80 images from each class in the training set. The re-
maining 20 images per class are set for testing. We also
tested on the augmented version of this dataset by adding
cropped and rotated samples. We refer it to as ’MIT-67aug’
in our results.
Coral Classification: Moorea Labelled Corals
(MLC) [2] contains 2055 images collected over three years:
2008, 2009 and 2010. It contains random point annotation
(x, y, label) for the nine most abundant labels, four non coral
and five coral classes. We have used 87,428 images from
the year 2008 for training and the remaining 43,832 images
from the same year for testing. This is a challenging dataset
since each class exhibits a large variability in shape, color
and scale.
4.2. Experimental Settings
We use two deep ResNets to learn our proposed im-
age representations. The network architecture of the first
ResNet is shown in Fig. 3. The detailed achitecture of the
much deeper ResNet152 is similar to ResNet-50 and is il-
lustrated in detail in [14]. We use the pre-trained models of
these two networks which are publicly available. We im-
plemented our proposed method and sCNN classifier net-
Dataset Classes Res5c Res4f Res3d
Caltech 101 (30) 102 91.8 89.4 77.2
Caltech 256 (30) 257 75.4 45.2 46.0
Caltech 256 (60) 257 79.3 53.4 44.1
MIT-67 67 71.1 69.0 51.4
MLC 9 76.8 78.8 77
Table 1: Performance comparison of ResFeats extracted
from different convolutional layers of ResNet-50. The num-
ber in the parenthesis denotes the number of samples per
class that is used for training.
work in MatConvNet[29]. LibSVM [4] was used for train-
ing the support vector machines used for classification. n-
fold cross validation was used to find the best parameters for
SVM with n = 4. Note that the PCA-SVM was only tested
for the highest performing ResFeats i.e., ResFeats-152.
The classification accuracies reported in Sec. 4.3 and 4.4
were achieved by using the sCNN for dimensionality reduc-
tion and classification. A performance comparison between
sCNN and PCA-SVM module is given in Sec. 4.5 for Res-
Feats extracted from ResNet-152.
4.3. Performance Analysis: ResFeats
In Table 1, we present the classification accuracies of
ResFeats extracted from the output of the 3rd, 4th and 5th
convolutional layers on our test datasets. ResFeats from
the 5th convolutional layer (Res5c) outperform others for
all datasets except the MLC. The difference in the clas-
sification accuracy of the ResFeats extracted from differ-
ent layers tends to follow a pattern that can be associated
with the number of classes in the dataset. When the num-
ber of classes increases, the difference in the accuracies of
Res5c, Res4f and Res3d also increases. For Caltech-256
(257 classes), the difference in the accuracy of Res5c and
Res3d ranges between 30-35%. This difference is negli-
gible for MLC dataset which only has nine classes. We
conclude that high level features (i.e. Res5c) show the best
performance on all datasets except MLC. The same pattern
was observed for the corresponding features extracted from
ResNet-152.
4.4. Performance Analysis: CNN features vs Res-
Feats
Table 2 compares the performance of ResFeats with their
CNN counterparts for a given dataset. The overall classi-
fication accuracy is used to evaluate the performance. To
keep the comparison fair, standard train-test splits are used
for all datasets. For a fair comparison of classification per-
formance, we only consider the methods which have used
CNN features without any post-processing. We compare
the CNN features with ResFeats extracted from a 50-layer
Dataset CNN Features ResFeats-50 ResFeats-152
Caltech 101 (30) 86.5 [30] 91.8 92.6
Caltech 256 (30) 70.6 [30] 75.4 78.0
Caltech 256 (60) 74.2 [30] 79.3 81.9
MIT-67 58.4 [23] 71.1 73.0
MIT-67aug 69.0 [23] 73.0 74.0
MLC 72.9 [17] 78.8 80.0
Table 2: Performance comparison of the baseline CNN
features with the baseline ResFeats without any additional
post-prcessing of feature vectors. The number in the paren-
thesis denotes the number of samples per class that is used
for training.
ResNet and a deeper 152-layer ResNet. ResFeats-50 con-
sistently outperform the CNN features by a margin of at
least 4%. Table 4 also shows that ResFeats-152 further im-
proves the classification accuracy by 1-2%. We conclude
that ResFeats perform significantly better than the corre-
sponding CNN based features. Moreover, ResFeats ex-
tracted from a deeper ResNet perform better than the ones
extracted from shallower ResNets.
4.5. Image Classification Results
The experiments above compare our ResNet based fea-
ture representation with off-the-shelf CNN features. In
this section, we compare the performance of ResFeats with
other state-of-the-art methods for each dataset.
Caltech-101: We randomly select 30 images per class
for training and compare our results with the other existing
methods in Table 3. ResFeats with a PCA-SVM classifier
beats the current state-of-the-art (He et al. [13]) by 1.3%. It
is worth mentioning here that the authors in [13] used the
spatial pyramid pooling layer in their network to achieve a
93.4% accuracy. We, however, have achieved state-of-the-
art accuracy without adding any post-processing modules
to ResFeats. This demonstrates the superior classification
power of ResFeats.
Caltech-256: We randomly select 30 and 60 images per
class for training and report the classification accuracies in
Table 4. Our method (both classification modules) outper-
forms the current state-of-the-art in both experiments. Ta-
ble. 4 reports an absolute gain of 8.9% and 4.5% on pre-
vious state-of-the-art methods on Caltech-256 datasets with
30 and 60 training samples per class respectively.
MIT-67: We report our results on the standard split
(80 train, 20 test) on MIT-67 and the augmented version
(MIT67-aug) of this dataset in Table 5. We use 16 augmen-
tations of each image: five crops, two rotations and mir-
rored images of these. The data augmentation used in our
experiments is consistent with the one used in [23]. Table
5 shows that ResFeats perform better than all the previous
Method Cal-101 (30)
Bo et al. [3] 81.4
Zeiler & Fergus [30] 86.5
Chatfield et al. [5] 88.4
He et al. [13] 93.4
ResFeats-50 + sCNN 91.8
ResFeats-152 + sCNN 92.6
ResFeats-152 + PCA-SVM 94.7
Table 3: Performance evaluation on Caltech-101 dataset.
The number in the parenthesis denotes the number of sam-
ples per class that is used for training.
Method Cal-256 (30) Cal-256 (60)
Sohn et al. [26] 42.1 47.9
Bo et al. [3] 48.0 55.2
Zeiler & Fergus [30] 70.6 74.2
Chatfield et al. [5] – 77.6
ResFeats-50 + sCNN 75.4 79.3
ResFeats-152 + sCNN 78.0 81.9
ResFeats-152 + PCA-SVM 79.5 82.1
Table 4: Performance evaluation on Caltech-256 dataset.
The number in the parenthesis denotes the number of sam-
ples per class that is used for training.
methods except [6] for the non-augmented dataset. The best
performing method on MIT-67, Cimpoi et al. used deep fil-
ter banks that are extracted from VGGnet at multiple scales
followed by a Fisher Vector (FV) encoding to achieve state-
of-the-art performance on MIT-67. However, it is important
to note that applying FV encoding to ResFeats is compu-
tationally expensive because of the large size of ResFeats
(Res5c has more than 100k elements). Also, this method
extracted features from the last layer convolution layer of
VGGnet by using multiple sizes of each training image. In
contrast, we only use a fixed size (224×224) to extract Res-
Feats. For MIT67aug, our method beats the previous best
performance by a margin of 8.1%.
MLC: We use the same experimental protocol for MLC
dataset as given in [2]. Table 6 shows the classification ac-
curacies for MLC dataset achieved by previous methods.
Our proposed method achieves an accuracy gain of 6.8%
over the baseline performance of [2]. Off-the-shelf Res-
Feats outperform the cost-sensitive CNN of [17] and multi-
scale hybrid feature (CNN + hand-crafted feature) approach
of [20].
Fig. 5 shows a comparison of the classification accuracy
86.5
70.6
74.2
58.4
69
72.9
93.4
70.6
77.6
81
69
77.9
92.6
78
81.9
73.7
74.9
80
94.7
79.5
82.1
75.6
77.1
80.8
50
60
70
80
90
100
CNN features off-the-shelf Current state-of-the-art ResFeats sCNN ResFeats-PCA-SVM
86.5
70.6
74.2
58.4
69
72.9
93.4
70.6
77.6
81
69
77.9
94.7
79.5
82.1
75.6
77.1
80.8
50
60
70
80
90
100
CNN features off-the-shelf Current state-of-the-art ResFeats off-the-shelf
Figure 5: The improvement achieved by replacing CNN off-the-shelf features with ResFeats for the datasets we used in our
experiments. Current state-of-the-art performances are also given for each dataset.
Method MIT-67 MIT-67 aug
Razavian et al. [23] 58.4 69.0
Gong et al. [10] 68.9 –
Khan et al. [17] 70.9 –
Zhou et al. [31] 70.8 –
Azizpour et al. [1] 71.3 –
Liu et al. [19] 71.5 –
Hayat et al. [12] 74.4 –
Cimpoi et al. [6] 81.0 –
ResFeats-50 + sCNN classifier 71.1 73.0
ResFeats-152 + sCNN classifier 73.7 74.9
ResFeats-152 + PCA-SVM 75.6 77.1
Table 5: Performance evaluation on MIT-67 dataset.
Method MLC
Beijbom et al. [2] 74.0
Khan et al. [17] 75.2
Mahmood et al. [20] 77.9
ResFeats-50+ sCNN classifier 78.8
ResFeats-152 + sCNN classifier 80.0
ResFeats-152 + PCA-SVM 80.8
Table 6: Performance evaluation on MLC dataset.
of off-the-shelf CNN representations, ResFeats and current
state-of-the-art methods. The results are reported for all the
datasets that were used in our experiments. The ResFeats
consistently outperformed the CNN features by a large mar-
gin. It must be noted in Fig. 5 that for CNN features,
only those results are reported which do not use any ad-
ditional post-processing module. ResFeats with PCA-SVM
achieved state-of-the-art classification performances for all
the datasets except MIT-67.
5. Conclusion
In this paper, we used features extracted from deep
ResNets off-the-shelf to address three image classification
tasks: object, scene and coral classification. We investi-
gated the effectiveness of transfer learning of the ResFeats.
We showed that the ResFeats extracted from the deeper lay-
ers of a ResNet perform better than the shallower ResFeats.
We experimentally confirm that our proposed features are
powerful and have a classification accuracy that is higher
than the CNN off-the-shelf features. Finally, we improve
the state-of-the-art accuracy on Caltech-101, Caltech-256
and MLC datasets. It is worth to further investigate the
prospective applications of ResFeats for computer vision
tasks such as object localization, image segmentation, in-
stance retrieval and attribute detection.
6. Acknowledgements
We thankfully acknowledge Nvidia for providing us a
Titan-X GPU for the experiments involved in this research.
References
[1] H. Azizpour, A. Sharif Razavian, J. Sullivan, A. Maki, and
S. Carlsson. From generic to specific deep representations
for visual recognition. In Proceedings of the IEEE Con-
ference on Computer Vision and Pattern Recognition Work-
shops, pages 36–45, 2015. 1, 4, 7
[2] O. Beijbom, P. J. Edmunds, D. Kline, B. G. Mitchell,
D. Kriegman, et al. Automated annotation of coral reef
survey images. In Computer Vision and Pattern Recogni-
tion (CVPR), 2012 IEEE Conference on, pages 1170–1177.
IEEE, 2012. 3, 5, 6, 7
[3] L. Bo, X. Ren, and D. Fox. Multipath sparse coding using
hierarchical matching pursuit. In Proceedings of the IEEE
Conference on Computer Vision and Pattern Recognition,
pages 660–667, 2013. 6
[4] C.-C. Chang and C.-J. Lin. LIBSVM: A library for
support vector machines. ACM Transactions on Intelli-
gent Systems and Technology, 2:27:1–27:27, 2011. Soft-
ware available at http://www.csie.ntu.edu.tw/
˜cjlin/libsvm. 5
[5] K. Chatfield, K. Simonyan, A. Vedaldi, and A. Zisserman.
Return of the devil in the details: Delving deep into convo-
lutional nets. arXiv preprint arXiv:1405.3531, 2014. 2, 6
[6] M. Cimpoi, S. Maji, and A. Vedaldi. Deep filter banks for
texture recognition and segmentation. In Proceedings of the
IEEE Conference on Computer Vision and Pattern Recogni-
tion, pages 3828–3836, 2015. 2, 4, 6, 7
[7] J. Donahue, Y. Jia, O. Vinyals, J. Hoffman, N. Zhang,
E. Tzeng, and T. Darrell. Decaf: A deep convolutional acti-
vation feature for generic visual recognition. In ICML, pages
647–655, 2014. 1, 2
[8] L. Fei-Fei, R. Fergus, and P. Perona. One-shot learning of
object categories. IEEE transactions on pattern analysis and
machine intelligence, 28(4):594–611, 2006. 4
[9] R. Girshick, J. Donahue, T. Darrell, and J. Malik. Rich fea-
ture hierarchies for accurate object detection and semantic
segmentation. In Computer Vision and Pattern Recognition
(CVPR), 2014 IEEE Conference on, pages 580–587. IEEE,
2014. 2
[10] Y. Gong, L. Wang, R. Guo, and S. Lazebnik. Multi-scale
orderless pooling of deep convolutional activation features.
In Computer Vision–ECCV 2014, pages 392–407. Springer,
2014. 2, 7
[11] G. Griffin, A. Holub, and P. Perona. Caltech-256 object cat-
egory dataset. 2007. 5
[12] M. Hayat, S. H. Khan, M. Bennamoun, and S. An. A spatial
layout and scale invariant feature representation for indoor
scene classification. IEEE Transactions on Image Process-
ing, 25(10):4829–4841, Oct 2016. 2, 7
[13] K. He, X. Zhang, S. Ren, and J. Sun. Spatial pyramid pool-
ing in deep convolutional networks for visual recognition.
In Computer Vision–ECCV 2014, pages 346–361. Springer,
2014. 1, 2, 6
[14] K. He, X. Zhang, S. Ren, and J. Sun. Deep residual learn-
ing for image recognition. arXiv preprint arXiv:1512.03385,
2015. 1, 2, 3, 4, 5
[15] K. He, X. Zhang, S. Ren, and J. Sun. Identity mappings in
deep residual networks. arXiv preprint arXiv:1603.05027,
2016. 1, 3
[16] H. Jégou, M. Douze, C. Schmid, and P. Pérez. Aggregat-
ing local descriptors into a compact image representation.
In Computer Vision and Pattern Recognition (CVPR), 2010
IEEE Conference on, pages 3304–3311. IEEE, 2010. 2
[17] S. H. Khan, M. Bennamoun, F. Sohel, and R. Togneri. Cost
sensitive learning of deep feature representations from im-
balanced data. arXiv preprint arXiv:1508.03422, 2015. 3, 6,
7
[18] A. Krizhevsky, I. Sutskever, and G. E. Hinton. Imagenet
classification with deep convolutional neural networks. In
Advances in neural information processing systems, pages
1097–1105, 2012. 1, 2
[19] L. Liu, C. Shen, and A. van den Hengel. The treasure beneath
convolutional layers: Cross-convolutional-layer pooling for
image classification. In Proceedings of the IEEE Conference
on Computer Vision and Pattern Recognition, pages 4749–
4757, 2015. 4, 7
[20] A. Mahmood, M. Bennamoun, S. An, F. Sohel, F. Boussaid,
R. Hovey, G. Kendrick, and R. Fisher. Coral classification
with hybrid feature representations. In Image Processing
(ICIP), 2016 IEEE International Conference on, pages 519–
523. IEEE, 2016. 3, 6, 7
[21] A. Mahmood, M. Bennamoun, S. An, F. Sohel, F. Bous-
said, R. Hovey, G. Kendrick, and R. Fisher. Coral classifica-
tion with hybrid feature representations. In OCEANS. IEEE,
2016. 3
[22] A. Quattoni and A. Torralba. Recognizing indoor scenes.
In Computer Vision and Pattern Recognition, 2009. CVPR
2009. IEEE Conference on, pages 413–420. IEEE, 2009. 5
[23] A. S. Razavian, H. Azizpour, J. Sullivan, and S. Carlsson.
Cnn features off-the-shelf: an astounding baseline for recog-
nition. In Computer Vision and Pattern Recognition Work-
shops (CVPRW), 2014 IEEE Conference on, pages 512–519.
IEEE, 2014. 1, 2, 4, 6, 7
[24] P. Sermanet, D. Eigen, X. Zhang, M. Mathieu, R. Fergus,
and Y. LeCun. Overfeat: Integrated recognition, localization
and detection using convolutional networks. In ICLR, 2014.
2
[25] K. Simonyan and A. Zisserman. Very deep convolutional
networks for large-scale image recognition. arXiv preprint
arXiv:1409.1556, 2014. 1, 2, 4
[26] K. Sohn, D. Y. Jung, H. Lee, and A. O. Hero. Efficient learn-
ing of sparse, distributed, convolutional feature representa-
tions for object recognition. In 2011 International Confer-
ence on Computer Vision, pages 2643–2650. IEEE, 2011. 6
[27] R. K. Srivastava, K. Greff, and J. Schmidhuber. Highway
networks. ICML Workshop, 2015. 3
[28] C. Szegedy, W. Liu, Y. Jia, P. Sermanet, S. Reed,
D. Anguelov, D. Erhan, V. Vanhoucke, and A. Rabinovich.
Going deeper with convolutions. In Proceedings of the IEEE
Conference on Computer Vision and Pattern Recognition,
pages 1–9, 2015. 3
[29] A. Vedaldi and K. Lenc. Matconvnet – convolutional neural
networks for matlab. In Proceeding of the ACM Int. Conf. on
Multimedia, 2015. 5
[30] M. D. Zeiler and R. Fergus. Visualizing and understanding
convolutional networks. In European Conference on Com-
puter Vision, pages 818–833. Springer, 2014. 1, 2, 4, 5, 6
[31] B. Zhou, A. Lapedriza, J. Xiao, A. Torralba, and A. Oliva.
Learning deep features for scene recognition using places
database. In Advances in neural information processing sys-
tems, pages 487–495, 2014. 7
