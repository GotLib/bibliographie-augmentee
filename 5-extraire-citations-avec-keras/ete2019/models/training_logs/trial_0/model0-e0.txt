Using Theano backend.
WARNING (theano.tensor.blas): Using NumPy C-API based implementation for BLAS functions.
/home/pierre/.local/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:516: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint8 = np.dtype([("qint8", np.int8, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:517: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint8 = np.dtype([("quint8", np.uint8, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:518: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint16 = np.dtype([("qint16", np.int16, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:519: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint16 = np.dtype([("quint16", np.uint16, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:520: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint32 = np.dtype([("qint32", np.int32, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorflow/python/framework/dtypes.py:525: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  np_resource = np.dtype([("resource", np.ubyte, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:541: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint8 = np.dtype([("qint8", np.int8, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:542: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint8 = np.dtype([("quint8", np.uint8, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:543: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint16 = np.dtype([("qint16", np.int16, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:544: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_quint16 = np.dtype([("quint16", np.uint16, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:545: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint32 = np.dtype([("qint32", np.int32, 1)])
/home/pierre/.local/lib/python3.6/site-packages/tensorboard/compat/tensorflow_stub/dtypes.py:550: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  np_resource = np.dtype([("resource", np.ubyte, 1)])
(8, 1024, 32)
(8, 1024, 16)
(8, 1024, 16)
(8, 1024, 32)
(8, 1024, 64)
(8, 1024, 89)
(8, 1024, 89, 1)
(8, 1024, 89, 9)
(8, 1024, 9, 89)
models.py:109: UserWarning: Update your `Model` call to the Keras 2 API: `Model(inputs=/input_1, outputs=InplaceDim...)`
  model = Model(inputs=inputs, output=output)
Model: "model_1"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input_1 (InputLayer)         (8, 1024, 89)             0         
_________________________________________________________________
dense_1 (Dense)              (8, 1024, 32)             2880      
_________________________________________________________________
dense_2 (Dense)              (8, 1024, 16)             528       
_________________________________________________________________
conv1d_1 (Conv1D)            (8, 1024, 16)             2320      
_________________________________________________________________
conv1d_2 (Conv1D)            (8, 1024, 32)             4640      
_________________________________________________________________
conv1d_3 (Conv1D)            (8, 1024, 64)             18496     
_________________________________________________________________
conv1d_4 (Conv1D)            (8, 1024, 89)             5785      
_________________________________________________________________
reshape_1 (Reshape)          (8, 1024, 89, 1)          0         
_________________________________________________________________
conv2d_1 (Conv2D)            (8, 1024, 89, 9)          90        
_________________________________________________________________
permute_1 (Permute)          (8, 1024, 9, 89)          0         
=================================================================
Total params: 34,739
Trainable params: 34,739
Non-trainable params: 0
_________________________________________________________________
#>> Model0 settings :
NB_EPOCHS = 3
DILATION_RATE = 2 
NB_STEPS = 1024 # nb of characters in one sample from a text
BATCH_SIZE = 8 # nb of NB_STEPS-characters samples in a batch
ALPH_SIZE = 89 # not including the unknown character, which is embedded as a full-zeros ALPH_SIZE-length vector 
               # (moreover unknown chars are ignored and not included in the dataset)
WINDOW_RADIUS = 4


None
['1404.3606.pdf.txt', '1409.0473v7.pdf.txt', '1412.3555v1.pdf.txt', '1412.7054.pdf.txt', '1504.07889.pdf.txt', '1505.02000.pdf.txt', '1511.05960.pdf.txt', '1609.04846.pdf.txt', '1611.03530.pdf.txt', '1701.04489.pdf.txt', '1703.06870.pdf.txt', '1704.01664.pdf.txt', '1704.04368.pdf.txt', '1705.07962.pdf.txt', '1705.08039.pdf.txt', '1707.04244.pdf.txt', '1708.05148.pdf.txt', '1708.05344.pdf.txt', '1709.03450.pdf.txt', '1709.06680.pdf.txt', '1710.05941.pdf.txt', '1710.06632.pdf.txt', '1712.03541.pdf.txt', '1712.05319.pdf.txt', '1801.01402.pdf.txt', '1801.09321.pdf.txt', '1802.01021.pdf.txt', '1802.02353.pdf.txt', '1802.03268.pdf.txt', '1802.06955.pdf.txt', '1803.02780.pdf.txt', '1803.10615.pdf.txt', '1803.10837.pdf.txt', '1803.11395.pdf.txt', '1804.00815.pdf.txt', '1804.04340.pdf.txt', '2011+_ICDM12_RankTopic_.pdf.txt', '2014 - DeepWalk: Online Learning of Social Representations.pdf.txt', '2016 - node2vec: Scalable Feature Learning for Networks.pdf.txt', '2016_Sentence_Clustering_using_PageRank_Topic_Model.pdf.txt', '2017 - Attention Is All You Need.pdf.txt', '2017 - Get To The Point: Summarization with Pointer-Generator Networks.pdf.txt', '2017 - Modeling Relational Data with Graph Convolutional Networks.pdf.txt', '2017_Context_Embeddings_for_NLP.pdf.txt', '2017_The_enhancement_of_TextRank_algorithm_by_using_word2vec_and_its_applications_on_topic_extraction.pdf.txt', '2018 - Deep contextualized word representations.pdf.txt', '2018 - Fine-tuning the Ant Colony System algorithm.pdf.txt', '2018 - Improving Language Understanding by generative pre-training.pdf.txt', '2018 - Learning Unsupervised Learning Rules.pdf.txt', '2018 - Neural Ordinary Differential Equations.pdf.txt', '2018 - Universal Language Model Fine-tuning for Text Classification.pdf.txt', '2018-Learning-Embeddings-for-157-languages.pdf.txt', '2018_From_Text_to_Topics_in_Healthcare_Records__An_Unsupervised_Graph_Partitioning_Methodology.pdf.txt', 'AnArchitectureForParallelTopicModels.pdf.txt', 'Character-level-convolutional-networks-for-text-classification.pdf.txt', 'Colorless green recurrent networks dream hierarchically.pdf.txt', 'Data-dependent Initializations of Convolutional Neural Networks.pdf.txt', 'Deep Communicating Agents for Abstractive Summarization.pdf.txt', 'Deep-Pyramid-CNN-for-Text-Categorization.pdf.txt', 'DeepCommunicatingAgentsForAbstractiveSummarization.pdf.txt', 'Dense_Associative_Memory_For_Pattern_Recognition.pdf.txt', 'LSTM_DSSM_IEEE_TASLP.pdf.txt', 'Loss_Surfaces.pdf.txt', 'On Optimal Parameters for Ant Colony Optimization algorithms.pdf.txt', 'Optimization_As_A_Model_For_few_shot_Learning.pdf.txt', 'PageRank_on_semantic_networks_with_application_to_wsd.pdf.txt', 'Personnalized-Pagerank-for-Word-Sense-Disambiguation-2009.pdf.txt', 'Powerful_Generative_Model_Using_Random_Weights.pdf.txt', 'QRNN.pdf.txt', 'Random_BackProp_And_The_Learning_Channel.pdf.txt', 'Representation_Learning_By_Learning_To_Count.pdf.txt', 'Representation_Learning_On_Graphs.pdf.txt', 'ResFeats.pdf.txt', 'Rethinking_Generalization.pdf.txt', 'Supervised Learning of Universal Sentence Representations from Natural Language Inference Data.pdf.txt', 'The strange geometry of skip-gram with negative sampling.pdf.txt', 'VeryDeepCNNforTextClasification.pdf.txt', 'adversarialExamplesDetectionInConvNets.pdf.txt', 'clustering-paraphrases-by-word-sense.pdf.txt', 'cnnSparsity.pdf.txt', 'd41586-018-07196-1.pdf.txt', 'learningHierarchicalFeaturesFromGenerativeModels.pdf.txt', 'multiscaleHierarchicalCnn.pdf.txt', 'multiscaleResidualMixtureOfPca.pdf.txt', 'pcaInitializedAutoEncoders.pdf.txt', 'rfp0191-wangAemb.pdf.txt', 'train-nn-with-ant-system.pdf.txt', 'unsupervised text summarization using sentence embeddings.pdf.txt']
72953
72953
mul: 249
(71, 1024, 89)
(71, 1024, 9, 89)
Epoch 1/1
64/71 [==========================>...] - ETA: 0s - loss: 0.0196 - acc: 0.0154 - mean_absolute_error: 0.118450353
50353
mul: 177
(49, 1024, 89)
(49, 1024, 9, 89)
Epoch 1/1
48/49 [============================>.] - ETA: 0s - loss: 0.0195 - acc: 0.0344 - mean_absolute_error: 0.118323899
23899
mul: 347
(23, 1024, 89)
(23, 1024, 9, 89)
Epoch 1/1
16/23 [===================>..........] - ETA: 0s - loss: 0.0196 - acc: 0.0456 - mean_absolute_error: 0.118334519
34519
mul: 727
(33, 1024, 89)
(33, 1024, 9, 89)
Epoch 1/1
32/33 [============================>.] - ETA: 0s - loss: 0.0197 - acc: 0.0613 - mean_absolute_error: 0.118577012
77012
mul: 212
(75, 1024, 89)
(75, 1024, 9, 89)
Epoch 1/1
72/75 [===========================>..] - ETA: 0s - loss: 0.0196 - acc: 0.0854 - mean_absolute_error: 0.118457830
57830
mul: 486
(56, 1024, 89)
(56, 1024, 9, 89)
Epoch 1/1
56/56 [==============================] - 3s 50ms/step - loss: 0.0194 - acc: 0.1360 - mean_absolute_error: 0.1181
38743
38743
mul: 855
(37, 1024, 89)
(37, 1024, 9, 89)
Epoch 1/1
32/37 [========================>.....] - ETA: 0s - loss: 0.0192 - acc: 0.1887 - mean_absolute_error: 0.1177109480
109480
mul: 936
(106, 1024, 89)
(106, 1024, 9, 89)
Epoch 1/1
104/106 [============================>.] - ETA: 0s - loss: 0.0185 - acc: 0.2620 - mean_absolute_error: 0.116551286
51286
mul: 86
(50, 1024, 89)
(50, 1024, 9, 89)
Epoch 1/1
48/50 [===========================>..] - ETA: 0s - loss: 0.0177 - acc: 0.3684 - mean_absolute_error: 0.114713990
13990
mul: 678
(13, 1024, 89)
(13, 1024, 9, 89)
Epoch 1/1
 8/13 [=================>............] - ETA: 0s - loss: 0.0171 - acc: 0.4407 - mean_absolute_error: 0.113661965
61965
mul: 525
(60, 1024, 89)
(60, 1024, 9, 89)
Epoch 1/1
56/60 [===========================>..] - ETA: 0s - loss: 0.0173 - acc: 0.3754 - mean_absolute_error: 0.113948661
48661
mul: 533
(47, 1024, 89)
(47, 1024, 9, 89)
Epoch 1/1
40/47 [========================>.....] - ETA: 0s - loss: 0.0167 - acc: 0.4322 - mean_absolute_error: 0.112866775
66775
mul: 215
(65, 1024, 89)
(65, 1024, 9, 89)
Epoch 1/1
64/65 [============================>.] - ETA: 0s - loss: 0.0163 - acc: 0.4613 - mean_absolute_error: 0.112226946
26946
mul: 322
(26, 1024, 89)
(26, 1024, 9, 89)
Epoch 1/1
24/26 [==========================>...] - ETA: 0s - loss: 0.0163 - acc: 0.4522 - mean_absolute_error: 0.112237223
37223
mul: 359
(36, 1024, 89)
(36, 1024, 9, 89)
Epoch 1/1
32/36 [=========================>....] - ETA: 0s - loss: 0.0162 - acc: 0.4555 - mean_absolute_error: 0.112029205
29205
mul: 533
(28, 1024, 89)
(28, 1024, 9, 89)
Epoch 1/1
24/28 [========================>.....] - ETA: 0s - loss: 0.0162 - acc: 0.4456 - mean_absolute_error: 0.112167811
67811
mul: 227
(66, 1024, 89)
(66, 1024, 9, 89)
Epoch 1/1
64/66 [============================>.] - ETA: 0s - loss: 0.0163 - acc: 0.4489 - mean_absolute_error: 0.112265261
65261
mul: 749
(63, 1024, 89)
(63, 1024, 9, 89)
Epoch 1/1
56/63 [=========================>....] - ETA: 0s - loss: 0.0166 - acc: 0.3640 - mean_absolute_error: 0.113024709
24709
mul: 133
(24, 1024, 89)
(24, 1024, 9, 89)
Epoch 1/1
24/24 [==============================] - 1s 46ms/step - loss: 0.0159 - acc: 0.4741 - mean_absolute_error: 0.1116
29016
29016
mul: 344
(28, 1024, 89)
(28, 1024, 9, 89)
Epoch 1/1
24/28 [========================>.....] - ETA: 0s - loss: 0.0159 - acc: 0.4790 - mean_absolute_error: 0.111542232
42232
mul: 248
(41, 1024, 89)
(41, 1024, 9, 89)
Epoch 1/1
40/41 [============================>.] - ETA: 0s - loss: 0.0161 - acc: 0.4445 - mean_absolute_error: 0.112050506
50506
mul: 330
(49, 1024, 89)
(49, 1024, 9, 89)
Epoch 1/1
48/49 [============================>.] - ETA: 0s - loss: 0.0160 - acc: 0.4738 - mean_absolute_error: 0.111615642
15642
mul: 282
(15, 1024, 89)
(15, 1024, 9, 89)
Epoch 1/1
 8/15 [===============>..............] - ETA: 0s - loss: 0.0160 - acc: 0.4532 - mean_absolute_error: 0.111762556
62556
mul: 92
(61, 1024, 89)
(61, 1024, 9, 89)
Epoch 1/1
56/61 [==========================>...] - ETA: 0s - loss: 0.0158 - acc: 0.4791 - mean_absolute_error: 0.111522528
22528
mul: 0
(0, 1024, 89)
(0, 1024, 9, 89)
Epoch 1/1
30104
30104
mul: 408
(29, 1024, 89)
(29, 1024, 9, 89)
Epoch 1/1
24/29 [=======================>......] - ETA: 0s - loss: 0.0157 - acc: 0.4944 - mean_absolute_error: 0.111258237
58237
mul: 893
(56, 1024, 89)
(56, 1024, 9, 89)
Epoch 1/1
56/56 [==============================] - 3s 46ms/step - loss: 0.0158 - acc: 0.4685 - mean_absolute_error: 0.1114
61343
61343
mul: 927
(59, 1024, 89)
(59, 1024, 9, 89)
Epoch 1/1
56/59 [===========================>..] - ETA: 0s - loss: 0.0153 - acc: 0.5341 - mean_absolute_error: 0.110546041
46041
mul: 985
(44, 1024, 89)
(44, 1024, 9, 89)
Epoch 1/1
40/44 [==========================>...] - ETA: 0s - loss: 0.0156 - acc: 0.4789 - mean_absolute_error: 0.111153686
53686
mul: 438
(52, 1024, 89)
(52, 1024, 9, 89)
Epoch 1/1
48/52 [==========================>...] - ETA: 0s - loss: 0.0158 - acc: 0.4686 - mean_absolute_error: 0.111452171
52171
mul: 971
(50, 1024, 89)
(50, 1024, 9, 89)
Epoch 1/1
48/50 [===========================>..] - ETA: 0s - loss: 0.0159 - acc: 0.4611 - mean_absolute_error: 0.111443749
43749
mul: 741
(42, 1024, 89)
(42, 1024, 9, 89)
Epoch 1/1
40/42 [===========================>..] - ETA: 0s - loss: 0.0157 - acc: 0.4649 - mean_absolute_error: 0.111351220
51220
mul: 20
(50, 1024, 89)
(50, 1024, 9, 89)
Epoch 1/1
48/50 [===========================>..] - ETA: 0s - loss: 0.0155 - acc: 0.5007 - mean_absolute_error: 0.110880135
80135
mul: 263
(78, 1024, 89)
(78, 1024, 9, 89)
Epoch 1/1
72/78 [==========================>...] - ETA: 0s - loss: 0.0157 - acc: 0.4753 - mean_absolute_error: 0.111134127
34127
mul: 335
(33, 1024, 89)
(33, 1024, 9, 89)
Epoch 1/1
32/33 [============================>.] - ETA: 0s - loss: 0.0157 - acc: 0.4668 - mean_absolute_error: 0.111355774
55774
mul: 478
(54, 1024, 89)
(54, 1024, 9, 89)
Epoch 1/1
48/54 [=========================>....] - ETA: 0s - loss: 0.0155 - acc: 0.4957 - mean_absolute_error: 0.110950557
50557
mul: 381
(49, 1024, 89)
(49, 1024, 9, 89)
Epoch 1/1
48/49 [============================>.] - ETA: 0s - loss: 0.0155 - acc: 0.4950 - mean_absolute_error: 0.110950795
50795
mul: 619
(49, 1024, 89)
(49, 1024, 9, 89)
Epoch 1/1
48/49 [============================>.] - ETA: 0s - loss: 0.0168 - acc: 0.4270 - mean_absolute_error: 0.113161231
61231
mul: 815
(59, 1024, 89)
(59, 1024, 9, 89)
Epoch 1/1
56/59 [===========================>..] - ETA: 0s - loss: 0.0157 - acc: 0.5003 - mean_absolute_error: 0.111029358
29358
mul: 686
(28, 1024, 89)
(28, 1024, 9, 89)
Epoch 1/1
24/28 [========================>.....] - ETA: 0s - loss: 0.0154 - acc: 0.4870 - mean_absolute_error: 0.110739456
39456
mul: 544
(38, 1024, 89)
(38, 1024, 9, 89)
Epoch 1/1
32/38 [========================>.....] - ETA: 0s - loss: 0.0155 - acc: 0.4985 - mean_absolute_error: 0.110866775
66775
mul: 215
(65, 1024, 89)
(65, 1024, 9, 89)
Epoch 1/1
64/65 [============================>.] - ETA: 0s - loss: 0.0153 - acc: 0.5168 - mean_absolute_error: 0.110444675
44675
mul: 643
(43, 1024, 89)
(43, 1024, 9, 89)
Epoch 1/1
40/43 [==========================>...] - ETA: 0s - loss: 0.0156 - acc: 0.4779 - mean_absolute_error: 0.111058473
58473
mul: 105
(57, 1024, 89)
(57, 1024, 9, 89)
Epoch 1/1
56/57 [============================>.] - ETA: 0s - loss: 0.0158 - acc: 0.4703 - mean_absolute_error: 0.111316913
16913
mul: 529
(16, 1024, 89)
(16, 1024, 9, 89)
Epoch 1/1
16/16 [==============================] - 1s 46ms/step - loss: 0.0156 - acc: 0.4676 - mean_absolute_error: 0.1110
55559
55559
mul: 263
(54, 1024, 89)
(54, 1024, 9, 89)
Epoch 1/1
48/54 [=========================>....] - ETA: 0s - loss: 0.0156 - acc: 0.4756 - mean_absolute_error: 0.111026259
26259
mul: 659
(25, 1024, 89)
(25, 1024, 9, 89)
Epoch 1/1
24/25 [===========================>..] - ETA: 0s - loss: 0.0158 - acc: 0.4321 - mean_absolute_error: 0.111540639
40639
mul: 703
(39, 1024, 89)
(39, 1024, 9, 89)
Epoch 1/1
32/39 [=======================>......] - ETA: 0s - loss: 0.0158 - acc: 0.4774 - mean_absolute_error: 0.111271838
71838
mul: 158
(70, 1024, 89)
(70, 1024, 9, 89)
Epoch 1/1
64/70 [==========================>...] - ETA: 0s - loss: 0.0156 - acc: 0.4830 - mean_absolute_error: 0.110953943
53943
mul: 695
(52, 1024, 89)
(52, 1024, 9, 89)
Epoch 1/1
48/52 [==========================>...] - ETA: 0s - loss: 0.0156 - acc: 0.4556 - mean_absolute_error: 0.111146167
46167
mul: 87
(45, 1024, 89)
(45, 1024, 9, 89)
Epoch 1/1
40/45 [=========================>....] - ETA: 0s - loss: 0.0156 - acc: 0.4841 - mean_absolute_error: 0.111024952
24952
mul: 376
(24, 1024, 89)
(24, 1024, 9, 89)
Epoch 1/1
24/24 [==============================] - 1s 52ms/step - loss: 0.0157 - acc: 0.4665 - mean_absolute_error: 0.1111
23715
23715
mul: 163
(23, 1024, 89)
(23, 1024, 9, 89)
Epoch 1/1
16/23 [===================>..........] - ETA: 0s - loss: 0.0153 - acc: 0.5194 - mean_absolute_error: 0.110443920
43920
mul: 912
(42, 1024, 89)
(42, 1024, 9, 89)
Epoch 1/1
40/42 [===========================>..] - ETA: 0s - loss: 0.0152 - acc: 0.5063 - mean_absolute_error: 0.110432883
32883
mul: 115
(32, 1024, 89)
(32, 1024, 9, 89)
Epoch 1/1
32/32 [==============================] - 2s 47ms/step - loss: 0.0157 - acc: 0.4694 - mean_absolute_error: 0.1111
47634
47634
mul: 530
(46, 1024, 89)
(46, 1024, 9, 89)
Epoch 1/1
40/46 [=========================>....] - ETA: 0s - loss: 0.0156 - acc: 0.4983 - mean_absolute_error: 0.111043998
43998
mul: 990
(42, 1024, 89)
(42, 1024, 9, 89)
Epoch 1/1
40/42 [===========================>..] - ETA: 0s - loss: 0.0155 - acc: 0.5000 - mean_absolute_error: 0.110855213
55213
mul: 941
(53, 1024, 89)
(53, 1024, 9, 89)
Epoch 1/1
48/53 [==========================>...] - ETA: 0s - loss: 0.0154 - acc: 0.4891 - mean_absolute_error: 0.110736892
36892
mul: 28
(36, 1024, 89)
(36, 1024, 9, 89)
Epoch 1/1
32/36 [=========================>....] - ETA: 0s - loss: 0.0155 - acc: 0.4800 - mean_absolute_error: 0.110955016
55016
mul: 744
(53, 1024, 89)
(53, 1024, 9, 89)
Epoch 1/1
48/53 [==========================>...] - ETA: 0s - loss: 0.0154 - acc: 0.4922 - mean_absolute_error: 0.110741299
41299
mul: 339
(40, 1024, 89)
(40, 1024, 9, 89)
Epoch 1/1
40/40 [==============================] - 2s 46ms/step - loss: 0.0151 - acc: 0.5188 - mean_absolute_error: 0.1103
84043
84043
mul: 75
(82, 1024, 89)
(82, 1024, 9, 89)
Epoch 1/1
80/82 [============================>.] - ETA: 0s - loss: 0.0158 - acc: 0.4202 - mean_absolute_error: 0.111650813
50813
mul: 637
(49, 1024, 89)
(49, 1024, 9, 89)
Epoch 1/1
48/49 [============================>.] - ETA: 0s - loss: 0.0159 - acc: 0.4408 - mean_absolute_error: 0.111627870
27870
mul: 222
(27, 1024, 89)
(27, 1024, 9, 89)
Epoch 1/1
24/27 [=========================>....] - ETA: 0s - loss: 0.0152 - acc: 0.5180 - mean_absolute_error: 0.110237549
37549
mul: 685
(36, 1024, 89)
(36, 1024, 9, 89)
Epoch 1/1
32/36 [=========================>....] - ETA: 0s - loss: 0.0153 - acc: 0.5147 - mean_absolute_error: 0.110533963
33963
mul: 171
(33, 1024, 89)
(33, 1024, 9, 89)
Epoch 1/1
32/33 [============================>.] - ETA: 0s - loss: 0.0153 - acc: 0.5136 - mean_absolute_error: 0.110435590
35590
mul: 774
(34, 1024, 89)
(34, 1024, 9, 89)
Epoch 1/1
32/34 [===========================>..] - ETA: 0s - loss: 0.0155 - acc: 0.4796 - mean_absolute_error: 0.110829577
29577
mul: 905
(28, 1024, 89)
(28, 1024, 9, 89)
Epoch 1/1
24/28 [========================>.....] - ETA: 0s - loss: 0.0153 - acc: 0.5166 - mean_absolute_error: 0.110435568
35568
mul: 752
(34, 1024, 89)
(34, 1024, 9, 89)
Epoch 1/1
32/34 [===========================>..] - ETA: 0s - loss: 0.0156 - acc: 0.4883 - mean_absolute_error: 0.110988443
88443
mul: 379
(86, 1024, 89)
(86, 1024, 9, 89)
Epoch 1/1
80/86 [==========================>...] - ETA: 0s - loss: 0.0155 - acc: 0.4577 - mean_absolute_error: 0.110940423
40423
mul: 487
(39, 1024, 89)
(39, 1024, 9, 89)
Epoch 1/1
32/39 [=======================>......] - ETA: 0s - loss: 0.0155 - acc: 0.4902 - mean_absolute_error: 0.110878218
78218
mul: 394
(76, 1024, 89)
(76, 1024, 9, 89)
Epoch 1/1
72/76 [===========================>..] - ETA: 1s - loss: 0.0154 - acc: 0.5086 - mean_absolute_error: 0.110535628
35628
mul: 812
(34, 1024, 89)
(34, 1024, 9, 89)
Epoch 1/1
32/34 [===========================>..] - ETA: 0s - loss: 0.0157 - acc: 0.4711 - mean_absolute_error: 0.111151958
51958
mul: 758
(50, 1024, 89)
(50, 1024, 9, 89)
Epoch 1/1
48/50 [===========================>..] - ETA: 0s - loss: 0.0154 - acc: 0.5044 - mean_absolute_error: 0.110642383
42383
mul: 399
(41, 1024, 89)
(41, 1024, 9, 89)
Epoch 1/1
40/41 [============================>.] - ETA: 0s - loss: 0.0156 - acc: 0.4792 - mean_absolute_error: 0.111027057
27057
mul: 433
(26, 1024, 89)
(26, 1024, 9, 89)
Epoch 1/1
24/26 [==========================>...] - ETA: 0s - loss: 0.0152 - acc: 0.5146 - mean_absolute_error: 0.110337243
37243
mul: 379
(36, 1024, 89)
(36, 1024, 9, 89)
Epoch 1/1
32/36 [=========================>....] - ETA: 0s - loss: 0.0154 - acc: 0.4983 - mean_absolute_error: 0.110648659
48659
mul: 531
(47, 1024, 89)
(47, 1024, 9, 89)
Epoch 1/1
40/47 [========================>.....] - ETA: 0s - loss: 0.0154 - acc: 0.4962 - mean_absolute_error: 0.110738661
38661
mul: 773
(37, 1024, 89)
(37, 1024, 9, 89)
Epoch 1/1
32/37 [========================>.....] - ETA: 0s - loss: 0.0154 - acc: 0.5007 - mean_absolute_error: 0.110639692
39692
mul: 780
(38, 1024, 89)
(38, 1024, 9, 89)
Epoch 1/1
32/38 [========================>.....] - ETA: 0s - loss: 0.0155 - acc: 0.4929 - mean_absolute_error: 0.110710695
10695
mul: 455
(10, 1024, 89)
(10, 1024, 9, 89)
Epoch 1/1
 8/10 [=======================>......] - ETA: 0s - loss: 0.0153 - acc: 0.5191 - mean_absolute_error: 0.110437416
37416
mul: 552
(36, 1024, 89)
(36, 1024, 9, 89)
Epoch 1/1
32/36 [=========================>....] - ETA: 0s - loss: 0.0155 - acc: 0.4841 - mean_absolute_error: 0.110837969
37969
mul: 81
(37, 1024, 89)
(37, 1024, 9, 89)
Epoch 1/1
32/37 [========================>.....] - ETA: 0s - loss: 0.0155 - acc: 0.4757 - mean_absolute_error: 0.110826948
26948
mul: 324
(26, 1024, 89)
(26, 1024, 9, 89)
Epoch 1/1
24/26 [==========================>...] - ETA: 0s - loss: 0.0155 - acc: 0.4544 - mean_absolute_error: 0.111024710
24710
mul: 134
(24, 1024, 89)
(24, 1024, 9, 89)
Epoch 1/1
24/24 [==============================] - 1s 57ms/step - loss: 0.0154 - acc: 0.4936 - mean_absolute_error: 0.1107
58318
58318
mul: 974
(56, 1024, 89)
(56, 1024, 9, 89)
Epoch 1/1
56/56 [==============================] - 3s 58ms/step - loss: 0.0154 - acc: 0.5030 - mean_absolute_error: 0.1107
51751
51751
mul: 551
(50, 1024, 89)
(50, 1024, 9, 89)
Epoch 1/1
48/50 [===========================>..] - ETA: 0s - loss: 0.0155 - acc: 0.4899 - mean_absolute_error: 0.110732412
32412
mul: 668
(31, 1024, 89)
(31, 1024, 9, 89)
Epoch 1/1
24/31 [======================>.......] - ETA: 0s - loss: 0.0156 - acc: 0.4833 - mean_absolute_error: 0.1110articles that caused errors: 88
Durée d'exécution (mins) : 9.269297508398692

