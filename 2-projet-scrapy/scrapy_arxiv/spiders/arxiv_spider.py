

import regex as re
from random import random as rd
#import pickle
import json
from time import sleep
from random import random as rd

#from scrapy.crawler import CrawlerProcess
from scrapy import spiders
from scrapy.http.request import Request
from scrapy.selector import Selector
#from scrapy.exporters import CsvItemExporter, JsonItemExporter, JsonLinesItemExporter, XmlItemExporter
from scrapy_arxiv.items import Article, ArticleLoader, Biblio
#from scrapy_arxiv.pipelines import ArticleRetrieverPipeline, JsonWriterPipeline


BIBLIO_LOC_0 = "/home/pierre/Documents/ECM/3A_bis/nlp-arxiv-ecm/depot_GotLib_BA/2-projet-scrapy/biblio-test"
META_DIR_0 = BIBLIO_LOC_0 + "/articles-metadata"
PDF_DIR_0 = BIBLIO_LOC_0 + "/articles-pdf"
LATEX_DIR_0 = BIBLIO_LOC_0 + "/articles-latex"

# pour faire manuellement un corpus sur lequel travailler la visualisation du graphe (networkX + plotly)
VIZ_BIBLIO_LOC = "/home/pierre/Documents/ECM/3A_bis/nlp-arxiv-ecm/depot_GotLib_BA/6-graph-viz/biblio-test-viz"
VIZ_META_DIR = "/métadonnées"
VIZ_PDF_DIR = "/pdf"

META_DIR = VIZ_BIBLIO_LOC + VIZ_META_DIR
PDF_DIR = VIZ_BIBLIO_LOC + VIZ_PDF_DIR

BIB_LOAD = VIZ_BIBLIO_LOC + "/Biblio_instance.h5"
BIB_SAVE = BIB_LOAD

MAX_TRIALS = 50
ENTRIES_PER_REQUEST = 10
USE_AUTHORS_FIELD = True

## construction des urls à regarder dans l'API et dans OAI:
def api_url(title, authors, max_results=ENTRIES_PER_REQUEST, start_results=0, use_authors_field=USE_AUTHORS_FIELD):
    if use_authors_field:
        url= "https://export.arxiv.org/api/query?search_query=ti:" + title + "&"
        for auth in authors:
            url += "au:" + auth + "+OR+"
        url = url.strip("OR+")
        url += "&start=" + str(start_results) + "&max_results=" + str(max_results)
        return url
    else:
        return "https://export.arxiv.org/api/query?search_query=ti:" + title + "&start=" + str(start_results) + "&max_results=" + str(max_results)

def oai_url(identifier):
    return "https://export.arxiv.org/oai2?verb=GetRecord&identifier=oai:arXiv.org:" + identifier + "&metadataPrefix=arXiv"

def pdf_url(identifier):
    return "https://arxiv.org/pdf/" + str(identifier) + ".pdf"

def abs_url(identifier):
    return "https://arxiv.org/abs/" + str(identifier)

## export des items, pdf et latex au format JSON:
def export_metadata_json(item_article):
    with open(META_DIR + '/' + item_article['identifier'] + '_meta.json', 'w') as f:
        line = json.dumps(dict(item_article)) + "\n"
        f.write(line)

        
samples_titles =  ["Character-level Convolutional Networks for Text Classification", 
                   "A General-Purpose Tagger with Convolutional Neural Networks", 
                   "Deep contextualized word representations", "PCANet: A Simple Deep Learning Baseline for Image Classification?",
                   "Text Understanding from Scratch",
                   "A Brief Survey of Text Mining: Classification, Clustering and Extraction Techniques",
                   "VISUALIZING AND UNDERSTANDING RECURRENT NETWORKS",
                   "Character-Aware Neural Language Models",
                   "On the period mod m of polynomially-recursive sequences: a case study", 
                   "One-Shot Imitation Learning"]

samples_authors = [['Xiang ZHANG', 'Junbo ZHAO', 'Yann LECUN'], 
                   ["Xiang Yu", "Agnieszka Faleńska"], 
                   ["Tsung-Han Chan", "Kui Jia", "Shenghua Gao", "Jiwen Lu", "Zinan Zeng", "Yi Ma"],
                   ["Xiang Zhang", "Yann LeCun"], 
                   ["Mehdi Allahyari", "Seyedamin Pouriyeh", "Mehdi Assefi", "Saied Safaei", \
                    "Elizabeth D. Trippe", "Juan B. Gutierrez", "Krys Kochut"],
                   ["Andrej Karpathy", "Justin Johnson", "Li Fei-Fei"],
                   ["Yoon Kim", "Yacine Jernite", "David Sontag", "Alexander M. Rush"],
                   [" Cyril Banderier, Florian Luca"],
                   ["Yan Duan"]
                  ]
which=-3
title_test = samples_titles[which]
authors_test = samples_authors[which]

not_in_arxiv=[
                {'title'  : "Improving Language Understanding By Generative Pre Training", 
                 'authors': ["Alec Radford", "Karthik Narasimhan", "Tim Salimans", "Ilya Sutskever"]
                },

                {'title'  : "CLUSTERING APPROACH FOR CLASSIFICATION OF RESEARCH ARTICLES BASED ON KEYWORD SEARCH",
                 'authors': ["Dr. S. K. Jayanthi", "C. Kavi Priya"]
                }
             ]

api_template_url = "https://export.arxiv.org/api/query?search_query=ti:" + "TITLE" + "+AND+au:" + "AUTHOR KEYNAME" + "&max_results=1"
oai_template_url = "https://export.arxiv.org/oai2?verb=GetRecord&identifier=oai:arXiv.org:" + "XXXX.XXXX" + "&metadataPrefix=arXiv"


class ArXivSpider(spiders.Spider):
    name = "arxiv_spider"
    
    def __init__(self, title_start=title_test, authors_start=authors_test, entries_per_request=ENTRIES_PER_REQUEST,\
                 max_trials=MAX_TRIALS, *args, **kwargs):
        super(ArXivSpider, self).__init__(*args, **kwargs)
        self.title_start = ArticleLoader().normalize_title(title_start)
        self.authors_start = authors_start
        self.entries_per_request = entries_per_request
        self.start_urls = [api_url(self.title_start, authors_start, entries_per_request)]
        self.max_trials = max_trials
        self.bib = Biblio(loc=VIZ_BIBLIO_LOC)
        #self.bib.load_bib(BIB_LOAD)

    def start_requests(self):
        urls = self.start_urls
        for url in urls:
            api_request = Request(url=url, callback=self.get_article)
            api_request.meta['target_title'] = self.title_start
            api_request.meta['target_authors'] = self.authors_start
            api_request.meta['entries_per_request'] = self.entries_per_request
            api_request.meta['start'] = 0
            yield api_request

    def get_article(self, response): #récupère les métadonnées d'un article, et son pdf (grâce à la métadonnée 'identifier' qui le caractérise)

        print("\t### ENTER get_article PARSER FUNCTION ###")

        sel = Selector(response=response)
        sel.remove_namespaces()
        print("\t\tentries retournées: " + str(len(sel.xpath('//entry').getall())))
        print("\t\t*** ENTER 'FOR' LOOP ON ENTRIES ***")
        print("\t\ttitre recherché : " + response.meta['target_title'])

        retry = True
        for count, entry in enumerate(sel.xpath('//entry').getall()):
            if response.meta['start'] + count + 1 < self.max_trials :
                entry_sel = Selector(text=entry)#.remove_namespaces()
                a = Article()
                al = ArticleLoader(item=a, selector=entry_sel, response=entry)
                al.add_xpath('identifier', "//entry/id/text()")
                al.add_xpath('title', "//entry/title/text()")
                al.add_xpath('created', '//published/text()')
                al.add_xpath('updated', '//updated/text()')
                al.add_xpath('authors', '//author/name/text()')       
                al.add_xpath('abstract', '//summary/text()')
                al.add_value('found', True)
                al.load_item()
                print("\t\t\tresponse.meta['target_title']: " +  str(response.meta['target_title']))
                print('\t\t\ta[title]: ' + str(a['title']) + '\n')

                if a['title'] == response.meta['target_title']:
                    retry = False
                    print('\t\t\t*** TITLE OK! ***')
                    
                    export_metadata_json(a)
                    self.bib.add_article(a)

                    pdf_request = Request(url = pdf_url(a['identifier']), callback = self.get_pdf)
                    yield pdf_request

                    refs_request = Request(url = abs_url(a['identifier']),  # pour récupérer les refs ET les citations (!=)
                                           callback = self.get_refs,  
                                           meta = {'article': a, 'article_loader': al})
                    yield refs_request # (<--get_refs inutilisable pour le moment à cause du pb avec le javascript)

                    print("\t\t*** EXIT 'FOR' LOOP ON ENTRIES ***")
                    #self.bib.save_bib(BIB_SAVE)
                    break
            else:
                print("\t\tOn n'a pas pu trouver l'article.")
                retry = False
                break

        if retry:
            new_meta = response.meta
            new_meta['start'] += new_meta['entries_per_request']
            yield Request(meta=new_meta,
                          callback=self.get_article,
                          url=api_url(title=response.meta['target_title'], 
                                      authors=response.meta['target_authors'], 
                                      start_results=response.meta['start'], 
                                      max_results=response.meta['entries_per_request'])) 

        print("\t### EXIT get_article PARSER FUNCTION ###")

    def get_pdf(self, response):

        print("\t### ENTER get_pdf PARSER FUNCTION ###")

        path = PDF_DIR
        name = response.url.split('/')[-1]
        with open(path + '/' + name, 'wb') as f:
            f.write(response.body)

        print("\t### EXIT get_pdf PARSER FUNCTION ###")
    
    def get_refs(self, response): # ...and loop back, yielding a Request to get each article mentionned in the refs.
        """from the abs_url html response :
           - gets the references and the citations for this article `a` , thanks to bibex 
           - builds an Article object from each ref/citation (with `found`==False)
           - adds it to self.bib with Biblio.add_article(a, edges_from=`references`, edges_towards=`citations`)
        """
        
        #return # get back to already working parts while get_refs isn't OK 
      
        print("\t### ENTER get_refs PARSER FUNCTION ###")

        refs = []
        citations = []

        print(response.body)

        # -> le code qui suit devait récupérer les metadonnées de chaque article dans les références de l'article courant.
        #    Ce n'est plus possible car finalement on va se passer de Splash
        """
        refs_sel = response.xpath('//div[@id="col-references"]').get()
        assert refs_sel is not None, "refs_sel is None : no bibex ?"
        refs_sel.remove_namespaces()
        for ref in refs_sel('//div[@class="bib-paper"]').getall():
            ar = Article()
            arl = ArticleLoader(item=a, selector=refs_sel)
            arl.add_xpath('title',   '//a[@class="notinfluential mathjax"]/text()')
            arl.add_xpath('created', '//span[@class="jinfo"]/span[@class="year"]/text()')
            arl.add_xpath('journal', '//splan[@class="jinfo"]/span[@class="venue"]/text()')
            arl.add_xpath('authors', '//div[@class="bib-authors"]/a/text()')
            arl.add_value('found', False)
            arl.load_item()
            refs.append(ar) 
        """
        # => TODO : entrainer un modèle de RN pour :
        #           - identifier les refs dans le texte brut
        #           - les parser en métadonnées (titre, auteurs, date, journal) au niveau des caractères.

        api_url = api_url(title=ar['title'], authors=ar['authors'])
        api_request = Request(url=api_url, callback=get_article)
        api_request.meta['target_title'] = ar['title']
        api_request.meta['target_authors'] = ar['authors']
        api_request.meta['entries_per_request'] = self.entries_per_request
        api_request.meta['start'] = 0
        yield api_request

        # -> sans Splash, on ne peut plus avoir les citations vers l'article courant car elles ne sont pas dans le PDF (que sur la page web)
        """
        citations_sel = response.xpath('//div[@id="col-citations"]').get()
        citations_sel.remove_namespaces()
        for cit in citations_sel('//div[@class="bib-paper"]').getall():
            ac = Article()
            acl = ArticleLoader(item=a, selector=citations_sel)
            acl.add_xpath('title',   '//a[@class="notinfluential mathjax"]/text()')
            acl.add_xpath('created', '//span[@class="jinfo"]/span[@class="year"]/text()')
            acl.add_xpath('journal', '//splan[@class="jinfo"]/span[@class="venue"]/text()')
            acl.add_xpath('authors', '//div[@class="bib-authors"]/a/text()')       
            acl.add_value('found', False)
            acl.load_item()
            citations.append(ac)

            api_url = api_url(title=ac['title'], authors=ac['authors'])
            api_request = Request(url=api_url, callback=get_article)
            api_request.meta['target_title'] = ac['title']
            api_request.meta['target_authors'] = ac['authors']
            api_request.meta['entries_per_request'] = self.entries_per_request
            api_request.meta['start'] = 0
            yield api_request
        """

        print("\t### EXIT get_refs PARSER FUNCTION ###")


#process = CrawlerProcess(settings={
#    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
#})

#process.crawl(ArXivSpider, title=title_test, authors=authors_test, references=[])
#process.start()



