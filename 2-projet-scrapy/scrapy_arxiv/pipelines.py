# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
"""
import json
from scrapy.exporters import CsvItemExporter, JsonItemExporter, JsonLinesItemExporter, XmlItemExporter
from scrapy.exceptions import DropItem

BIBLIO_DIR = "/home/pierre/Documents/ECM/3A_bis/nlp-arxiv-ecm/code/intermediary_results/biblio-test"
META_DIR = BIBLIO_DIR + "/articles-metadata"
PDF_DIR = BIBLIO_DIR + "/articles-pdf"
LATEX_DIR = BIBLIO_DIR + "/articles-latex"


class ArticleRetrieverPipeline(object):
    def process_item(self, article, arxiv_spider, title):
        if article['title'] == title:
            return article
        else:
            raise DropItem("Not the right article yet")

class JsonWriterPipeline(object):
    def open_spider(self, article, arxiv_spider):
        self.file = open(META_DIR + '/' + article.identifier + '_meta.json' , 'w')

    def close_spider(self, arxiv_spider):
        self.file.close()

    def process_item(self, article, arxiv_spider):
        line = json.dumps(dict(article)) + "\n"
        self.file.write(line)
        return article
"""








