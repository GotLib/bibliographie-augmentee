# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import numpy as np
import pandas as pd
import unidecode
from regex import findall
from scrapy import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst, Join, Compose


class Article(Item):
    identifier = Field()
    title = Field()
    journal = Field()
    created = Field()
    updated = Field()
    authors = Field()
    #categories = Field()
    abstract = Field()
    #references = Field() # liste des identifiers des articles cités par cet articles (donc antérieurs)
    #citations = Field()  # liste d'identifiers des articles qui citent cet article (donc postérieurs)
    found = Field() # True si on a pu le trouver sur arXiv grâce à l'api ; False si on l'a trouvé dans des citations
                    # ou références d'un autre article mais qu'on n'a pas pu trouver sa propre page sur arxiv.
                    # les articles avec found=False ont leur ligne et leur colonne remplies de -1 dans la matrice d'adjacence,
                    # et ne figurent donc pas dans le graphe.

class ArticleLoader(ItemLoader):

    def group_by_individual(self, values):
        l = int(len(values)/2)
        return [{'keyname': values[2*i].upper(), 'forename': values[2*i+1]} for i in range(l)]

    def split_categories_str(self, values):
        for v in values:
            yield v.split(' ')

    def reformat_abstract(self, values):
        values = "".join(values).replace("\n", " ")
        return "".join(values)

    def get_id_number(self, values):
        return values[0].split("/")[-1].split('v')[0]

    def parse_date(self, values):
        return values[0].split("T")[0]

    def unfold_singletons(self, values):
        if type(values) == list:
            if len(values) == 1:
                return values[0]
            else: 
                return values
        else:
            return values
    
    def sort_authors_without_dup(self, values):
        dup_index = []
        values.sort()
        aut_prev = values[0]
        for i, aut in zip(range(1, len(values)), values):
            aut = values[i]
            if aut.lower() == aut_prev.lower():
                dup_index.append(i-1)
            aut_prev = aut
        for d in dup_index:
            values.pop(d)
        return values  

    def normalize_title(self, values):
        title = values
        if type(title) == list:
            title = title[0] 
        title = title.lower()
        norm_dict = {'à': 'a',
                     'â': 'a',
                     'ä': 'a',
                     'é': 'e',
                     'è': 'e', 
                     'ê': 'e',
                     'ë': 'e',
                     'î': 'i',
                     'ï': 'i',
                     'ô': 'o',
                     'ö': 'o',
                     'ù': 'u',
                     'û': 'u', 
                     'ü': 'u',
                     'ŷ': 'y',
                     'ÿ': 'y',
                     'ç': 'c',
                     '_': ' '
                     }
        #norm_table = str.maketrans(norm_dict)
        #normchar_title = str.translate(title, norm_dict)
        normchar_title = unidecode.unidecode(title)

        split_title = findall(r"[\w]+", normchar_title)
        norm_title = " ".join(split_title).title()
        
        # necessary because underscores actually don't get replaced by whitespaces in normchar_title :
        resplit_title = norm_title.split('_')
        norm_title = " ".join(resplit_title).title()
        return norm_title

    default_output_processor = unfold_singletons

    identifier_in = get_id_number
    identifier_out = TakeFirst()

    title_in = normalize_title

    created_in = parse_date

    updated_in = parse_date

    authors_out = sort_authors_without_dup

    categories_in = split_categories_str
    caegories_out = TakeFirst()

    abstract_in = reformat_abstract


class Biblio:
    
    def __init__(self, loc, corpus=[], adjacency_matrix=None):
        self.loc = loc # chemin vers le dossier contenant la biblio
        self.corpus = corpus # TODO : Décider si corpus contient seulement les id (les objets articles etant stockés ailleurs)
                             # ou carrément les articles eux-mêmes.
        self.adjacency_matrix = adjacency_matrix # un dataframe contenant un 2d_array avec des 0, des 1 et des -1, 
                                                 # indexé par les identifiers des articles de self.corpus (pour les 2 dimensions)
        if self.adjacency_matrix is None:
            self.adjacency_matrix = adjacency_matrix=pd.DataFrame(data=np.empty((0)))
        elif type(self.adjacency_matrix) == type(""): # -> si on a donné le chemin vers une matrice d'adjacence stockée en local:
            self.adjacency_matrix = self.load_matrix(loc + adjacency_matrix)

    def add_article(self, a, edges_from=[], edges_towards=[]):

        if a['identifier'] in self.corpus: # idéalement il faudrait vérifier si des champs de a contiennent des valeurs pas encore dans son homologue 
                             # de self.corpus, et le cas échéant mettre celui-ci à jour

            print("--> a is in the corpus")
            print('a.__dict__ : ' + str(a.__dict__))
            k = np.where(self.adjacency_matrix.index==a['identifier'])

            for e in edges_towards:
                j = np.where(self.adjacency_matrix.columns==e)[0][0]
                self.adjacency_matrix[k, j] = 1
                self.adjacency_matrix[j, k] = -1
            for e in edges_from:
                i = np.where(self.adjacency_matrix.index==e)[0][0]
                self.adjacency_matrix[k, i] = -1
                self.adjacency_matrix[i, k] = 1
            """                
            for l in range(self.adjacency_matrix.shape[0]):
                if self.adjacency_matrix[l, k] == -1:
                    self.adjacency_matrix[l, k] == 0
                if self.adjacency_matrix[k, l] == -1:
                    self.adjacency_matrix[k, l] == 0
            """

        else:
            print("--> a is NOT YET in the corpus.")
            self.corpus.append(a)
            print('self.adjacency_matrix : ' + str(self.adjacency_matrix))
            print('type(self.adjacency_matrix) : ' + str(type(self.adjacency_matrix)))
            print('type(self.adjacency_matrix) : ' + str(type(self.adjacency_matrix.index)))
            try: # on part du principe que la matrice d'adjacence contient déjà des éléments
                new_index = np.stack(self.adjacency_matrix.index, np.array([a['identifier']]))
            except: # si au contraire elle est vide, la ligne ci-dessus provoquera une erreur : le premier array est vide
                new_index = np.array([a['identifier']]) 
            print(new_index)
            print(new_index)
            
            if a['found'] == True:
                print("type of self.adjacency_matrix.index : " + str(type(self.adjacency_matrix.index))) 
                new_row = np.zeros((1, len(self.corpus)-1), dtype=int)
                print('np new_row.shape : ')
                print(new_row.shape)
                new_col = np.zeros((len(self.corpus), 1), dtype=int)
                print('np new_col.shape : ')
                print(new_col.shape)
                for e in edges_towards:
                    j = np.where(self.adjacency_matrix.index==e)[0][0]
                    new_row[0, j] = 1
                    new_col[j, 0] = -1
        
                for e in edges_from:
                    i = np.where(self.adjacency_matrix.index==e)[0][0]
                    new_col[i, 0] = 1
                    new_row[0, i] = -1

                print('type de self.adjacency_matrix.index : ' + str(type(self.adjacency_matrix.index)))
                test = a['identifier']
                new_row = pd.DataFrame(data=new_row, index=[a['identifier']], columns=self.adjacency_matrix.index)
                new_col = pd.DataFrame(data=new_col, index=new_index, columns=[a['identifier']])
                print('pd new_row : ')
                print(new_row)
                print('pd new_col : ')
                print(new_col)

            else:
                new_row = - np.ones((len(self.corpus)-1), dtype=int)
                new_row = pd.DataFrame(data=new_row, index=[a['identifier']], columns=list(self.adjacency_matrix.index))
                
                new_col = - np.ones((len(self.corpus)), dtype=int)
                new_col = pd.DataFrame(data=new_col, index=new_index, columns=[a['identifier']])
                

            bib = pd.concat([self.adjacency_matrix, new_row])
            bib = bib.join(new_col)
            #print(bib)
        
            self.adjacency_matrix = bib

    def save_bib(self, savepath):
        store = pd.HDFStore(savepath)
        store['adj_matrix'] = np.array(self.adjacency_matrix)
        store['corpus'] = np.array(self.corpus)
        store.close()

    def load_bib(self, path):
        try:
            store = pd.HDFStore(path)
            self.adjacency_matrix = store['adj_matrix']
            self.corpus = list(store['corpus'])
            self.close()
        except: 
            print("no bib saved yet at " + path + ".")


if __name__=='__main__':
 
    corpus = [Article(identifier='1', found=True), Article(identifier='2', found=True), Article(identifier='3', found=True), Article(identifier='4', found=True)]  
    index = [a['identifier'] for a in corpus]
    print(index)
    adjacency_matrix=pd.DataFrame(data=np.array([[0,1,0,1], [-1,0,1,0], [0,-1,0,1], [-1,0,-1,0]]), index=index, columns=index)
    bib = Biblio(loc="./", corpus=corpus, adjacency_matrix=adjacency_matrix)
    print('bib.adjacency_matrix:')
    print(bib.adjacency_matrix)

    print('a7: ' )
    print(Article(identifier='7', found=True))

    bib.add_article(Article(identifier='5', found=True), edges_from=['1', '3', '4'], edges_towards=[])
    print(bib.adjacency_matrix)

    bib.add_article(Article(identifier='6', found=True), edges_from=[], edges_towards=['1', '2', '5'])
    print(bib.adjacency_matrix)


