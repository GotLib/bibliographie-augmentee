# encoding : utf8

""" montre les similitudes (cosinus et pdt scalaire) entre les vecteurs de 2 caractères donnés.
    -> plus ou moins comme evaluate_embeddings.py"""


import numpy as np
import pandas as pd
import h5py
from time import time

N_VOCAB = 94

REFS_FILE = "./intermediary_results/refs-dataset/refs-samples-1ter.csv"
REFS_INDEX_LOC = "./intermediary_results/indexEncoded-20refs-allconcat.npz"
ELMO_EMBEDS = "./pre-trained embeddings/elmo_2x4096_512_2048cnn_2xhighway_weights_PubMed_only.hdf5"

ELMO_ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ&~"\\\'(-`_^@)[]{}=*/+#?,.;:§!<>|°\n\t '
ELMO_VOCAB = {'0': 49, '1': 50, '2': 51, '3': 52, '4': 53, '5': 54, '6': 55, '7': 56, '8': 57, '9': 58, 'a': 98, 'b': 99, 'c': 100, 'd': 101, 'e': 102, 'f': 103, 'g': 104, 'h': 105, 'i': 106, 'j': 107, 'k': 108, 'l': 109, 'm': 110, 'n': 111, 'o': 112, 'p': 113, 'q': 114, 'r': 115, 's': 116, 't': 117, 'u': 118, 'v': 119, 'w': 120, 'x': 121, 'y': 122, 'z': 123, 'A': 66, 'B': 67, 'C': 68, 'D': 69, 'E': 70, 'F': 71, 'G': 72, 'H': 73, 'I': 74, 'J': 75, 'K': 76, 'L': 77, 'M': 78, 'N': 79, 'O': 80, 'P': 81, 'Q': 82, 'R': 83, 'S': 84, 'T': 85, 'U': 86, 'V': 87, 'W': 88, 'X': 89, 'Y': 90, 'Z': 91, '&': 39, '~': 127, '"': 35, '\\': 93, "'": 40, '(': 41, '-': 46, '`': 97, '_': 96, '^': 95, '@': 65, ')': 42, '[': 92, ']': 94, '{': 124, '}': 126, '=': 62, '*': 43, '/': 48, '+': 44, '#': 36, '?': 64, ',': 45, '.': 47, ';': 60, ':': 59, '!': 34, '<': 61, '>': 63, '|': 125, '°': 195, '\n': 11, '\t': 10, ' ': 33}
ELMO_REVERSE_VOCAB = {49: '0', 50: '1', 51: '2', 52: '3', 53: '4', 54: '5', 55: '6', 56: '7', 57: '8', 58: '9', 98: 'a', 99: 'b', 100: 'c', 101: 'd', 102: 'e', 103: 'f', 104: 'g', 105: 'h', 106: 'i', 107: 'j', 108: 'k', 109: 'l', 110: 'm', 111: 'n', 112: 'o', 113: 'p', 114: 'q', 115: 'r', 116: 's', 117: 't', 118: 'u', 119: 'v', 120: 'w', 121: 'x', 122: 'y', 123: 'z', 66: 'A', 67: 'B', 68: 'C', 69: 'D', 70: 'E', 71: 'F', 72: 'G', 73: 'H', 74: 'I', 75: 'J', 76: 'K', 77: 'L', 78: 'M', 79: 'N', 80: 'O', 81: 'P', 82: 'Q', 83: 'R', 84: 'S', 85: 'T', 86: 'U', 87: 'V', 88: 'W', 89: 'X', 90: 'Y', 91: 'Z', 39: '&', 127: '~', 35: '"', 93: '\\', 40: "'", 41: '(', 46: '-', 97: '`', 96: '_', 95: '^', 65: '@', 42: ')', 92: '[', 94: ']', 124: '{', 126: '}', 62: '=', 43: '*', 48: '/', 44: '+', 36: '#', 64: '?', 45: ',', 47: '.', 60: ';', 59: ':', 34: '!', 61: '<', 63: '>', 125: '|', 195: '°', 11: '\n', 10: '\t', 33: ' '}


class Chars2vecs:
    
    def __init__(self, embeddings=ELMO_EMBEDS, data=REFS_INDEX_LOC, vocab=ELMO_VOCAB):
        self.vocab = vocab
        f = h5py.File(ELMO_EMBEDS)
        vectors = f['char_embed'][:, :]
        self.embeddings = vectors
        self.data = data


    def convert(self, inp):
        vecs = []
        for char in inp:
            i = self.vocab[char]
            vecs.append(self.embeddings[i, :])
        return np.array(vecs)



    def dot_sim(self,v1, v2):
        """ returns the dot-product similarity of vectors v1 and v2 (1d-arrays)"""
        return np.dot(v1, v2)

    def cosine_sim(self,v1, v2):
        """ returns the cosine similarity between vectors v1 and v2 (1d-arrays)"""
        return np.dot(v1, v2) / math.sqrt(np.dot(v1, v1)*np.dot(v2, v2))



if __name__=='__main__':
    
    c2v = Chars2vecs()
    rep1 = c2v.convert("Ordinary")
    rep2 = c2v.convert("[1] I. Sutskever,")
    
    #print('rep1: ')
    #print(rep1)
    #print('rep2: ')
    #print(rep2)


    for v1, v2 in zip(rep1[:-1], rep1[1:]):
        print(c2v.dot_sim(v1, v2))

    print('\n\n\n')

    for v1, v2 in zip(rep2[:-1], rep2[1:]):
        print(c2v.dot_sim(v1, v2))

    print('\n\n\n')

    for v1, v2 in zip(rep1[:5], rep2[:5]):
        print(c2v.dot_sim(v1, v2))













