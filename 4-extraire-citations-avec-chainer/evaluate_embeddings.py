# encoding: utf-8

""" montre les similitudes (cosinus et pdt scalaire) entre les vecteurs de 2 caractères donnés.
    -> plus ou moins comme chars2vec.py"""

import numpy as np
import pandas as pd
import math
import h5py

TRIAL = 9
EMBEDDINGS_LOC = "./intermediary_results/1 - trained models/trial-" + str(TRIAL) + "/context2vec.targets_embeddings"

REFS_FILE = "./intermediary_results/refs-dataset/refs-samples-1ter.csv"
TRAIN_FULLTEXT_LOC = "./intermediary_results/indexEncoded-fulltext-allconcat.npz"
TRAIN_REFS_LOC = "./intermediary_results/indexEncoded-20refs-allconcat.npz"


ELMO = "./pre-trained embeddings/elmo_2x4096_512_2048cnn_2xhighway_weights_PubMed_only.hdf5"
ELMO_ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ&~"\\\'(-`_^@)[]{}=*/+#?,.;:§!<>|°\n\t '
ELMO_VOCAB = {'0': 49, '1': 50, '2': 51, '3': 52, '4': 53, '5': 54, '6': 55, '7': 56, '8': 57, '9': 58, 'a': 98, 'b': 99, 'c': 100, 'd': 101, 'e': 102, 'f': 103, 'g': 104, 'h': 105, 'i': 106, 'j': 107, 'k': 108, 'l': 109, 'm': 110, 'n': 111, 'o': 112, 'p': 113, 'q': 114, 'r': 115, 's': 116, 't': 117, 'u': 118, 'v': 119, 'w': 120, 'x': 121, 'y': 122, 'z': 123, 'A': 66, 'B': 67, 'C': 68, 'D': 69, 'E': 70, 'F': 71, 'G': 72, 'H': 73, 'I': 74, 'J': 75, 'K': 76, 'L': 77, 'M': 78, 'N': 79, 'O': 80, 'P': 81, 'Q': 82, 'R': 83, 'S': 84, 'T': 85, 'U': 86, 'V': 87, 'W': 88, 'X': 89, 'Y': 90, 'Z': 91, '&': 39, '~': 127, '"': 35, '\\': 93, "'": 40, '(': 41, '-': 46, '`': 97, '_': 96, '^': 95, '@': 65, ')': 42, '[': 92, ']': 94, '{': 124, '}': 126, '=': 62, '*': 43, '/': 48, '+': 44, '#': 36, '?': 64, ',': 45, '.': 47, ';': 60, ':': 59, '!': 34, '<': 61, '>': 63, '|': 125, '°': 195, '\n': 11, '\t': 10, ' ': 33}
ELMO_REVERSE_VOCAB = {49: '0', 50: '1', 51: '2', 52: '3', 53: '4', 54: '5', 55: '6', 56: '7', 57: '8', 58: '9', 98: 'a', 99: 'b', 100: 'c', 101: 'd', 102: 'e', 103: 'f', 104: 'g', 105: 'h', 106: 'i', 107: 'j', 108: 'k', 109: 'l', 110: 'm', 111: 'n', 112: 'o', 113: 'p', 114: 'q', 115: 'r', 116: 's', 117: 't', 118: 'u', 119: 'v', 120: 'w', 121: 'x', 122: 'y', 123: 'z', 66: 'A', 67: 'B', 68: 'C', 69: 'D', 70: 'E', 71: 'F', 72: 'G', 73: 'H', 74: 'I', 75: 'J', 76: 'K', 77: 'L', 78: 'M', 79: 'N', 80: 'O', 81: 'P', 82: 'Q', 83: 'R', 84: 'S', 85: 'T', 86: 'U', 87: 'V', 88: 'W', 89: 'X', 90: 'Y', 91: 'Z', 39: '&', 127: '~', 35: '"', 93: '\\', 40: "'", 41: '(', 46: '-', 97: '`', 96: '_', 95: '^', 65: '@', 42: ')', 92: '[', 94: ']', 124: '{', 126: '}', 62: '=', 43: '*', 48: '/', 44: '+', 36: '#', 64: '?', 45: ',', 47: '.', 60: ';', 59: ':', 34: '!', 61: '<', 63: '>', 125: '|', 195: '°', 11: '\n', 10: '\t', 33: ' '}


ALPHABET = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^',
             '_', '`', '{', '|', '}', '~', ' ', '\t', '\n', '\r', '\x0b', '\x0c', '\n', ' ', '\t', '\r']
VOCAB = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
         'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24,
         'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35,
         'A': 36, 'B': 37, 'C': 38, 'D': 39, 'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49, 'O': 50,
         'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59, 'Y': 60, 'Z': 61,
         '!': 62, '"': 63, '#': 64, '$': 65, '%': 66, '&': 67, "'": 68, '(': 69, ')': 70, '*': 71, '+': 72, ',': 73, '-': 74, '.': 75,
         '/': 76, ':': 77, ';': 78, '<': 79, '=': 80, '>': 81, '?': 82, '@': 83, '[': 84, '\\': 85, ']': 86, '^': 87, '_': 88, '`': 89,
         '{': 90, '|': 91, '}': 92, '~': 93, ' ': 101, '\t': 102, '\n': 100, '\r': 103, '\x0b': 98, '\x0c': 99}
REVERSE_VOCAB = {0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9',
                10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f', 16: 'g', 17: 'h', 18: 'i', 19: 'j', 20: 'k', 21: 'l', 22: 'm', 23: 'n',
                24: 'o', 25: 'p', 26: 'q', 27: 'r', 28: 's', 29: 't', 30: 'u', 31: 'v', 32: 'w', 33: 'x', 34: 'y', 35: 'z',
                36: 'A', 37: 'B', 38: 'C', 39: 'D', 40: 'E', 41: 'F', 42: 'G', 43: 'H', 44: 'I', 45: 'J', 46: 'K', 47: 'L', 48: 'M', 49: 'N',
                50: 'O', 51: 'P', 52: 'Q', 53: 'R', 54: 'S', 55: 'T', 56: 'U', 57: 'V', 58: 'W', 59: 'X', 60: 'Y', 61: 'Z',
                62: '!', 63: '"', 64: '#', 65: '$', 66: '%', 67: '&', 68: "'", 69: '(', 70: ')', 71: '*', 72: '+', 73: ',', 74: '-', 75: '.',
                76: '/', 77: ':', 78: ';', 79: '<', 80: '=', 81: '>', 82: '?', 83: '@', 84: '[', 85: '\\', 86: ']', 87: '^', 88: '_', 89: '`',
                90: '{', 91: '|', 92: '}', 93: '~', 94: ' ', 95: '\t', 96: '\n', 97: '\r', 98: '\x0b', 99: '\x0c', 100: '\n', 101: ' ',
                102: '\t', 103: '\r'}


def get_vector_from_txt(char, loc=EMBEDDINGS_LOC, vocab=VOCAB):
    """Returns the embedding vector of the chararcter 'char' :
       - gets the index of char from vocab,
       - gets the right line in the file at location 'loc',
       - then converts it in numpy 1d-array format.
    """
    i = vocab[char]
    with open(loc, 'r') as ef:
        lines = ef.readlines()
        i_line = lines[5 + 2*i]
        vector = np.array(i_line.split(' ')[1:], dtype=float)
        return vector

def get_vector_from_hdf5(char, loc=ELMO, vocab=ELMO_VOCAB):
    """same thing, but from the hdf5 file with elmo embeddings"""
    i = vocab[char]
    f = h5py.File(loc)
    vectors = f['char_embed'][:, :]
    return vectors[i, :]


def dot_sim(v1, v2):
    """ returns the dot-product similarity of vectors v1 and v2 (1d-arrays)"""
    return np.dot(v1, v2)

def cosine_sim(v1, v2):
    """ returns the cosine similarity between vectors v1 and v2 (1d-arrays)"""
    return np.dot(v1, v2) / math.sqrt(np.dot(v1, v1)*np.dot(v2, v2))


if __name__=='__main__':

    get_vector = get_vector_from_hdf5

    """
    va = get_vector('a')
    vb = get_vector('b')
    vc = get_vector('c')
    vd = get_vector('d')
    vo = get_vector('o')
    ve = get_vector('e')
    vx = get_vector('x')

    v0 = get_vector('0')
    v1 = get_vector('1')
    v2 = get_vector('2')
    v5 = get_vector('5')
    v_slash = get_vector('/')
    v_dot = get_vector('.')
    v_tilde = get_vector('~')

    sim = cosine_sim
    sim = dot_sim

    print(sim(va, ve))
    print(sim(va, vo))
    print(sim(ve, vo))
    print(sim(vc, vd))
    print(sim(vb, vc))
    print("\n")

    print(sim(v0, v1))
    print(sim(v0, v5))
    print(sim(v2, v5))
    print("\n")

    print(sim(va, vc))
    print(sim(vo, vd))
    print(sim(ve, vx))
    print('\n')
    
    print(sim(v1, vc))
    print(sim(va, v1))
    print(sim(vb, v2)) 
    print(sim(va, v_tilde))
    print(sim(va, v_dot)) 
    """

    """
    print(va)
    print(vb)
    print("\n")
 
    print(v1)
    print(v2)
    print("\n")

    print(v_slash)
    print(v_dot)
    print(v_dot)
    print("\n")
    """






