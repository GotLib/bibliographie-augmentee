# encoding : utf8


"""ce script servait uniquement à récupérer la correspondance index-caractère utilisée
   pour les vecteurs elmo pré-entrainés de cette librairie (allennlp). """

from allennlp.data.token_indexers.elmo_indexer import ELMoCharacterMapper

alphabet='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ&~"\\\'(-`_^@)[]{}=*/+#?,.;:!<>|°\n\t '
print(len(alphabet))
ecm = ELMoCharacterMapper()
elmo_char2index = {}
elmo_index2char = {}
for char in alphabet:
    print('char:' + char)
    elmo_char2index[char] = ecm.convert_word_to_char_ids(char)[1]
    print('ecm.convert_to_ids' + str(elmo_char2index[char]))
    elmo_index2char[elmo_char2index[char]] = char
    


print(len(elmo_char2index.keys()))
print(elmo_char2index)
print(len(elmo_index2char.keys()))
print(elmo_index2char)





