"""Sample script of word embedding model.

This code implements skip-gram model and continuous-bow model.
"""
from __future__ import division

import logging

import argparse
import collections

import numpy as np
import six

import chainer
from chainer.backends import cuda

import chainer.initializers as I
import chainer.optimizers as O
import chainer.functions as F
import chainer.links as L

from chainer import reporter
from chainer import training
from chainer.training import extensions

from time import time


ALPHABET = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
            '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^',
             '_', '`', '{', '|', '}', '~', ' ', '\t', '\n', '\r', '\x0b', '\x0c', '\n', ' ', '\t', '\r']

VOCAB = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 
         'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24,
         'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35,
         'A': 36, 'B': 37, 'C': 38, 'D': 39, 'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49, 'O': 50,
         'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59, 'Y': 60, 'Z': 61,
         '!': 62, '"': 63, '#': 64, '$': 65, '%': 66, '&': 67, "'": 68, '(': 69, ')': 70, '*': 71, '+': 72, ',': 73, '-': 74, '.': 75,
         '/': 76, ':': 77, ';': 78, '<': 79, '=': 80, '>': 81, '?': 82, '@': 83, '[': 84, '\\': 85, ']': 86, '^': 87, '_': 88, '`': 89, 
         '{': 90, '|': 91, '}': 92, '~': 93, ' ': 101, '\t': 102, '\n': 100, '\r': 103, '\x0b': 98, '\x0c': 99}

REVERSE_VOCAB = {0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9',
                10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f', 16: 'g', 17: 'h', 18: 'i', 19: 'j', 20: 'k', 21: 'l', 22: 'm', 23: 'n',
                24: 'o', 25: 'p', 26: 'q', 27: 'r', 28: 's', 29: 't', 30: 'u', 31: 'v', 32: 'w', 33: 'x', 34: 'y', 35: 'z', 
                36: 'A', 37: 'B', 38: 'C', 39: 'D', 40: 'E', 41: 'F', 42: 'G', 43: 'H', 44: 'I', 45: 'J', 46: 'K', 47: 'L', 48: 'M', 49: 'N', 
                50: 'O', 51: 'P', 52: 'Q', 53: 'R', 54: 'S', 55: 'T', 56: 'U', 57: 'V', 58: 'W', 59: 'X', 60: 'Y', 61: 'Z',
                62: '!', 63: '"', 64: '#', 65: '$', 66: '%', 67: '&', 68: "'", 69: '(', 70: ')', 71: '*', 72: '+', 73: ',', 74: '-', 75: '.', 
                76: '/', 77: ':', 78: ';', 79: '<', 80: '=', 81: '>', 82: '?', 83: '@', 84: '[', 85: '\\', 86: ']', 87: '^', 88: '_', 89: '`', 
                90: '{', 91: '|', 92: '}', 93: '~', 94: ' ', 95: '\t', 96: '\n', 97: '\r', 98: '\x0b', 99: '\x0c', 100: '\n', 101: ' ', 
                102: '\t', 103: '\r'}

fulltext="./intermediary_results/indexEncoded-fulltext-allconcat.npz"
refs_only="./intermediary_results/indexEncoded-20refs-allconcat.npz"
TRAIN_DATA_LOC = refs_only

"""
from trial 3 to 5 : training on full text, but very bad embeddings (similarity ~= 1 whatever the chars) 
    -> too large batchsize (64) ? not enough parameters (10-20 units)? not enough data ?
from trial 6 : (can't keep starting on top of trial T-1 = 5, because of different shapes)
training on refs only,with smaller batchsize (16) and with more params (but less data : same as above with only references parts)
trial 7 : contrastive loss while always tend toward the same value for all embeddings : start from very different values ?
from trial 10 : citation_parser_v3
"""
TRIAL = 10
SAVED_MODELS_LOC = "./intermediary_results/1 - trained models/trial-" + str(TRIAL) + '/'
MODEL_TO_LOAD = "./intermediary_results/1 - trained models/trial-" + str(TRIAL-1) +  "/model_iter_313" # total : 4 epochs at trial-5/model_iter_7292

SLICE_SIZE = 10000
EPOCHS = 10
BATCHSIZE = 8
WINDOW= 5
BPROP_LEN = 30
GRADCLIP = 5

N_VOCAB = 105

CTX_EMBEDDINGS_UNITS = 82

TARGET_EMBEDDINGS_UNITS = 82
LSTM_UNITS = [CTX_EMBEDDINGS_UNITS, TARGET_EMBEDDINGS_UNITS / 2]

MLPCTX_UNITS = [109, 231, 107]
MLPTRG_UNITS = [71, 103, 157, 105] 

N_UNITS = 0

TEST = False



class Context2vec(chainer.Chain):
    """from the 2016 context2vec paper"""

    def __init__(self, n_vocab, n_units, loss_func, input_ctx_embeddings_units=INPUT_CTX_EMBEDDINGS_UNITS,
                       output_ctx_embeddings_units=OUTPUT_CTX_EMBEDDINGS_UNITS, target_embeddings_units=TARGET_EMBEDDINGS_UNITS,
                       lstm_units=LSTM_UNITS, mlpctx_units=MLPCTX_UNITS, mlptrg_units=MLPTRG_UNITS, window_size=WINDOW):
        super(Context2vec, self).__init__()
        self.window_size = window_size
        with self.init_scope():
            self.target_embed = L.EmbedID(n_vocab, target_embeddings_units, initialW=I.Uniform(10000. / target_embeddings_units))
            self.mlptrg_l1 = L.Linear(in_size=target_embeddings_units, out_size=mlptrg_units[0])
            self.mlptrg_l2 = L.Linear(in_size=mlptrg_units[0], out_size=mlptrg_units[1])
            self.mlptrg_l3 = L.Linear(in_size=mlptrg_units[1], out_size=mlptrg_units[2])
            self.mlptrg_l4 = L.Linear(in_size=mlptrg_units[2], out_size=mlptrg_units[3])
            self.mlptrg_lo = L.Linear(in_size=mlptrg_units[3], out_size=target_embeddings_units)

            self.input_ctx_embed = L.EmbedID(n_vocab, input_ctx_embeddings_units, initialW=I.Uniform(10000. / input_ctx_embeddings_units))
            self.forward_lstm = L.NStepLSTM(in_size=input_ctx_embeddings_units, out_size=lstm_units, n_layers=1, dropout=0.4)
            self.backward_lstm = L.NStepLSTM(in_size=input_ctx_embeddings_units, out_size=lstm_units, n_layers=1, dropout=0.4)
            self.mlpctx_l1 = L.Linear(in_size=2*lstm_units, out_size=mlpctx_hidden_units, initialW=I.Uniform(1. / (2*lstm_units)))
            self.mlpctx_lo = L.Linear(in_size=mlpctx_units[0], out_size=mlpctx_output_ctx_embeddings_units, initialW=I.Uniform(100. / mlpctx_units))
            self.output_ctx_embed= L.Linear(mlpctx_output_units, output_ctx_embeddings_units, initialW=I.Uniform(100. / output_ctx_embeddings_units))
            self.loss_func = loss_func
            
    def forward(self, x, contexts):
        try:
            assert x.shape[0] == contexts.shape[0], "batchsize of target words != batchsize of contexts words : \n" + str(x.shape[0]) + '!=' + str(contexts.shape[0])
        except:
            return chainer.Variable(np.zeros(1))        

        embed_target = self.target_embed(x)
        mlptrg_h1 = F.dropout(F.relu(self.mlptrg_l1(embed_target)), ratio=0.4)
        mlptrg_h2 = F.dropout(F.relu(self.mlptrg_l2(embed_target)), ratio=0.4)
        mlptrg_h3 = F.dropout(F.relu(self.mlptrg_l3(embed_target)), ratio=0.4)
        mlptrg_h4 = F.dropout(F.relu(self.mlptrg_l4(embed_target)), ratio=0.4)
        mlptrg_o = F.dropout(F.softmax(self.mlptrg_lo(embed_target)), ratio=0.4)
        

        embed_words_contexts = self.input_ctx_embed(contexts)

        pre_hy, pre_cy, pre_ys = self.forward_lstm(hx=None, cx=None, xs=list(embed_words_contexts[:, :int(embed_words_contexts.shape[1]/2) ,:]))   
        post_hy, post_cy, post_ys = self.backward_lstm(hx=None, cx=None, xs=list(embed_words_contexts[:, int(embed_words_contexts.shape[1]/2):, :]))
        pre_context = np.hstack([v.data for v in pre_hy])
        post_context = np.hstack([v.data for v in post_hy])

        full_context = F.concat((np.array(pre_context), np.array(post_context)), axis=1)

        mlpctx_h = F.dropout(F.relu(self.mlpctx_hidden(full_context)), ratio=0.25)
        mlpctx_o = F.dropout(F.tanh(self.mlpctx_output(mlpctx_h)), ratio=0.25)
        
        sentential_context = F.softmax(self.output_ctx_embed(mlp_o))
        
        true_context = 
        true_target = 
        loss = self.loss_func(sentential_context, embed_target)

        return loss


class SoftmaxCrossEntropyLoss(chainer.Chain):
    """Softmax cross entropy loss function preceded by linear transformation.

    """

    def __init__(self, n_in, n_out):
        super(SoftmaxCrossEntropyLoss, self).__init__()
        with self.init_scope():
            self.out = L.Linear(n_in, n_out, initialW=0)

    def forward(self, x, t):
        return F.softmax_cross_entropy(self.out(x), t)


class WindowIterator(chainer.dataset.Iterator):
    """Dataset iterator to create a batch of sequences at different positions.

    This iterator returns a pair of the current words and the context words.
    """

    def __init__(self, dataset, window=WINDOW, batch_size=BATCHSIZE, repeat=True):
        self.dataset = np.array(dataset, np.int32)
        self.window = window  # size of context window
        self.batch_size = batch_size
        self._repeat = repeat
        # order is the array which is shuffled ``[window, window + 1, ...,
        # len(dataset) - window - 1]``
        self.order = np.random.permutation(
            len(dataset) - window * 2).astype(np.int32)
        self.order += window
        self.current_position = 0
        # Number of completed sweeps over the dataset. In this case, it is
        # incremented if every word is visited at least once after the last
        # increment.
        self.epoch = 0
        # True if the epoch is incremented at the last iteration.
        self.is_new_epoch = False

    def __next__(self):
        """This iterator returns a list representing a mini-batch.

        Each item indicates a different position in the original sequence.
        """
        if not self._repeat and self.epoch > 0:
            raise StopIteration

        i = self.current_position
        i_end = i + self.batch_size
        position = self.order[i:i_end]
        w = np.random.randint(self.window - 1) + 1
        offset = np.concatenate([np.arange(-w, 0), np.arange(1, w + 1)])
        pos = position[:, None] + offset[None, :]
        contexts = self.dataset.take(pos)
        center = self.dataset.take(position)
        
        if i_end >= len(self.order):
            np.random.shuffle(self.order)
            self.epoch += 1
            self.is_new_epoch = True
            self.current_position = 0
        else:
            self.is_new_epoch = False
            self.current_position = i_end
        #print(contexts.shape)
        return center, contexts

    @property
    def epoch_detail(self):
        return self.epoch + float(self.current_position) / len(self.order)

    def serialize(self, serializer):
        self.current_position = serializer('current_position',
                                           self.current_position)
        self.epoch = serializer('epoch', self.epoch)
        self.is_new_epoch = serializer('is_new_epoch', self.is_new_epoch)
        if self.order is not None:
            serializer('order', self.order)


class BPTTUpdater(training.updaters.StandardUpdater):

    def __init__(self, train_iter, optimizer, converter, device, bprop_len):
        super(BPTTUpdater, self).__init__(iterator=train_iter, optimizer=optimizer, converter=converter, device=device)
        self.bprop_len = bprop_len
        self.iterator = train_iter
        self.optimizer = optimizer
        self.device = device
        self.converter = converter

    # The core part of the update routine can be customized by overriding.
    def update_core(self):
        loss = 0
        # When we pass one iterator and optimizer to StandardUpdater.__init__,
        # they are automatically named 'main'.
        train_iter = self.get_iterator('main')
        optimizer = self.get_optimizer('main')

        # Progress the dataset iterator for bprop_len words at each iteration.
        for i in range(self.bprop_len):
            # Get the next batch (a list of tuples of two word IDs)
            batch = train_iter.__next__()

            #logging.debug("batch len : " + str(len(batch)) + ".")

            # Concatenate the word IDs to matrices and send them to the device
            # self.converter does this job
            # (it is chainer.dataset.concat_examples by default)
            x, t = self.converter(batch, self.device)

            #logging.debug("x.shape : " + str(x.shape) + " ; t.shape : " + str(t.shape) + ".")

            # Compute the loss at this time step and accumulate it
            try:
                loss += optimizer.target(chainer.Variable(x), chainer.Variable(t))
            except:
                print("Error while computing the loss, at epoch " + str(self.iterator.epoch_detail))
                loss += chainer.Variable(np.zeros(1))

        loss += optimizer.target(chainer.Variable(x), chainer.Variable(t))

        optimizer.target.cleargrads()  # Clear the parameter gradients
        loss.backward()  # Backprop
        loss.unchain_backward()  # Truncate the graph
        optimizer.update()  # Update the parameters


class serialize_model(chainer.training.Extension):

    def __init__(self, model, out=SAVED_MODELS_LOC, compression=False, trigger=(1, 'iteration'), priority=100, name=None):
        super(serialize_model, self).__init__()
        self.trigger = trigger
        self.priority = priority
        self.name = name
        self.model = model
        self.out = out
        self.compression = compression

    def __call__(trainer, model, out=SAVED_MODELS_LOC, compression=False):
        chainer.serializers.save_npz(out+"predictor_last-iter", model, compression)



def convert(batch, device):
    center, contexts = batch
    if device >= 0:
        center = cuda.to_gpu(center)
        contexts = cuda.to_gpu(contexts)
    return center, contexts


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', '-g', default=-1, type=int,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--unit', '-u', default=N_UNITS, type=int,
                        help='number of units')
    parser.add_argument('--window', '-w', default=WINDOW, type=int,
                        help='window size')
    parser.add_argument('--batchsize', '-b', type=int, default=BATCHSIZE,
                        help='learning minibatch size')
    parser.add_argument('--epoch', '-e', default=EPOCHS, type=int,
                        help='number of epochs to learn')
    parser.add_argument('--model', '-m', choices=['skipgram', 'cbow', 'context2vec'],
                        default='context2vec',
                        help='model type ("skipgram", "cbow", "context2vec")')
    parser.add_argument('--negative-size', default=5, type=int,
                        help='number of negative samples')
    parser.add_argument('--out-type', '-o', choices=['hsm', 'ns', 'original'],
                        default='original',
                        help='output model type ("hsm": hierarchical softmax, '
                        '"ns": negative sampling, "original": '
                        'no approximation)')
    parser.add_argument('--out', default=SAVED_MODELS_LOC,
                        help='Directory to output the result')
    parser.add_argument('--test', dest='test', action='store_true')
    parser.set_defaults(test=False)
    args = parser.parse_args()

    if args.gpu >= 0:
        chainer.backends.cuda.get_device_from_id(args.gpu).use()
        cuda.check_cuda_available()

    print('GPU: {}'.format(args.gpu))
    print('# unit: {}'.format(args.unit))
    print('Window: {}'.format(args.window))
    print('Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    print('Training model: {}'.format(args.model))
    print('Output type: {}'.format(args.out_type))
    print('')

    # Load the dataset
    slice_size = SLICE_SIZE
    data = np.load(TRAIN_DATA_LOC)['arr_0']
    print("train size : " + str(len(data)))
 
    train_start = 0
    train_end = 25
    train = data[slice_size*train_start:slice_size*train_end]

    val_start = 25
    val_end = 26
    val = np.load(TRAIN_DATA_LOC)['arr_0'][slice_size*val_start:slice_size*val_end]

    counts = collections.Counter(train)
    counts.update(collections.Counter(val))
    n_vocab = N_VOCAB #max(train) + 3

    if args.test:
        train = train[:100]
        val = val[:100]

    vocab = ALPHABET
    word2index = VOCAB
    index2word = REVERSE_VOCAB

    print('n_vocab: %d' % n_vocab)
    print('data length: %d' % len(train))

    if args.out_type == 'hsm':
        HSM = L.BinaryHierarchicalSoftmax
        tree = HSM.create_huffman_tree(counts)
        loss_func = HSM(args.unit, tree)
        loss_func.W.data[...] = 0
    elif args.out_type == 'ns':
        cs = [counts[w] for w in range(len(counts))]
        loss_func = L.NegativeSampling(args.unit, cs, args.negative_size)
        loss_func.W.data[...] = 0
    elif args.out_type == 'original':
        loss_func = SoftmaxCrossEntropyLoss(args.unit, n_vocab)
    else:
        raise Exception('Unknown output type: {}'.format(args.out_type))

    # Choose the model
    if args.model == 'skipgram':
        model = SkipGram(n_vocab, args.unit, loss_func)
    elif args.model == 'cbow':
        model = ContinuousBoW(n_vocab, args.unit, loss_func)
    elif args.model == 'context2vec':
        model = Context2vec(n_vocab, args.unit, loss_func)
        #model = L.Classifier(model, lossfun=F.contrastive)
    else:
        raise Exception('Unknown model type: {}'.format(args.model))

    if args.gpu >= 0:
        model.to_gpu()

    #chainer.serializers.load_npz(file=MODEL_TO_LOAD, obj=model)

    # Set up an optimizer
    optimizer = O.Adam()
    optimizer.setup(model)

    # Set up an iterator
    train_iter = WindowIterator(train, args.window, args.batchsize)
    #val_iter = WindowIterator(val, args.window, args.batchsize, repeat=False)

    # Set up an updater
    updater = BPTTUpdater(train_iter, optimizer, converter=convert, device=args.gpu, bprop_len=BPROP_LEN)

    # Set up a trainer
    trainer = training.Trainer(updater, (EPOCHS, 'epoch'), out=args.out)

    #interval = 10 if TEST else 10
    trainer.extend(serialize_model(model=model, out=SAVED_MODELS_LOC, compression=False))
    #trainer.extend(extensions.Evaluator(
        #val_iter, model, converter=convert, device=args.gpu))
    trainer.extend(extensions.LogReport())#, trigger=(interval, 'iteration'))
    trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'main/accuracy', 'elapsed_time']))#, 
                                           #'validation/main/accuracy', 'validation/main/loss']))#, trigger=(interval, 'iteration'))
    trainer.extend(extensions.ProgressBar())
    if extensions.PlotReport.available():
        trainer.extend(extensions.PlotReport(['main/loss'], 'epoch', file_name='loss.png'))#, trigger=(interval, 'iteration'))
    trainer.extend(extensions.snapshot())
    trainer.extend(extensions.snapshot_object(model, 'model_iter_{.updater.iteration}'))

    trainer.run()

    # Save the embeddings for the target words
    with open(SAVED_MODELS_LOC + 'context2vec.targets_embeddings', 'a') as f:
        f.write("\n\n\n\n***\t" + str(time()) + '%d %d\n' % (len(VOCAB), args.unit))
        w = cuda.to_cpu(model.target_embed.W.data)
        for i, wi in enumerate(w):
            v = ' '.join(map(str, wi))
            if i in VOCAB.values():
                f.write(str(i) + "<->" + "'%s' %s\n\n" % (REVERSE_VOCAB[i], v))

    with open(SAVED_MODELS_LOC + 'context2vec.ctx_in', 'a') as f:
        f.write("\n\n\n\n***\t" + str(time()) + '%d %d\n' % (len(VOCAB), args.unit))
        w = cuda.to_cpu(model.input_ctx_embed.W.data)
        f.write('W:')
        for wi in w:
            f.write(str(wi))

    with open(SAVED_MODELS_LOC + 'context2vec.ctx_out', 'a') as f:
        f.write("\n\n\n\n***\t" + str(time()) + '%d %d\n' % (len(VOCAB), args.unit))
        w = cuda.to_cpu(model.output_ctx_embed.W.data)
        b = cuda.to_cpu(model.output_ctx_embed.b.data)
        f.write('W:')
        for wi in w:
            f.write(str(wi))
        f.write('\nb:')
        for bi in b:
            f.write(str(bi))
        f.write('\n')


if __name__ == '__main__':

    t1 = time()

    logging.basicConfig(filename='./logs/debug_citation_parser_v2.log',level=logging.DEBUG)
    logging.info("\n")

    main()

    t2 = time()
    print("time of execution : " + str(t2-t1) + " seconds.")



