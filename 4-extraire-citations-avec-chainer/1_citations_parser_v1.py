from __future__ import division

import logging

import numpy as np
import pandas as pd
from time import time

import argparse 
import collections 
 
import chainer 
from chainer.backends import cuda 
import chainer.functions as F 
import chainer.initializers as I 
import chainer.optimizers as O 
import chainer.links as L 
from chainer.links import EmbedID, Convolution1D, LSTM, Linear

from chainer import reporter 
from chainer import training 
from chainer.training import extensions 
 

t1 = time()


TRAIN_DATA_LOC = "./intermediary_results/indexEncoded-fulltext-allconcat.npz"

TRIAL = "trial-2/"
SAVED_MODELS_LOC = "./intermediary_results/1 - trained models/" + TRIAL
MODEL_TO_LOAD = "./intermediary_results/1 - trained models/trial-1/predictor_last-iter"

LEN_SEQS = 1 # length of sequences of chars returned by iterator in the (data_samples, corresponding_targets) 
               # couples (1 for non-conv 1st layers).
BATCHSIZE = 20
WINDOW=20
BPROP_LEN = 5
GRADCLIP = 5

N_VOCAB = 102
N_EMBEDDINGS = 90

TEST = False


class CharCNN(chainer.Chain):
    
    def __init__(self, n_vocab=N_VOCAB, n_units=N_EMBEDDINGS, in1_channels=None, out1_channels=16):
        super(CharCNN, self).__init__()
        with self.init_scope():
            self.embed = EmbedID(n_vocab, n_units)#, initialW=I.Uniform(1. / n_units))
            self.l1a = Convolution1D(in_channels=in1_channels, out_channels=out1_channels, ksize=(7), pad=3)
            self.l1b = Convolution1D(in_channels=in1_channels, out_channels=out1_channels, ksize=(13), pad=6)
            self.l1c = Convolution1D(in_channels=in1_channels, out_channels=out1_channels, ksize=(21), pad=10)
            
    def forward(self, x):
        print('x : ' +str(x.shape))

        e = self.embed(x)
        print('e : ' + str(e.data.shape))

        l1ao = F.tanh(self.l1a(e))
        print('l1ao : ' + str(l1ao.data.shape))
        l1bo = F.tanh(self.l1b(e))
        print('l1bo : ' + str(l1bo.data.shape))
        l1co = F.tanh(self.l1c(e))
        print('l1co : ' + str(l1co.data.shape))

        mp1a = F.max_pooling_1d(l1ao, ksize=90)
        print('mp1a : ' + str(mp1a.data.shape))
        mp1b = F.max_pooling_1d(l1bo, ksize=90)
        print('mp1b : ' + str(mp1b.data.shape))
        mp1c = F.max_pooling_1d(l1co, ksize=90)
        print('mp1c : ' + str(mp1c.data.shape))
   
        concat = F.concat((mp1a, mp1b, mp1c), axis=1)
        
        return concat

class CharRNNLM(chainer.Chain):

    def __init__(self, n_vocab=N_VOCAB, n_units=N_EMBEDDINGS):
        super(CharRNNLM, self).__init__()
        with self.init_scope():
            self.embed = L.EmbedID(n_vocab, n_units)
            self.l1 = L.LSTM(n_units, n_units)
            self.l2 = L.LSTM(n_units, n_units)
            self.l3 = L.Linear(n_units, n_vocab)

        for param in self.params():
            param.data[...] = np.random.uniform(-0.1, 0.1, param.data.shape)

    def reset_state(self):
        self.l1.reset_state()
        self.l2.reset_state()

    def forward(self, x):
        print('x : ' +str(x.shape))

        h0 = self.embed(x)
        print('h0 : ' + str(h0.shape))

        h1 = self.l1(F.dropout(h0))
        print('h1 : ' + str(h1.shape))

        h2 = self.l2(F.dropout(h1))
        print('h2 : ' + str(h2.shape))

        y = self.l3(F.dropout(h2))
        print('y : ' + str(y.shape))

        return y


class ParallelSequentialIterator(chainer.dataset.Iterator):

    def __init__(self, dataset, batch_size, repeat=True, len_seqs=LEN_SEQS):
        self.dataset = dataset
        self.batch_size = batch_size  # batch size
        # Number of completed sweeps over the dataset. In this case, it is
        # incremented if every word is visited at least once after the last
        # increment.
        self.epoch = 0
        # True if the epoch is incremented at the last iteration.
        self.is_new_epoch = False
        self.repeat = repeat
        length = len(dataset)
        # Offsets maintain the position of each sequence in the mini-batch.
        self.offsets = [i * length // batch_size for i in range(batch_size)]
        # NOTE: this is not a count of parameter updates. It is just a count of
        # calls of ``__next__``.
        self.iteration = 0
        # use -1 instead of None internally
        self._previous_epoch_detail = -1.
        
        self.len_seqs = len_seqs
        

    def __next__(self):
        # This iterator returns a list representing a mini-batch. Each item
        # indicates a different position in the original sequence. Each item is
        # represented by a pair of two word IDs. The first word is at the
        # "current" position, while the second word at the next position.
        # At each iteration, the iteration count is incremented, which pushes
        # forward the "current" position.
        length = len(self.dataset)
        if not self.repeat and self.iteration * self.batch_size >= length:
            # If not self.repeat, this iterator stops at the end of the first
            # epoch (i.e., when all words are visited once).
            raise StopIteration
        cur_chars = self.get_chars()
        self._previous_epoch_detail = self.epoch_detail
        self.iteration += 1
        next_chars = self.get_chars()

        epoch = self.iteration * self.batch_size * self.len_seqs // length
        self.is_new_epoch = self.epoch < epoch
        if self.is_new_epoch:
            self.epoch = epoch

        return list(zip(cur_chars, next_chars))

    @property
    def epoch_detail(self):
        # Floating point version of epoch.
        return self.iteration * self.batch_size / len(self.dataset)

    @property
    def previous_epoch_detail(self):
        if self._previous_epoch_detail < 0:
            return None
        return self._previous_epoch_detail

    def get_chars(self):
        # It returns a list of current words.
        batch = [self.dataset[(offset + (self.iteration * self.len_seqs)) % len(self.dataset):
                             (offset + ((self.iteration+1) * self.len_seqs)) % len(self.dataset)]
                for offset in self.offsets]
        
        if self.len_seqs == 1 :
            #batch = [sample[..., -1] for sample in batch]
            pass
        for s in range(len(batch)):
            #print('batch sample : ' + str(batch[s].shape))
            pass
               
        return batch

    def serialize(self, serializer):
        # It is important to serialize the state to be recovered on resume.
        self.iteration = serializer('iteration', self.iteration)
        self.epoch = serializer('epoch', self.epoch)
        try:
            self._previous_epoch_detail = serializer(
                'previous_epoch_detail', self._previous_epoch_detail)
        except KeyError:
            # guess previous_epoch_detail for older version
            self._previous_epoch_detail = self.epoch + \
                (self.current_position - self.batch_size) / len(self.dataset)
            if self.epoch_detail > 0:
                self._previous_epoch_detail = max(
                    self._previous_epoch_detail, 0.)
            else:
                self._previous_epoch_detail = -1.


class WindowIterator(chainer.dataset.Iterator):
    """Dataset iterator to create a batch of sequences at different positions.

    This iterator returns a pair of the current chars and the context chars.
    """

    def __init__(self, dataset, batch_size=BATCHSIZE, window=WINDOW, repeat=True):
        self.dataset = np.array(dataset, np.int32)
        self.window = window  # size of context window
        self.batch_size = batch_size
        self._repeat = repeat
        # order is the array which is shuffled ``[window, window + 1, ...,
        # len(dataset) - window - 1]``
        self.order = np.random.permutation(
            len(dataset) - window * 2).astype(np.int32)
        self.order += window
        self.current_position = 0
        # Number of completed sweeps over the dataset. In this case, it is
        # incremented if every word is visited at least once after the last
        # increment.
        self.epoch = 0
        # True if the epoch is incremented at the last iteration.
        self.is_new_epoch = False

    def __next__(self):
        """This iterator returns a list representing a mini-batch.

        Each item indicates a different position in the original sequence.
        """
        if not self._repeat and self.epoch > 0:
            raise StopIteration

        i = self.current_position
        i_end = i + self.batch_size
        position = self.order[i:i_end]
        w = np.random.randint(self.window - 1) + 1
        offset = np.concatenate([np.arange(-w, 0), np.arange(1, w + 1)])
        pos = position[:, None] + offset[None, :]
        contexts = self.dataset.take(pos)
        center = self.dataset.take(position)

        if i_end >= len(self.order):
            np.random.shuffle(self.order)
            self.epoch += 1
            self.is_new_epoch = True
            self.current_position = 0
        else:
            self.is_new_epoch = False
            self.current_position = i_end

        return center, contexts

    @property
    def epoch_detail(self):
        return self.epoch + float(self.current_position) / len(self.order)

    def serialize(self, serializer):
        self.current_position = serializer('current_position',
                                           self.current_position)
        self.epoch = serializer('epoch', self.epoch)
        self.is_new_epoch = serializer('is_new_epoch', self.is_new_epoch)
        if self._order is not None:
            serializer('_order', self._order)


class BPTTUpdater(training.updaters.StandardUpdater):

    def __init__(self, train_iter, optimizer, converter, device=-1, bprop_len=10):
        super(BPTTUpdater, self).__init__(train_iter, optimizer)
        self.bprop_len = bprop_len
        self.device = device
        self.converter = converter

    # The core part of the update routine can be customized by overriding.
    def update_core(self):
        loss = 0
        # When we pass one iterator and optimizer to StandardUpdater.__init__,
        # they are automatically named 'main'.
        train_iter = self.get_iterator('main')
        optimizer = self.get_optimizer('main')

        # Progress the dataset iterator for bprop_len words at each iteration.
        for i in range(self.bprop_len):
            # Get the next batch (a list of tuples of two word IDs)
            batch = train_iter.__next__()
            
            logging.debug("batch len : " + str(len(batch)) + ".")

            # Concatenate the word IDs to matrices and send them to the device
            # self.converter does this job
            # (it is chainer.dataset.concat_examples by default)
            x, t = self.converter(batch, self.device)
            
            logging.debug("x.shape : " + str(x.shape) + " ; t.shape : " + str(t.shape) + ".")

            # Compute the loss at this time step and accumulate it
            loss += optimizer.target(chainer.Variable(x), chainer.Variable(t))

        optimizer.target.cleargrads()  # Clear the parameter gradients
        loss.backward()  # Backprop
        loss.unchain_backward()  # Truncate the graph
        optimizer.update()  # Update the parameters


class serialize_model(chainer.training.Extension):

    def __init__(self, model, out=SAVED_MODELS_LOC, compression=False, trigger=(1, 'iteration'), priority=100, name=None):    
        super(serialize_model, self).__init__()
        self.trigger = trigger
        self.priority = priority
        self.name = name
        self.model = model
        self.out = out
        self.compression = compression

    def __call__(trainer, model, out=SAVED_MODELS_LOC, compression=False):
        chainer.serializers.save_npz(out+"predictor_last-iter", model, compression)


def compute_perplexity(result):
    result['perplexity'] = np.exp(result['main/loss'])
    if 'validation/main/loss' in result:
        result['val_perplexity'] = np.exp(result['validation/main/loss'])

def convert(batch, device): # If using WindowIterator
    center, contexts = batch
    if device >= 0:
        center = cuda.to_gpu(center)
        contexts = cuda.to_gpu(contexts)
    return center, contexts



if __name__=='__main__':    

    
    logging.basicConfig(filename='./logs/debug_citation_parser.log',level=logging.DEBUG)

    slice_size = 80000
    data = np.load(TRAIN_DATA_LOC)['arr_0']

    train_slice = 0
    train = data[800000*train_slice:800000*(train_slice+10)] 

    val_slice = 10
    val = np.load(TRAIN_DATA_LOC)['arr_0'][800000*val_slice:800000*(val_slice+2)] 

    print("vocab_size : " + str(len(set(train)))) # check that size of alphabet is actually 102

    predictor = CharRNNLM()
    #predictor = chainer.serializers.load_npz(file=MODEL_TO_LOAD, obj=predictor)
    model = L.Classifier(predictor)
    model.compute_accuracy = False  # we only want the perplexity 


    # Set up an optimizer
    optimizer = O.Adam()
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer_hooks.GradientClipping(GRADCLIP))

    # Set up an iterator
    train_iter = WindowIterator(train, batch_size=BATCHSIZE)
    val_iter = WindowIterator(val, batch_size=BATCHSIZE)
    # Set up an updater
    updater = BPTTUpdater(train_iter, optimizer, bprop_len=BPROP_LEN, converter=convert, device=-1)
    #updater = training.updaters.StandardUpdater(train_iter, optimizer, converter=convert,  device=-1)

    # Set up a trainer
    trainer = training.Trainer(updater, out=SAVED_MODELS_LOC)

    interval = 10 if TEST else 100

    #"""
    #trainer.extend(serialize_model(model=model.predictor, out=SAVED_MODELS_LOC, compression=False))
    trainer.extend(extensions.dump_graph('main/loss'))
    trainer.extend(extensions.LogReport(postprocess=compute_perplexity, trigger=(interval, 'iteration')))
    trainer.extend(extensions.PrintReport(['epoch', 'iteration', 'perplexity', 'elapsed_time']), trigger=(interval, 'iteration'))
    trainer.extend(extensions.snapshot())
    trainer.extend(extensions.snapshot_object(model,  'model_iter_{.updater.iteration}'))
    if extensions.PlotReport.available():
        trainer.extend(extensions.PlotReport(['main/loss'], 'epoch', file_name='loss.png'))
        trainer.extend(extensions.PlotReport(['main/perplexity'], 'epoch', file_name='accuracy.png'))
    """

    trainer.extend(extensions.LogReport())
    trainer.extend(extensions.snapshot(filename='snapshot_epoch-{.updater.epoch}'))
    trainer.extend(extensions.snapshot_object(model.predictor, filename='model_epoch-{.updater.epoch}'))
    trainer.extend(extensions.Evaluator(val_iter, model, device=-1))
    trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'main/accuracy', 
                                           'validation/main/loss', 'validation/main/accuracy', 'elapsed_time']))
    trainer.extend(extensions.PlotReport(['main/loss', 'validation/main/loss'], x_key='epoch', file_name='loss.png'))
    trainer.extend(extensions.PlotReport(['main/accuracy', 'validation/main/accuracy'], x_key='epoch', file_name='accuracy.png'))
    trainer.extend(extensions.dump_graph('main/loss'))
    """
    t = time()
    logging.info("At " + str(t-t1)  + " : starting to run trainer.")

    trainer.run()


    t2 = time()
    print("exec time (seconds): " + str(t2-t1))








