from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.stacklayout import StackLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput


class BibForm(StackLayout):
    def __init__(self, **kwargs):
        super(BibForm, self).__init__(**kwargs)

        l1 = Label(text='Titre: ')
        self.add_widget(l1)
        t1 = TextInput(multiline=False)
        self.add_widget(t1)

        l2 = Label(text='Auteurs: ')
        self.add_widget(l2)
        t2 = TextInput(multiline=False)
        self.add_widget(t2)
        t2b = TextInput(multiline=False)
        self.add_widget(t2b)
        t2c = TextInput(multiline=False)
        self.add_widget(t2c)
               

        l3 = Label(text='Date: ')
        self.add_widget(l3)
        t3 = TextInput(multiline=False)
        self.add_widget(t3)

        l4 = Label(text='Journal: ')
        self.add_widget(l4)
        t4 = TextInput(multiline=False)
        self.add_widget(t4)

        bf = Button(text='Envoyer')

class ManualBib(App):

    def build(self):
        return BibForm()


if __name__=='__main__':

    ManualBib().run()


