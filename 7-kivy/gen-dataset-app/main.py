from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput


class RootWidget(Widget):

    pass


class DatasetGenerator(App):
    
    def build(self):
        return RootWidget()



if __name__=='__main__':
    
    DatasetGenerator().run()


